Attribute VB_Name = "UniEditMod"
Option Explicit

Public Declare Function ShowWindow Lib "user32" (ByVal hWnd As Long, ByVal nCmdShow As Long) As Long
'Public Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
'Public Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" (ByVal lpPrevWndFunc As Long, ByVal hwnd As Long, ByVal Msg As Long, ByVal WParam As Long, ByVal lParam As Long) As Long
'Public Declare Function DestroyWindow Lib "user32" (ByVal hwnd As Long) As Long
'Public Declare Function SendMessageW Lib "user32" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal WParam As Long, ByVal lParam As Long) As Long
Public Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long

Public Declare Function SetFocusToWindow Lib "user32" Alias "SetFocus" (ByVal hWnd As Long) As Long
'Public Declare Function GetDC Lib "user32.dll" (ByVal hwnd As Long) As Long
'Public Declare Function GetDeviceCaps Lib "gdi32.dll" (ByVal hDC As Long, ByVal nIndex As Long) As Long
'Public Declare Function MulDiv Lib "kernel32.dll" (ByVal lNumber As Long, ByVal lNumerator As Long, ByVal lDenominator As Long) As Long
'Public Declare Function CreateFont Lib "gdi32.dll" Alias "CreateFontA" ( _
  ByVal lHeight As Long, _
  ByVal lWidth As Long, _
  ByVal lEscapement As Long, _
  ByVal lOrientation As Long, _
  ByVal lBold As Long, _
  ByVal lItalic As Long, _
  ByVal lUnderline As Long, _
  ByVal lStrikethrough As Long, _
  ByVal lCharset As Long, _
  ByVal lOutputPrecision As Long, _
  ByVal lClipPrecision As Long, _
  ByVal lQuality As Long, _
  ByVal lPitchAndFamily As Long, _
  ByVal strFontName As String) As Long
  
'Public Declare Function TranslateColor Lib "olepro32.dll" Alias "OleTranslateColor" (ByVal clr As OLE_COLOR, ByVal palet As Long, col As Long) As Long
Public Declare Function GetAsyncKeyState Lib "user32" (ByVal vKey As Long) As Integer


' Window styles
Public Const WS_CHILD = &H40000000
Public Const WS_CLIPSIBLINGS = &H4000000
' ExWindowStyles
Public Const WS_EX_NOPARENTNOTIFY = &H4&
' ShowWindow commands
Public Const SW_SHOWNORMAL = 1
Public Const WM_ERASEBKGND = &H14
'Public Const WM_SETTEXT = &HC
'Public Const WM_GETTEXT As Long = &HD
Public Const ES_MULTILINE = &H4&
Public Const ES_AUTOVSCROLL = &H40&
Public Const WM_PAINT = &HF&
Public Const SWP_NOZORDER = &H4
Public Const WM_GETTEXTLEN = &HE
Public Const EM_GETSEL = &HB0
Public Const EM_SETSEL = &HB1
Public Const EM_SCROLLCARET = &HB7

Public Const WM_CHAR = &H102
'Public Const WM_KEYUP = &H101
'Public Const WM_KEYDOWN = &H100
Public Const WM_PASTE = &H302
Public Const WM_CUT = &H300
Public Const WM_CLEAR = &H303
Public Const WM_UNDO = &H304
Public Const WM_MOUSEMOVE = &H200
Public Const WM_MBUTTONDOWN = &H207
'Public Const WM_LBUTTONDOWN = &H201
Public Const WM_XBUTTONDOWN = &H20B

Public Const WM_SETFONT = &H30
Public Const LOGPIXELSY = 90

'Public Const GWL_WNDPROC = (-4)

Public Const WM_SETFOCUS = &H7
Public Const WM_KILLFOCUS = &H8

Public Const WM_COMMAND As Long = &H111
Public Const EN_CHANGE = &H300

Public UniEditWindows As New Collection

Private Function WindowProc(ByVal hWnd As Long, ByVal uMsg As Long, ByVal WParam As Long, ByVal lParam As Long) As Long
  Dim I As Long
  
  For I = 1 To UniEditWindows.Count
    If UniEditWindows(I).EditHwnd = hWnd Then
      WindowProc = UniEditWindows(I).PIncommingEvent(uMsg, WParam, lParam)
      Exit For
    End If
  Next
End Function

Private Function WindowProcUc(ByVal hWnd As Long, ByVal uMsg As Long, ByVal WParam As Long, ByVal lParam As Long) As Long
  Dim I As Long
  
  For I = 1 To UniEditWindows.Count
    If UniEditWindows(I).UChWnd = hWnd Then
      WindowProcUc = UniEditWindows(I).UCIncommingEvent(uMsg, WParam, lParam)
      Exit For
    End If
  Next
End Function

Public Function SetNewUniEditWindowLong(ByVal hWnd As Long) As Long
  SetNewUniEditWindowLong = SetWindowLong(hWnd, GWL_WNDPROC, AddressOf WindowProc)
End Function

Public Function SetNewUniEditWindowLongUc(ByVal hWnd As Long) As Long
  SetNewUniEditWindowLongUc = SetWindowLong(hWnd, GWL_WNDPROC, AddressOf WindowProcUc)
End Function
