VERSION 5.00
Begin VB.UserControl UniEdit 
   BackColor       =   &H80000005&
   ClientHeight    =   3600
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4800
   ControlContainer=   -1  'True
   ScaleHeight     =   240
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   320
End
Attribute VB_Name = "UniEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public EditHwnd As Long

Private oldEditWnd As Long
Private oldWnd As Long

Private cFontItalic As Boolean
Private cFontBold As Boolean
Private cFontName As String
Private cFontSize As Long

Private FocusFlag As Long

Private m_IPAOHookStruct As IPAOHookStruct

Event Change()
Event KeyDown(KeyCode As Integer, Shift As Integer)
Event KeyPress(KeyAscii As Integer)
Event KeyPressed(KeyAscii As Integer)
Event KeyUp(KeyCode As Integer, Shift As Integer)
Event MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Event MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)

'########### Events ###############

Public Function PIncommingEvent(uMsg As Long, WParam As Long, lParam As Long) As Long
  Dim TempVal As Integer
  Dim CancelEv As Boolean
  Const WM_IME_CHAR As Long = &H286
  
  Select Case uMsg
    Case WM_CHAR
      TempVal = CInt(WParam)
      RaiseEvent KeyPress(TempVal)
      CancelEv = TempVal = 0
      WParam = TempVal
    Case WM_KEYDOWN
      TempVal = CInt(WParam)
      RaiseEvent KeyDown(TempVal, ShiftState)
      CancelEv = TempVal = 0
      WParam = TempVal
    Case WM_KEYUP
      TempVal = CInt(WParam)
      RaiseEvent KeyUp(TempVal, ShiftState)
      CancelEv = TempVal = 0
      WParam = TempVal
    Case WM_MOUSEMOVE
      RaiseEvent MouseMove(0, 0, lParam Mod 65536, lParam \ 65536)
    Case WM_MBUTTONDOWN
      RaiseEvent MouseDown(2, 0, lParam Mod 65536, lParam \ 65536)
    Case WM_LBUTTONDOWN
      RaiseEvent MouseDown(1, 0, lParam Mod 65536, lParam \ 65536)
    Case WM_XBUTTONDOWN
      RaiseEvent MouseDown(3, 0, lParam Mod 65536, lParam \ 65536)
    Case WM_IME_CHAR
      SelText = SelText + ChrW(WParam)
      CancelEv = True
  End Select
  
  If CancelEv = False Then PIncommingEvent = CallWindowProc(oldEditWnd, EditHwnd, uMsg, WParam, lParam)
  
  Select Case uMsg
    Case WM_SETFOCUS
      If FocusFlag = 0 And Not UserControl Is Nothing Then UserControl.SetFocus
    Case WM_KILLFOCUS
      If Not UserControl Is Nothing Then
        If WParam <> UserControl.hWnd Then FocusFlag = 0
      End If
    Case WM_CHAR, WM_CUT, WM_UNDO, WM_CLEAR, WM_PASTE
      TempVal = CInt(WParam)
      RaiseEvent KeyPressed(TempVal)
      CancelEv = TempVal = 0
      WParam = TempVal
  End Select
End Function

Public Function UCIncommingEvent(uMsg As Long, WParam As Long, lParam As Long) As Long
  Dim TempVal As Integer
  
  UCIncommingEvent = CallWindowProc(oldWnd, UserControl.hWnd, uMsg, WParam, lParam)
 
  Select Case uMsg
    
    Case WM_SETFOCUS
    
      ' --------------------------------------------------------------------------
      Dim pOleObject                  As IOleObject
      Dim pOleInPlaceSite             As IOleInPlaceSite
      Dim pOleInPlaceFrame            As IOleInPlaceFrame
      Dim pOleInPlaceUIWindow         As IOleInPlaceUIWindow
      Dim pOleInPlaceActiveObject     As IOleInPlaceActiveObject
      Dim PosRect                     As RECT
      Dim ClipRect                    As RECT
      Dim FrameInfo                   As OLEINPLACEFRAMEINFO
      Dim grfModifiers                As Long
      Dim AcceleratorMsg              As Msg
      
      'Get in-place frame and make sure it is set to our in-between
      'implementation of IOleInPlaceActiveObject in order to catch
      'TranslateAccelerator calls
      Set pOleObject = Me
      Set pOleInPlaceSite = pOleObject.GetClientSite
      pOleInPlaceSite.GetWindowContext pOleInPlaceFrame, pOleInPlaceUIWindow, VarPtr(PosRect), VarPtr(ClipRect), VarPtr(FrameInfo)
      CopyMemory pOleInPlaceActiveObject, m_IPAOHookStruct.ThisPointer, 4
      pOleInPlaceFrame.SetActiveObject pOleInPlaceActiveObject, vbNullString
      If Not pOleInPlaceUIWindow Is Nothing Then
         pOleInPlaceUIWindow.SetActiveObject pOleInPlaceActiveObject, vbNullString
      End If
      ' Clear up the inbetween implementation:
      CopyMemory pOleInPlaceActiveObject, 0&, 4
      ' --------------------------------------------------------------------------
      If FocusFlag < 2 Or WParam <> UserControl.hWnd Then
        FocusFlag = 2
        SetFocusToWindow EditHwnd
      End If
    Case WM_COMMAND
      Select Case WParam \ 65536
        Case EN_CHANGE
          RaiseEvent Change
      End Select
  End Select
End Function

Public Function UChWnd() As Long
  UChWnd = UserControl.hWnd
End Function

Public Function hWnd() As Long
  hWnd = EditHwnd
End Function


'########### Propertys ###############

Property Let Text(pText As String)
  SendMessage EditHwnd, WM_SETTEXT, Len(pText), pText
End Property

Property Get Text() As String
  Dim Buffer As String
  Dim I As Long

  I = SendMessageA(EditHwnd, WM_GETTEXTLEN, 0&, 0&) + 1
  Buffer = String(I, 0)
  I = SendMessage(EditHwnd, WM_GETTEXT, I, Buffer)
  Text = Mid(Buffer, 1, I)
End Property


Property Let SelStart(pSelStart As Long)
  SendMessageA EditHwnd, EM_SETSEL, pSelStart, pSelStart
  RaiseEvent Change
End Property

Property Get SelStart() As Long
  Dim sStart As Long
  SendMessageA EditHwnd, EM_GETSEL, VarPtr(sStart), 0&
  SelStart = sStart
End Property


Property Let SelLength(cSelLen As Long)
  Dim sStart As Long
  SendMessageA EditHwnd, EM_GETSEL, VarPtr(sStart), 0&
  SendMessageA EditHwnd, EM_SETSEL, sStart, cSelLen + sStart
End Property

Property Get SelLength() As Long
  Dim sLen As Long
  SendMessageA EditHwnd, EM_GETSEL, 0&, VarPtr(sLen)
  SelLength = sLen - SelStart
End Property


Property Let FontName(cInData As String)
  cFontName = cInData
  ApplyFont
End Property

Property Get FontName() As String
  FontName = cFontName
End Property


Property Let FontItalic(cInData As Boolean)
  cFontItalic = cInData
  ApplyFont
End Property

Property Get FontItalic() As Boolean
  FontItalic = cFontItalic
End Property


Property Let FontBold(cInData As Boolean)
  cFontBold = cInData
  ApplyFont
End Property

Property Get FontBold() As Boolean
  FontBold = cFontBold
End Property


Property Let FontSize(cInData As Long)
  cFontSize = cInData
  ApplyFont
End Property

Property Get FontSize() As Long
  FontSize = cFontSize
End Property


Property Let ForeColor(cInData As Long)
  UserControl.ForeColor = cInData
End Property

Property Get ForeColor() As Long
  ForeColor = UserControl.ForeColor
End Property


Property Let BackColor(cInData As Long)
  UserControl.BackColor = cInData
End Property

Property Get BackColor() As Long
  BackColor = UserControl.BackColor
End Property

Property Let SelText(cText As String)
  Dim sStart As Long
  sStart = SelStart
  Text = Mid(Text, 1, sStart) + cText + Mid(Text, SelLength + sStart + 1)
  SendMessageA EditHwnd, EM_SETSEL, Len(cText) + sStart, Len(cText) + sStart
End Property

Property Get SelText() As String
  Dim sStart As Long
  Dim sLen As Long
  SendMessageA EditHwnd, EM_GETSEL, VarPtr(sStart), VarPtr(sLen)
  SelText = Mid(Text, sStart + 1, sLen - sStart)
End Property
'####################################

Private Function ApplyFont() As Boolean
  Dim hFont As Long
  Dim lHeight As Long
  Dim BoldSize As Long
  
  If cFontBold Then
    BoldSize = 700
  Else
    BoldSize = 400
  End If
  
  lHeight = -MulDiv(cFontSize, GetDeviceCaps(GetDC(EditHwnd), LOGPIXELSY), 72)
  hFont = CreateFont(lHeight, 0&, 0&, 0&, BoldSize, cFontItalic, 0&, 0&, 0&, 0&, 0&, 0&, 0&, cFontName)
  If SendMessageA(EditHwnd, WM_SETFONT, hFont, 1&) <> 0 And hFont <> 0 Then ApplyFont = True
End Function

Public Sub SetFocus()
  'SetFocusToWindow EditHwnd
End Sub

Public Sub Refresh()
  SendMessageA EditHwnd, WM_ERASEBKGND, GetDC(EditHwnd), 0&
  SendMessageA EditHwnd, WM_PAINT, GetDC(EditHwnd), 0&
End Sub

Private Sub ClearWindow()
  Dim I As Long
  
  For I = 1 To UniEditWindows.Count
    If UniEditWindows(I) Is Me Then
      UniEditWindows.Remove I
      Exit For
    End If
  Next
  
  SetWindowLong EditHwnd, GWL_WNDPROC, oldEditWnd
  SetWindowLong UserControl.hWnd, GWL_WNDPROC, oldWnd
  DestroyWindow EditHwnd
  EditHwnd = 0
End Sub

Private Sub UserControl_Initialize()
  Dim IPAO As IOleInPlaceActiveObject

  With m_IPAOHookStruct
    Set IPAO = Me
    CopyMemory .IPAOReal, IPAO, 4
    CopyMemory .TBEx, Me, 4
    .lpVTable = IPAOVTable
    .ThisPointer = VarPtr(m_IPAOHookStruct)
  End With
End Sub

Private Sub InitializeEditWindow()
  Dim TitelOut As String
  Dim Style As Long
  
  'Exit Sub '###################################################################
  If EditHwnd = 0 And UserControl.Ambient.UserMode Then
    Style = WS_CHILD Or ES_AUTOVSCROLL Or WS_CLIPSIBLINGS Or ES_MULTILINE
  
    EditHwnd = CreateWindowEx(WS_EX_NOPARENTNOTIFY, "Edit", "", Style, 0, 0, UserControl.ScaleWidth, UserControl.ScaleHeight, UserControl.hWnd, 0, App.hInstance, 0)
    
    If EditHwnd Then
      oldEditWnd = SetNewUniEditWindowLong(EditHwnd)
      oldWnd = SetNewUniEditWindowLongUc(UserControl.hWnd)
      ShowWindow EditHwnd, SW_SHOWNORMAL
      UniEditWindows.Add Me
    End If
  End If
End Sub

Private Sub UserControl_InitProperties()
  InitializeEditWindow
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
  InitializeEditWindow
End Sub

Private Sub UserControl_LostFocus()
  FocusFlag = 0
End Sub

Private Sub UserControl_Resize()
  If EditHwnd Then SetWindowPos EditHwnd, 0&, 0, 0, UserControl.ScaleWidth, UserControl.ScaleHeight, SWP_NOZORDER
End Sub

Private Sub UserControl_Terminate()
  If EditHwnd Then ClearWindow
  With m_IPAOHookStruct
    CopyMemory .IPAOReal, 0&, 4
    CopyMemory .TBEx, 0&, 4
  End With
End Sub

Private Property Get ShiftState() As Integer
   ShiftState = Abs(Sgn(GetAsyncKeyState(vbKeyShift))) * vbShiftMask Or Abs(Sgn(GetAsyncKeyState(vbKeyControl))) * vbCtrlMask
End Property

Friend Function TranslateAccelerator(lpMsg As VBOleGuids.Msg) As Long
  TranslateAccelerator = S_FALSE
  Select Case lpMsg.message
    Case WM_KEYDOWN
      Select Case (lpMsg.WParam And &HFFFF&)
        Case 37, 38, 39, 40
          SendMessageA EditHwnd, lpMsg.message, lpMsg.WParam, lpMsg.lParam
          TranslateAccelerator = S_OK
      End Select
    Case WM_KEYUP
      Select Case (lpMsg.WParam And &HFFFF&)
        Case 37, 38, 39, 40
          SendMessageA EditHwnd, lpMsg.message, lpMsg.WParam, lpMsg.lParam
          TranslateAccelerator = S_OK
      End Select
    Case WM_CHAR
      If (lpMsg.WParam And &HFFFF&) = 9 Then
        RaiseEvent KeyPress(9)
        TranslateAccelerator = S_OK
      End If
  End Select
End Function
