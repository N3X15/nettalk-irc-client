VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DccServ"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public GetMode As Integer
Public Port As Long
Private WithEvents DccWSock As CSocket
Attribute DccWSock.VB_VarHelpID = -1

Private Sub Class_Initialize()
  Set DccWSock = New CSocket
End Sub

Private Sub Class_Terminate()
  If DccWSock.State > 1 Then DccWSock.Disconnect
End Sub

Public Function Listen(PortNumber As Long) As Boolean
  Port = PortNumber
  DccWSock.LocalPort = PortNumber
  DccWSock.Listen
  Listen = (DccWSock.State = sockListening)
End Function

Private Sub DccWSock_ConnectionRequest()
  Dim DCCConn As DCCConnection
  Set DCCConn = New DCCConnection
  DCCConn.ConnID = getDccID
  DCCConn.Nick = "Server"
  DCCConn.Server = ""
  DCCConn.User = "Client"
  DCCConn.Port = DccWSock.LocalPort
  DCCConn.Mode = 1
  DCCConn.GetMode = GetMode
  If GetMode < 2 Then DCCConn.OpenWindow
  DCCConn.Accept DccWSock
  DCCconns.Add DCCConn
  ntScript_Call "DCCServAccept", CStr(Port), CStr(DCCConn.GetSeocket.RemoteHost), DCCConn.ConnID
End Sub
