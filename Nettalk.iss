[Setup]
AppName=Nettalk
AppVerName=Nettalk 6.7
AppPublisher=Nicolas Kruse
AppPublisherURL=http://www.ntalk.de
AppSupportURL=http://www.ntalk.de
AppUpdatesURL=http://update.ntalk.de
DefaultDirName={pf}\Nettalk6
DisableProgramGroupPage=yes
ShowLanguageDialog=yes
WizardImageFile=Setup.bmp


[Languages]
Name: en; MessagesFile: "compiler:Default.isl"
Name: es; MessagesFile: "compiler:Languages\Spanish.isl"
Name: de; MessagesFile: "compiler:Languages\German.isl"
Name: nl; MessagesFile: "compiler:Languages\Dutch.isl"
Name: ru; MessagesFile: "compiler:Languages\Russian.isl"
Name: hu; MessagesFile: "compiler:Languages\Hungarian.isl"
Name: fr; MessagesFile: "compiler:Languages\French.isl"
Name: it; MessagesFile: "compiler:Languages\Italian.isl"
Name: sw; MessagesFile: "compiler:Default.isl"
Name: pt; MessagesFile: "compiler:Languages\Portuguese.isl"


[Tasks]
Name: "desktopicon"; Description: "Verkn�pfung auf dem Desktop erstellen"; Languages: de; GroupDescription: "Zus�tzliche Verkn�pfungen:"; MinVersion: 4,4
Name: "desktopicon"; Description: "Create a shortcut on the desktop"; Languages: en nl ru hu es fr it sw; GroupDescription: "Additional shortcuts:"; MinVersion: 4,4

Name: "autostart"; Description: "Autostart"; Languages: de; GroupDescription: "Zus�tzliche Verkn�pfungen:"; MinVersion: 4,4
Name: "autostart"; Description: "Startup"; Languages: en nl ru hu es fr it sw; GroupDescription: "Additional shortcuts:"; MinVersion: 4,4

Name: "multiuser"; Description: "F�r jeden Benutzer des Computers"; Languages: de; GroupDescription: "Verkn�pfungen:"; MinVersion: 4,4; Flags: exclusive
Name: "multiuser"; Description: "For each user on this computer"; Languages: en nl ru hu es fr it sw; GroupDescription: "Shortcuts:"; MinVersion: 4,4; Flags: exclusive

Name: "singeluser"; Description: "Nur f�r diesen Benutzer des Computers"; Languages: de; GroupDescription: "Verkn�pfungen:"; MinVersion: 4,4; Flags: exclusive unchecked
Name: "singeluser"; Description: "Only for this user on the computer"; Languages: en nl ru hu es fr it sw; GroupDescription: "Shortcuts:"; MinVersion: 4,4; Flags: exclusive unchecked

Name: "autoupdate"; Description: "W�chentlich nach Updates schauen"; Languages: de; GroupDescription: "Weitere Einstellungen:"; MinVersion: 4,4
Name: "autoupdate"; Description: "Check weekly for Updates"; Languages: en nl ru hu es fr it sw; GroupDescription: "Additional preferences:"; MinVersion: 4,4

;Name: "singeluserex"; Description: "Einstellungen im Programmverzeichnis ablegen (Nicht empfohlen!)"; Languages: de; GroupDescription: "Weitere Einstellungen:"; MinVersion: 4,4; Flags: unchecked
;Name: "singeluserex"; Description: "Save settings in the application directory (Not recommended!)"; Languages: en nl ru hu es fr it sw; GroupDescription: "Additional preferences:"; MinVersion: 4,4; Flags: unchecked


[Files]
Source: "Nettalk.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "PORT.DLL"; DestDir: "{app}"; Flags: ignoreversion; OnlyBelowVersion: 5.0,5.0
Source: "Deutsch.ulng"; DestDir: "{app}"; Flags: ignoreversion
Source: "English.ulng"; DestDir: "{app}"; Flags: ignoreversion
Source: "Nederlands.ulng"; DestDir: "{app}"; Flags: ignoreversion
Source: "Russian.ulng"; DestDir: "{app}"; Flags: ignoreversion
Source: "Magyar.ulng"; DestDir: "{app}"; Flags: ignoreversion
Source: "Spanish.ulng"; DestDir: "{app}"; Flags: ignoreversion
Source: "Simplified_Chinese.ulng"; DestDir: "{app}"; Flags: ignoreversion
Source: "French.ulng"; DestDir: "{app}"; Flags: ignoreversion
Source: "Italian.ulng"; DestDir: "{app}"; Flags: ignoreversion
Source: "Swedish.ulng"; DestDir: "{app}"; Flags: ignoreversion
Source: "Portuguese.ulng"; DestDir: "{app}"; Flags: ignoreversion
Source: "Msg.wav"; DestDir: "{app}"; Flags: ignoreversion
Source: "Words.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "servers.ini"; DestDir: "{app}"; Flags: ignoreversion
Source: "Servers.srv"; DestDir: "{app}"; Flags: ignoreversion
Source: "DarkGlass.skn"; DestDir: "{app}"; Flags: ignoreversion
Source: "GreenCrystal.skn"; DestDir: "{app}"; Flags: ignoreversion
Source: "Kontrast.skn"; DestDir: "{app}"; Flags: ignoreversion
Source: "Classic.skn"; DestDir: "{app}"; Flags: ignoreversion
Source: "SilverGlass7.skn"; DestDir: "{app}"; Flags: ignoreversion
Source: "msvbvm60.dll"; DestDir: "{sys}"; OnlyBelowVersion: 5.0,6.0; Flags: restartreplace uninsneveruninstall sharedfile regserver

;On Windows XP and before:
Source: "Nettalk_before_Vista.ini"; DestName: "Nettalk.ini";   DestDir: "{app}"; OnlyBelowVersion: 5.0,6.0
Source: "Nettalk_before_Vista.gif"; DestName: "Nettalkgb.gif"; DestDir: "{app}"; OnlyBelowVersion: 5.0,6.0

;On Vista:
Source: "Nettalk_Vista.ini";        DestName: "Nettalk.ini";   DestDir: "{app}"; MinVersion: 5.0,6.0; OnlyBelowVersion: 5.0,6.1
Source: "Nettalk_Vista.jpg";        DestName: "Nettalkgb.jpg"; DestDir: "{app}"; MinVersion: 5.0,6.0; OnlyBelowVersion: 5.0,6.1

;On Windows 7:
Source: "Nettalk_Windows7.ini";     DestName: "Nettalk.ini";   DestDir: "{app}"; MinVersion: 5.0,6.1
Source: "Nettalk_Windows7.jpg";     DestName: "Nettalkgb.jpg"; DestDir: "{app}"; MinVersion: 5.0,6.1

Source: "ShortCut.txt"; DestDir: "{app}"

Source: "Language-DE.ini"; DestName: "Language.ini"; Languages: de; DestDir: "{app}"
Source: "Language-EN.ini"; DestName: "Language.ini"; Languages: en; DestDir: "{app}"
Source: "Language-NL.ini"; DestName: "Language.ini"; Languages: nl; DestDir: "{app}"
Source: "Language-RU.ini"; DestName: "Language.ini"; Languages: ru; DestDir: "{app}"
Source: "Language-HU.ini"; DestName: "Language.ini"; Languages: hu; DestDir: "{app}"
Source: "Language-ES.ini"; DestName: "Language.ini"; Languages: es; DestDir: "{app}"
Source: "Language-IT.ini"; DestName: "Language.ini"; Languages: it; DestDir: "{app}"
Source: "Language-FR.ini"; DestName: "Language.ini"; Languages: fr; DestDir: "{app}"
Source: "Language-SW.ini"; DestName: "Language.ini"; Languages: sw; DestDir: "{app}"
Source: "Language-PT.ini"; DestName: "Language.ini"; Languages: pt; DestDir: "{app}"

Source: "AutoUpdate\Update.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "AutoUpdate\Update.ini"; DestDir: "{app}"; Flags: ignoreversion

;Source: "Runmode.ini"; DestDir: "{app}"; Flags: ignoreversion; Tasks: singeluserex

Source: "script.txt"; DestDir: "{app}"; Flags: ignoreversion; Tasks: autoupdate


[UninstallDelete]
Type: filesandordirs; Name: "{userappdata}\Nettalk"; Tasks: multiuser
;Type: filesandordirs; Name: "{app}\Preferences"; Tasks: singeluserex
Type: files; Name: "{app}\script.txt"
Type: files; Name: "{app}\ScriptValues.ini"


[Registry]
;Multiuser:
Root: HKLM; Subkey: "Software\Classes\.xdcc"; ValueType: string; ValueName: ""; ValueData: "NettalkXDCCFile"; Flags: uninsdeletevalue; Tasks: multiuser
Root: HKLM; Subkey: "Software\Classes\NettalkXDCCFile"; ValueType: string; ValueName: ""; ValueData: "XDCC-Datei"; Flags: uninsdeletekey; Languages: de; Tasks: multiuser
Root: HKLM; Subkey: "Software\Classes\NettalkXDCCFile"; ValueType: string; ValueName: ""; ValueData: "XDCC-File"; Flags: uninsdeletekey; Languages: en nl ru hu es fr it sw; Tasks: multiuser
Root: HKLM; Subkey: "Software\Classes\NettalkXDCCFile\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\Nettalk.exe,9"; Tasks: multiuser
Root: HKLM; Subkey: "Software\Classes\NettalkXDCCFile\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\Nettalk.exe"" ""%1"""; Tasks: multiuser

Root: HKLM; Subkey: "Software\Classes\.ntscript"; ValueType: string; ValueName: ""; ValueData: "NettalkScriptFile"; Flags: uninsdeletevalue; Tasks: multiuser
Root: HKLM; Subkey: "Software\Classes\NettalkScriptFile"; ValueType: string; ValueName: ""; ValueData: "Nettalk Script Datei"; Flags: uninsdeletekey; Languages: de; Tasks: multiuser
Root: HKLM; Subkey: "Software\Classes\NettalkScriptFile"; ValueType: string; ValueName: ""; ValueData: "Nettalk script file"; Flags: uninsdeletekey; Languages: en nl ru hu es fr it sw; Tasks: multiuser
Root: HKLM; Subkey: "Software\Classes\NettalkScriptFile\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\Nettalk.exe,8"; Tasks: multiuser
Root: HKLM; Subkey: "Software\Classes\NettalkScriptFile\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\Nettalk.exe"" ""%1"""; Tasks: multiuser

Root: HKLM; Subkey: "Software\Classes\nettalk"; ValueType: string; ValueName: "URL Protocol"; ValueData: "Nettalk-Chat"; Flags: uninsdeletevalue; Tasks: multiuser
Root: HKLM; Subkey: "Software\Classes\nettalk\Shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\Nettalk.exe"" ""%1"""; Flags: uninsdeletevalue; Tasks: multiuser
Root: HKLM; Subkey: "Software\Classes\nettalk\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\Nettalk.exe"; Flags: uninsdeletevalue; Tasks: multiuser

Root: HKLM; Subkey: "Software\Classes\irc"; ValueType: string; ValueName: "URL Protocol"; ValueData: "Nettalk-Chat"; Flags: uninsdeletevalue; Tasks: multiuser
Root: HKLM; Subkey: "Software\Classes\irc\Shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\Nettalk.exe"" ""%1"""; Flags: uninsdeletevalue; Tasks: multiuser
Root: HKLM; Subkey: "Software\Classes\irc\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\Nettalk.exe"; Flags: uninsdeletevalue; Tasks: multiuser

Root: HKLM; Subkey: "Software\Classes\dcc"; ValueType: string; ValueName: "URL Protocol"; ValueData: "Nettalk-Chat"; Flags: uninsdeletevalue; Tasks: multiuser
Root: HKLM; Subkey: "Software\Classes\dcc\Shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\Nettalk.exe"" ""%1"""; Flags: uninsdeletevalue; Tasks: multiuser
Root: HKLM; Subkey: "Software\Classes\dcc\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\Nettalk.exe"; Flags: uninsdeletevalue; Tasks: multiuser

Root: HKLM; Subkey: "Software\Classes\xdcc"; ValueType: string; ValueName: "URL Protocol"; ValueData: "Nettalk-Chat"; Flags: uninsdeletevalue; Tasks: multiuser
Root: HKLM; Subkey: "Software\Classes\xdcc\Shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\Nettalk.exe"" ""%1"""; Flags: uninsdeletevalue; Tasks: multiuser
Root: HKLM; Subkey: "Software\Classes\xdcc\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\Nettalk.exe"; Flags: uninsdeletevalue; Tasks: multiuser

;Singeluser:
Root: HKCU; Subkey: "Software\Classes\.xdcc"; ValueType: string; ValueName: ""; ValueData: "NettalkXDCCFile"; Flags: uninsdeletevalue; Tasks: singeluser
Root: HKCU; Subkey: "Software\Classes\NettalkXDCCFile"; ValueType: string; ValueName: ""; ValueData: "XDCC-Datei"; Flags: uninsdeletekey; Languages: de; Tasks: singeluser
Root: HKCU; Subkey: "Software\Classes\NettalkXDCCFile"; ValueType: string; ValueName: ""; ValueData: "XDCC-File"; Flags: uninsdeletekey; Languages: en nl ru hu es fr it sw; Tasks: singeluser
Root: HKCU; Subkey: "Software\Classes\NettalkXDCCFile\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\Nettalk.exe,9"; Tasks: singeluser
Root: HKCU; Subkey: "Software\Classes\NettalkXDCCFile\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\Nettalk.exe"" ""%1"""; Tasks: singeluser

Root: HKCU; Subkey: "Software\Classes\.ntscript"; ValueType: string; ValueName: ""; ValueData: "NettalkScriptFile"; Flags: uninsdeletevalue; Tasks: singeluser
Root: HKCU; Subkey: "Software\Classes\NettalkScriptFile"; ValueType: string; ValueName: ""; ValueData: "Nettalk Script Datei"; Flags: uninsdeletekey; Languages: de; Tasks: singeluser
Root: HKCU; Subkey: "Software\Classes\NettalkScriptFile"; ValueType: string; ValueName: ""; ValueData: "Nettalk script file"; Flags: uninsdeletekey; Languages: en nl ru hu es fr it sw; Tasks: singeluser
Root: HKCU; Subkey: "Software\Classes\NettalkScriptFile\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\Nettalk.exe,8"; Tasks: singeluser
Root: HKCU; Subkey: "Software\Classes\NettalkScriptFile\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\Nettalk.exe"" ""%1"""; Tasks: singeluser

Root: HKCU; Subkey: "Software\Classes\nettalk"; ValueType: string; ValueName: "URL Protocol"; ValueData: "Nettalk-Chat"; Flags: uninsdeletevalue; Tasks: singeluser
Root: HKCU; Subkey: "Software\Classes\nettalk\Shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\Nettalk.exe"" ""%1"""; Flags: uninsdeletevalue; Tasks: singeluser
Root: HKCU; Subkey: "Software\Classes\nettalk\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\Nettalk.exe"; Flags: uninsdeletevalue; Tasks: singeluser

Root: HKCU; Subkey: "Software\Classes\irc"; ValueType: string; ValueName: "URL Protocol"; ValueData: "Nettalk-Chat"; Flags: uninsdeletevalue; Tasks: singeluser
Root: HKCU; Subkey: "Software\Classes\irc\Shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\Nettalk.exe"" ""%1"""; Flags: uninsdeletevalue; Tasks: singeluser
Root: HKCU; Subkey: "Software\Classes\irc\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\Nettalk.exe"; Flags: uninsdeletevalue; Tasks: singeluser

Root: HKCU; Subkey: "Software\Classes\dcc"; ValueType: string; ValueName: "URL Protocol"; ValueData: "Nettalk-Chat"; Flags: uninsdeletevalue; Tasks: singeluser
Root: HKCU; Subkey: "Software\Classes\dcc\Shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\Nettalk.exe"" ""%1"""; Flags: uninsdeletevalue; Tasks: singeluser
Root: HKCU; Subkey: "Software\Classes\dcc\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\Nettalk.exe"; Flags: uninsdeletevalue; Tasks: singeluser

[Icons]
Name: "{userstartup}\Nettalk"; Filename: "{app}\Nettalk.exe"; MinVersion: 4,4; Tasks: autostart
Name: "{userprograms}\Nettalk"; Filename: "{app}\Nettalk.exe"; Tasks: singeluser
Name: "{commonprograms}\Nettalk"; Filename: "{app}\Nettalk.exe"; Tasks: multiuser
Name: "{userdesktop}\Nettalk"; Filename: "{app}\Nettalk.exe"; MinVersion: 4,4; Tasks: desktopicon and singeluser
Name: "{commondesktop}\Nettalk"; Filename: "{app}\Nettalk.exe"; MinVersion: 4,4; Tasks: desktopicon and multiuser

[Run]
Filename: "{app}\Nettalk.exe"; Parameters: "irc.ntalk.de"; Languages: de; Description: "Den offiziellen Nettalk-Channel betreten"; Flags: postinstall nowait unchecked
Filename: "{app}\Nettalk.exe"; Parameters: "irc.ntalk.de"; Languages: en nl ru hu es fr it sw; Description: "Enter the official Nettalk channel"; Flags: postinstall nowait unchecked

