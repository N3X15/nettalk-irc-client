VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "plusMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Type MenuButton
    Hide As Boolean
    Disable As Boolean
    Text As String
    TextHide As Boolean
    Icon As Integer
    Slash As Boolean
    XAnf As Long
    XEnd As Long
    ColorSellect As Boolean
    Activ As Boolean
    DisIcon As Integer
End Type

Private BMIndex As Integer
Private BIndex As Integer
Private Obj As PictureBox
Private Buttons() As MenuButton

Event Click(Index As Integer, Button As Integer)

Public Sub Add(Caption As String, Optional Hidden As Boolean, Optional TextHidden As Boolean, Optional Slash As Boolean, Optional IconIndex As Integer, Optional DisAbleIconIndex As Integer, Optional ColorView As Boolean)
  Dim I As Integer
  I = UBound(Buttons) + 1
  ReDim Preserve Buttons(I)
  Buttons(I).Text = Caption
  Buttons(I).Hide = Hidden
  Buttons(I).TextHide = TextHidden
  Buttons(I).Slash = Slash
  Buttons(I).Icon = IconIndex
  Buttons(I).ColorSellect = ColorView
  Buttons(I).Activ = True
  Buttons(I).DisIcon = DisAbleIconIndex
End Sub

Public Sub SetVal(ByVal Index As Integer, Optional Caption As String, Optional Hidden As Boolean, Optional TextHidden As Boolean, Optional Slash As Boolean, Optional IconIndex As Integer)
  Dim I As Integer
  I = Index
  Buttons(I).Text = Caption
  Buttons(I).Hide = Hidden
  Buttons(I).TextHide = TextHidden
  Buttons(I).Slash = Slash
  Buttons(I).Icon = IconIndex
End Sub

Public Sub SetIcon(ByVal Index As Integer, ByVal IconIndex As Integer)
  Buttons(Index).Icon = IconIndex
End Sub

Public Sub SetEnable(ByVal Index As Integer, Enabled As Boolean)
  Buttons(Index).Activ = Enabled
End Sub

Public Sub ButtonVisible(ByVal Index As Integer, Visible As Boolean)
  Buttons(Index).Hide = Visible = False
End Sub

Public Sub MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Dim I As Integer
  For I = 1 To UBound(Buttons)
    If Buttons(I).XAnf < X And Buttons(I).XEnd > X Then
      If Buttons(I).ColorSellect = True Then
        ShowColors Buttons(I).XAnf + Obj.Left + 1, Obj.Height + Obj.Top - 1
      Else
        BIndex = I
        BRef I
      End If
    End If
  Next
End Sub

Public Sub MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Dim I As Integer
  Dim I2 As Integer
  Dim nPoint As POINTAPI
  Dim nHWnd As Long
  GetCursorPos nPoint
  nHWnd = WindowFromPoint(nPoint.X, nPoint.Y)
  If nHWnd = Obj.hwnd Then
    If GetCapture() <> nHWnd Then
      SetCapture nHWnd
    End If
    For I = 1 To UBound(Buttons)
      If Buttons(I).XAnf < X And Buttons(I).XEnd > X Then
        If BMIndex = I Then Exit Sub
        If Buttons(I).TextHide = True Then ShowToolTip X, Y, Buttons(I).Text Else ShowToolTip 0, 0, "", True
        I2 = BMIndex
        BMIndex = I
        BRef I2
        BRef I
        I2 = 1
        Exit For
      End If
    Next
    If I2 = 0 Then
      ShowToolTip 0, 0, "", True
      I2 = BMIndex
      BMIndex = 0
      BRef I2
      Obj.Refresh
    End If
  Else
    ShowToolTip 0, 0, "", True
    I = BMIndex
    I2 = BIndex
    BMIndex = 0
    BIndex = 0
    BRef I
    BRef I2
    Obj.Refresh
    ReleaseCapture
  End If
End Sub

Public Sub MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Dim I As Integer
  Dim I2 As Integer
  I2 = BIndex
  I = BIndex
  BIndex = 0
  BRef I
  If Form1.UInfoMode = 3 Then
    Form1.UInfoMode = 4
  Else
    If Form1.Visible = True Then Form1.Text1.SetFocus
  End If
  If I2 <> 0 And Buttons(I2).Activ = True Then RaiseEvent Click(I2, Button)
  MouseMove Button, Shift, X, Y
End Sub

Public Sub Inst(PicBox As PictureBox)
  ReDim Preserve Buttons(0)
  PicBox.AutoRedraw = True
  PicBox.ScaleMode = 3
  Set Obj = PicBox
End Sub

Public Sub Refresh()
  Dim I As Integer
  Dim Ipos As Long
  Dim menText As RECT
  
  Obj.Line (0, 0)-(Obj.ScaleWidth, 0), CoVal.ButtonShadow
  DrawBG 0, Obj.ScaleWidth
  For I = 1 To UBound(Buttons)
    Buttons(I).XAnf = Ipos
    If Buttons(I).Hide = True Then
      Buttons(I).XEnd = Ipos
    ElseIf Buttons(I).TextHide = False Then
      Buttons(I).XEnd = Ipos + TextWW(Buttons(I).Text, Obj.hDC) + 9 + 16
      Ipos = Ipos + TextWW(Buttons(I).Text, Obj.hDC) + 12 + 16 + 3
    Else
      Buttons(I).XEnd = Ipos + 5 + 16
      Ipos = Ipos + 8 + 16 + 3
    End If
  Next
  For I = 1 To UBound(Buttons)
    BRef I
  Next
End Sub

Public Sub DrawBG(PosX As Long, Width As Long)
  'DrawFlow Obj, PosX, 1 , Width, Obj.ScaleHeight - 1, CoVal.Light3D, CoVal.BackColor
  DrawFlow Obj, PosX, 1, Width, Obj.ScaleHeight / 2 + 1, CoVal.Highlight3D, CoVal.BackColor
  If GetColorLightnes(CoVal.ButtonShadow) < GetColorLightnes(CoVal.MenuText) Then
    DrawFlow Obj, PosX, Obj.ScaleHeight / 2, Width, Obj.ScaleHeight / 2, GetMixedColor(CoVal.ButtonShadow, CoVal.Light3D, 70), CoVal.Light3D
    Obj.Line (PosX, Obj.ScaleHeight - 1)-(PosX + Width, Obj.ScaleHeight - 1), GetMixedColor(CoVal.Highlight3D, CoVal.Light3D, 30)
  Else
    DrawFlow Obj, PosX, Obj.ScaleHeight / 2, Width, Obj.ScaleHeight / 2, GetMixedColor(CoVal.ButtonShadow, CoVal.Light3D, 20), CoVal.Light3D
    Obj.Line (PosX, Obj.ScaleHeight - 1)-(PosX + Width, Obj.ScaleHeight - 1), GetMixedColor(CoVal.Highlight3D, CoVal.Light3D, 10)
  End If
End Sub


Public Sub BRef(I As Integer)
  Dim menText As RECT
  
  If I = 0 Or Buttons(I).Hide = True Then Exit Sub
  menText.Left = Buttons(I).XAnf + 16
  menText.Top = (Obj.ScaleHeight - 16) / 2 + 1
  If Buttons(I).TextHide = False Then
    menText.Right = menText.Left + TextWW(Buttons(I).Text, Obj.hDC) + 12
  Else
    menText.Right = menText.Left + 8
  End If
  menText.Bottom = (Obj.ScaleHeight + 16) / 2 + 3
  If Buttons(I).Slash = True Then Obj.Line (menText.Left - 16, menText.Top - 3)-(menText.Left - 16, menText.Bottom), CoVal.ButtonShadow
  If I = BIndex And Buttons(I).Activ = True Then
    DrawFlow Obj, menText.Left - 15, menText.Top - 4, menText.Right + 13 - menText.Left, menText.Bottom - menText.Top + 4, CoVal.Light3D, &H8000000D
    Obj.ForeColor = CoVal.Shadow3D
    Obj.Line (menText.Left - 15, menText.Top - 4)-(menText.Right - 3, menText.Bottom), &H8000000D, B
    PrintIcon Obj, menText.Left - 12, menText.Top - 1, Buttons(I).Icon
    If Buttons(I).TextHide = False Then DrawText Obj.hDC, Buttons(I).Text, Len(Buttons(I).Text), menText, 1
  ElseIf I = BMIndex And Buttons(I).Activ = True Then
    DrawFlow Obj, menText.Left - 15, menText.Top - 4, menText.Right + 13 - menText.Left, menText.Bottom - menText.Top + 30, CoVal.Light3D, &H8000000D, False, menText.Bottom - menText.Top + 4
    Obj.ForeColor = CoVal.MenuText
    Obj.Line (menText.Left - 15, menText.Top - 4)-(menText.Right - 3, menText.Bottom), &H8000000D, B
    PrintIcon Obj, menText.Left - 13, menText.Top - 2, Buttons(I).Icon
    If Buttons(I).TextHide = False Then DrawText Obj.hDC, Buttons(I).Text, Len(Buttons(I).Text), menText, 1
  Else
    DrawBG menText.Left - 15, menText.Right + 13 - menText.Left
    If Buttons(I).Activ = True Then
      Obj.ForeColor = CoVal.MenuText
      PrintIcon Obj, menText.Left - 12, menText.Top - 1, Buttons(I).Icon
    Else
      Obj.ForeColor = CoVal.GrayText
      PrintIcon Obj, menText.Left - 12, menText.Top - 1, Buttons(I).DisIcon
      'Obj.DrawMode = 9
      'DrawFlow Obj, menText.Left - 12, menText.Top - 1, 16, 16, CoVal.Light3D, CoVal.BackColor
      'Obj.DrawMode = 13
    End If
    If Buttons(I).TextHide = False Then DrawText Obj.hDC, Buttons(I).Text, Len(Buttons(I).Text), menText, 1
  End If
End Sub

Public Sub ShowToolTip(ByVal X As Single, ByVal Y As Single, Caption As String, Optional OFF As Boolean)
  If OFF = True Then
    If Form1.UInfoMode <> 2 Then Exit Sub
    Form1.UserInfoPic.Visible = False: Exit Sub
  End If
  Form1.UserInfoPic.FontBold = False
  Form1.UserInfoPic.Height = Form1.UserInfoPic.TextHeight("H") + 3
  Form1.UserInfoPic.Width = TextWW(Caption, Form1.UserInfoPic.hDC) + 4
  Form1.UserInfoPic.Top = Y + Obj.Top + 25
  Form1.UserInfoPic.Left = X + Obj.Left - Form1.UserInfoPic.Width / 3
  Form1.UserInfoPic.Visible = True
  Form1.UserInfoPic.Line (0, 0)-(Form1.UserInfoPic.Width - 1, Form1.UserInfoPic.Height - 1), &H80000018, BF
  Form1.UserInfoPic.Line (0, 0)-(Form1.UserInfoPic.Width - 1, Form1.UserInfoPic.Height - 1), &H80000012, B
  Form1.UserInfoPic.ForeColor = &H80000012
  'Form1.UserInfoPic.CurrentX = 2
  'Form1.UserInfoPic.CurrentY = 1
  'Form1.UserInfoPic.Print Caption
  TextOutW Form1.UserInfoPic.hDC, 2, 1, StrPtr(Caption), Len(Caption)
  Form1.UInfoMode = 2
End Sub

Public Sub ShowColors(ByVal X As Single, ByVal Y As Single)
  Dim I As Integer
  Form1.UserInfoPic.FontBold = False
  Form1.UserInfoPic.Height = 15 * 17 + 4 + 18
  Form1.UserInfoPic.Width = 120
  Form1.UserInfoPic.Top = Y
  Form1.UserInfoPic.Left = X
  Form1.UserInfoPic.Line (0, 0)-(Form1.UserInfoPic.Width - 1, Form1.UserInfoPic.Height - 1), &H80000005, BF
  Form1.UserInfoPic.Line (0, 0)-(Form1.UserInfoPic.Width - 1, Form1.UserInfoPic.Height - 1), &H8000000D, B
  Form1.UserInfoPic.CurrentX = 2
  Form1.UserInfoPic.CurrentY = 1
  For I = 0 To 15
    Form1.UserInfoPic.Line (4, 4 + I * 17)-Step(12, 12), &H80000012, B
    Form1.UserInfoPic.Line (5, 5 + I * 17)-Step(10, 10), GetIRCColor(I), BF
    Form1.UserInfoPic.CurrentX = 19
    Form1.UserInfoPic.CurrentY = 4 + I * 17
    Form1.UserInfoPic.ForeColor = &H80000012
    Form1.UserInfoPic.Print GetIRCColorName(I)
    Form1.UserInfoPic.CurrentX = Form1.UserInfoPic.ScaleWidth - 20
    Form1.UserInfoPic.CurrentY = 4 + I * 17
    Form1.UserInfoPic.ForeColor = CoVal.GrayText
    Form1.UserInfoPic.Print Format(I, "00")
  Next
  Form1.UInfoMode = 3
  Form1.UserInfoPic.Visible = True
  Form1.UserInfoPic.SetFocus
End Sub

Public Sub ClearIndex()
  Dim I As Integer
  Dim I2 As Integer
  ShowToolTip 0, 0, "", True
  I = BMIndex
  I2 = BIndex
  BMIndex = 0
  BIndex = 0
  BRef I
  BRef I2
  Obj.Refresh
  ReleaseCapture
End Sub
