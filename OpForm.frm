VERSION 5.00
Begin VB.Form Form5 
   BorderStyle     =   3  'Fester Dialog
   Caption         =   "Einstellungen"
   ClientHeight    =   6450
   ClientLeft      =   45
   ClientTop       =   540
   ClientWidth     =   6600
   BeginProperty Font 
      Name            =   "MS Shell Dlg"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "OpForm.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form5"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   430
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   440
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'Fenstermitte
   Begin VB.PictureBox picOpt 
      BackColor       =   &H00808080&
      BorderStyle     =   0  'Kein
      Height          =   5775
      Index           =   3
      Left            =   5880
      ScaleHeight     =   5775
      ScaleWidth      =   4815
      TabIndex        =   51
      TabStop         =   0   'False
      Top             =   5760
      Visible         =   0   'False
      Width           =   4815
      Begin VB.PictureBox Picture2 
         BorderStyle     =   0  'Kein
         Height          =   4935
         Left            =   -1200
         ScaleHeight     =   4935
         ScaleWidth      =   4695
         TabIndex        =   84
         TabStop         =   0   'False
         Top             =   720
         Width           =   4695
         Begin VB.CheckBox Check26 
            Caption         =   "Im Untermen� anzeigen"
            Height          =   255
            Left            =   1200
            TabIndex        =   86
            Top             =   480
            Width           =   2415
         End
         Begin VB.ComboBox Combo5 
            Height          =   315
            Left            =   1200
            Style           =   2  'Dropdown-Liste
            TabIndex        =   88
            Top             =   1440
            Width           =   1575
         End
         Begin VB.CommandButton Command18 
            Caption         =   "< Zur�ck"
            Height          =   340
            Left            =   120
            TabIndex        =   90
            Top             =   4440
            Width           =   1215
         End
         Begin VB.TextBox Text10 
            Height          =   285
            Left            =   1200
            MaxLength       =   1024
            TabIndex        =   85
            Top             =   120
            Width           =   3135
         End
         Begin VB.TextBox Text11 
            Height          =   285
            Left            =   1200
            MaxLength       =   1024
            TabIndex        =   87
            Top             =   960
            Width           =   3135
         End
         Begin VB.TextBox Text12 
            Height          =   1935
            Left            =   120
            MaxLength       =   2048
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertikal
            TabIndex        =   89
            Top             =   2340
            Width           =   4215
         End
         Begin VB.Label Label13 
            Caption         =   "Bezug:"
            Height          =   255
            Left            =   120
            TabIndex        =   94
            Top             =   1500
            Width           =   1095
         End
         Begin VB.Label Label9 
            Caption         =   "Auszuf�hrende Befehle:"
            Height          =   255
            Left            =   120
            TabIndex        =   93
            Top             =   2040
            Width           =   1935
         End
         Begin VB.Label Label12 
            Caption         =   "Kurzbefehl:"
            Height          =   255
            Left            =   120
            TabIndex        =   92
            Top             =   1005
            Width           =   1095
         End
         Begin VB.Label Label11 
            Caption         =   "Men�titel:"
            Height          =   255
            Left            =   120
            TabIndex        =   91
            Top             =   180
            Width           =   1095
         End
      End
      Begin VB.CommandButton Command20 
         Caption         =   "Entfernen"
         Enabled         =   0   'False
         Height          =   340
         Left            =   3240
         TabIndex        =   97
         Top             =   1200
         Width           =   1335
      End
      Begin VB.CommandButton Command19 
         Caption         =   "Neu"
         Height          =   340
         Left            =   3240
         TabIndex        =   96
         Top             =   720
         Width           =   1335
      End
      Begin VB.ListBox List3 
         Height          =   4740
         Left            =   120
         TabIndex        =   95
         Top             =   720
         Width           =   2895
      End
      Begin VB.CommandButton Command6 
         Caption         =   "Runter"
         Enabled         =   0   'False
         Height          =   340
         Left            =   3240
         TabIndex        =   18
         Top             =   2160
         Width           =   1335
      End
      Begin VB.CommandButton Command5 
         Caption         =   "Hoch"
         Enabled         =   0   'False
         Height          =   340
         Left            =   3240
         TabIndex        =   17
         Top             =   1680
         Width           =   1335
      End
      Begin VB.Line Line6 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   490
         Y2              =   490
      End
      Begin VB.Label CapLabel 
         Caption         =   "Shortcuts"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   3
         Left            =   120
         TabIndex        =   52
         Top             =   120
         Width           =   4335
      End
      Begin VB.Line Line5 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   5760
         Y2              =   5760
      End
   End
   Begin VB.PictureBox picOpt 
      BackColor       =   &H00808080&
      BorderStyle     =   0  'Kein
      Height          =   5775
      Index           =   5
      Left            =   6120
      ScaleHeight     =   5775
      ScaleWidth      =   4815
      TabIndex        =   53
      TabStop         =   0   'False
      Top             =   240
      Visible         =   0   'False
      Width           =   4815
      Begin VB.CommandButton Command15 
         Caption         =   "Einstellungen"
         Height          =   340
         Left            =   3360
         TabIndex        =   78
         Top             =   1785
         Width           =   1335
      End
      Begin VB.CommandButton Command14 
         Caption         =   "Aktualisieren"
         Height          =   340
         Left            =   3360
         TabIndex        =   31
         Top             =   2205
         Width           =   1335
      End
      Begin VB.CommandButton Command12 
         Caption         =   "L�schen"
         Height          =   340
         Left            =   3360
         TabIndex        =   32
         Top             =   2620
         Width           =   1335
      End
      Begin VB.CommandButton Command11 
         Caption         =   "Starten"
         Height          =   340
         Left            =   3360
         TabIndex        =   29
         Top             =   960
         Width           =   1335
      End
      Begin VB.CommandButton Command10 
         Caption         =   "Beenden"
         Height          =   340
         Left            =   3360
         TabIndex        =   30
         Top             =   1365
         Width           =   1335
      End
      Begin VB.ListBox List2 
         Height          =   2400
         Left            =   120
         Sorted          =   -1  'True
         TabIndex        =   28
         Top             =   960
         Width           =   3015
      End
      Begin VB.Line Line22 
         BorderColor     =   &H80000010&
         X1              =   105
         X2              =   4785
         Y1              =   3570
         Y2              =   3570
      End
      Begin VB.Label Label14 
         Caption         =   "Liste:"
         Height          =   255
         Left            =   120
         TabIndex        =   70
         Top             =   720
         Width           =   1935
      End
      Begin VB.Line Line15 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   490
         Y2              =   490
      End
      Begin VB.Label CapLabel 
         Caption         =   "Plugins"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   5
         Left            =   120
         TabIndex        =   54
         Top             =   120
         Width           =   4335
      End
      Begin VB.Line Line7 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   5760
         Y2              =   5760
      End
   End
   Begin VB.PictureBox picOpt 
      BackColor       =   &H00808080&
      BorderStyle     =   0  'Kein
      Height          =   5775
      Index           =   0
      Left            =   1080
      ScaleHeight     =   5775
      ScaleWidth      =   4815
      TabIndex        =   43
      TabStop         =   0   'False
      Top             =   1800
      Visible         =   0   'False
      Width           =   4815
      Begin VB.TextBox Text13 
         Height          =   285
         Left            =   3720
         TabIndex        =   99
         Top             =   1420
         Width           =   855
      End
      Begin VB.CheckBox Check11 
         Caption         =   "Automatisches Rejoin"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   4965
         Width           =   4695
      End
      Begin VB.CheckBox Check6 
         Caption         =   "Raumliste beim Start wieder automatisch aufrufen"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   4605
         Width           =   4695
      End
      Begin VB.CheckBox Check5 
         Caption         =   "R�ume beim Start wieder automatisch betreten"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   4245
         Width           =   4695
      End
      Begin VB.CheckBox Check4 
         Caption         =   "Verbindungen beim Start wieder automatisch aufbauen"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   3885
         Width           =   4695
      End
      Begin VB.CheckBox Check10 
         Caption         =   "Fenster wenn minimiert ganz ausblenden"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   3165
         Width           =   4575
      End
      Begin VB.CheckBox Check9 
         Caption         =   "Nettalk mit X Beenden"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   2805
         Width           =   4575
      End
      Begin VB.CheckBox Check7 
         Caption         =   "Timestamp anzeigen"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   2085
         Width           =   4575
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Raum-Text loggen"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   1080
         Width           =   4575
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Privat-Text loggen"
         Height          =   255
         Left            =   120
         TabIndex        =   0
         Top             =   720
         Width           =   4575
      End
      Begin VB.Label Label6 
         Caption         =   "Maximale gr��e der Log in KByte:"
         Height          =   255
         Left            =   120
         TabIndex        =   98
         Top             =   1440
         Width           =   3495
      End
      Begin VB.Label CapLabel 
         Caption         =   "Allgemein"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   0
         Left            =   120
         TabIndex        =   48
         Top             =   120
         Width           =   4335
      End
      Begin VB.Line Line13 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   490
         Y2              =   490
      End
      Begin VB.Line Line11 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   5760
         Y2              =   5760
      End
      Begin VB.Line Line10 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   3645
         Y2              =   3645
      End
      Begin VB.Line Line8 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   1890
         Y2              =   1890
      End
      Begin VB.Line Line9 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   2565
         Y2              =   2565
      End
   End
   Begin VB.PictureBox picOpt 
      BackColor       =   &H00808080&
      BorderStyle     =   0  'Kein
      Height          =   5775
      Index           =   1
      Left            =   720
      ScaleHeight     =   5775
      ScaleWidth      =   4815
      TabIndex        =   44
      TabStop         =   0   'False
      Top             =   1080
      Visible         =   0   'False
      Width           =   4815
      Begin VB.CheckBox Check22 
         Caption         =   "Nachricht einblenden wenn aktiviert"
         Height          =   255
         Left            =   600
         TabIndex        =   14
         Top             =   3600
         Width           =   4095
      End
      Begin VB.CheckBox Check21 
         Caption         =   "Wave-Datei wiedergeben"
         Height          =   255
         Left            =   600
         TabIndex        =   13
         Top             =   3240
         Width           =   3015
      End
      Begin VB.CheckBox Check20 
         Caption         =   "Signalton �ber PC-Speaker ausgeben"
         Height          =   255
         Left            =   600
         TabIndex        =   12
         Top             =   2880
         Width           =   4095
      End
      Begin VB.CommandButton Command9 
         Caption         =   "..."
         Height          =   255
         Left            =   3720
         TabIndex        =   69
         Top             =   3240
         Width           =   375
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   600
         MaxLength       =   1024
         TabIndex        =   15
         Top             =   4725
         Width           =   3735
      End
      Begin VB.CheckBox Check16 
         Caption         =   "Nachricht einblenden wenn aktiviert"
         Height          =   255
         Left            =   600
         TabIndex        =   16
         Top             =   5160
         Width           =   4095
      End
      Begin VB.CommandButton Command4 
         Caption         =   "..."
         Height          =   255
         Left            =   3720
         TabIndex        =   50
         Top             =   1440
         Width           =   375
      End
      Begin VB.CheckBox Check13 
         Caption         =   "Signalton �ber PC-Speaker ausgeben"
         Height          =   255
         Left            =   600
         TabIndex        =   9
         Top             =   1080
         Width           =   4095
      End
      Begin VB.CheckBox Check14 
         Caption         =   "Wave-Datei wiedergeben"
         Height          =   255
         Left            =   600
         TabIndex        =   10
         Top             =   1440
         Width           =   3015
      End
      Begin VB.CheckBox Check15 
         Caption         =   "Nachricht einblenden wenn aktiviert"
         Height          =   255
         Left            =   600
         TabIndex        =   11
         Top             =   1800
         Width           =   4095
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   4080
         Y2              =   4080
      End
      Begin VB.Label Label4 
         Caption         =   "Bei eingang �ffentlicher Nachrichten in folgenden R�umen:"
         Height          =   255
         Left            =   120
         TabIndex        =   68
         Top             =   4320
         Width           =   4695
      End
      Begin VB.Line Line3 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   2280
         Y2              =   2280
      End
      Begin VB.Label Label28 
         Caption         =   "Wenn �ffentliche Nachrichten den eigenden Nick enthalten:"
         Height          =   255
         Left            =   120
         TabIndex        =   67
         Top             =   2520
         Width           =   4695
      End
      Begin VB.Line Line12 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   490
         Y2              =   490
      End
      Begin VB.Label CapLabel 
         Caption         =   "Nachrichten"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   1
         Left            =   120
         TabIndex        =   47
         Top             =   120
         Width           =   4335
      End
      Begin VB.Line Line4 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   5760
         Y2              =   5760
      End
      Begin VB.Label Label3 
         Caption         =   "Bei eingang privater Nachrichten:"
         Height          =   255
         Left            =   120
         TabIndex        =   45
         Top             =   720
         Width           =   4575
      End
   End
   Begin VB.PictureBox OList 
      BackColor       =   &H00808080&
      BorderStyle     =   0  'Kein
      Height          =   5775
      Left            =   120
      ScaleHeight     =   385
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   100
      TabIndex        =   42
      TabStop         =   0   'False
      Top             =   120
      Width           =   1500
   End
   Begin VB.CommandButton Command3 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   340
      Left            =   2640
      TabIndex        =   38
      Top             =   6000
      Width           =   1215
   End
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Caption         =   "Abbrechen"
      Height          =   340
      Left            =   3960
      TabIndex        =   39
      Top             =   6000
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "�bernehmen"
      Height          =   340
      Left            =   5280
      TabIndex        =   40
      Top             =   6000
      Width           =   1215
   End
   Begin VB.PictureBox picOpt 
      BackColor       =   &H00808080&
      BorderStyle     =   0  'Kein
      Height          =   5775
      Index           =   4
      Left            =   75
      ScaleHeight     =   5775
      ScaleWidth      =   4815
      TabIndex        =   46
      TabStop         =   0   'False
      Top             =   1440
      Visible         =   0   'False
      Width           =   4815
      Begin VB.CommandButton Command16 
         Caption         =   "Speichern..."
         Height          =   300
         Left            =   3000
         TabIndex        =   76
         Top             =   5250
         Width           =   1215
      End
      Begin VB.ComboBox Combo4 
         Height          =   315
         Left            =   750
         Style           =   2  'Dropdown-Liste
         TabIndex        =   74
         Top             =   5250
         Width           =   2145
      End
      Begin VB.CheckBox Check18 
         Caption         =   "Fett"
         Height          =   255
         Left            =   2520
         TabIndex        =   23
         Top             =   3045
         Width           =   2055
      End
      Begin VB.ComboBox Combo3 
         Height          =   315
         Left            =   1320
         Style           =   2  'Dropdown-Liste
         TabIndex        =   22
         Top             =   3000
         Width           =   855
      End
      Begin VB.ComboBox Combo2 
         Height          =   315
         Left            =   1320
         Sorted          =   -1  'True
         Style           =   2  'Dropdown-Liste
         TabIndex        =   21
         Top             =   2640
         Width           =   2415
      End
      Begin VB.CheckBox Check17 
         Caption         =   "Hintergrundbild verwenden"
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   3780
         Width           =   4335
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   1680
         Style           =   2  'Dropdown-Liste
         TabIndex        =   27
         Top             =   4500
         Width           =   2055
      End
      Begin VB.CommandButton Command8 
         Caption         =   "..."
         Height          =   255
         Left            =   3840
         TabIndex        =   26
         Top             =   4170
         Width           =   375
      End
      Begin VB.TextBox Text3 
         Height          =   285
         Left            =   480
         MaxLength       =   1024
         TabIndex        =   25
         Top             =   4140
         Width           =   3255
      End
      Begin VB.CommandButton Command7 
         Caption         =   "�ndern..."
         Enabled         =   0   'False
         Height          =   300
         Left            =   3000
         TabIndex        =   20
         Top             =   1560
         Width           =   1095
      End
      Begin VB.PictureBox ColorPic 
         Appearance      =   0  '2D
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   340
         Left            =   3000
         ScaleHeight     =   315
         ScaleWidth      =   1305
         TabIndex        =   56
         TabStop         =   0   'False
         Top             =   960
         Width           =   1335
      End
      Begin VB.ListBox List1 
         Height          =   1425
         Left            =   120
         TabIndex        =   19
         Top             =   720
         Width           =   2655
      End
      Begin VB.Line Line16 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   2400
         Y2              =   2400
      End
      Begin VB.Label Label8 
         Caption         =   "Skins:"
         Height          =   255
         Left            =   120
         TabIndex        =   75
         Top             =   5280
         Width           =   630
      End
      Begin VB.Line Line21 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   5040
         Y2              =   5040
      End
      Begin VB.Label Label20 
         Caption         =   "Schriften auflisten..."
         ForeColor       =   &H8000000D&
         Height          =   255
         Left            =   1320
         TabIndex        =   60
         Top             =   2670
         Visible         =   0   'False
         Width           =   3255
      End
      Begin VB.Label Label19 
         Caption         =   "Schriftgr��e:"
         Height          =   255
         Left            =   120
         TabIndex        =   59
         Top             =   3045
         Width           =   1215
      End
      Begin VB.Label Label17 
         Caption         =   "Schriftart:"
         Height          =   255
         Left            =   120
         TabIndex        =   58
         Top             =   2670
         Width           =   1215
      End
      Begin VB.Line Line18 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   3540
         Y2              =   3540
      End
      Begin VB.Label Label18 
         Caption         =   "Anordnung:"
         Height          =   255
         Left            =   480
         TabIndex        =   57
         Top             =   4545
         Width           =   1215
      End
      Begin VB.Label Label16 
         Caption         =   "Farbe:"
         Height          =   255
         Left            =   3000
         TabIndex        =   55
         Top             =   720
         Width           =   1335
      End
      Begin VB.Line Line17 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   5760
         Y2              =   5760
      End
      Begin VB.Label CapLabel 
         Caption         =   "Aussehen"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   4
         Left            =   120
         TabIndex        =   49
         Top             =   120
         Width           =   4335
      End
      Begin VB.Line Line14 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   490
         Y2              =   490
      End
   End
   Begin VB.PictureBox picOpt 
      BackColor       =   &H00808080&
      BorderStyle     =   0  'Kein
      Height          =   5775
      Index           =   2
      Left            =   1800
      ScaleHeight     =   5775
      ScaleWidth      =   4815
      TabIndex        =   61
      TabStop         =   0   'False
      Tag             =   "AltTimer"
      Top             =   360
      Visible         =   0   'False
      Width           =   4815
      Begin VB.CheckBox Check30 
         Caption         =   "v4a"
         Height          =   255
         Left            =   3960
         TabIndex        =   100
         Top             =   2985
         Width           =   735
      End
      Begin VB.TextBox Text9 
         Height          =   285
         Left            =   2220
         TabIndex        =   83
         Text            =   "192.168.0.1:80"
         Top             =   2955
         Width           =   1605
      End
      Begin VB.CheckBox Check3 
         Caption         =   "Sock-Proxy nutzen:"
         Height          =   240
         Left            =   60
         TabIndex        =   82
         Top             =   2985
         Width           =   2130
      End
      Begin VB.CommandButton Command17 
         Caption         =   "..."
         Height          =   255
         Left            =   4050
         TabIndex        =   81
         Top             =   1820
         Width           =   375
      End
      Begin VB.TextBox Text8 
         Height          =   285
         Left            =   1395
         MaxLength       =   1024
         TabIndex        =   79
         Top             =   1785
         Width           =   2520
      End
      Begin VB.CheckBox Check25 
         Caption         =   "DCC-Verbindungen automatisch annehmen"
         Height          =   255
         Left            =   120
         TabIndex        =   77
         Top             =   1470
         Width           =   4335
      End
      Begin VB.CheckBox Check24 
         Caption         =   "Lokalen DCC-Server aktivieren"
         Height          =   255
         Left            =   60
         TabIndex        =   72
         Top             =   4545
         Width           =   4335
      End
      Begin VB.TextBox Text7 
         Height          =   285
         Left            =   1740
         MaxLength       =   5
         TabIndex        =   71
         Top             =   4905
         Width           =   855
      End
      Begin VB.CheckBox Check23 
         Caption         =   "Sekund�re IP f�r DCC-Verbindungen nutzen"
         Height          =   255
         Left            =   120
         TabIndex        =   34
         Top             =   1100
         Width           =   4335
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   1020
         TabIndex        =   37
         Text            =   "60"
         Top             =   3915
         Width           =   615
      End
      Begin VB.CheckBox Check12 
         Caption         =   "Automatisches wiederverbinden"
         Height          =   255
         Left            =   60
         TabIndex        =   36
         Top             =   3555
         Width           =   4575
      End
      Begin VB.TextBox Text6 
         Height          =   285
         Left            =   1440
         MaxLength       =   4
         TabIndex        =   35
         Top             =   2385
         Width           =   855
      End
      Begin VB.TextBox Text4 
         Height          =   285
         Left            =   3600
         MaxLength       =   5
         TabIndex        =   33
         Top             =   700
         Width           =   855
      End
      Begin VB.CheckBox Check19 
         Caption         =   "Einen standard-DCC-Port festlegen:"
         Height          =   255
         Left            =   120
         TabIndex        =   41
         Top             =   720
         Width           =   3495
      End
      Begin VB.Line Line25 
         BorderColor     =   &H80000010&
         X1              =   90
         X2              =   4770
         Y1              =   2790
         Y2              =   2790
      End
      Begin VB.Label Label5 
         Caption         =   "Pfad:"
         Height          =   255
         Left            =   480
         TabIndex        =   80
         Top             =   1815
         Width           =   885
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000010&
         X1              =   105
         X2              =   4785
         Y1              =   2220
         Y2              =   2220
      End
      Begin VB.Line Line24 
         BorderColor     =   &H80000010&
         X1              =   60
         X2              =   4740
         Y1              =   4365
         Y2              =   4365
      End
      Begin VB.Label Label29 
         Caption         =   "Port:"
         Height          =   255
         Left            =   420
         TabIndex        =   73
         Top             =   4935
         Width           =   1335
      End
      Begin VB.Label Label2 
         Caption         =   "sec erneut versuchen"
         Height          =   255
         Left            =   1725
         TabIndex        =   66
         Top             =   3945
         Width           =   2700
      End
      Begin VB.Label Label1 
         Caption         =   "Alle"
         Height          =   255
         Left            =   420
         TabIndex        =   65
         Top             =   3945
         Width           =   615
      End
      Begin VB.Label Label26 
         Caption         =   "sec"
         Height          =   255
         Left            =   2400
         TabIndex        =   64
         Top             =   2415
         Width           =   1335
      End
      Begin VB.Label Label24 
         Caption         =   "IRC-Timeout:"
         Height          =   255
         Left            =   120
         TabIndex        =   63
         Top             =   2415
         Width           =   1335
      End
      Begin VB.Line Line23 
         BorderColor     =   &H80000010&
         X1              =   90
         X2              =   4770
         Y1              =   3390
         Y2              =   3390
      End
      Begin VB.Line Line20 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   5760
         Y2              =   5760
      End
      Begin VB.Label CapLabel 
         Caption         =   "Verbindungen"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Index           =   2
         Left            =   120
         TabIndex        =   62
         Top             =   120
         Width           =   4335
      End
      Begin VB.Line Line19 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   490
         Y2              =   490
      End
   End
End
Attribute VB_Name = "Form5"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private ScList() As String
Private bColorList() As Long
Private TemoWavePath As String
Private TemoWavePath2 As String
Private OldOpFI As Integer
Private NewLEntr As Integer
Const MONO_SPACE_FONTS = "\Akkurat Mono\Andale Mono\Arial Monospaced\Vera Sans Mono\Botanika Mono\Consolas\Courier\CourierHP\Courier New\Fontcraft Courier\DejaVu Sans Mono\FF Elementa\Etelka Monospace\FF Eureka Mono\Fedra Mono\Fixed\Fixedsys\Fixedsys Excelsior\FreeMono\Free Monospaced\Helvetica Mono\HVEdit\HVRaster\HVTerminal\Windows Terminal\Hyperfont\Isonorm\Inconsolata\Letter Gothic\Lucida Console\Lucida Sans Typewriter\Lucida Typewriter\Monaco\Monospace\MS Gothic\MS Mincho\OCR-A\OCR-B\Orator\Ormaxx\Pragmata\Prestige\ProFont\Sydnie\Terminal\"
Const LColumH = 20

Private Sub OListRedraw()
  Dim menText As RECT
  Dim IconIndex As Integer
  Dim I As Integer
  
  OList.Line (0, 0)-(OList.ScaleWidth, OList.ScaleHeight), &H80000016, BF
  OList.Line (0, 0)-(0, OList.ScaleHeight - 1), &H80000010
  OList.Line (0, 0)-(OList.ScaleWidth - 1, 0), &H80000010
  OList.Line (OList.ScaleWidth - 1, 0)-(OList.ScaleWidth - 1, OList.ScaleHeight - 1), &H80000014
  OList.Line (0, OList.ScaleHeight - 1)-(OList.ScaleWidth - 1, OList.ScaleHeight - 1), &H80000014
  
  For I = 0 To picOpt.Count - 1
    OList.ForeColor = &H80000007
    
    menText.Left = 5
    menText.Top = I * 64 + 32
    menText.Right = OList.ScaleWidth - 5
    menText.Bottom = menText.Top + 20
    
    If I = OpFrameIndex Then
      DrawFlow OList, 5, menText.Top - 24, OList.ScaleWidth - 5, 45, &H80000016, &H8000000F, True
      DrawFlow OList, 5, menText.Top - 25, OList.ScaleWidth - 5, 1, &H80000016, &H80000014, True
      DrawFlow OList, 5, menText.Top + 21, OList.ScaleWidth - 5, 1, &H80000016, &H80000010, True
      OList.FontBold = True
      If picOpt(I).Visible = False Then picOpt(I).Visible = True
    Else
      OList.FontBold = False
      If picOpt(I).Visible = True Then picOpt(I).Visible = False
    End If
    Select Case I
      Case 0
        IconIndex = 20
      Case 1
        IconIndex = 35
      Case 2
        IconIndex = 21
      Case 3
        IconIndex = 52
      Case 4
        IconIndex = 51
      Case 5
        IconIndex = 53
    End Select
    PrintIcon OList, (OList.ScaleWidth - 16) / 2, menText.Top - 18, IconIndex
    DrawText OList.hDC, CapLabel(I).Caption, Len(CapLabel(I).Caption), menText, 1
  Next
End Sub

Private Sub RefSCList()
  Dim I As Integer
  Dim aInd As Integer
  Dim tInd As Integer
  Dim DAT As String
  
  aInd = List3.ListIndex
  tInd = List3.TopIndex
  List3.Clear
  For I = 0 To UBound(ScList, 2)
    If Len(ScList(2, I)) Then
      DAT = ScList(2, I)
    Else
      DAT = ScList(0, I)
    End If
    If Len(DAT) Then List3.Additem DAT
  Next
  If List3.ListCount - 1 < aInd Then aInd = List3.ListCount - 1
  If List3.ListCount - 1 < tInd Then tInd = List3.ListCount - 1
  If tInd > -1 Then List3.TopIndex = tInd
  List3.ListIndex = aInd
End Sub

Private Sub Check12_Click()
  CheckEnable
End Sub

Private Sub Check14_Click()
  CheckEnable
End Sub

Private Sub Check17_Click()
  CheckEnable
  Combo4.ListIndex = -1
End Sub

Private Sub Check18_Click()
  Combo4.ListIndex = -1
End Sub

Private Sub Check19_Click()
  CheckEnable
End Sub

Private Sub Check21_Click()
  CheckEnable
End Sub

Private Sub Check24_Click()
  CheckEnable
End Sub

Private Sub Check25_Click()
  Text8.Enabled = Check25.Value = 1
  Label5.Enabled = Check25.Value = 1
  Command17.Enabled = Check25.Value = 1
End Sub

Private Sub Check3_Click()
  Text9.Enabled = Check3.Value = 1
  Check30.Enabled = Check3.Value = 1
End Sub

Private Sub Combo1_Click()
  Combo4.ListIndex = -1
End Sub

Private Sub Combo2_Click()
  Combo4.ListIndex = -1
End Sub

Private Sub Combo2_GotFocus()
  Dim I As Integer
  If Combo2.ListCount > 1 Then Exit Sub
  Combo2.Visible = False
  Label20.Visible = True
  Screen.MousePointer = 11
  Me.Refresh
  Combo2.Clear
  For I = 0 To Screen.FontCount - 1
    If InStr(1, MONO_SPACE_FONTS, "\" + Screen.Fonts(I) + "\") Then
        Combo2.Additem "* " + Screen.Fonts(I) + " (Mono space)"
    Else
        Combo2.Additem Screen.Fonts(I)
    End If
  Next
  Label20.Visible = False
  Combo2.Visible = True
  Screen.MousePointer = 0
  SendMessageA Combo2.hWnd, WM_LBUTTONDOWN, ByVal 0, 0
  SendMessageA Combo2.hWnd, WM_LBUTTONUP, ByVal 0, 0
End Sub

Private Sub Combo3_Click()
  Combo4.ListIndex = -1
End Sub

Private Sub Command1_Click()
  Dim I As Long
  Dim I2 As Integer
  
  If Picture2.Visible = True Then Command18_Click
  For I = 0 To UBound(ScList, 2)
    If ScList(0, I) + ScList(1, I) + ScList(2, I) <> "" Then I2 = I
  Next
  
  ReDim ComTranzList(0 To 3, 0 To I2)
  For I = 0 To I2
    ComTranzList(0, I) = ScList(0, I)
    ComTranzList(1, I) = ScList(1, I)
    ComTranzList(2, I) = ScList(2, I)
    ComTranzList(3, I) = ScList(3, I)
  Next
  
  'Page 1
  LogmodePrivate = CBool(Check1.Value)
  LogmodeRoom = CBool(Check2.Value)
  ShowTimeStampText = CBool(Check7.Value)
  SetGlobalTimeShow ShowTimeStampText
  NoQuitWithX = CBool(Check9.Value) = False
  HideFormWithMin = CBool(Check10.Value)
  RemaindConns = CBool(Check4.Value)
  RemaindRooms = CBool(Check5.Value)
  RemaindList = CBool(Check6.Value)
  AutoReJoin = CBool(Check11.Value)
  If Val(Text13.Text) > 128000 Then Text13.Text = 128000
  If PrivLogMaxSize = 0 Or PublLogMaxSize = 0 Then
    PrivLogMaxSize = Val(Text13.Text) / 2
    PublLogMaxSize = Val(Text13.Text) / 2
  Else
    I = PrivLogMaxSize + PublLogMaxSize
    PrivLogMaxSize = Val(Text13.Text) * PrivLogMaxSize / I
    PublLogMaxSize = Val(Text13.Text) * PublLogMaxSize / I
  End If
  
  'Page2
  PlayBeep = CBool(Check13.Value)
  PlayWav = CBool(Check14.Value)
  WavePath = TemoWavePath
  EaOSDPriv = CBool(Check15.Value)
  
  PlayBeep2 = CBool(Check20.Value)
  PlayWav2 = CBool(Check21.Value)
  WavePath2 = TemoWavePath2
  EaOSDPriv2 = CBool(Check22.Value)
  
  EaOSDRoom = CBool(Check16.Value)
  OsdRoomList = Text2.Text
  
  'Conns
  If Check19.Value = 1 Then
    StaticDCCPort = Abs(Val(Text4.Text))
  Else
    StaticDCCPort = Abs(Val(Text4.Text)) * -1
  End If
  If Check24.Value = 1 Then
    DCCServerPort = Abs(Val(Text7.Text))
  Else
    DCCServerPort = Abs(Val(Text7.Text)) * -1
  End If
  
  I = InStr(1, Text9.Text, ":")
  If I > 0 Then
    SockProxyServerGlob = Left(Text9.Text, I - 1)
    SockProxyPortGlob = Val(Mid(Text9.Text, I + 1))
  Else
    SockProxyServerGlob = Text9.Text
    SockProxyPortGlob = 80
  End If
  If Check30.Value = 0 Then
    SockProxyTypeGlob = 40
  Else
    SockProxyTypeGlob = 45
  End If
  
  If Check3.Value = 1 Then
    SockProxyPortGlob = Abs(SockProxyPortGlob)
  Else
    SockProxyPortGlob = Abs(SockProxyPortGlob) * -1
  End If
  UseSecIP = CBool(Check23.Value)
  dccAutoAcc = CBool(Check25.Value)
  DCCPath = Text8.Text
  If Val(Text6.Text) < 30 Then Text6.Text = 30
  IRCTimeOut = Val(Text6.Text)
  CheckDCCServ 'DCC-Server starten/beenden
  
  'Font
  If Combo2.ListCount > 2 Then
    If Combo2.ListIndex < 0 Then Combo2.ListIndex = 0
    FontTypeName = Combo2.List(Combo2.ListIndex)
    If Left(FontTypeName, 2) = "* " Then
      I = InStr(1, FontTypeName, " (Mono space)")
      If I > 0 Then
        FontTypeName = Mid(FontTypeName, 3, Len(FontTypeName) - 15)
      End If
    End If
  End If
  FontTypeSize = Combo3.ListIndex + 6
  FontTypeBold = CBool(Check18.Value)
     
  'HGBild
  If HGBildPath <> Trim(Text3.Text) Then
    HGBildPath = Trim(Text3.Text)
    RefreshBGP
  End If
  
  HGDrawMode = Combo1.ListIndex + 1
  If Check17.Value = 0 Or HGBildPath = "" Then HGDrawMode = 0
  
  'Farben
  CoVal.ClientMSG = bColorList(0)
  CoVal.ErrorMSG = bColorList(1)
  CoVal.FlusterVon = bColorList(2)
  CoVal.Link = bColorList(3)
  CoVal.NormalText = bColorList(4)
  CoVal.NormalVon = bColorList(5)
  CoVal.Notice = bColorList(6)
  CoVal.ServerMSG = bColorList(7)
  CoVal.UserMSG = bColorList(8)
  Form1.pTBox.BackColor = bColorList(9)
  Form1.pUList.ForeColor = bColorList(10)
  Form1.pUList.BackColor = bColorList(11)
  Form1.Text1.ForeColor = bColorList(12)
  Form1.Text1.BackColor = bColorList(13)
  CoVal.TimeStamp = bColorList(14)
  CoVal.OwnNick = bColorList(15)
  CoVal.ButtonShadow = bColorList(16)
  CoVal.Light3D = bColorList(17)
  CoVal.BackColor = bColorList(18)
  CoVal.MenuText = bColorList(19)
  CoVal.GrayText = bColorList(20)
  CoVal.Shadow3D = bColorList(21)
  CoVal.Highlight3D = bColorList(22)
  CoVal.ButtonText = bColorList(23)
  
  'Skin
  If Combo4.ListIndex > -1 Then
    On Error Resume Next
    If Len(Dir(UserPath + Combo4.Text + ".skn")) > 0 Then
      ReadSkinPaket UserPath + Dir(UserPath + Combo4.Text + ".skn")
    ElseIf Len(Dir(AppPath + Combo4.Text + ".skn")) > 0 Then
      ReadSkinPaket AppPath + Dir(AppPath + Combo4.Text + ".skn")
    End If
    On Error GoTo 0
    RefreshBGP
  End If
  
  On Error Resume Next
  Form1.pTBox.FontItalic = False
  Form1.pTBox.FontName = FontTypeName
  Form1.pTBox.FontSize = FontTypeSize
  Form1.pTBox.FontBold = FontTypeBold
  Form1.pTexthg.FontItalic = False
  Form1.pTexthg.FontName = FontTypeName
  Form1.pTexthg.FontBold = FontTypeBold
  Form1.pTexthg.FontSize = FontTypeSize
  Form1.Text1.FontItalic = False
  Form1.Text1.FontName = FontTypeName
  Form1.Text1.FontBold = FontTypeBold
  Form1.Text1.FontSize = FontTypeSize
  On Error GoTo 0
  Form1.GlobalColorRefresh
  
  If Check12.Value = 1 Then
    RetryTime = Val(Text1.Text)
  Else
    RetryTime = 0
  End If
End Sub

Public Sub RefPluginList()
  Dim I As Integer
  Dim DAT As String
  List2.Clear
  For I = 0 To UBound(PluginList)
    If Len(PluginList(I).Name) > 0 Then
      List2.Additem UCase(PluginList(I).Name) + " -> gestartet"
    End If
  Next
  DAT = Dir(UserPath + "*.plg")
  Do Until Len(DAT) = 0
    For I = 0 To UBound(PluginList)
      If LCase(PluginList(I).Name) = LCase(Left(DAT, Len(DAT) - 4)) Then Exit For
    Next
    If I > UBound(PluginList) Then List2.Additem UCase(Left(DAT, Len(DAT) - 4))
    DAT = Dir
  Loop
  DAT = Dir(AppPath + "*.plg")
  Do Until Len(DAT) = 0
    For I = 0 To UBound(PluginList)
      If LCase(PluginList(I).Name) = LCase(Left(DAT, Len(DAT) - 4)) Then Exit For
    Next
    If I > UBound(PluginList) Then List2.Additem UCase(Left(DAT, Len(DAT) - 4))
    DAT = Dir
  Loop
  ButtenEa
End Sub

Private Sub ButtenEa()
  Command11.Enabled = False
  Command10.Enabled = False
  Command12.Enabled = False
  Command15.Enabled = False
  If List2.ListIndex < 0 Then Exit Sub
  If InStr(List2.Text, "->") > 0 Then
    Command10.Enabled = True
    Command15.Enabled = True
  Else
    Command11.Enabled = True
    Command12.Enabled = True
  End If
End Sub

Private Sub Command10_Click()
  If List2.ListIndex > -1 Then UnloadPlugin CutDirFromPath(List2.Text, " ->")
End Sub

Private Sub Command11_Click()
  If List2.ListIndex > -1 Then StartPlugin List2.Text
End Sub

Private Sub Command12_Click()
  Dim DAT As String
  If List2.ListIndex > -1 Then
    On Error Resume Next
    DAT = UserPath + List2.Text + ".plg"
    If Len(Dir(DAT)) = 0 Then DAT = AppPath + List2.Text + ".plg"
    Kill DAT
    If Err.Number <> 0 Then MsgBox Err.Description, vbCritical
    On Error GoTo 0
    RefPluginList
  End If
End Sub

Private Sub Command14_Click()
  RefPluginList
End Sub

Private Sub Command15_Click()
  If List2.ListIndex > -1 Then ShowPluginValues CutDirFromPath(List2.Text, " ->")
End Sub

Private Sub Command16_Click()
  Dim DAT As String
  Dim I As Integer
  
  Command1_Click ' �bernehmen
  
  Do
    ShowSave DAT, "Skin" + Chr(0) + "*.skn", UserPath, "", I, Me.hWnd
    If DAT = "" Then Exit Sub
    If Mid(DAT, Len(DAT) - 3, 1) <> "." And Mid(DAT, Len(DAT) - 4, 1) <> "." Then
      DAT = DAT + ".skn"
    End If
    On Error Resume Next
    If Dir(DAT) <> "" Then
      On Error GoTo 0
      If MsgBox(TeVal.FileExist, vbQuestion + vbYesNo) = vbYes Then Exit Do
    Else
      On Error GoTo 0
      Exit Do
    End If
  Loop
  CreateSkinPaket DAT
  RefSkinlist
End Sub

Private Sub RefSkinlist()
  Dim DAT As String
  Combo4.Clear
  DAT = Dir(UserPath + "*.skn")
  Do Until DAT = ""
    Combo4.Additem Left(DAT, Len(DAT) - 4)
    DAT = Dir
  Loop
  DAT = Dir(AppPath + "*.skn")
  Do Until DAT = ""
    Combo4.Additem Left(DAT, Len(DAT) - 4)
    DAT = Dir
  Loop
End Sub

Private Sub Command17_Click()
  Dim DAT As String
  DAT = GetFolder(Me.hWnd, Me.Caption)
  If Len(DAT) > 0 Then Text8.Text = DAT
End Sub

Private Sub Command18_Click()
  Dim I As Long
  Dim I2 As Long
  
  Picture2.Visible = False
  List3.Visible = True
  Command5.Visible = True
  Command6.Visible = True
  Command19.Visible = True
  Command20.Visible = True
  If Len(Text11.Text) Or Len(Text10.Text) Then
    If NewLEntr > -1 And NewLEntr < List3.ListCount Then
      For I = List3.ListCount - 2 To NewLEntr Step -1
        ScList(2, I + 1) = ScList(2, I)
        ScList(0, I + 1) = ScList(0, I)
        ScList(1, I + 1) = ScList(1, I)
        ScList(3, I + 1) = ScList(3, I)
      Next
      List3.ListIndex = NewLEntr
    End If
    If InStr(1, Text10.Text, Chr(13)) Or InStr(1, Text11.Text, Chr(13)) Then Exit Sub
    ScList(2, List3.ListIndex) = IIf(Check26.Value, ">", "") + Text10.Text
    If Left(Text11.Text, 1) = "/" Then
      ScList(0, List3.ListIndex) = Mid(Text11.Text, 2)
    Else
      ScList(0, List3.ListIndex) = Text11.Text
    End If
    ScList(3, List3.ListIndex) = Combo5.ListIndex + 10
    ScList(1, List3.ListIndex) = ""
    I = -1
    Do
      I2 = I
      I = InStr(I + 2, Text12.Text, vbNewLine)
      If I = 0 Then I = Len(Text12.Text) + 1
      If I2 > -1 Then ScList(1, List3.ListIndex) = ScList(1, List3.ListIndex) + vbTab + vbTab
      If Mid(Text12.Text, I2 + 2, 1) = "/" Then
        ScList(1, List3.ListIndex) = ScList(1, List3.ListIndex) + Mid(Text12.Text, I2 + 3, I - I2 - 3)
      Else
        ScList(1, List3.ListIndex) = ScList(1, List3.ListIndex) + Mid(Text12.Text, I2 + 2, I - I2 - 2)
      End If
    Loop Until I = Len(Text12.Text) + 1
    RefSCList
  Else
    Command20_Click
  End If
  NewLEntr = -1
End Sub

Private Sub Command19_Click()
  ReDim Preserve ScList(3, List3.ListCount)
  NewLEntr = List3.ListIndex
  List3.Additem "-"
  List3.ListIndex = List3.ListCount - 1
  ScList(3, List3.ListIndex) = -1
  List3_DblClick
End Sub

Private Sub Command2_Click()
  Unload Me
End Sub

Private Sub Command20_Click()
  Dim I As Integer
  
  For I = List3.ListIndex To List3.ListCount - 2
    ScList(2, I) = ScList(2, I + 1)
    ScList(0, I) = ScList(0, I + 1)
    ScList(1, I) = ScList(1, I + 1)
    ScList(3, I) = ScList(3, I + 1)
  Next
  ScList(2, I) = ""
  ScList(0, I) = ""
  ScList(1, I) = ""
  ScList(3, I) = ""
  RefSCList
End Sub

Private Sub Command3_Click()
  Command1_Click
  Form1.SaveMainValues False
  Unload Me
End Sub

Private Sub Command4_Click()
  Dim File As String
  Dim DefoultStr As String
  On Error Resume Next
  DefoultStr = CutDirFromPath(TemoWavePath)
  If DefoultStr = "" Then DefoultStr = LastLPath
  Do
    File = ShowOpen("Wave-files" + Chr(0) + "*.wav", Me.hWnd, DefoultStr)
    If File = "" Then Exit Sub
  Loop While Dir(File) = ""
  If Err <> 0 Then MsgBox Err.Description, vbCritical
  On Error GoTo 0
  TemoWavePath = File
End Sub

Private Sub Command5_Click()
  Dim TempVal(0 To 3) As String
  If List3.ListIndex = 0 Then Exit Sub
  TempVal(0) = ScList(0, List3.ListIndex)
  TempVal(1) = ScList(1, List3.ListIndex)
  TempVal(2) = ScList(2, List3.ListIndex)
  TempVal(3) = ScList(3, List3.ListIndex)
  
  ScList(0, List3.ListIndex) = ScList(0, List3.ListIndex - 1)
  ScList(1, List3.ListIndex) = ScList(1, List3.ListIndex - 1)
  ScList(2, List3.ListIndex) = ScList(2, List3.ListIndex - 1)
  ScList(3, List3.ListIndex) = ScList(3, List3.ListIndex - 1)
  
  List3.ListIndex = List3.ListIndex - 1
  ScList(0, List3.ListIndex) = TempVal(0)
  ScList(1, List3.ListIndex) = TempVal(1)
  ScList(2, List3.ListIndex) = TempVal(2)
  ScList(3, List3.ListIndex) = TempVal(3)
  RefSCList
End Sub

Private Sub Command6_Click()
  Dim TempVal(0 To 3) As String
  If List3.ListIndex = List3.ListCount - 1 Then Exit Sub
  TempVal(0) = ScList(0, List3.ListIndex)
  TempVal(1) = ScList(1, List3.ListIndex)
  TempVal(2) = ScList(2, List3.ListIndex)
  TempVal(3) = ScList(3, List3.ListIndex)
  
  ScList(0, List3.ListIndex) = ScList(0, List3.ListIndex + 1)
  ScList(1, List3.ListIndex) = ScList(1, List3.ListIndex + 1)
  ScList(2, List3.ListIndex) = ScList(2, List3.ListIndex + 1)
  ScList(3, List3.ListIndex) = ScList(3, List3.ListIndex + 1)
  
  List3.ListIndex = List3.ListIndex + 1
  ScList(0, List3.ListIndex) = TempVal(0)
  ScList(1, List3.ListIndex) = TempVal(1)
  ScList(2, List3.ListIndex) = TempVal(2)
  ScList(3, List3.ListIndex) = TempVal(3)
  RefSCList
End Sub

Private Sub Command7_Click()
  Dim TColor As Long
  If List1.ListIndex = -1 Then Exit Sub
  TColor = ShowColor(Me)
  If TColor < 0 Then Exit Sub
  ColorPic.BackColor = TColor
  bColorList(List1.ListIndex) = TColor
  Combo4.ListIndex = -1
End Sub

Private Sub Command8_Click()
  Dim File As String
  Dim DefoultStr As String
  On Error Resume Next
  DefoultStr = CutDirFromPath(Text3.Text)
  If DefoultStr = "" Then DefoultStr = LastLPath
  Do
    File = ShowOpen("Picture-files" + Chr(0) + "*.bmp;*.jpg;*.gif;*.wmf;*.jpeg", Me.hWnd, DefoultStr)
    If File = "" Then Exit Sub
  Loop While Dir(File) = ""
  If Err <> 0 Then MsgBox Err.Description, vbCritical
  On Error GoTo 0
  Text3.Text = File
End Sub

Private Sub Command9_Click()
  Dim File As String
  Dim DefoultStr As String
  On Error Resume Next
  DefoultStr = CutDirFromPath(TemoWavePath2)
  If DefoultStr = "" Then DefoultStr = LastLPath
  Do
    File = ShowOpen("Wave-files" + Chr(0) + "*.wav", Me.hWnd, DefoultStr)
    If File = "" Then Exit Sub
  Loop While Dir(File) = ""
  If Err <> 0 Then MsgBox Err.Description, vbCritical
  On Error GoTo 0
  TemoWavePath2 = File
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = 9 And Shift = 2 Then
    OpFrameIndex = OpFrameIndex + 1
    If OpFrameIndex > 5 Then OpFrameIndex = 0
    OList_MouseDown 1, 0, -2000, 2000
  End If
  If KeyCode = 9 And Shift = 3 Then
    OpFrameIndex = OpFrameIndex - 1
    If OpFrameIndex < 0 Then OpFrameIndex = 5
    OList_MouseDown 1, 0, -2000, 2000
  End If
End Sub


Private Sub Form_Load()
  Dim I As Integer
  Dim I2 As Integer
  
  InitFormFont Me
  
  OldOpFI = -1
  NewLEntr = -1
  OpFormIsLoaded = True
    
'  Me.Height = 430 * Screen.TwipsPerPixelY + (Me.Height - Me.ScaleHeight * Screen.TwipsPerPixelY)
'  Me.Width = 440 * Screen.TwipsPerPixelX + (Me.Width - Me.ScaleWidth * Screen.TwipsPerPixelX)
'  Debug.Print Me.Height ' -> 6930
'  Debug.Print Me.Width  ' -> 6690
  
  LoadFormStrs Me
  
  For I = 0 To UBound(ComTranzList, 2)
    If Len(ComTranzList(0, I)) > 0 Or Len(ComTranzList(2, I)) > 0 Then I2 = I
  Next

  ReDim ScList(0 To 3, 0 To I2 + 1)
  For I = 0 To I2
    ScList(0, I) = ComTranzList(0, I)
    ScList(1, I) = ComTranzList(1, I)
    ScList(2, I) = ComTranzList(2, I)
    ScList(3, I) = ComTranzList(3, I)
  Next
  ScList(3, I2 + 1) = CStr(-1)
  
  For I = 0 To picOpt.Count - 1
    picOpt(I).Left = 114
    picOpt(I).Top = 8
    picOpt(I).BackColor = Me.BackColor
  Next
  
  '------------------------------
  'Page 1
  Check1.Value = CInt(LogmodePrivate) * -1
  Check2.Value = CInt(LogmodeRoom) * -1
  Check7.Value = CInt(ShowTimeStampText) * -1
  Check9.Value = CInt(NoQuitWithX = False) * -1
  Check10.Value = CInt(HideFormWithMin) * -1
  Check4.Value = CInt(RemaindConns) * -1
  Check5.Value = CInt(RemaindRooms) * -1
  Check6.Value = CInt(RemaindList) * -1
  Check11.Value = CInt(AutoReJoin) * -1
  Text13.Text = PrivLogMaxSize + PublLogMaxSize
  
  'Page2
  Check13.Value = CInt(PlayBeep) * -1
  Check14.Value = CInt(PlayWav) * -1
  TemoWavePath = WavePath
  Check15.Value = CInt(EaOSDPriv) * -1
  
  Check20.Value = CInt(PlayBeep2) * -1
  Check21.Value = CInt(PlayWav2) * -1
  TemoWavePath2 = WavePath2
  Check22.Value = CInt(EaOSDPriv2) * -1
  
  Check16.Value = CInt(EaOSDRoom) * -1
  Text2.Text = OsdRoomList
  
  'Conns
  Text6.Text = IRCTimeOut
  Check25.Value = CInt(dccAutoAcc) * -1
  If Len(DCCPath) = 0 Then DCCPath = LastSPath
  Text8.Text = DCCPath
  Text8.Enabled = Check25.Value = 1
  Command17.Enabled = Check25.Value = 1
  Label5.Enabled = Check25.Value = 1
  
  If StaticDCCPort < 1 Then
    Check19.Value = 0
    If StaticDCCPort = 0 Then
      StaticDCCPort = 3900
    Else
      Text4.Text = StaticDCCPort * -1
    End If
  Else
    Check19.Value = 1
    Text4.Text = StaticDCCPort
  End If
  If DCCServerPort < 1 Then
    Check24.Value = 0
    If DCCServerPort = 0 Then
      DCCServerPort = -59
    End If
    Text7.Text = DCCServerPort * -1
  Else
    Check24.Value = 1
    Text7.Text = DCCServerPort
  End If
  If SockProxyPortGlob < 1 Then
    Check3.Value = 0
    If SockProxyPortGlob = 0 Then
      SockProxyPortGlob = 80
    Else
      Text9.Text = SockProxyServerGlob + ":" + CStr(SockProxyPortGlob * -1)
    End If
  Else
    Check3.Value = 1
     Text9.Text = SockProxyServerGlob + ":" + CStr(SockProxyPortGlob)
  End If
  Check30.Value = CInt(SockProxyTypeGlob = 45) * -1
  
  Text9.Enabled = Check3.Value = 1
  Check30.Enabled = Check3.Value = 1
  Check23.Value = IIf(UseSecIP, 1, 0)
  If RetryTime = 0 Then
    Check12.Value = 0
    Text1.Text = 30
  Else
    Check12.Value = 1
    Text1.Text = RetryTime
  End If

    
  'Forntlist ---------------------
  
  Combo2.Additem FontTypeName
  Combo2.ListIndex = 0

  For I = 6 To 24
    Combo3.Additem CStr(I)
  Next
  If FontTypeSize < 7 Or FontTypeSize > 24 Then FontTypeSize = 8
  Combo3.ListIndex = FontTypeSize - 6
  Check18.Value = CInt(FontTypeBold) * -1
    
  'HG-Bild -----------------------
  
  Combo1.Additem TeVal.opPos1
  Combo1.Additem TeVal.opPos2
  If HGDrawMode > 0 Then
    Check17.Value = 1
    If HGDrawMode < 3 Then
      Combo1.ListIndex = HGDrawMode - 1
    Else
      Combo1.ListIndex = 0
    End If
  Else
    Combo1.ListIndex = 0
    Check17.Value = 0
  End If
  Text3.Text = HGBildPath
  
  'Farben -----------------------
  
  ReDim bColorList(0 To 23)
  List1.Additem TeVal.cClientMsg
  bColorList(0) = CoVal.ClientMSG
  List1.Additem TeVal.cErrMsg
  bColorList(1) = CoVal.ErrorMSG
  List1.Additem TeVal.cNickW
  bColorList(2) = CoVal.FlusterVon
  List1.Additem TeVal.cLink
  bColorList(3) = CoVal.Link
  List1.Additem TeVal.cText
  bColorList(4) = CoVal.NormalText
  List1.Additem TeVal.cNickN
  bColorList(5) = CoVal.NormalVon
  List1.Additem TeVal.cNotice
  bColorList(6) = CoVal.Notice
  List1.Additem TeVal.cMSG
  bColorList(7) = CoVal.ServerMSG
  List1.Additem TeVal.chMSG
  bColorList(8) = CoVal.UserMSG
  List1.Additem TeVal.ccHG
  bColorList(9) = Form1.pTBox.BackColor
  List1.Additem TeVal.ctUserL
  bColorList(10) = Form1.pUList.ForeColor
  List1.Additem TeVal.cULHG
  bColorList(11) = Form1.pUList.BackColor
  List1.Additem TeVal.cTextF
  bColorList(12) = Form1.Text1.ForeColor
  List1.Additem TeVal.cTextFHG
  bColorList(13) = Form1.Text1.BackColor
  List1.Additem TeVal.cTimeStamp
  bColorList(14) = CoVal.TimeStamp
  List1.Additem TeVal.cOwnNick
  bColorList(15) = CoVal.OwnNick
  List1.Additem "Windows color (ButtonShadow)"
  bColorList(16) = CoVal.ButtonShadow
  List1.Additem "Windows color (3D Light)"
  bColorList(17) = CoVal.Light3D
  List1.Additem "Windows color (BackColor)"
  bColorList(18) = CoVal.BackColor
  List1.Additem "Windows color (MenuText)"
  bColorList(19) = CoVal.MenuText
  List1.Additem "Windows color (GrayText)"
  bColorList(20) = CoVal.GrayText
  List1.Additem "Windows color (3D Shadow)"
  bColorList(21) = CoVal.Shadow3D
  List1.Additem "Windows color (3D Highlight)"
  bColorList(22) = CoVal.Highlight3D
  List1.Additem "Windows color (ButtonText)"
  bColorList(23) = CoVal.ButtonText
  
  '------------------------------
    
  Command1.Caption = TeVal.butApply
  Command2.Caption = TeVal.butCancel
  Command3.Caption = TeVal.butOK
  Me.Caption = TeVal.menOpt
  
  '------------------------------
  
  Combo5.Additem TeVal.bezUser
  Combo5.Additem TeVal.bezChannel
  Combo5.Additem TeVal.bezGlobal
  
  Picture2.Left = 0
  Picture2.Top = 720
  Picture2.Visible = False
  Check26.Enabled = False
  
  
  CheckEnable
  
  If OpFrameIndex > picOpt.Count - 1 Then OpFrameIndex = picOpt.Count - 1
  OListRedraw
  RefPluginList
  RefSkinlist
  RefSCList
End Sub

Private Sub CheckEnable()
  If Check12.Value = 0 Then
    Label1.Enabled = False: Label2.Enabled = False: Text1.Enabled = False
  Else
    Label1.Enabled = True: Label2.Enabled = True: Text1.Enabled = True
  End If
  If Check14.Value = 0 Then
    Command4.Enabled = False
  Else
    Command4.Enabled = True
  End If
  If Check21.Value = 0 Then
    Command9.Enabled = False
  Else
    Command9.Enabled = True
  End If
  If Check24.Value = 0 Then
    Label29.Enabled = False
    Text7.Enabled = False
  Else
    Label29.Enabled = True
    Text7.Enabled = True
  End If
  If Check17.Value = 0 Then
    Label18.Enabled = False
    Combo1.Enabled = False
    Text3.Enabled = False
    Command8.Enabled = False
  Else
    Label18.Enabled = True
    Combo1.Enabled = True
    Text3.Enabled = True
    Command8.Enabled = True
  End If
  If Check19.Value = 0 Then
    Text4.Enabled = False
  Else
    Text4.Enabled = True
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  OpFormIsLoaded = False
End Sub

Private Sub List1_Click()
  If List1.ListIndex = -1 Then Command7.Enabled = False: Exit Sub
  ColorPic.BackColor = bColorList(List1.ListIndex)
  Command7.Enabled = True
End Sub

Private Sub List2_Click()
  ButtenEa
End Sub

Private Sub List3_Click()
  Command5.Enabled = List3.ListIndex > 0
  Command6.Enabled = List3.ListIndex < List3.ListCount
  Command19.Enabled = True
  Command20.Enabled = True
End Sub

Private Sub List3_DblClick()
  Dim I As Long
  Dim I2 As Long

  Picture2.Visible = True
  List3.Visible = False
  Command5.Visible = False
  Command6.Visible = False
  Command19.Visible = False
  Command20.Visible = False
  If Left(ScList(2, List3.ListIndex), 1) = ">" Then
    Text10.Text = Mid(ScList(2, List3.ListIndex), 2)
    Check26.Value = 1
  Else
    Text10.Text = ScList(2, List3.ListIndex)
    Check26.Value = 0
  End If
  If Len(ScList(0, List3.ListIndex)) Then
    Text11.Text = "/" + ScList(0, List3.ListIndex)
  Else
    Text11.Text = ""
  End If
  
  Text12.Text = ""
  I = -1
  Do
    I2 = I
    I = InStr(I + 2, ScList(1, List3.ListIndex), vbTab + vbTab)
    If I = 0 Then I = Len(ScList(1, List3.ListIndex)) + 1
    If I2 > -1 Then Text12.Text = Text12.Text + vbNewLine
    If Len(Trim(Mid(ScList(1, List3.ListIndex), I2 + 2, I - I2 - 2))) > 0 Then
      Text12.Text = Text12.Text + "/" + Mid(ScList(1, List3.ListIndex), I2 + 2, I - I2 - 2)
    End If
  Loop Until I = Len(ScList(1, List3.ListIndex)) + 1
  
  If Val(ScList(3, List3.ListIndex)) < 10 Then
    Combo5.ListIndex = 0
  Else
    Combo5.ListIndex = Val(ScList(3, List3.ListIndex)) - 10
  End If
End Sub

Private Sub OList_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Int(Y / 64) < picOpt.Count Then OpFrameIndex = Int(Y / 64)
  If OpFrameIndex <> OldOpFI Then
    OListRedraw
    If Picture2.Visible And OpFrameIndex = 3 Then Command18_Click
    OldOpFI = OpFrameIndex
  End If
End Sub

Private Sub OList_Paint()
  OListRedraw
End Sub

Private Sub Text10_Change()
  Check26.Enabled = CBool(Len(Text10.Text))
End Sub

Private Sub Text12_GotFocus()
  Command3.Default = False
End Sub

Private Sub Text12_LostFocus()
  Command3.Default = True
End Sub

