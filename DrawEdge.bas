Attribute VB_Name = "PixelDraw"
Option Explicit

Private MyPixelFeld(5, 37) As Long
Private Colo(2, 4) As Long
Private RGB_Farbe(3) As Long
Private Inted As Boolean

Public Sub DrawEdge(Obj As PictureBox, ByVal X As Long, ByVal Y As Long, ccB As Long, ccH As Long, ccS As Long, ccG As Long)
  Dim I As Long
  Dim I2 As Long
  Dim Ic As Long
  Dim RColor As Long
  
  If Inted = False Then IntPixArray
  
  TranslateColor ccB, 0, RGB_Farbe(0)
  TranslateColor ccH, 0, RGB_Farbe(1)
  TranslateColor ccS, 0, RGB_Farbe(2)
  TranslateColor ccG, 0, RGB_Farbe(3)
  
  For I = 0 To 3
    Colo(0, I) = RGB_Farbe(I) Mod 256
    RGB_Farbe(I) = (RGB_Farbe(I) - Colo(0, I)) / 256
    Colo(1, I) = RGB_Farbe(I) Mod 256
    Colo(2, I) = (RGB_Farbe(I) - Colo(1, I)) / 256
  Next
  
  For I = 0 To UBound(MyPixelFeld, 2)
    RColor = 0
    For Ic = 0 To 2
      Colo(Ic, 4) = 0
      For I2 = 0 To 3
        Colo(Ic, 4) = Colo(Ic, 4) + Colo(Ic, I2) * MyPixelFeld(2 + I2, I) / 256
      Next
    Next
    Obj.PSet (MyPixelFeld(0, I) + X, MyPixelFeld(1, I) + Y), RGB(Colo(0, 4), Colo(1, 4), Colo(2, 4))
  Next
End Sub

Private Sub IntPixArray()
  MyPixelFeld(0, 0) = 0: MyPixelFeld(1, 0) = 0
  MyPixelFeld(2, 0) = 207: MyPixelFeld(4, 0) = 0
  MyPixelFeld(3, 0) = 0: MyPixelFeld(5, 0) = 48
  MyPixelFeld(0, 1) = 0: MyPixelFeld(1, 1) = 1
  MyPixelFeld(2, 1) = 255: MyPixelFeld(4, 1) = 0
  MyPixelFeld(3, 1) = 0: MyPixelFeld(5, 1) = 0
  MyPixelFeld(0, 2) = 0: MyPixelFeld(1, 2) = 2
  MyPixelFeld(2, 2) = 29: MyPixelFeld(4, 2) = 0
  MyPixelFeld(3, 2) = 226: MyPixelFeld(5, 2) = 0
  MyPixelFeld(0, 3) = 1: MyPixelFeld(1, 3) = 0
  MyPixelFeld(2, 3) = 165: MyPixelFeld(4, 3) = 0
  MyPixelFeld(3, 3) = 0: MyPixelFeld(5, 3) = 90
  MyPixelFeld(0, 4) = 1: MyPixelFeld(1, 4) = 1
  MyPixelFeld(2, 4) = 233: MyPixelFeld(4, 4) = 0
  MyPixelFeld(3, 4) = 0: MyPixelFeld(5, 4) = 22
  MyPixelFeld(0, 5) = 1: MyPixelFeld(1, 5) = 2
  MyPixelFeld(2, 5) = 71: MyPixelFeld(4, 5) = 0
  MyPixelFeld(3, 5) = 184: MyPixelFeld(5, 5) = 0
  MyPixelFeld(0, 6) = 1: MyPixelFeld(1, 6) = 3
  MyPixelFeld(2, 6) = 16: MyPixelFeld(4, 6) = 0
  MyPixelFeld(3, 6) = 239: MyPixelFeld(5, 6) = 0
  MyPixelFeld(0, 7) = 2: MyPixelFeld(1, 7) = 0
  MyPixelFeld(2, 7) = 102: MyPixelFeld(4, 7) = 0
  MyPixelFeld(3, 7) = 0: MyPixelFeld(5, 7) = 153
  MyPixelFeld(0, 8) = 2: MyPixelFeld(1, 8) = 1
  MyPixelFeld(2, 8) = 207: MyPixelFeld(4, 8) = 0
  MyPixelFeld(3, 8) = 0: MyPixelFeld(5, 8) = 48
  MyPixelFeld(0, 9) = 2: MyPixelFeld(1, 9) = 2
  MyPixelFeld(2, 9) = 104: MyPixelFeld(4, 9) = 0
  MyPixelFeld(3, 9) = 150: MyPixelFeld(5, 9) = 0
  MyPixelFeld(0, 10) = 2: MyPixelFeld(1, 10) = 3
  MyPixelFeld(2, 10) = 14: MyPixelFeld(4, 10) = 1
  MyPixelFeld(3, 10) = 240: MyPixelFeld(5, 10) = 0
  MyPixelFeld(0, 11) = 3: MyPixelFeld(1, 11) = 0
  MyPixelFeld(2, 11) = 51: MyPixelFeld(4, 11) = 0
  MyPixelFeld(3, 11) = 0: MyPixelFeld(5, 11) = 204
  MyPixelFeld(0, 12) = 3: MyPixelFeld(1, 12) = 1
  MyPixelFeld(2, 12) = 187: MyPixelFeld(4, 12) = 0
  MyPixelFeld(3, 12) = 0: MyPixelFeld(5, 12) = 68
  MyPixelFeld(0, 13) = 3: MyPixelFeld(1, 13) = 2
  MyPixelFeld(2, 13) = 170: MyPixelFeld(4, 13) = 1
  MyPixelFeld(3, 13) = 84: MyPixelFeld(5, 13) = 0
  MyPixelFeld(0, 14) = 3: MyPixelFeld(1, 14) = 3
  MyPixelFeld(2, 14) = 69: MyPixelFeld(4, 14) = 3
  MyPixelFeld(3, 14) = 184: MyPixelFeld(5, 14) = 0
  MyPixelFeld(0, 15) = 3: MyPixelFeld(1, 15) = 4
  MyPixelFeld(2, 15) = 53: MyPixelFeld(4, 15) = 9
  MyPixelFeld(3, 15) = 193: MyPixelFeld(5, 15) = 0
  MyPixelFeld(0, 16) = 3: MyPixelFeld(1, 16) = 5
  MyPixelFeld(2, 16) = 16: MyPixelFeld(4, 16) = 3
  MyPixelFeld(3, 16) = 237: MyPixelFeld(5, 16) = 0
  MyPixelFeld(0, 17) = 4: MyPixelFeld(1, 17) = 0
  MyPixelFeld(2, 17) = 12: MyPixelFeld(4, 17) = 0
  MyPixelFeld(3, 17) = 0: MyPixelFeld(5, 17) = 243
  MyPixelFeld(0, 18) = 4: MyPixelFeld(1, 18) = 1
  MyPixelFeld(2, 18) = 117: MyPixelFeld(4, 18) = 0
  MyPixelFeld(3, 18) = 0: MyPixelFeld(5, 18) = 138
  MyPixelFeld(0, 19) = 4: MyPixelFeld(1, 19) = 2
  MyPixelFeld(2, 19) = 159: MyPixelFeld(4, 19) = 8
  MyPixelFeld(3, 19) = 0: MyPixelFeld(5, 19) = 88
  MyPixelFeld(0, 20) = 4: MyPixelFeld(1, 20) = 3
  MyPixelFeld(2, 20) = 127: MyPixelFeld(4, 20) = 50
  MyPixelFeld(3, 20) = 78: MyPixelFeld(5, 20) = 0
  MyPixelFeld(0, 21) = 4: MyPixelFeld(1, 21) = 4
  MyPixelFeld(2, 21) = 84: MyPixelFeld(4, 21) = 48
  MyPixelFeld(3, 21) = 122: MyPixelFeld(5, 21) = 0
  MyPixelFeld(0, 22) = 4: MyPixelFeld(1, 22) = 5
  MyPixelFeld(2, 22) = 46: MyPixelFeld(4, 22) = 31
  MyPixelFeld(3, 22) = 178: MyPixelFeld(5, 22) = 0
  MyPixelFeld(0, 23) = 4: MyPixelFeld(1, 23) = 6
  MyPixelFeld(2, 23) = 31: MyPixelFeld(4, 23) = 28
  MyPixelFeld(3, 23) = 196: MyPixelFeld(5, 23) = 0
  MyPixelFeld(0, 24) = 4: MyPixelFeld(1, 24) = 7
  MyPixelFeld(2, 24) = 9: MyPixelFeld(4, 24) = 8
  MyPixelFeld(3, 24) = 239: MyPixelFeld(5, 24) = 0
  MyPixelFeld(0, 25) = 5: MyPixelFeld(1, 25) = 1
  MyPixelFeld(2, 25) = 34: MyPixelFeld(4, 25) = 0
  MyPixelFeld(3, 25) = 0: MyPixelFeld(5, 25) = 221
  MyPixelFeld(0, 26) = 5: MyPixelFeld(1, 26) = 2
  MyPixelFeld(2, 26) = 45: MyPixelFeld(4, 26) = 18
  MyPixelFeld(3, 26) = 0: MyPixelFeld(5, 26) = 192
  MyPixelFeld(0, 27) = 5: MyPixelFeld(1, 27) = 3
  MyPixelFeld(2, 27) = 84: MyPixelFeld(4, 27) = 75
  MyPixelFeld(3, 27) = 0: MyPixelFeld(5, 27) = 96
  MyPixelFeld(0, 28) = 5: MyPixelFeld(1, 28) = 4
  MyPixelFeld(2, 28) = 104: MyPixelFeld(4, 28) = 151
  MyPixelFeld(3, 28) = 0: MyPixelFeld(5, 28) = 0
  MyPixelFeld(0, 29) = 5: MyPixelFeld(1, 29) = 5
  MyPixelFeld(2, 29) = 73: MyPixelFeld(4, 29) = 182
  MyPixelFeld(3, 29) = 0: MyPixelFeld(5, 29) = 0
  MyPixelFeld(0, 30) = 5: MyPixelFeld(1, 30) = 6
  MyPixelFeld(2, 30) = 54: MyPixelFeld(4, 30) = 201
  MyPixelFeld(3, 30) = 0: MyPixelFeld(5, 30) = 0
  MyPixelFeld(0, 31) = 5: MyPixelFeld(1, 31) = 7
  MyPixelFeld(2, 31) = 40: MyPixelFeld(4, 31) = 215
  MyPixelFeld(3, 31) = 0: MyPixelFeld(5, 31) = 0
  MyPixelFeld(0, 32) = 6: MyPixelFeld(1, 32) = 2
  MyPixelFeld(2, 32) = 4: MyPixelFeld(4, 32) = 13
  MyPixelFeld(3, 32) = 0: MyPixelFeld(5, 32) = 238
  MyPixelFeld(0, 33) = 6: MyPixelFeld(1, 33) = 3
  MyPixelFeld(2, 33) = 11: MyPixelFeld(4, 33) = 34
  MyPixelFeld(3, 33) = 0: MyPixelFeld(5, 33) = 210
  MyPixelFeld(0, 34) = 6: MyPixelFeld(1, 34) = 4
  MyPixelFeld(2, 34) = 26: MyPixelFeld(4, 34) = 77
  MyPixelFeld(3, 34) = 0: MyPixelFeld(5, 34) = 153
  MyPixelFeld(0, 35) = 6: MyPixelFeld(1, 35) = 5
  MyPixelFeld(2, 35) = 37: MyPixelFeld(4, 35) = 111
  MyPixelFeld(3, 35) = 0: MyPixelFeld(5, 35) = 108
  MyPixelFeld(0, 36) = 6: MyPixelFeld(1, 36) = 6
  MyPixelFeld(2, 36) = 39: MyPixelFeld(4, 36) = 167
  MyPixelFeld(3, 36) = 0: MyPixelFeld(5, 36) = 50
  MyPixelFeld(0, 37) = 6: MyPixelFeld(1, 37) = 7
  MyPixelFeld(2, 37) = 16: MyPixelFeld(4, 37) = 239
  MyPixelFeld(3, 37) = 0: MyPixelFeld(5, 37) = 0
End Sub
