Attribute VB_Name = "Plugins"
Option Explicit

Public Type PluginType
    Path As String
    Name As String
    Whd As Long
    State As Integer
    DontRestart As Boolean
End Type

Public Type PlEventType
    Name As String
    PluginIndex As Integer
End Type

Private PListFlag As Boolean
Private EListFlag As Boolean
Private UpdateWID As Long

Private EventList() As PlEventType

Public PluginList() As PluginType

Private Sub RemPlgEvent(PlgIndex As Integer, Optional Name As String = "")
  Dim I As Integer
  Dim I2 As Integer
  If EListFlag = False Then ReDim EventList(0): EListFlag = True
  For I = 0 To UBound(EventList)
    If (LCase(EventList(I).Name) = LCase(Name) Or Len(Name) = 0) _
    And EventList(I).PluginIndex = PlgIndex Then
      EventList(I).Name = ""
    End If
    If Len(EventList(I).Name) > 0 Then I2 = I
  Next
  If I2 < UBound(EventList) Then ReDim Preserve EventList(I2)
End Sub

Private Sub AddPlgEvent(PlgIndex As Integer, Name As String)
  Dim I As Integer
  If EListFlag = False Then ReDim EventList(0): EListFlag = True
  For I = 0 To UBound(EventList)
    If Len(EventList(I).Name) = 0 Then Exit For
  Next
  If I > UBound(EventList) Then ReDim Preserve EventList(I)
  EventList(I).Name = Name
  EventList(I).PluginIndex = PlgIndex
End Sub

Public Sub SendEventToPlg(EventName As String, Paras As String)
  Dim I As Integer
  If EListFlag = False Then ReDim EventList(0): EListFlag = True
  Do Until I > UBound(EventList)
    If LCase(EventList(I).Name) = LCase(EventName) Then
      SendMSG LCase(EventName) + " " + Paras, PluginList(EventList(I).PluginIndex).Whd
    End If
    I = I + 1
  Loop
End Sub

Public Function StartPlugin(Name As String) As Integer
  Dim DAT As String
  Dim PlgName As String
  Dim I As Integer
  If Len(Name) = 0 Then Exit Function
  On Error Resume Next
  If InStr(1, Name, ":") = 2 Or Left(Name, 2) = "\\" Then
    PlgName = CutDirFromPath(CutDirFromPath(Name, "\", True), ".")
  Else
    PlgName = Name
  End If
  For I = 0 To UBound(PluginList)
    If LCase(PlgName) = LCase(PluginList(I).Name) Then
      StartPlugin = 2
      Exit Function
    End If
  Next
  If Name <> PlgName Then
    DAT = Name
  Else
    DAT = Dir(UserPath + Name + ".plg")
    If Len(DAT) = 0 Then DAT = AppPath + Dir(AppPath + Name + ".plg") Else DAT = UserPath + DAT
  End If
  If Len(Dir(DAT)) = 0 Then StartPlugin = 1: Exit Function
  Shell DAT + " " + CStr(Form1.DataPort.hwnd), vbNormalFocus
  StartPlugin = 0
  If Err <> 0 Then MsgBox "Can't load plugin:  " + Name + " (" + Err.Description + ")", vbCritical
  On Error GoTo 0
End Function

Public Function SendDataToPlugin(Name As String, sCommand As String, sText As String) As Long
  Dim I As Integer
  If Len(Name) = 0 Then Exit Function
  For I = 0 To UBound(PluginList)
    If LCase(Name) = LCase(PluginList(I).Name) Then
      SendDataToPlugin = SendMSG(sCommand + " " + sText, PluginList(I).Whd)
      Exit For
    End If
  Next
End Function

Public Function UnloadPlugin(Name As String) As Long
  Dim I As Integer
  If Len(Name) = 0 Then Exit Function
  For I = 0 To UBound(PluginList)
    If LCase(Name) = LCase(PluginList(I).Name) Then
      UnloadPlugin = SendMSG("unload", PluginList(I).Whd)
      Exit For
    End If
  Next
End Function

Public Function AddPlugin(Name As String) As Integer
  Dim I As Integer
  If PListFlag = False Then ReDim PluginList(0): PListFlag = True
  For I = 0 To UBound(PluginList)
    If LCase(Name) = LCase(PluginList(I).Name) Then
      AddPlugin = -1
      Exit Function
    End If
  Next
  If I > UBound(PluginList) Then
    For I = 0 To UBound(PluginList)
      If "" = PluginList(I).Name Then Exit For
    Next
    If I > UBound(PluginList) Then
      ReDim Preserve PluginList(I)
    End If
    PluginList(I).Name = Name
  End If
  AddPlugin = I
End Function

Public Sub ShowUpdater()
  Dim DAT As String
  DAT = AppPath + "AutoUpdate\Update.exe"
  If Dir(DAT) = "" Then DAT = AppPath + "Update.exe"
  If UpdateWID <> 0 Then
    If SendMSG("show", UpdateWID) = 0 Then UpdateWID = 0: ShowUpdater
  Else
    ShellExecute 0&, vbNullString, DAT, CStr(Form1.DataPort.hwnd), vbNullString, vbNormalFocus
  End If
End Sub

Private Function SendMSG(Text As String, WindowID As Long) As Long
  Dim Erg As Long
  Dim I As Integer
  Erg = SendMessage(WindowID, WM_SETTEXT, -1, Text)
  If Erg = 0 Then
    For I = 0 To UBound(PluginList)
      If WindowID = PluginList(I).Whd Then
        RemPlgEvent I
        PluginList(I).Name = ""
        Exit Function
      End If
    Next
  End If
  SendMSG = Erg
End Function

Public Sub LoadAllPlugins(Data As String)
  Dim I As Integer
  Dim I2 As Integer
  Do
    I2 = I
    I = InStr(I + 1, Data, ">")
    If I = 0 Then I = Len(Data) + 1
    StartPlugin Trim(Mid(Data, I2 + 1, I - I2 - 1))
  Loop Until I = Len(Data) + 1
End Sub

Public Function UnloadPlugins(Optional OnlyList As Boolean) As String
  Dim I As Integer
  Dim DAT As String
  DAT = "LoadedPlugins ="
  For I = 0 To UBound(PluginList)
    If Len(PluginList(I).Name) > 0 Then
      If PluginList(I).DontRestart = False Then
        DAT = DAT + " >" + PluginList(I).Name
      End If
      If OnlyList = False Then SendMSG "unload", PluginList(I).Whd
    End If
  Next
  UnloadPlugins = DAT
End Function

Public Sub ShowPluginValues(Name As String)
  Dim I As Integer
  If Len(Name) = 0 Then Exit Sub
  For I = 0 To UBound(PluginList)
    If LCase(Name) = LCase(PluginList(I).Name) Then
      SendMSG "show", PluginList(I).Whd
      Exit For
    End If
  Next
End Sub

Public Sub PluginEvent(plFrom As String, plComm As String, plData As String)
  Dim I As Integer
  Dim DAT As String
  Dim DAT2 As String
  
  If plFrom = "updateapp" Then
    If plComm = "inst" Then If Len(plData) < 13 Then UpdateWID = Val(plData)
    If plComm = "show" Then Form1.ShowMainWindow
    If plComm = "unload" Then UpdateWID = 0
    If plComm = "nettalkunload" Then Unload Form1
    Exit Sub
  ElseIf plFrom = "secnettalkapp" Then
    If plComm = "show" Then
      LastCommandLine = plData
      If PhraseCLine = False Then Form1.ShowMainWindow
    End If
    Exit Sub
  Else
    Select Case LCase(plComm)
      Case "inst"
        I = InStr(1, plData, " ")
        If I > 0 And I < Len(plData) Then
          DAT = Mid(plData, I + 1)
          DAT2 = Left(plData, I - 1)
        Else
          DAT2 = plData
        End If
        If Len(DAT2) < 13 Then
          I = AddPlugin(plFrom)
          If I > -1 Then
            PluginList(I).Whd = Val(DAT2)
            PluginList(I).State = 1
            PluginList(I).DontRestart = (Val(DAT2) > 0)
            ntScript_Call "PluginEvent", plFrom, "1", "", , , , , True
          Else
            ntScript_Call "PluginEvent", plFrom, "0", "", , , , , True
          End If
          If OpFormIsLoaded Then Form5.RefPluginList
        End If
      Case "unload"
        ntScript_Call "PluginEvent", plFrom, "2", "", , , , , True
        For I = 0 To UBound(PluginList)
          If LCase(plFrom) = LCase(PluginList(I).Name) Then
            PluginList(I).Name = ""
            RemPlgEvent I
            Exit For
          End If
        Next
        If OpFormIsLoaded Then Form5.RefPluginList
      Case "addevent"
        For I = 0 To UBound(PluginList)
          If LCase(plFrom) = LCase(PluginList(I).Name) Then
            AddPlgEvent I, plData
            Exit For
          End If
        Next
      Case "remevent"
        For I = 0 To UBound(PluginList)
          If LCase(plFrom) = LCase(PluginList(I).Name) Then
            RemPlgEvent I, plData
            Exit For
          End If
        Next
      Case "request"
         DAT = Evaluate(plData, DAT2)
         If Len(DAT2) Then
           SendDataToPlugin plFrom, "returnerror", DAT2
         Else
           SendDataToPlugin plFrom, "return", DAT
         End If
      Case "sendcom"
        I = InStr(1, plData, " ")
        If I > 0 And I < Len(plData) Then
          DAT = Mid(plData, I + 1)
          I = Val(Left(plData, I - 1))
          If Not FrameByID(I) Is Nothing Then FrameByID(I).SendM DAT, True
        End If
      Case "send"
        ntScript_Call "PluginEvent", plFrom, "3", plData, , , , , True
      Case "errordone"
        ntScriptErrorDone = True
      Case "scriptstart"
        ntScript_Reset
      Case "scriptstop"
        ntScript_Unload
      Case "getscripttext"
        SendDataToPlugin plFrom, "scripttext", ntScript
      Case "setscripttext"
        ntScript_Unload
        ntScript = plData
    End Select
  End If
End Sub
