VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DCCConnection"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Mode As Integer ' 0=Server, 1=Client
Public GetMode As Integer 'Ohne <*>
Public Buffer As String
Public Server As String
Public Port As Long
Public User As String
Public State As Integer
Public Nick As String
Public TrState As Integer
Public ConnID As Integer

Private TimePoint As Long
Private LastNL As Boolean
Private ProxyState As Integer
Private GetChatF As ChatFrame

Private WithEvents wins As CSocket
Attribute wins.VB_VarHelpID = -1

Public Sub SetCaption(Caption As String)
  If Not GetChatF Is Nothing Then GetChatF.Caption = Caption
  Form1.ColumRedraw
  Form1.pStateIntRefresh
  DrawTitel
  If Not GetChatF Is Nothing Then GetChatF.TextF.RefText
End Sub

Public Sub Connect()
  If State > 1 Or ntScript_Call("Serv_Connect", CStr(ConnID)) = False Then Exit Sub
  State = 3
  TrState = 1
  LastNL = True
  If Not GetChatF Is Nothing Then GetChatF.Caption = Me.User
  Form1.ColumRedraw
  If Not GetChatF Is Nothing Then GetChatF.PrintText "~ " + TeVal.ConStart, CoVal.ClientMSG, , True
  Form1.ColumRedraw
  If SockProxyPortGlob > 0 Then
    wins.Connect SockProxyServerGlob, SockProxyPortGlob
    ProxyState = 1
  Else
    wins.Connect Server, Port
    ProxyState = 0
  End If
  TimePoint = AltTimer + 10
  StartTimer Me, 500, 1
  If FrontCFrame Is GetChatF Then
    Form1.pStateIntRefresh
    DrawTitel
  End If
End Sub

Public Sub OpenPort()
  State = 3
  TrState = 1
  If Not GetChatF Is Nothing Then GetChatF.Caption = Me.User
  Form1.ColumRedraw
  If Not GetChatF Is Nothing Then GetChatF.PrintText "~ " + TeVal.ConStart, CoVal.ClientMSG, , True
  Form1.ColumRedraw
  If StaticDCCPort > 0 Then
    wins.LocalPort = StaticDCCPort
  Else
    StaticDCCPort = 0
  End If
  wins.Listen
  If wins.State = 1 Then wins.LocalPort = 0: wins.Listen
  Port = GetLocalPort(wins.SocketHandle)
End Sub

Public Function GetSeocket() As CSocket
  If wins Is Nothing Then Exit Function
  Set GetSeocket = wins
End Function

Public Sub Accept(ServerCSock As CSocket)
  Dim I As Integer
  
  wins.Accept ServerCSock
  If Not GetChatF Is Nothing Then GetChatF.Caption = wins.RemoteHost
  Server = wins.RemoteHost
  If Not GetChatF Is Nothing Then GetChatF.PrintText "~ " + TeVal.ConStart + "  " + TeVal.ConOK, CoVal.ClientMSG
  State = 4
  TrState = 1
  If FrontCFrame Is GetChatF Then
    Form1.pStateIntRefresh
    DrawTitel
  End If
  Form1.ColumRedraw
End Sub

Private Sub Class_Initialize()
  Set wins = New CSocket
End Sub

Public Sub OpenWindow()
  Set GetChatF = New ChatFrame
  GetChatF.Caption = "Open..."
  GetChatF.Inst cfStatus, 2
  Set GetChatF.DCCConn = Me
  ChatFrames.Add GetChatF
  SetFrontFrame GetChatF
End Sub

Public Sub TimerEvent(TimerID As Integer)
  If wins Is Nothing Then Exit Sub
  If TimerID = 1 Then
    If wins.State <> 3 Or AltTimeDiff(TimePoint) > 0 Then
      If Not GetChatF Is Nothing Then GetChatF.PrintText "  " + TeVal.ConErr, CoVal.ErrorMSG, , False
    Else
      StartTimer Me, 500, 1
    End If
  End If
End Sub

Private Sub Class_Terminate()
  If State > 1 Then CloseConnection
  KillTimerByID 1, Me
  KillTimerByID 2, Me
End Sub

Private Sub wins_Closed()
  Dim I As Integer
  
  CloseConnection
  If GetMode > 1 Then
    For I = 1 To DCCconns.Count
      If DCCconns(I) Is Me Then
        DCCconns.Remove I
        Exit For
      End If
    Next
  End If
End Sub

Public Sub CloseConnection()
  Dim I As Integer
  
  If wins Is Nothing Then Exit Sub
  KillTimerByID 1, Me
  If wins.State > 1 Then wins.Disconnect
  For I = 1 To ChatFrames.Count
    If ChatFrames(I).DCCConn Is Me Then
      GetChatF.PrintText "~ " + TeVal.ConClose, CoVal.ClientMSG, , True
      SetAl GetChatF, 4, User, TeVal.DccConnCancel
    End If
  Next
  State = 1
  If Not AppUnloading Then
    If FrontCFrame.DCCConn Is Me Then Form1.pStateIntRefresh
    Form1.ColumRedraw
  End If
  ntScript_Call "Serv_Closed", CStr(ConnID)
End Sub

Public Sub Quit()
  Dim I As Integer
  KillTimerByID 2, Me
  If wins.State > 1 Then wins.Disconnect
  If Not GetChatF Is Nothing Then GetChatF.QuitFrame
  For I = DCCconns.Count To 1 Step -1
    If DCCconns(I) Is Me Then
      DCCconns.Remove I
    End If
  Next
End Sub

Public Sub SendLine(Data As String, Optional NoLineBreak As Boolean)
  If wins Is Nothing Then Exit Sub
  If NoLineBreak Then
    wins.SendData Data
    SendedBytes = SendedBytes + Len(Data)
  Else
    wins.SendData Data + Chr(13) + Chr(10)
    SendedBytes = SendedBytes + Len(Data) + 2
  End If
  If (FrontCFrame.FrameType = cfServerList Or FrontCFrame.FrameType = cfScript Or FrontCFrame.FrameType = cfDCC) And _
  Form1.Visible = True And Form1.WindowState <> 1 Then Form1.pStateRefresh
End Sub

Private Sub PraseCommand(Data As String)
  Dim ChatFrs As ChatFrame
  
  If GetMode > 1 Then
    ntScript_Call "RecvDCCMsg", Data, User, "0", CStr(ConnID)
  Else
    For Each ChatFrs In ChatFrames
      If ChatFrs.DCCConn Is Me Then
        If ntScript_Call("RecvDCCMsg", Data, User, CStr(ChatFrs.FrameID), CStr(ConnID)) Then
          DrawSLine ChatFrs, 0, "<" + User + ">", CoVal.NormalVon, Data
          SetAl ChatFrs, 4, User, Data
        End If
        Exit For
      End If
    Next
  End If
End Sub

Private Sub PrasePaket(Data As String, NoNewLine As Boolean)
  Dim ChatFrs As ChatFrame
      
  If GetMode > 1 Then
    ntScript_Call "RecvDCCMsg", Data + IIf(NoNewLine, "", vbNewLine), User, "0", CStr(ConnID)
  Else
    For Each ChatFrs In ChatFrames
      If ChatFrs.DCCConn Is Me Then
        If ntScript_Call("RecvDCCMsg", Data, User, CStr(ChatFrs.FrameID), CStr(ConnID)) Then
          ChatFrs.PrintText Data, CoVal.NormalText, True, LastNL
        End If
      End If
    Next
  End If
  LastNL = NoNewLine = False
End Sub

Private Sub wins_Connected()
  Dim I As Integer
  
  If SockProxyPortGlob > 0 Then SendProxyLogin wins, Server, "", Port, SockProxyTypeGlob
  KillTimerByID 1, Me
  If Not GetChatF Is Nothing Then GetChatF.PrintText "  " + TeVal.ConOK, CoVal.ClientMSG, , False
  State = 4
  TrState = 1
  If FrontCFrame Is GetChatF Then
    Form1.pStateIntRefresh
    DrawTitel
  End If
  Form1.ColumRedraw
  ntScript_Call "Serv_Connected", CStr(ConnID)
End Sub

Private Sub wins_ConnectionRequest()
  wins.Accept wins
  Server = wins.RemoteHost
  If Not GetChatF Is Nothing Then GetChatF.PrintText "  " + TeVal.ConOK, CoVal.ClientMSG, , False
  State = 4
  ProxyState = 0
  TrState = 1
  Form1.ColumRedraw
  If FrontCFrame Is GetChatF Then
    Form1.pStateIntRefresh
    DrawTitel
  End If
End Sub

Private Sub WinS_DataArrival()
  Dim iStart As Long
  Dim iLen As Long
  Dim BinData As String
    
  Do
    iStart = wins.Recive(BinData)
    If iStart = 0 Then
      Exit Do
    Else
      RecevedBytes = RecevedBytes + iStart
      Buffer = Buffer + BinData
      If ProxyState = 1 Then
        Buffer = Mid(Buffer, 9)
        ProxyState = 2
      End If
      iStart = 0
      Do
        iLen = iStart
        If iLen = 0 Then iLen = -1
        iStart = InStr(iStart + 2, Buffer, Chr(10))
        If Left(Buffer, 1) = Chr(10) And iLen = -1 Then iLen = 0
        If GetMode = 1 Or GetMode = 3 Then
          If iStart > 0 Then
            If Mid(Buffer, iStart - 1, 1) = Chr(13) Then
              PrasePaket Mid(Buffer, iLen + 2, iStart - iLen - 3), False
              iStart = iStart - 1
            Else
              PrasePaket Mid(Buffer, iLen + 2, iStart - iLen - 2), False
              iStart = iStart - 1
            End If
            If Len(Buffer) < iStart - 1 Then Exit Do
          Else
            If Len(Buffer) - iLen - 1 > 0 Then
              If Right(Buffer, 1) = Chr(13) Then
                PrasePaket Mid(Buffer, iLen + 2, Len(Buffer) - iLen - 2), False
              Else
                PrasePaket Right(Buffer, Len(Buffer) - iLen - 1), True
              End If
            End If
            Buffer = ""
            Exit Do
          End If
        Else
          If iStart > 0 Then
            If Mid(Buffer, iStart - 1, 1) = Chr(13) Then
              PraseCommand Mid(Buffer, iLen + 2, iStart - iLen - 3)
              iStart = iStart - 1
            Else
              PraseCommand Mid(Buffer, iLen + 2, iStart - iLen - 2)
              iStart = iStart - 1
            End If
            If Len(Buffer) < iStart - 1 Then Exit Do
          Else
            Buffer = Right(Buffer, Len(Buffer) - iLen - 1)
            Exit Do
          End If
        End If
      Loop
      If FrontCFrame.FrameType = cfServerList And Form1.Visible = True Then Form1.pStateRefresh
    End If
  Loop
  If FrontCFrame.TCHange And FrontCFrame.DCCConn Is Me Then
    FrontCFrame.TextF.Refresh
    FrontCFrame.TCHange = False
  End If
End Sub

Private Sub wins_Error(Error As Long, Description As String)
  Dim I As Integer
  For I = 1 To ChatFrames.Count
    If ChatFrames(I).DCCConn Is Me Then
      If State = 3 Then
        ChatFrames(I).PrintText "  " + TeVal.ConErr, CoVal.ErrorMSG, , False
      Else
        ChatFrames(I).PrintText "- " + TeVal.ConErr, CoVal.ErrorMSG, , True
      End If
    End If
  Next
  CloseConnection
End Sub
