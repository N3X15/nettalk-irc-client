VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ChatFrame"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public WithEvents TextF As AdvancedText
Attribute TextF.VB_VarHelpID = -1
Public WithEvents UListF As IconList
Attribute UListF.VB_VarHelpID = -1

Public Caption As String
Public Changed As Integer
Public IrcConn As IRCConnection
Public DCCConn As DCCConnection
Public ConnTyp As Integer '-1 -> Keine, 0 -> IRC, 1 -> Nettalk, 2 -> DCC
Public FrameType As Integer '0 -> Room, 1 -> Server, 2 -> Rooms-Liste, 3 -> Server-Liste, 4 -> Privat, 5 -> Script, 6 -> Filetranz, 7 -> LogView, 8 -> Debug
Public Wisp As Boolean
Public NoBeepTime As Long
Public LastWisperer As String
Public ntBlink As Long
Public ChanTopic As String
Public BannList As String
Public BannListState As Integer
Public ModeRequesState As Boolean
Public ChanValForm As Boolean
Public TCHange As Boolean
Public InChannel As Boolean
Public FrameID As Long
Public TempWisperTo As String
Public UserListRef As Single
Public AwayMarker As Boolean

Private DbClick As String

Public Sub Inst(Optional FrType As Integer = 0, Optional ConnectionType As Integer)
  ConnTyp = ConnectionType
  FrameType = FrType
  Select Case FrameType
    Case cfServerList, cfDCC, cfFriends
      Set UListF = New IconList
      UListF.Inst Form1.pUList, Form1.UlistScr, 3, 16, False, ConnTyp
    Case cfRoomList
      Set UListF = New IconList
      UListF.Inst Form1.pUList, Form1.UlistScr, 3, 0, False, ConnTyp
    Case cfRoom
      Set UListF = New IconList
      Set TextF = New AdvancedText
      UListF.Inst Form1.pUList, Form1.UlistScr, 3, 7, True, ConnTyp
      TextF.Inst Form1.pTBox, Form1.VScroll1, 16536
      TextF.SetShowTime ShowTimeStampText
    Case cfStatus, cfDebug, cfPrivate, cfWindow
      Set TextF = New AdvancedText
      TextF.Inst Form1.pTBox, Form1.VScroll1, 4096
      TextF.SetShowTime ShowTimeStampText And FrameType <> cfDebug And FrameType <> cfWindow, False, FrameType = cfDebug Or FrameType = cfWindow
    Case cfScript
    Case cfLog
      Set TextF = New AdvancedText
      TextF.Inst Form1.pTBox, Form1.VScroll1, 4096
      TextF.SetShowTime False, True
    Case Else
      Set TextF = New AdvancedText
      TextF.Inst Form1.pTBox, Form1.VScroll1, 4096
  End Select
  NoBeepTime = AltTimer
End Sub

Public Sub AnswerDCCRequest(User As String, FileName As String, Port As Long, Position As Double)
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim hIndex As Integer
  
  For I = 1 To DCCFileConns.Count
    If DCCFileConns(I).SendGetMode = 0 Then
      I2 = 0
      If DCCFileConns(I).ByteCount = Position Then I2 = I2 + 1
      If LCase(DCCFileConns(I).SenderFN) = LCase(FileName) Then I2 = I2 + 2
      If DCCFileConns(I).LocalPort = Port And DCCFileConns(I).State = 2 Then I2 = I2 + 5
      If I2 > I3 Then I3 = I2: hIndex = I
    End If
  Next
  If I3 < 2 Then Exit Sub
  If DCCFileConns(hIndex).State <> 4 Then
    If DCCFileConns(hIndex).State = 1 Then DCCFileConns(hIndex).OpenPort
    DCCFileConns(hIndex).tByteCount = Position
    If ConnTyp = 0 Then
      IrcConn.SendLine "PRIVMSG " + User + " :" _
      + Chr(1) + "DCC ACCEPT " + FileName + " " + CStr(Port) + " " + Format(Position, "0") + Chr(1)
    End If
  End If
End Sub

Public Function RequestDCCFile(User As String, FileName As String, PeerIP As String, Port As Long, FileSize As Currency) As Integer
  Dim hIndex As Integer
  
  If DccConnExisting(User, FileName, PeerIP, Port, FileSize, IrcConn.Server, Caption, hIndex) > 13 Then
    If DCCFileConns(hIndex).PeerPort = 0 Then
      RequestDCCFile = hIndex
    Else
      DCCFileConns(hIndex).PeerIP = PeerIP
      DCCFileConns(hIndex).User = User
      DCCFileConns(hIndex).PeerPort = Port
      DCCFileConns(hIndex).FileSize = FileSize
      If DCCFileConns(hIndex).ByteCount = 0 Then
        DCCFileConns(hIndex).Connect
      Else
        IrcConn.SendLine "PRIVMSG " + User + " :" _
        + Chr(1) + "DCC RESUME " + FileName + " " + CStr(Port) + " " + Format(DCCFileConns(hIndex).ByteCount, "0") + Chr(1)
      End If
      ShowDCCFrame True
      RequestDCCFile = -1
    End If
  End If
End Function

Public Sub ResendDCCFile(User As String, FileName As String, IPAdrr As Double, Port As Long, FileSize As Currency)
  If ConnTyp = 0 Then
    IPAdrr = GetLocalDCCIP(ConvIPtoDouble(GetLocalIP(IrcConn.GetSeocket.SocketHandle)), _
    IrcConn.GetSeocket.LocalRooterIP, ConvIPtoDouble(GetPeerHostByAddr(IrcConn.GetSeocket.SocketHandle), True))
    
    IrcConn.SendLine "PRIVMSG " + User + " :" _
    + Chr(1) + "DCC SEND " + FileName + " " + CStr(IPAdrr) + " " + CStr(Port) + " " + Format(FileSize, "0") + Chr(1)
  End If
End Sub

Public Function SendM(Data As String, Optional NoScriptEvent As Boolean) As Boolean
  Dim I As Long
  Dim I2 As Long
  Dim I3 As Long
  Dim retVal As Boolean
  Dim fColor As String
  Dim bColor As String
  Dim DAT As String
  Dim DAT2 As String
  Dim OfPos As Long
  Dim NewOfPos As Long
  
  For I = Len(Data) To 1 Step -1
    If Mid(Data, I, 1) <> " " And I2 = 0 Then I2 = I
    If I > 1 Then
      If Mid(Data, I, 1) = Chr(10) And Mid(Data, I - 1, 1) <> Chr(13) Then Data = Left(Data, I - 1) + Chr(13) + Mid(Data, I)
    End If
    If I < Len(Data) Then
      If Mid(Data, I, 1) = Chr(13) And Mid(Data, I + 1, 1) <> Chr(10) Then Data = Left(Data, I) + Chr(10) + Mid(Data, I + 1)
    End If
  Next
  Data = Left(Data, I2)
  If Left(Data, 1) = "?" And Len(Data) > 1 Then
    If Left(Data, 2) = "??" Then
      If Right(Data, 1) <> "?" Then Data = Mid(Data, 2)
    Else
      If Mid(Data, 2, 1) = "@" Then
        Data = CStr(Evaluate(Mid(Data, 3), DAT)) + " = " + Mid(Data, 3)
      ElseIf Mid(Data, 2, 1) = "#" Then
        Data = CStr(Evaluate(Mid(Data, 3), DAT))
      Else
        If TextF Is Nothing Then
          DAT2 = Evaluate(Mid(Data, 2), DAT)
          If Len(DAT) Then
            Form1.Text1.Text = Form1.Text1.Text + vbNewLine + DAT
            Form1.Text1.SelLength = Len(Form1.Text1.Text)
          Else
            Form1.Text1.Text = Form1.Text1.Text + vbNewLine + DAT2
            Form1.Text1.SelLength = Len(Form1.Text1.Text)
          End If
        Else
          DrawSLine Me, 1, "- Calc: ", CoVal.ClientMSG, CStr(Evaluate(Mid(Data, 2), DAT)) + " = " + Mid(Data, 2)
          If Len(DAT) Then
            DrawSLine Me, 1, "- Calc: ", CoVal.ClientMSG, DAT
            DAT = ""
          End If
          SendM = True
          If TCHange And FrontCFrame Is Me Then
            FrontCFrame.TextF.Refresh
            TCHange = False
          End If
        End If
        Exit Function
      End If
    End If
  End If
  
  I = 0
  Do
    I = InStr(I + 1, Data, "<$")
    I3 = InStr(I + 1, Data, ",")
    I2 = InStr(I + 1, Data, ">")
    If I > 0 And I2 > 0 Then
      If I2 = Len(Data) Then Exit Function
      If I3 = 0 Or I3 > I2 Then
        fColor = Mid(Data, I + 2, I2 - I - 2)
        If Not IsNumeric(fColor) Then fColor = Format(GetIRCColorNumber(fColor), "00")
        If Val(fColor) > -1 Then
          Data = Left(Data, I - 1) + Chr(3) + fColor + Mid(Data, I2 + 1)
        Else
          Data = Left(Data, I - 1) + Mid(Data, I2 + 1)
        End If
      Else
        fColor = Mid(Data, I + 2, I3 - I - 2)
        If Not IsNumeric(fColor) Then fColor = Format(GetIRCColorNumber(fColor), "00")
        bColor = Mid(Data, I3 + 1, I2 - I3 - 1)
        If Not IsNumeric(bColor) Then bColor = Format(GetIRCColorNumber(bColor), "00")
        If Val(fColor) > -1 And Val(bColor) > -1 Then
          Data = Left(Data, I - 1) + Chr(3) + fColor + "," + bColor + Mid(Data, I2 + 1)
        Else
          Data = Left(DAT, I - 1) + Mid(Data, I2 + 1)
        End If
      End If
    End If
  Loop Until I = 0
  
  I = 0
  I2 = 0
  Do
    I3 = InStr(I + 1, Data, vbNewLine)
    I = InStr(I + 1, Data, vbTab)
    If I3 <> 0 And I3 < I Then
      I2 = I3 + 1
    End If
    If I = 0 Then Exit Do
    Data = Left(Data, I - 1) + Space(8 - ((I - I2 - 1) Mod 8)) + Mid(Data, I + 1)
  Loop
  
  If ConnTyp = 0 Then ' #IRC
    I = -1
    I3 = 2
    Do
      I2 = I + I3
      I = InStr(I2, Data, Chr(13) + Chr(10))
      If I = 0 Then I = Len(Data) + 1
      If I > I2 Then
        If I - I2 > 1024 Then
          I = I2 + 1024
          I3 = 0
        Else
          I3 = 2
        End If
        DAT = Mid(Data, I2, I - I2)
        If Left(DAT, 1) = "/" Then
          SendM = IrcConn.SendComm(Right(DAT, Len(DAT) - 1), Me)
        Else
          If Me.FrameType = cfRoom Or Me.FrameType = cfPrivate Then
            If IrcConn.State = 4 Then
              If WisperTo = "" Or Form1.Check1.Value = 0 Then
                If NoScriptEvent Then
                  retVal = True
                ElseIf Me.FrameType = cfRoom Then
                  retVal = ntScript_Call("SendMsg", DAT, Me.Caption, CStr(FrameID), CStr(IrcConn.ConnID))
                Else
                  retVal = ntScript_Call("SendQuery", DAT, Me.Caption, CStr(FrameID), CStr(IrcConn.ConnID))
                End If
                If retVal Then
                  
                  OfPos = 1
                  Do
                    If Len(DAT) - OfPos + 1 > LINE_MAX_LEN Then
                      NewOfPos = InStr(OfPos + LINE_MAX_LEN - 48, DAT, " ")
                      If NewOfPos > OfPos + LINE_MAX_LEN Or NewOfPos = 0 Then NewOfPos = OfPos + LINE_MAX_LEN
                    Else
                      NewOfPos = Len(DAT) + 1
                    End If
                    IrcConn.AddStringToSendBuff "PRIVMSG " + Me.Caption + " :", IrcConn.GetCharSet(False)
                    IrcConn.AddStringToSendBuff EncodeText(Mid(DAT, OfPos, NewOfPos - OfPos), IrcConn.CTCAktiv, IrcConn.CTCKey), IrcConn.GetCharSet(True)
                    IrcConn.AddBrToSendBuff
                    IrcConn.SendFromBuff
                    
                    DrawSLine Me, 0, "<" + IrcConn.Nick + ">", CoVal.OwnNick, Mid(DAT, OfPos, NewOfPos - OfPos), 3, , , IrcConn.CTCAktiv
                    WriteLog Format(Now, "dd.mm.yyyy hh:mm:ss") + " <" + IrcConn.Nick + "> " + DAT, Me
                    OfPos = NewOfPos
                  Loop Until OfPos > Len(DAT)
                End If
              Else
                If NoScriptEvent Then
                  retVal = True
                Else
                  retVal = ntScript_Call("SendNotice", DAT, Me.Caption, CStr(FrameID), CStr(IrcConn.ConnID))
                End If
                If retVal Then
                
                  OfPos = 1
                  Do
                    If Len(DAT) - OfPos + 1 > LINE_MAX_LEN Then
                      NewOfPos = InStr(OfPos + LINE_MAX_LEN - 48, DAT, " ")
                      If NewOfPos > OfPos + LINE_MAX_LEN Or NewOfPos = 0 Then NewOfPos = OfPos + LINE_MAX_LEN
                    Else
                      NewOfPos = Len(DAT) + 1
                    End If
                  
                    IrcConn.AddStringToSendBuff "NOTICE " + WisperTo + " :", IrcConn.GetCharSet(False)
                    IrcConn.AddStringToSendBuff EncodeText(Mid(DAT, OfPos, NewOfPos - OfPos), IrcConn.CTCAktiv, IrcConn.CTCKey), IrcConn.GetCharSet(True)
                    IrcConn.AddBrToSendBuff
                    IrcConn.SendFromBuff
                    
                    DrawSLine Me, 0, "<" + IrcConn.Nick + " " + TeVal.WispersTo + " " + WisperTo + ">", CoVal.FlusterVon, DAT, 3, , , IrcConn.CTCAktiv
                    WriteLog Format(Now, "dd.mm.yyyy hh:mm:ss") + " <" + IrcConn.Nick + "> " + Mid(Mid(DAT, OfPos, NewOfPos - OfPos), OfPos, NewOfPos - OfPos), Me, WisperTo
                    
                    OfPos = NewOfPos
                  Loop Until OfPos > Len(DAT)
                  
                End If
              End If
              SendM = True
            End If
          Else
            SendM = IrcConn.SendComm(DAT, Me)
          End If
        End If
      End If
    Loop Until I > Len(Data) Or I > 5000
  ElseIf ConnTyp = 2 Then ' #DCC
    I = -1
    I3 = 2
    If DCCConn.GetMode = 1 And Len(Data) = 0 Then DCCConn.SendLine vbNewLine
    Do
      I2 = I + I3
      I = InStr(I2, Data, Chr(13) + Chr(10))
      If I = 0 Then I = Len(Data) + 1
      If I > I2 Then
        If I - I2 > 1024 Then
          I = I2 + 1024
          I3 = 0
        Else
          I3 = 2
        End If
        DAT = Mid(Data, I2, I - I2)
        If Left(DAT, 1) = "/" Then
          SendM = FSendComm(Right(DAT, Len(DAT) - 1), Me)
        Else
          If DCCConn.State = 4 Then
            If NoScriptEvent Then
              retVal = True
            Else
              retVal = ntScript_Call("SendDCCMsg", DAT, Me.Caption, CStr(FrameID), CStr(DCCConn.ConnID))
            End If
            If retVal Then
              DCCConn.SendLine DAT
              DrawSLine Me, 0, "<" + DCCConn.Nick + ">", CoVal.NormalVon, DAT
              'WriteLog Format(Now, "dd.mm.yyyy hh:mm:ss") + " <" + DCCConn.Nick + "> " + DAT, Me
            End If
            SendM = True
          End If
        End If
      End If
    Loop Until I > Len(Data) Or I > 5000
  Else
    I = -1
    I3 = 2
    Do
      I2 = I + I3
      I = InStr(I2, Data, Chr(13) + Chr(10))
      If I = 0 Then I = Len(Data) + 1
      If I > I2 Then
        If I - I2 > 1024 Then
          I = I2 + 1024
          I3 = 0
        Else
          I3 = 2
        End If
        DAT = Mid(Data, I2, I - I2)
        If Left(DAT, 1) = "/" Then
          SendM = FSendComm(Right(DAT, Len(DAT) - 1), Me)
        Else
          SendM = FSendComm(DAT, Me)
        End If
      End If
    Loop Until I > Len(Data) Or I > 5000
  End If
  If TCHange And FrontCFrame Is Me Then
    FrontCFrame.TextF.Refresh
    TCHange = False
  End If
End Function

Public Function FSendComm(Data As String, ChatFrs As ChatFrame) As Boolean
  Dim sDAT(0 To 9) As String
  Dim I As Long
  Dim I2 As Integer
  Dim Iind As Integer
  Dim OutStr As String
  Dim OutStr2 As String
  Dim LIndex As Integer
  Dim Sendable As Boolean
  If Left(Data, 1) = "/" Then Data = Mid(Data, 2)
  
  If ntScript_Call("SendCommand", Data, CStr(ChatFrs.FrameID)) = False Then
    FSendComm = True
    Exit Function
  End If
  
  Do
    OutStr = CharReplace(Data, ChatFrs, , , LIndex)
    If Len(OutStr) Then
      I = 0
      Iind = 0
      Do
        I2 = I
        I = InStr(I + 1, OutStr, " ")
        If I = 0 Or Iind > 8 Then Exit Do
        sDAT(Iind) = Mid(OutStr, I2 + 1, I - I2 - 1)
        Iind = Iind + 1
      Loop
      sDAT(Iind) = Mid(OutStr, I2 + 1)
      If CCommands(ChatFrs, sDAT, OutStr, OutStr2) Then
        Sendable = True
      Else
        OutStr2 = OutStr
      End If
    End If
    LIndex = LIndex + 1
  Loop While LIndex
  FSendComm = Sendable
End Function

Public Sub PrintText(Data As String, ByVal FontColor As Long, Optional NoRefr As Boolean, Optional NewLine As Boolean = True, Optional Trigger As Long, Optional Uline As Boolean, Optional TriggerStr As String, Optional NoRT As Boolean)
  Dim I As Long
  Dim I2 As Long
  Dim DAT As String
  Dim sPara As Integer
  Dim sAltPara As Integer
  Dim sColor As Integer
  Dim sAltColor As Integer
  Dim sBackColor As Integer
  Dim sAltBackcolor As Integer
  Dim sAltTrigger As Long
  Dim sAltUline As String
  Dim CCheck As Integer
  Dim Bdat As Long
  Dim sUline As Integer
  Dim sTrigger As Long
   
  If TextF Is Nothing Or Len(Data) > 1000000 Then Exit Sub
  sAltColor = -1
  sAltBackcolor = -1
  sAltPara = -1
  sAltUline = -1
  sColor = -1
  sBackColor = -1
  sPara = -1
  sUline = -1
  I = 1
  For I = 1 To Len(Data)
    Bdat = AscW(Mid(Data, I, 1))
    If sTrigger <> 0 Then
      If Len(Data) > I Then
        I2 = 0
        Select Case AscB(Mid(Data, I + 1, 1))
          Case 0 To 32
            I2 = 1
          Case 33, 34, 39 To 42, 44, 59, 60, 62, 91, 93, 34, 123, 124, 125
            If I + 1 = Len(Data) Then
              I2 = 1
            Else
              If AscB(Mid(Data, I + 2, 1)) = 32 Then I2 = 1
            End If
        End Select
        If I2 = 1 Then
          If sTrigger = -2 Or sTrigger = -1 Or sTrigger = -6 Then sAltTrigger = sTrigger * -1
          sTrigger = 0
          sUline = 0
        Else
          DAT = DAT + ChrW(Bdat)
        End If
      Else
        If sTrigger = -2 Or sTrigger = -1 Or sTrigger = -6 Then sAltTrigger = sTrigger * -1
        sTrigger = 0
        sUline = 0
      End If
    End If
    If Bdat > 31 Or Bdat = 13 Or Bdat = 10 Or Bdat < 0 Then
      If sTrigger <> 0 Then
      ElseIf Trigger <> 0 Then
        DAT = DAT + ChrW(Bdat)
      ElseIf UCase(Mid(Data, I, 2)) = "\\" Then
        sTrigger = -1
        sUline = 1
        I = I - 1
      ElseIf UCase(Mid(Data, I, 7)) = "HTTP://" Then
        sTrigger = -1
        sUline = 1
        I = I - 1
      ElseIf UCase(Mid(Data, I, 8)) = "HTTPS://" Then
        sTrigger = -1
        sUline = 1
        I = I - 1
      ElseIf UCase(Mid(Data, I, 6)) = "FTP://" Then
        sTrigger = -1
        sUline = 1
        I = I - 1
      ElseIf UCase(Mid(Data, I, 6)) = "IRC://" Then
        sTrigger = -2
        sUline = 1
        I = I - 1
      ElseIf UCase(Mid(Data, I, 10)) = "NETTALK://" Then
        sTrigger = -2
        sUline = 1
        I = I - 1
      ElseIf UCase(Mid(Data, I, 9)) = "TELNET://" Then
        sTrigger = -2
        sUline = 1
        I = I - 1
      ElseIf UCase(Mid(Data, I, 6)) = "DCC://" Then
        sTrigger = -2
        sUline = 1
        I = I - 1
      ElseIf UCase(Mid(Data, I, 4)) = "WWW." Then
        sTrigger = -1
        sUline = 1
        I = I - 1
      ElseIf UCase(Mid(Data, I, 4)) = "irc." Then
        sTrigger = -2
        sUline = 1
        I = I - 1
      ElseIf UCase(Mid(Data, I, 7)) = "ED2K://" Then
        sTrigger = -1
        sUline = 1
        I = I - 1
      ElseIf UCase(Mid(Data, I, 10)) = "TORRENT://" Then
        sTrigger = -1
        sUline = 1
        I = I - 1
      ElseIf Mid(Data, I, 1) = "#" And Len(Data) > I Then
        Select Case AscB(Mid(Data, I + 1, 1))
          Case 0 To 34, 39 To 42, 44, 59, 60, 62, 91, 93, 34, 123, 124, 125
            DAT = DAT + Chr(Bdat)
          Case Else
            sTrigger = -6
            sUline = 0
            I = I - 1
        End Select
      Else
        DAT = DAT + ChrW(Bdat)
      End If
    Else
      If Bdat = 15 Then
        sColor = -1
        sBackColor = -1
        sUline = -1
        sPara = -1
      ElseIf Bdat = 3 Then
        CCheck = 0
        For I2 = I + 1 To Len(Data)
          Select Case AscB(Mid(Data, I2, 1))
            Case 44 ' ","
              CCheck = I2
              If I2 - I < 4 Then sColor = Val(Mid(Data, I + 1, I2 - I - 1))
              If Not IsNumeric(Mid(Data, I2 + 1, 1)) Then Exit For
            Case 0 To 47, 58 To 255
              Exit For
          End Select
          If (I2 > I + 2 And CCheck = 0) Or (I2 > CCheck + 2 And CCheck > 0) Then Exit For
        Next
        If CCheck = 0 Then
          If I2 - I < 2 Then
            sColor = -1
            sBackColor = -1
          ElseIf I2 - I < 4 Then
            sColor = Val(Mid(Data, I + 1, I2 - I - 1))
          End If
        Else
          If I2 - CCheck < 4 And I2 > CCheck Then sBackColor = Val(Mid(Data, CCheck + 1, I2 - CCheck - 1))
        End If
        I = I2 - 1
      ElseIf Bdat = 31 Then
        sUline = IIf(sUline = 1, 0, 1)
      ElseIf Bdat = 22 Then
        sPara = IIf(sPara = 1, 0, 1)
      End If
    End If
    If sTrigger <> sAltTrigger Or sUline <> sAltUline Or sAltColor <> sColor _
      Or sAltBackcolor <> sBackColor Or sAltPara <> sPara Or I >= Len(Data) Then
      If DAT <> "" Then
        If sAltTrigger = -2 Or sAltTrigger = -1 Then sAltTrigger = sAltTrigger * -1
        If sAltTrigger = 0 Then
          TextF.Trigger = Trigger
          TextF.TriggerStr = TriggerStr
        Else
          TextF.Trigger = sAltTrigger
        End If
        If sAltUline < 0 Then
          TextF.Underlined = Uline
        Else
          If sAltUline = 0 Then TextF.Underlined = False Else TextF.Underlined = True
        End If
        If sAltPara = 1 Then
          TextF.fColor = Form1.pTBox.BackColor
          TextF.bColor = FontColor
        Else
          TextF.fColor = GetIRCColor(sAltColor, FontColor)
          TextF.bColor = GetIRCColor(sAltBackcolor, -1)
        End If
        If TextF.bColor < 0 And Form1.pTBox.BackColor = TextF.fColor Then TextF.fColor = FontColor
        If NewLine = True Then TextF.PrText DAT: NewLine = False Else TextF.PrText DAT, False
        DAT = ""
      End If
      sAltColor = sColor
      sAltBackcolor = sBackColor
      sAltPara = sPara
      sAltUline = sUline
      sAltTrigger = sTrigger
    End If
  Next
  If IsNotifyNess And FrameType <> cfStatus And ConnTyp = 0 Then
    If Me.IrcConn.Silent = False Then
      If TrayIconState < 1 Then
        T.hIcon = Form1.TrayIcon(2)
        Shell_NotifyIcon NIM_MODIFY, T
        TrayIconState = 1
      End If
    End If
  End If
  If Me Is FrontCFrame Then
    TCHange = True
    If NoRT = False Then TextF.RefText NoRefr
  Else
    If Changed < 1 Then
      Changed = 1
      Form1.ColumRedraw True
    End If
  End If
End Sub

Public Sub CloseFrame()
  Dim ChatFrs As ChatFrame
  If ConnTyp = 0 Then ' #IRC
    Select Case FrameType
      Case cfRoom
        IrcConn.SendLine "PART " + Me.Caption
        Me.QuitFrame
      Case cfStatus: IrcConn.Quit
      Case cfDebug
        IrcConn.StartDebunging True
        Me.QuitFrame
      Case Else: Me.QuitFrame
    End Select
  End If
  If ConnTyp = 2 Then ' #DCC
    DCCConn.Quit
  End If
  If ConnTyp = -1 Then ' #Script
    If FrameType = cfScript Then
      If AppUnloading = False And ntScriptEnabled = True Then
        ntScript_Unload
        ntScript = Form1.Text2.Text
        Form1.SaveScriptFile
        ntScript_Reset
      Else
        ntScript = Form1.Text2.Text
        Form1.SaveScriptFile
      End If
      Me.QuitFrame
    ElseIf FrameType = cfLog Or FrameType = cfFriends Or FrameType = cfWindow Then
      Me.QuitFrame
    End If
  End If
End Sub

Public Sub QuitFrame()
  Dim I As Integer
  Dim I2 As Integer
  Dim SortedC() As Integer
  
  If Me.FrameType = cfServerList Then Exit Sub
  If LastFrontFrame Is Me Then Set LastFrontFrame = Nothing
  For I = ChatFrames.Count To 1 Step -1
    If ChatFrames(I) Is Me Then
      If LastFrontFrame Is Nothing Then
        GiveSortedC SortedC
        For I2 = 1 To UBound(SortedC)
          If ChatFrames(SortedC(I2)) Is Me Then Exit For
        Next
        Set LastFrontFrame = ChatFrames(SortedC(I2 - 1))
      End If
      ChatFrames.Remove I
      SetFrontFrame LastFrontFrame
      Exit For
    End If
  Next
  Form1.ColumRedraw
  ntScript_Call "FrameClosed", CStr(FrameID)
End Sub

Private Sub Class_Initialize()
  Dim I As Integer
  Dim I2 As Integer
  
  For I = 1 To 1024
    For I2 = 1 To ChatFrames.Count
      If ChatFrames(I2).FrameID = I Then Exit For
    Next
    If I2 > ChatFrames.Count Then
      FrameID = I
      Exit For
    End If
  Next
End Sub

Private Sub TextF_DblClick(Button As Integer)
  If Button = 1 Then
    If Len(DbClick) = 0 Then
      TextF.Scroll True
    Else
      PrivateFrame DbClick
    End If
  End If
End Sub

Private Sub TextF_NoTrigger(Button As Integer)
  DbClick = ""
  If Button = 2 Then
    Set SellCFrame = Me
    Form1.menH2copy.Visible = False
    Form1.menH2copytime.Visible = False
    Form1.menH2past.Visible = False
    Form1.menH2scroll.Visible = False
    Form1.menH2prop.Visible = FrameType = cfRoom
    Form1.menH2strich.Visible = FrameType = cfRoom
    Form1.menH2strich2.Visible = False
    Form1.RefScMenue
    If Len(TextF.SelledText) = 0 Then
      If Form1.VScroll1 < Form1.VScroll1.Max Then
        Form1.menH2scroll.Visible = True
        Form1.PopupMenuEx Form1.menH2, , , , Form1.menH2scroll
      Else
        Form1.PopupMenuEx Form1.menH2
      End If
    Else
      Form1.menH2copy.Visible = True
      If SellCFrame.FrameType = cfStatus Or SellCFrame.FrameType = cfRoom Or SellCFrame.FrameType = cfPrivate Then
        Form1.menH2copytime.Visible = True
      End If
      Form1.menH2past.Visible = True
      Form1.menH2strich2.Visible = True
      If Form1.VScroll1 < Form1.VScroll1.Max Then Form1.menH2scroll.Visible = True
      Form1.PopupMenuEx Form1.menH2, , , , Form1.menH2copy
    End If
  End If
End Sub

Private Sub TextF_Trigger(TriggerID As Long, TrigStr As String, Text As String, Button As Integer, FirstLinePart As String)
  Form1.LastTrigger = NickExtr(Text)
  If Button = 1 Then
    Select Case TriggerID
      Case 1 '"URL"
        CallWebSite Text, True
      Case 2
        LastCommandLine = Text
        PhraseCLine
      Case 3, 4, 5
        If Not FrontCFrame Is Nothing Then
          If FrontCFrame.FrameType = cfRoom Then
            UListF.GotoItem NickExtr(Text)
            UListF.ScrollToIndex
            UListF.Refresh
            DbClick = NickExtr(Text)
            WisperTo = NickExtr(Text)
            Form1.Check1.Visible = True
            Form1.TfRedraw
          ElseIf FrontCFrame.FrameType = cfStatus Then
            DbClick = NickExtr(Text)
          End If
        End If
      Case 6
        If Len(Text) < 5 And IsNumeric(Mid(Text, 2)) = True Then
          If Me.ConnTyp = 0 Then
            LastCommandLine = "IRC://" + IrcConn.Server + ":" + CStr(IrcConn.Port) + "/" + Me.Caption
            PhraseCLine "/msg " + NickExtr(FirstLinePart) + " xdcc send " + Text
          End If
        Else
          If Me.ConnTyp = 0 Then
            Me.IrcConn.SendLine "join " + Text ' #IRC
          End If
        End If
      Case 7
        StartDCCConnection TrigStr, , , Me
      Case 8
        ShowUpdater
      Case 9
        ntScript_Call "ScriptLinkClicked", Text, TrigStr, CStr(Button)
      Case 10
        LogDateTrigger = TrigStr
        StartTimer Form1, 1, 4
      Case Else
        MsgBox TrigStr, vbYesNo
    End Select
  Else
    Select Case TriggerID
      Case 1, 2
        If Len(TextF.SelledText) = 0 Then TextF.SelledText = Text
        TextF_NoTrigger Button
      Case 3, 4, 5
        Form1.UserInfoPic.Left = TextF.lX - Form1.pTBox.Left + Form1.pTapStrip.Left + 2
        Form1.UserInfoPic.Top = TextF.lY - Form1.UserInfoPic.Height + Form1.pTBox.Top + Form1.pMain.Top
        Form1.UserInfoPic.Height = 17
        Form1.UserInfoPic.ForeColor = &H80000013
        Form1.UserInfoPic.BackColor = &H80000003
        Form1.UserInfoPic.Cls
        'Form1.UserInfoPic.CurrentX = 3
        'Form1.UserInfoPic.CurrentY = 2
        Form1.UserInfoPic.FontBold = True
        Form1.UInfoMode = 0
        If Len(NickExtr(Text)) > 16 Then
          Form1.UserInfoPic.Width = TextWW(Left(NickExtr(Text), 16) + "...", Form1.UserInfoPic.hDC) + 10
          Form1.UserInfoPic.Visible = True
          'Form1.UserInfoPic.Print Left(NickExtr(Text), 16) + "..."
          TextOutW Form1.UserInfoPic.hDC, 3, 2, StrPtr(Left(NickExtr(Text), 16) + "..."), 16 + 3
        Else
          Form1.UserInfoPic.Width = TextWW(NickExtr(Text), Form1.UserInfoPic.hDC) + 10
          Form1.UserInfoPic.Visible = True
          'Form1.UserInfoPic.Print NickExtr(Text)
          TextOutW Form1.UserInfoPic.hDC, 3, 2, StrPtr(NickExtr(Text)), Len(NickExtr(Text))
        End If
        Form1.RefScMenue
        If Clipboard.GetFormat(vbCFBitmap) = True Then
          Form1.menH3filepast.Visible = True
        Else
          Form1.menH3filepast.Visible = False
        End If
        Form1.PopupMenuEx Form1.menH3, , , , Form1.menH3private
        Form1.UserInfoPic.Visible = False
      Case 9
        ntScript_Call "ScriptLinkClicked", Text, TrigStr, CStr(Button)
    End Select
  End If
End Sub

Private Function NickExtr(NickStr As String) As String
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim I4 As Integer
  Dim DAT As String
  DAT = Trim(NickStr)
  I2 = InStr(1, DAT, "<")
  If I2 > 0 And I2 < Len(NickStr) Then
    I = InStr(I2, NickStr, " ")
    I3 = InStr(I2, NickStr, ">")
    I4 = InStr(I2, NickStr, ":")
    If I3 < I Or I = 0 Then I = I3
    If I4 < I And I4 > I2 Then I2 = I4
    If I = 0 Then I = Len(NickStr)
    DAT = Mid(DAT, I2 + 1, I - I2 - 1)
  End If
  NickExtr = DAT
End Function


Private Sub UListF_DblClick()
  Dim CFrame As ChatFrame
  Dim I As Integer
  
  I = UListF.ListIndex
  If I > -1 And I < UListF.ListCount Then
    Select Case FrameType
      Case cfRoomList
        IrcConn.SendLine "join " + Me.UListF.GetListText(I, 0)
        
      Case cfRoom
        PrivateFrame IrcConn.ChatNoSign(UListF.List(UListF.ListIndex))
        
      Case cfServerList
        Set SellCFrame = Nothing
        For Each CFrame In ChatFrames
          If CFrame.FrameType = cfStatus Then
            If CFrame.IrcConn.ListID = ConnID(UListF.ListIndex) Then
              Set SellCFrame = CFrame
              Exit For
            End If
          End If
        Next
        StartConnection
        
      Case cfDCC
        ShowDCCTMenue UListF.ListIndex, True
        
      Case cfFriends
        If I > -1 Then
          LastCommandLine = "irc://" + UListF.GetListText(I, 2) _
          + "//" + UListF.GetListText(I, 0)
          PhraseCLine
        End If
    End Select
  End If
End Sub

Public Sub PrivateFrame(Name As String)
  Dim CFrame As ChatFrame
  Dim I As Integer
  
  If Name = "" Then Exit Sub
  If ConnTyp = 0 Then
    I = 0
    For Each CFrame In ChatFrames
      If CFrame.IrcConn Is Me.IrcConn And UCase(CFrame.Caption) = UCase(Name) Then
        SetFrontFrame CFrame
        I = 1
        Exit For
      End If
    Next
    If I = 0 Then NewFrame Me.IrcConn, Name, cfPrivate, True
  End If
End Sub

Public Function AddRemBann(Caption As String, Optional CheckOnly As Boolean, Optional OpNick As String, Optional BTime As String) As Boolean
  Dim I As Integer
  Dim I2 As Integer
  I = InStr(1, BannList, vbNewLine + Caption + " ", vbTextCompare)
  If CheckOnly Then
    AddRemBann = I > 0
  Else
    If I = 0 Then
      If Len(BTime) Then
        BannList = BannList + vbNewLine + Caption + " " + OpNick + "@" + BTime + vbNewLine
      Else
        BannList = BannList + vbNewLine + Caption + " " + OpNick + "@" + CStr(DateDiff("s", START_DATE, Now) - GMTDiff) + vbNewLine
      End If
      AddRemBann = True
    Else
      I2 = InStr(I + 1, BannList, vbNewLine)
      BannList = Left(BannList, I - 1) + Mid(BannList, I2 + 2)
      AddRemBann = False
    End If
  End If
End Function

Private Sub UListF_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Dim CFrame As ChatFrame
  If FrontCFrame.UListF.ListIndex > -1 And FrameType = cfRoom Then
    WisperTo = IrcConn.ChatNoSign(FrontCFrame.UListF.List(FrontCFrame.UListF.ListIndex))
    Form1.Check1.Visible = True
    Form1.TfRedraw
  End If
  If Button = 2 Then
    If UListF.ListIndex > -1 And FrameType = cfRoom Then
      Form1.LastTrigger = IrcConn.ChatNoSign(UListF.List(UListF.ListIndex))
      Form1.RefScMenue
      If Clipboard.GetFormat(vbCFBitmap) = True Then
        Form1.menH3filepast.Visible = True
      Else
        Form1.menH3filepast.Visible = False
      End If
      Form1.PopupMenuEx Form1.menH3, , , , Form1.menH3private
      If Form1.Visible = True Then Form1.Text1.SetFocus
    End If
    If UListF.ListIndex > -1 And UListF.ListIndex < UListF.ListCount And Me.FrameType = cfServerList Then
      Form1.menHneu.Visible = True
      Form1.menHedit.Visible = True
      Form1.menHcopy.Visible = True
      Form1.menHstrich1.Visible = True
      Form1.menHedit.Enabled = True
      Form1.menHStrich3.Visible = False
      Form1.menHlog.Visible = False
      Form1.menHpublicnotice.Visible = False
      Form1.menHsilent.Visible = False
      Form1.menHStrich4.Visible = False
      Form1.menHprop.Visible = False
      Form1.menHverbinden.Visible = True
      Form1.menHtrennen.Visible = True
      Form1.menHdel.Visible = True
      If ConnList(ConnID(UListF.ListIndex)).State = 0 Or ConnList(ConnID(UListF.ListIndex)).State = 3 Then
        Form1.menHverbinden.Enabled = True
        Form1.menHtrennen.Enabled = False
      Else
        Form1.menHverbinden.Enabled = False
        Form1.menHtrennen.Enabled = True
      End If
      If ConnList(ConnID(UListF.ListIndex)).State <> 0 Then
        Form1.menHclose.Visible = True
        Form1.menHdel.Enabled = False
        Form1.menHStrich2.Visible = True
      Else
        Form1.menHclose.Visible = False
        Form1.menHdel.Enabled = True
        Form1.menHStrich2.Visible = False
      End If
      Set SellCFrame = Nothing
      For Each CFrame In ChatFrames
        If CFrame.FrameType = cfStatus And CFrame.ConnTyp = 0 Then
          If CFrame.IrcConn.ListID = ConnID(UListF.ListIndex) Then
            Set SellCFrame = CFrame
            Exit For
          End If
        End If
      Next
      If Form1.menHverbinden.Enabled = True Then
        Form1.PopupMenuEx Form1.menH, , , , Form1.menHverbinden
      Else
        Form1.PopupMenuEx Form1.menH
      End If
    End If
    If UListF.ListIndex > -1 And UListF.ListIndex < UListF.ListCount And Me.FrameType = cfDCC Then
      ShowDCCTMenue UListF.ListIndex
    End If
    If UListF.ListIndex > -1 And UListF.ListIndex < UListF.ListCount And Me.FrameType = cfFriends Then
      Form1.PopupMenuEx Form1.menH7, , , , Form1.menH7private
    End If
  End If
End Sub
