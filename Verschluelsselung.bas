Attribute VB_Name = "Verschluelsselung"
Option Explicit

Const CodeChr As String = "0123456789abcdefghijklmnopqrstuvwxyz!?#%-+"
Const MultPl As Long = 71

Public Function KeyPassw(Passw As String) As String
  Dim I As Integer
  Dim I2 As Double
  Dim I3 As Byte
  Dim LastByte As Byte
  Dim Erg As String
  For I = 1 To Len(Passw)
    LastByte = I3
    I2 = AscB(Mid(Passw, I, 1))
    I3 = I2
    I2 = I2 + 13 + (255 * 3 - LastByte * 3) + 13 * I
    Do While I2 > 255
      I2 = I2 - 256
    Loop
    Erg = Erg + Format(I2, "000")
  Next
  KeyPassw = "[//Coded:" + Erg + "]"
End Function

Public Function DeKeyPassw(Passw As String) As String
  Dim I As Integer
  Dim I2 As Double
  Dim LastByte As Integer
  Dim Erg As String
  If Left(Passw, 9) = "[//Coded:" Then
    For I = 10 To Len(Passw) Step 3
      If I > Len(Passw) - 3 Then Exit For
      LastByte = I2
      I2 = Val(Mid(Passw, I, 3))
      I2 = I2 - 13 - (255 * 3 - LastByte * 3) - 13 * ((I - 10) / 3 + 1)
      Do While I2 < 0
        I2 = I2 + 256
      Loop
      If I2 < 256 Then Erg = Erg + Chr(I2)
    Next
  Else
    Erg = Passw
  End If
  DeKeyPassw = Erg
End Function

Public Function EncodeText(Text As String, AktivMode As Boolean, Passw As String, UTF8activ As Integer, UTF8static As Boolean) As String
  'Codieren
  If AktivMode Then
    'EncodeText = sEncodeText(EncodeUTF8(Text), Passw)
  Else
    EncodeText = ConvToSend(Text, UTF8activ, UTF8static, True)
  End If
End Function

Public Function DecodeText(Text As String, WasCoded As Boolean, Passw As String) As String
  'Decodieren
  If Left(Text, 6) = "[NTCTC" And Mid(Text, 10, 1) = "|" And Right(Text, 1) = "]" Then
    'DecodeText = DecodeUTF8(sDecodeText(Text, Passw))
    WasCoded = True
  Else
    DecodeText = Text
    WasCoded = False
  End If
End Function