Attribute VB_Name = "MouseGesturing"
Const BUFFER_COUNT = 255
Const PIXEL_RESOL = 8
Const MIN_POINTS = 10
Const MIN_PERCENT_DIFF = 45
Const MAX_PERCENT_DIFF = 55
Const MIN_PERCENT_LEN1 = 90
Const MIN_PERCENT_LEN2 = 20

Private TrackBufferX(BUFFER_COUNT - 1) As Single
Private TrackBufferY(BUFFER_COUNT - 1) As Single
Private BuffIndex As Long

Public Sub MGTrackMousePos(pX As Single, pY As Single)
  If BuffIndex < BUFFER_COUNT - 1 Then
    If Abs(TrackBufferX(BuffIndex) - pX) > PIXEL_RESOL _
    Or Abs(TrackBufferY(BuffIndex) - pY) > PIXEL_RESOL Then
      BuffIndex = BuffIndex + 1
      TrackBufferX(BuffIndex) = pX
      TrackBufferY(BuffIndex) = pY
    End If
  End If
End Sub

Public Sub MGMouseDown(pX As Single, pY As Single)
  BuffIndex = 0
  TrackBufferX(0) = pX
  TrackBufferY(0) = pY
End Sub

Public Function MGMouseUp() As Boolean
  Dim I As Long
  Dim Dx As Long
  Dim Dy As Long
  Dim MCount(3) As Long
  Dim MPosis(3) As Long
  Dim DirInd As Long
  Dim FirstDir As Long
  Dim SeconDir As Long
  Dim MGCom As Long
  
  If BuffIndex < BUFFER_COUNT And BuffIndex > MIN_POINTS Then
    For I = 0 To BuffIndex - 1
      Dx = TrackBufferX(I + 1) - TrackBufferX(I)
      Dy = TrackBufferY(I + 1) - TrackBufferY(I)
      If Dx = 0 Then Dx = 1
      If Dy = 0 Then Dy = 1
      If Dx > 0 Then
        If Dx >= Abs(Dy) Then DirInd = 0
      Else
        If -Dx >= Abs(Dy) Then DirInd = 2
      End If
      If Dy > 0 Then
        If Dy > Abs(Dx) Then DirInd = 3
      Else
        If -Dy > Abs(Dx) Then DirInd = 1
      End If
      MCount(DirInd) = MCount(DirInd) + 1
      MPosis(DirInd) = MPosis(DirInd) + I
    Next
    For I = 0 To 3
      If MCount(I) > MCount(FirstDir) Then FirstDir = I
    Next
    If FirstDir = 0 Then SeconDir = 1
    For I = 0 To 3
      If MCount(I) > MCount(SeconDir) And I <> FirstDir Then SeconDir = I
    Next
    
    If MCount(SeconDir) / BuffIndex * 100 < MIN_PERCENT_LEN2 Then
      If MCount(FirstDir) / BuffIndex * 100 > MIN_PERCENT_LEN1 Then
        SeconDir = -1
      End If
    Else
      I = (MPosis(FirstDir) / MCount(FirstDir) - MPosis(SeconDir) / MCount(SeconDir)) / BuffIndex * 100
      If Abs(I) > MIN_PERCENT_DIFF And Abs(I) < MAX_PERCENT_DIFF Then
        If MPosis(FirstDir) > MPosis(SeconDir) Then
          I = FirstDir
          FirstDir = SeconDir
          SeconDir = I
        End If
      Else
        FirstDir = -1
        SeconDir = -1
      End If
    End If
    
    MGCom = FirstDir * 10 + SeconDir
    
    Select Case MGCom
      Case -1 ' R
        Form1.ChangeFrontFrame 2
      Case 19 ' L
        Form1.ChangeFrontFrame 0
      Case 29 ' D
        Form1.Visible = False
      Case 9 ' U
        If FrontCFrame.FrameType = cfRoom Then
          Set SellCFrame = FrontCFrame
          Form8.Show 1 'Channel prop
        End If
      Case 2  ' RL
        Form1.ChangeFrontFrameUsed 2
      Case 20 ' LR
        Form1.ChangeFrontFrameUsed 0
      Case 10 ' UR
        Form1.ShowCValues
      Case 21 ' LU
        Form1.ShowScript
      Case 23 ' LD
        If FrontCFrame.FrameType = cfRoom Or FrontCFrame.FrameType = cfPrivate Then
          LoadFromLog FrontCFrame.Caption
        End If
      Case 12 ' UL
        Form5.Show 1 'Nettalk options
      Case 30 ' DR
        FrontCFrame.CloseFrame
    End Select
    MGMouseUp = True
    BuffIndex = 0
  End If
End Function

