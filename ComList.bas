Attribute VB_Name = "ComList"
Option Explicit

Public Sub AddToComList(Titel As String, SchortC As String, ComOut As String, ScType As Integer, Optional ScriptSc As Boolean)
  Dim I As Integer
  Dim sComOut As String
  Dim sSchortC As String
  
  If (Len(Titel) = 0 And Len(SchortC) = 0) Or Len(ComOut) = 0 Then Exit Sub
  If Left(ComOut, 1) = "/" Then sComOut = Right(ComOut, Len(ComOut) - 1) Else sComOut = ComOut
  If Left(SchortC, 1) = "/" Then sSchortC = Right(SchortC, Len(SchortC) - 1) Else sSchortC = SchortC
  If Len(Titel) Then
    For I = 0 To UBound(ComTranzList, 2)
      If LCase(ComTranzList(2, I)) = LCase(Titel) Then Exit For
    Next
  Else
    I = UBound(ComTranzList, 2) + 1
  End If
  If I > UBound(ComTranzList, 2) Then
    For I = 0 To UBound(ComTranzList, 2)
      If Len(ComTranzList(1, I)) = 0 Then
        ComTranzList(3, I) = CStr(ScType + 10)
        Exit For
      End If
    Next
  End If
  If I > UBound(ComTranzList, 2) Then
    ReDim Preserve ComTranzList(3, I)
    ComTranzList(3, I) = CStr(ScType + 10)
  End If
  ComTranzList(0, I) = sSchortC
  ComTranzList(1, I) = sComOut
  ComTranzList(2, I) = Titel
  If ScriptSc Then ComTranzList(3, I) = ComTranzList(3, I) + "s"
  Form1.RefScMenue
End Sub

Public Sub RemoveFromComList()
  Dim I As Integer
  
  For I = 0 To UBound(ComTranzList, 2)
    If InStr(1, ComTranzList(3, I), "s") Then
      ComTranzList(0, I) = ""
      ComTranzList(1, I) = ""
      ComTranzList(2, I) = ""
      ComTranzList(3, I) = ""
    End If
  Next
  Form1.RefScMenue
End Sub

Public Function CReplace(Data As String, Mask As String, ChatFrs As ChatFrame, Optional wins As CSocket) As String
  Dim I As Long
  Dim I2 As Integer
  Dim I3 As Integer
  Dim I4 As Integer
  Dim I5 As Integer
  Dim tempPos As Integer
  Dim sDAT(0 To 9) As String
  Dim OutStr As String
  Dim AguM As String
  Dim BagErr As String
    
  I = 0
  Do
    I2 = I
    I = InStr(I + 1, Data, " ")
    If I = 0 Or I3 > 8 Then Exit Do
    sDAT(I3) = Mid(Data, I2 + 1, I - I2 - 1)
    I3 = I3 + 1
  Loop
  sDAT(I3) = Mid(Data, I2 + 1)
  
  I2 = -1
  Do
    I3 = I2 + 1
    I2 = InStr(I2 + 2, Mask, "$")
    If I2 = 0 Then Exit Do
    OutStr = OutStr + Mid(Mask, I3 + 1, I2 - I3 - 1)
    If Mid(Mask, I2 + 1, 1) = ">" Then
      For I4 = Val(Mid(Mask, I2 + 2, 1)) + 1 To 9
        OutStr = OutStr + sDAT(I4)
        If I4 < 9 Then If Len(sDAT(I4 + 1)) > 0 Then OutStr = OutStr + " "
      Next
      I2 = I2 + 1
    ElseIf Mid(Mask, I2 + 1, 1) = "<" Then
      For I4 = 1 To Val(Mid(Mask, I2 + 2, 1)) - 1
        If I4 > 1 Then If Len(sDAT(I4)) > 0 Then OutStr = OutStr + " "
        OutStr = OutStr + sDAT(I4)
      Next
      I2 = I2 + 1
    ElseIf Val(Mid(Mask, I2 + 1, 1)) > 0 Then
      OutStr = OutStr + sDAT(Val(Mid(Mask, I2 + 1, 1)))
    ElseIf Mid(Mask, I2 + 1, 1) = "(" Then
      I4 = InStr(I2 + 2, Mask, ":")
      I5 = InStr(I2 + 2, Mask, ")")
      If I5 < I4 Then I4 = I5
      If Mid(Mask, I4 + 1, 1) = "'" Then
        tempPos = InStr(I4 + 2, Mask, "'") - 1
        If tempPos < 1 Then
          I5 = InStr(I2 + 2, Mask, ")")
          AguM = ""
        Else
          I5 = InStr(tempPos + 2, Mask, ")")
          AguM = Mid(Mask, I4 + 2, tempPos - I4 - 1)
        End If
      Else
        tempPos = I2
        If I5 > I4 Then AguM = Mid(Mask, I4 + 1, I5 - I4 - 1)
      End If
      If I4 > I5 Or I4 = 0 Then I4 = I5
      If I4 <> 0 Then
        Select Case LCase(Mid(Mask, I2 + 2, I4 - I2 - 2))
          Case "inputbox", "input"
            Load Form3
            If I5 > I4 Then
              Form3.Label3.Caption = AguM
            Else
              Form3.Label3.Caption = "Text:"
            End If
            Form3.Picture1.Visible = False
            Form3.Picture2.Visible = True
            Form3.Text3.TabIndex = 0
            Form3.Show 1
            If Len(Form3.Text3.Text) = 0 Then Unload Form3: Exit Function
            OutStr = OutStr + Form3.Text3.Text
            Unload Form3
          Case "nick"
            If ChatFrs.ConnTyp = 0 Then
              OutStr = OutStr + ChatFrs.IrcConn.Nick
            ElseIf ChatFrs.ConnTyp = 2 Then
              OutStr = OutStr + ChatFrs.DCCConn.Nick
            End If
          Case "roomname", "room"
            OutStr = OutStr + ChatFrs.Caption
          Case "lastuser"
            OutStr = OutStr + ChatFrs.LastWisperer
          Case "text"
            OutStr = OutStr + Form1.Text1.Text
          Case "host", "server"
            If Not wins Is Nothing Then OutStr = OutStr + wins.RemoteHost
          Case "port"
            If Not wins Is Nothing Then OutStr = OutStr + CStr(wins.RemotePort)
          Case "wid"
            If Not Form1 Is Nothing Then OutStr = OutStr + CStr(Form1.DataPort.hwnd)
          Case "user"
            OutStr = OutStr + sDAT(1)
          Case "$"
            OutStr = OutStr + "$"
          Case "calc"
            If I5 > I4 Then
              OutStr = OutStr + CStr(Evaluate(AguM, BagErr))
              If Len(BagErr) > 0 Then OutStr = OutStr + "(" + BagErr + ")"
            End If
          Case "useradr", "uadr"
            If Len(Data) > 0 And ChatFrs.ConnTyp = 0 Then
              I = Val(AguM)
              If I = 0 Then I = 1
              OutStr = OutStr + "$(UserAdr:" + sDAT(I) + ")"
            End If
          Case "userid", "id"
            If Len(Data) > 0 And ChatFrs.ConnTyp = 0 Then
              I = Val(AguM)
              If I = 0 Then I = 1
              OutStr = OutStr + "$(UserID:" + sDAT(I) + ")"
            End If
        End Select
        I2 = I5 - 1
      End If
    End If
  Loop
  If InStr(1, OutStr, "$(") = 0 Then
    CReplace = OutStr + Mid(Mask, I3 + 1)
  ElseIf ChatFrs.ConnTyp = 0 Then
    ChatFrs.IrcConn.ComBuffer = OutStr + Mid(Mask, I3 + 1)
    Set ChatFrs.IrcConn.BufferFrame = ChatFrs
    CReplace = "USERHOST " + sDAT(I)
  End If
End Function

Public Function CharReplace(Data As String, ChatFrs As ChatFrame, Optional wins As CSocket, Optional scIndex As Integer = -1, Optional Index As Integer) As String
  Dim I As Long
  Dim Ip1 As Long
  Dim Ip2 As Long
  Dim LCount As Integer
  Dim ComStr As String
  Dim OutStr As String
    
  I = InStr(1, Data, " ")
  If I = 0 Then
    ComStr = Data
  Else
    ComStr = Left(Data, I - 1)
  End If
  
  For I = 0 To UBound(ComTranzList, 2)
    If (UCase(ComTranzList(0, I)) = UCase(ComStr) And scIndex = -1 And Len(ComStr) > 0 _
    And (ChatFrs.FrameType = cfRoom Or Val(ComTranzList(3, I)) = 12 Or Val(ComTranzList(3, I)) < 11)) Or scIndex = I Then
      LCount = 0
      Ip1 = -1
      Do
        Ip2 = Ip1
        Ip1 = InStr(Ip1 + 2, ComTranzList(1, I), vbTab + vbTab)
        If Ip1 = 0 Then Ip1 = Len(ComTranzList(1, I)) + 1
        If Index = LCount Then
          If Len(Trim(Mid(ComTranzList(1, I), Ip2 + 2, Ip1 - Ip2 - 2))) Then
            OutStr = CReplace(Data, Mid(ComTranzList(1, I), Ip2 + 2, Ip1 - Ip2 - 2), ChatFrs, wins)
          End If
          Exit Do
        End If
        LCount = LCount + 1
        If Ip1 = Len(ComTranzList(1, I)) + 1 Then
          Index = -1
          Exit Do
        End If
      Loop
      Exit For
    End If
  Next
  
  If I > UBound(ComTranzList, 2) Then
    CharReplace = Data
    Index = -1
  Else
    CharReplace = OutStr
  End If
End Function
