Attribute VB_Name = "UnicodeWrapper"
Option Explicit

Public Declare Function SetWindowLongW Lib "user32" (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Public Declare Function SetWindowLongA Lib "user32" (ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long

Public Declare Function CallWindowProcW Lib "user32" (ByVal Adresse As Long, ByVal hWnd As Long, ByVal Msg As Long, ByVal WParam As Long, ByVal lParam As Long) As Long
Public Declare Function CallWindowProcA Lib "user32" (ByVal Adresse As Long, ByVal hWnd As Long, ByVal Msg As Long, ByVal WParam As Long, ByVal lParam As Long) As Long

Public Declare Function SendMessageW Lib "user32" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal WParam As Long, ByVal lParam As Long) As Long
Public Declare Function SendMessageA Lib "user32" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal WParam As Long, ByVal lParam As Long) As Long
Public Declare Function SendMessageAExt Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal WParam As Long, ByVal lParam As String) As Long

Public Declare Function DefWindowProcW Lib "user32" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal WParam As Long, ByVal lParam As Long) As Long
Public Declare Function DefWindowProcA Lib "user32" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal WParam As Long, ByVal lParam As String) As Long

Public Declare Function DrawTextW Lib "user32" (ByVal hDC As Long, ByVal lpStr As Long, ByVal Ncount As Long, lpRect As RECT, ByVal wFormat As Long) As Long
Public Declare Function DrawTextA Lib "user32" (ByVal hDC As Long, ByVal lpStr As String, ByVal Ncount As Long, lpRect As RECT, ByVal wFormat As Long) As Long

Public Declare Function CreateWindowExW Lib "user32" (ByVal dwExStyle As Long, ByVal lpClassName As Long, ByVal lpWindowName As Long, ByVal dwStyle As Long, ByVal X As Long, ByVal Y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hWndParent As Long, ByVal hMenu As Long, ByVal hInstance As Long, lpParam As Long) As Long
Public Declare Function CreateWindowExA Lib "user32" (ByVal dwExStyle As Long, ByVal lpClassName As String, ByVal lpWindowName As String, ByVal dwStyle As Long, ByVal X As Long, ByVal Y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hWndParent As Long, ByVal hMenu As Long, ByVal hInstance As Long, lpParam As Long) As Long

Private Declare Function GetTextExtentPoint32W Lib "gdi32" (ByVal hDC As Long, ByVal lpsz As Long, ByVal cbString As Long, lpSize As POINTAPI) As Long
Private Declare Function GetTextExtentPoint32A Lib "gdi32" (ByVal hDC As Long, ByVal lpsz As String, ByVal cbString As Long, lpSize As POINTAPI) As Long

Private Declare Function GetCommandLineW Lib "kernel32" () As Long
Private Declare Function GetCommandLineA Lib "kernel32" () As Long

'Private Declare Function DdeInitializeW Lib "user32" (pidInst As Long, ByVal pfnCallback As Long, ByVal afCmd As Long, ByVal ulRes As Long) As Integer
'Private Declare Function DdeInitializeA Lib "user32" (pidInst As Long, ByVal pfnCallback As Long, ByVal afCmd As Long, ByVal ulRes As Long) As Integer
'
'Private Declare Function DdeCreateStringHandleW Lib "user32" (ByVal idInst As Long, ByVal psz As String, ByVal iCodePage As Long) As Long
'Private Declare Function DdeCreateStringHandleA Lib "user32" (ByVal idInst As Long, ByVal psz As String, ByVal iCodePage As Long) As Long
'
'Private Declare Function DdeQueryStringW Lib "user32" (ByVal idInst As Long, ByVal hsz As Long, ByVal psz As String, ByVal cchMax As Long, ByVal iCodePage As Long) As Long
'Private Declare Function DdeQueryStringA Lib "user32" (ByVal idInst As Long, ByVal hsz As Long, ByVal psz As String, ByVal cchMax As Long, ByVal iCodePage As Long) As Long

Public Declare Function GetVersionExA Lib "kernel32" (lpVersionInformation As OSVERSIONINFO) As Integer

Private Type OSVERSIONINFO
   dwOSVersionInfoSize As Long
   dwMajorVersion As Long
   dwMinorVersion As Long
   dwBuildNumber As Long
   dwPlatformId As Long
   szCSDVersion As String * 128
End Type

Public IsNt As Boolean
Public IsAfterVista As Boolean

Public Function getVersion() As Long
   Dim osinfo As OSVERSIONINFO
   Dim retvalue As Integer
   osinfo.dwOSVersionInfoSize = 148
   osinfo.szCSDVersion = Space$(128)
   retvalue = GetVersionExA(osinfo)
   getVersion = osinfo.dwPlatformId * 1000 + osinfo.dwMajorVersion * 10 + osinfo.dwMinorVersion
End Function

Public Function SetWindowLong(ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
  If IsNt Then
    SetWindowLong = SetWindowLongW(hWnd, nIndex, dwNewLong)
  Else
    SetWindowLong = SetWindowLongA(hWnd, nIndex, dwNewLong)
  End If
End Function

Public Function CallWindowProc(ByVal Adresse As Long, ByVal hWnd As Long, ByVal Msg As Long, ByVal WParam As Long, lParam As Long) As Long
  If IsNt Then
    CallWindowProc = CallWindowProcW(Adresse, hWnd, Msg, WParam, lParam)
  Else
    CallWindowProc = CallWindowProcA(Adresse, hWnd, Msg, WParam, lParam)
  End If
End Function

Public Function SendMessage(ByVal hWnd As Long, ByVal wMsg As Long, ByVal WParam As Long, lParam As String) As Long
  If IsNt Then
    SendMessage = SendMessageW(hWnd, wMsg, WParam, StrPtr(lParam))
  Else
    SendMessage = SendMessageAExt(hWnd, wMsg, WParam, lParam)
  End If
End Function

Public Function DefWindowProc(ByVal hWnd As Long, ByVal wMsg As Long, ByVal WParam As Long, ByVal lParam As String) As Long
  If IsNt Then
    DefWindowProc = DefWindowProcW(hWnd, wMsg, WParam, StrPtr(lParam))
  Else
    DefWindowProc = DefWindowProcA(hWnd, wMsg, WParam, lParam)
  End If
End Function

Public Function DrawText(ByVal hDC As Long, ByVal lpStr As String, ByVal Ncount As Long, lpRect As RECT, ByVal wFormat As Long) As Long
  If IsNt Then
    DrawText = DrawTextW(hDC, StrPtr(lpStr), Ncount, lpRect, wFormat)
  Else
    DrawText = DrawTextA(hDC, lpStr, Ncount, lpRect, wFormat)
  End If
End Function

Public Function CreateWindowEx(ByVal dwExStyle As Long, ByVal lpClassName As String, ByVal lpWindowName As String, ByVal dwStyle As Long, ByVal X As Long, ByVal Y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hWndParent As Long, ByVal hMenu As Long, ByVal hInstance As Long, lpParam As Long) As Long
  If IsNt Then
    CreateWindowEx = CreateWindowExW(dwExStyle, StrPtr(lpClassName), StrPtr(lpWindowName), dwStyle, X, Y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam)
  Else
    CreateWindowEx = CreateWindowExA(dwExStyle, lpClassName, lpWindowName, dwStyle, X, Y, nWidth, nHeight, hWndParent, hMenu, hInstance, lpParam)
  End If
End Function

Public Function GetTextExtentPoint32(ByVal hDC As Long, ByVal lpsz As String, ByVal cbString As Long, lpSize As POINTAPI) As Long
  If IsNt Then
    GetTextExtentPoint32 = GetTextExtentPoint32W(hDC, StrPtr(lpsz), cbString, lpSize)
  Else
    GetTextExtentPoint32 = GetTextExtentPoint32A(hDC, lpsz, cbString, lpSize)
  End If
End Function

Public Function GetCommandline() As String
  Dim retString As String
  Dim ComStrPtr As Long
  
  If IsNt Then
    ComStrPtr = GetCommandLineW
    retString = String(lstrlenW(ByVal ComStrPtr), Chr(0))
    MemCopy ByVal StrPtr(retString), ByVal ComStrPtr, Len(retString) * 2
  Else
    ComStrPtr = GetCommandLineA
    retString = String(lstrlen(ByVal ComStrPtr), Chr(0))
    MemCopy ByVal retString, ByVal ComStrPtr, Len(retString)
  End If
  GetCommandline = retString
End Function

'Public Function DdeInitialize(pidInst As Long, ByVal pfnCallback As Long, ByVal afCmd As Long, ByVal ulRes As Long) As Integer
'  If IsNt Then
'    DdeInitialize = DdeInitializeW(pidInst, pfnCallback, afCmd, ulRes)
'  Else
'    DdeInitialize = DdeInitializeA(pidInst, pfnCallback, afCmd, ulRes)
'  End If
'End Function
'
'Public Function DdeCreateStringHandle(ByVal idInst As Long, psz As String, ByVal iCodePage As Long) As Long
'  If IsNt Then
'    DdeCreateStringHandle = DdeCreateStringHandleW(idInst, psz, iCodePage)
'  Else
'    DdeCreateStringHandle = DdeCreateStringHandleA(idInst, psz, iCodePage)
'  End If
'End Function
'
'Public Function DdeQueryString(ByVal idInst As Long, ByVal hsz As Long, psz As String, ByVal cchMax As Long, ByVal iCodePage As Long) As Long
'  If IsNt Then
'    DdeQueryString = DdeQueryStringW(idInst, hsz, psz, cchMax, iCodePage)
'  Else
'    DdeQueryString = DdeQueryStringA(idInst, hsz, psz, cchMax, iCodePage)
'  End If
'End Function
