Attribute VB_Name = "UDPWSock"
Option Explicit

Declare Function WaitSelect& Lib "wsock32.dll" Alias "select" _
  (ByVal nu&, SRead As Any, sWrite As Any, sErr As Any, sTOut As Any)
Declare Function sendto& Lib "wsock32.dll" (ByVal S&, Buf As Any, _
  ByVal BufLen&, ByVal flags&, sa As Any, ByVal LSa&)
'Declare Sub RtlMoveMemory Lib "kernel32" (D As Any, S As Any, ByVal B&)

Declare Function QueryPerformanceFrequency& Lib "kernel32" (x@)
Declare Function QueryPerformanceCounter& Lib "kernel32" (x@)

Public Const SO_SNDBUF& = &H1001, SO_RCVBUF& = &H1002

Private CW As New Collection

Public Function HPTimer#()
Dim x@: Static Frq@
  If Frq = 0 Then QueryPerformanceFrequency Frq
  If QueryPerformanceCounter(x) Then HPTimer = x / Frq
End Function

Public Sub CallWithDelay(UDP As CUDP)
  CW.Add UDP, CStr(SetTimer(0, 0, 1, AddressOf TProc))
End Sub
Public Sub TProc(ByVal hwnd&, ByVal uMsg&, ByVal ID&, ByVal lSysTime&)
Dim UDP As CUDP
  KillTimer 0, ID
  On Error Resume Next
  Set UDP = CW(CStr(ID)): CW.Remove CStr(ID)
  If Not UDP Is Nothing Then UDP.WaitCallBack
End Sub




