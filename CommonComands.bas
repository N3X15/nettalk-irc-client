Attribute VB_Name = "CommonComands"
Option Explicit

Private Function CallSub(PartIndex As Integer, ComplStr As String) As String
  Dim I As Integer
  Dim I2 As Integer
  Dim WIndex As Integer
  
  Do
    I = InStr(I + 1, ComplStr, " ")
    If I = 0 Then Exit Do
    WIndex = WIndex + 1
    If WIndex = PartIndex Then
      If Mid(ComplStr, I + 1, 1) = ":" Then
        CallSub = Mid(ComplStr, I + 2)
      Else
        I2 = InStr(I + 1, ComplStr, " ")
        If I2 = 0 Then I2 = Len(ComplStr) + 1
        CallSub = Mid(ComplStr, I + 1, I2 - I - 1)
      End If
      Exit Do
    End If
  Loop
End Function

Public Function CCommands(ChatFrs As ChatFrame, sDAT() As String, OutStr As String, OutStr2 As String) As Boolean
  Dim I As Long
  Dim I2 As Long
  Dim TempVal As Single
  
  CCommands = True
  Select Case LCase(sDAT(0))
    Case "dconnect"
      TempVal = Val(sDAT(2))
      If TempVal < 64001 And TempVal > 0 Then I = TempVal Else I = 6667
      TempVal = Val(Left(sDAT(3), 3))
      If Abs(TempVal) < 1024 Then I2 = TempVal Else I2 = 0
      StartDCCConnection "DCC:CHAT Server " + sDAT(1) + " " + CStr(I) + " -1 chat Client", False, I2
    Case "search"
      If Not ChatFrs.TextF Is Nothing Then
        ChatFrs.TextF.Search Mid(OutStr, Len(sDAT(0)) + 2)
      ElseIf ChatFrs.FrameType = cfRoomList Then
        ChatFrs.UListF.Search Mid(OutStr, Len(sDAT(0)) + 2)
      End If
    Case "cls"
      If Not ChatFrs.TextF Is Nothing Then ChatFrs.TextF.ClearText
    Case "call"
      ntScript_Call CallSub(1, OutStr), CallSub(2, OutStr), _
        CallSub(3, OutStr), CallSub(4, OutStr), _
        CallSub(5, OutStr), CallSub(6, OutStr), , , True
    Case "shellapp", "openurl"
      If LCase(Left(sDAT(1), 7)) = "http://" Then
        CallWebSite Mid(OutStr, Len(sDAT(0)) + 2), True
      Else
        On Error Resume Next
        Shell Mid(OutStr, Len(sDAT(0)) + 2), vbNormalFocus
        If Err > 0 Then
          CallWebSite Mid(OutStr, Len(sDAT(0)) + 2)
        End If
        On Error GoTo 0
      End If
    Case "calc"
      DrawSLine ChatFrs, 1, "- Calc: ", CoVal.ClientMSG, CStr(Evaluate(Mid(OutStr, Len(sDAT(0)) + 2), OutStr2)) + " = " + Mid(OutStr, Len(sDAT(0)) + 2)
      If OutStr2 <> "" Then
        DrawSLine ChatFrs, 1, "- Calc: ", CoVal.ClientMSG, OutStr2
        OutStr2 = ""
      End If
    Case "update"
      ShowUpdater
    Case "open"
      LastCommandLine = Mid(OutStr, Len(sDAT(0)) + 2)
      For I = 0 To ConnListCount - 1
        If LCase(LastCommandLine) = LCase(ConnList(I).Caption) Then
          If ConnList(I).State > 0 Then
            LastCommandLine = "IRC://" + ConnList(I).Server + ":" + CStr(ConnList(I).Port)
          Else
            LastCommandLine = ""
            StartConnection I
          End If
          Exit For
        End If
      Next
      If Len(LastCommandLine) Then PhraseCLine
    Case "info"
      Load Form6
      If Form6.Visible = False Then Form6.Show 1
    Case "version"
      If ChatFrs.TextF Is Nothing Then
        MsgBox App.Major & "." & App.Minor & "." & App.Revision _
        & "     " + App.LegalCopyright, vbInformation
      Else
        DrawSLine ChatFrs, 1, "- " + App.Title + "-Version: ", CoVal.ClientMSG, App.Major & "." & App.Minor & "." & App.Revision _
        & "     " + App.LegalCopyright
      End If
    Case "showlog", "logview"
      If Len(Trim(Mid(OutStr, Len(sDAT(0)) + 2))) > 0 Then
        LoadFromLog sDAT(1), -1, 0, sDAT(2)
      Else
        LoadFromLog ChatFrs.Caption
      End If
    Case "searchlog"
      LoadFromLog Mid(OutStr, Len(sDAT(0)) + 2), , True
    Case "values"
      TempVal = Val(sDAT(1))
      If TempVal < 7 And TempVal > 0 Then OpFrameIndex = TempVal - 1
      If Form5.Visible = True Then
        SetForegroundWindow Form5.hWnd
      Else
        Form5.Show 1
      End If
    Case "cvalues"
      Form1.ShowCValues
    Case "script"
      Form1.ShowScript
    Case "hide"
      Form1.Visible = False
    Case "srestart", "startscript"
      Form1.reStartScript
    Case "sstop", "stopscript"
      ntScript_Unload
    Case "setdccserver"
      TempVal = Val(sDAT(1))
      If TempVal > 0 And TempVal < 64001 Then
        DCCServerPort = TempVal
        DrawSLine ChatFrs, 2, "- " + TeVal.fFunction + " " + TeVal.fOn + " (" + CStr(DCCServerPort) + ")", CoVal.ClientMSG
      Else
        DCCServerPort = Abs(DCCServerPort) * -1
        DrawSLine ChatFrs, 2, "- " + TeVal.fFunction + " " + TeVal.fOff, CoVal.ClientMSG
      End If
      CheckDCCServ
    Case "secip"
      UseSecIP = UseSecIP = False
      DrawSLine ChatFrs, 2, "- " + TeVal.fFunction + " " + IIf(UseSecIP, TeVal.fOn, TeVal.fOff), CoVal.ClientMSG
    Case "silent"
      SoundStumm = SoundStumm = False
      Form1.menH4stumm.Checked = SoundStumm
      DrawSLine ChatFrs, 2, "- " + TeVal.fFunction + " " + IIf(SoundStumm, TeVal.fOn, TeVal.fOff), CoVal.ClientMSG
    Case "osdmode"
      Form1.OsdModeOnOff
      DrawSLine ChatFrs, 2, "- " + TeVal.fFunction + " " + IIf(UModeOSD, TeVal.fOn, TeVal.fOff), CoVal.ClientMSG
    Case "shortcut"
      AddToComList sDAT(1), vbNullString, Mid(OutStr, Len(sDAT(0)) + Len(sDAT(1)) + 3), 0
    Case "print", "echo"
      ChatFrs.PrintText Mid(OutStr, Len(sDAT(0)) + 2), CoVal.NormalText, False, True
    Case "publicnotice"
      DrawSLine ChatFrs, 2, "- " + TeVal.fFunction + " " + _
      IIf(AddRemOsdRL(ChatFrs.Caption), TeVal.fOn, TeVal.fOff), CoVal.ClientMSG
    Case "playbeep"
      MakeSignal CBool(Val(sDAT(1)))
    Case "setbeep"
      If Len(sDAT(1)) > 0 Then BeepArr = Mid(OutStr, Len(sDAT(0)) + 2)
      DrawSLine ChatFrs, 2, "- Beep: " + BeepArr, CoVal.ClientMSG
      MakeSignal True
    Case "getbeep"
      DrawSLine ChatFrs, 2, "- Beep: " + BeepArr, CoVal.ClientMSG
    Case "setdccpath"
      dccAutoAcc = False
      If Len(sDAT(0)) > 0 Then
        On Error Resume Next
        If Len(Dir(Mid(OutStr, Len(sDAT(0)) + 2), vbDirectory)) > 0 Then
          DCCPath = Mid(OutStr, Len(sDAT(0)) + 2)
          dccAutoAcc = True
        End If
        On Error GoTo 0
      End If
      DrawSLine ChatFrs, 2, "- " + TeVal.fFunction + " " + IIf(dccAutoAcc, TeVal.fOn, TeVal.fOff), CoVal.ClientMSG
    Case "showtime"
      ShowTimeStampText = ShowTimeStampText = False
      DrawSLine ChatFrs, 2, "- " + TeVal.fFunction + " " + _
      IIf(ShowTimeStampText, TeVal.fOn, TeVal.fOff), CoVal.ClientMSG
      SetGlobalTimeShow ShowTimeStampText
    Case "server"
      LastCommandLine = "IRC://" + sDAT(1)
      If Len(sDAT(2)) > 0 Then LastCommandLine = LastCommandLine + ":" + sDAT(2)
      PhraseCLine
    Case "smsg"
      For I = 1 To ChatFrames.Count
        If ChatFrames(I).FrameType = cfRoom Then
          ChatFrames(I).SendM Mid(OutStr, Len(sDAT(0)) + 2)
        End If
      Next
    Case "xdcc"
      LastCommandLine = sDAT(1)
      PhraseCLine Mid(OutStr, Len(sDAT(0)) + Len(sDAT(1)) + 3)
    Case "help", "hilfe"
      HTMLHelp_ShowTopic sDAT(1)
    Case "makeerror"
      MsgBox getVersion
    Case Else
      CCommands = False
  End Select
End Function

Public Function IsIgnored(ListStr As String, Nick As String) As Boolean
  IsIgnored = InStr(1, ListStr, " " + Nick + "@", vbTextCompare) > 0
End Function

Public Function UnIgnore(ListStr As String, Nick As String) As Boolean
  Dim I As Integer
  Dim I2 As Integer
  
  I = InStr(1, ListStr, " " + Nick + "@", vbTextCompare)
  If I > 0 Then
    I2 = InStr(I + 1, ListStr, " ")
    If I2 = 0 Then I2 = Len(ListStr) + 1
    ListStr = Left(ListStr, I - 1) + Mid(ListStr, I2)
    UnIgnore = True
  End If
End Function

Public Function SortIgnoreList(ListStr As String) As String
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim DAT As String
  Dim DateString As String
  Dim LastDate As Date
   
  DAT = " "
  I = InStr(1, ListStr, " ")
  Do
    I2 = I
    I3 = InStr(I2 + 1, ListStr, "@")
    I = InStr(I3 + 1, ListStr, " ")
    If I3 = 0 Then Exit Do
    If I = 0 Then I = Len(ListStr) + 1
    
    DateString = Mid(ListStr, I3 + 1, I - I3 - 1)
    
    If ConvToDate(LastDate, DateString) Then
      If DateDiff("d", LastDate, Date) < 7 Then
        DAT = DAT + Mid(ListStr, I2 + 1, I - I2 - 1) + " "
      End If
    End If
  Loop
  SortIgnoreList = DAT
End Function

Public Function GetSecureDate(SrcDateStr As String) As Date
  Dim Ret As Date
  
  If ConvToDate(Ret, SrcDateStr) Then
    GetSecureDate = Ret
  Else
    GetSecureDate = 0
  End If
End Function

Public Function ConvToDate(ByRef DestDate As Date, SrcDateStr As String) As Boolean
  Dim YearVal As Double
  Dim MoVal As Double
  Dim DayVal As Double
  Dim hhVal As Double
  Dim nnVal As Double
  Dim ssVal As Double
  
  YearVal = Val(Mid(SrcDateStr, 7, 4))
  If YearVal > 1900 And YearVal < 9999 Then
    MoVal = Val(Mid(SrcDateStr, 4, 2))
    If MoVal > 0 And MoVal < 13 Then
      DayVal = Val(Mid(SrcDateStr, 1, 2))
      If DayVal > 0 And MoVal < 32 Then
        DestDate = DateSerial(YearVal, MoVal, DayVal)
        ConvToDate = True
      End If
    End If
  End If
  If Len(SrcDateStr) > 11 Then
    hhVal = Val(Mid(SrcDateStr, 12, 2))
    If hhVal >= 0 And hhVal < 24 Then
      nnVal = Val(Mid(SrcDateStr, 15, 2))
      If nnVal >= 0 And nnVal < 60 Then
        ssVal = Val(Mid(SrcDateStr, 18, 2))
        If ssVal >= 0 And ssVal < 60 Then
          DestDate = DateAdd("s", ((hhVal * 60 + nnVal) * 60) + ssVal, DestDate)
        End If
      End If
    End If
  End If
End Function

