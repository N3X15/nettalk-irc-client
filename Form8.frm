VERSION 5.00
Begin VB.Form Form8 
   BorderStyle     =   3  'Fester Dialog
   Caption         =   "Eigenschaften"
   ClientHeight    =   5925
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7065
   BeginProperty Font 
      Name            =   "MS Shell Dlg"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Form8.frx":0000
   LinkTopic       =   "Form7"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   395
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   471
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'Fenstermitte
   Begin VB.CheckBox Check1 
      Caption         =   "+i"
      Height          =   255
      Index           =   2
      Left            =   4080
      TabIndex        =   10
      Top             =   4200
      Width           =   2895
   End
   Begin VB.CheckBox Check1 
      Caption         =   "+m"
      Height          =   255
      Index           =   3
      Left            =   4080
      TabIndex        =   12
      Top             =   4560
      Width           =   2895
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Neu..."
      Height          =   375
      Left            =   3000
      TabIndex        =   3
      Top             =   3120
      Width           =   1185
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Bearbeiten..."
      Height          =   375
      Left            =   4320
      TabIndex        =   2
      Top             =   3120
      Width           =   1185
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Entfernen"
      Height          =   375
      Left            =   5640
      TabIndex        =   1
      Top             =   3120
      Width           =   1185
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   2400
      MaxLength       =   128
      TabIndex        =   14
      Top             =   4920
      Width           =   1935
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Passwort verwenden:"
      Height          =   255
      Index           =   6
      Left            =   240
      TabIndex        =   13
      Top             =   4920
      Width           =   2055
   End
   Begin VB.CheckBox Check1 
      Caption         =   "+s"
      Height          =   255
      Index           =   5
      Left            =   240
      TabIndex        =   11
      Top             =   4560
      Width           =   3735
   End
   Begin VB.CheckBox Check1 
      Caption         =   "+p"
      Height          =   255
      Index           =   4
      Left            =   240
      TabIndex        =   9
      Top             =   4200
      Width           =   3735
   End
   Begin VB.CheckBox Check1 
      Caption         =   "+n"
      Height          =   255
      Index           =   1
      Left            =   4080
      TabIndex        =   8
      Top             =   3840
      Width           =   2895
   End
   Begin VB.CheckBox Check1 
      Caption         =   "+t"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   7
      Top             =   3840
      Width           =   3735
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H80000005&
      BorderStyle     =   0  'Kein
      Height          =   1935
      Left            =   240
      ScaleHeight     =   129
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   437
      TabIndex        =   0
      Top             =   1080
      Width           =   6555
      Begin VB.VScrollBar VScroll1 
         Height          =   1215
         Left            =   4680
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   120
         Width           =   255
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "‹bernehmen"
      Height          =   345
      Left            =   5760
      TabIndex        =   6
      Top             =   5490
      Width           =   1215
   End
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Caption         =   "Abbrechen"
      Height          =   345
      Left            =   4395
      TabIndex        =   5
      Top             =   5490
      Width           =   1215
   End
   Begin VB.CommandButton Command3 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   345
      Left            =   3030
      TabIndex        =   4
      Top             =   5490
      Width           =   1215
   End
   Begin VB.Line Line14 
      BorderColor     =   &H80000010&
      X1              =   16
      X2              =   456
      Y1              =   33
      Y2              =   33
   End
   Begin VB.Label CapLabel 
      Caption         =   "#"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   18
      Top             =   120
      Width           =   4335
   End
   Begin VB.Label Label2 
      Caption         =   "Raumoptionen:"
      Height          =   255
      Left            =   240
      TabIndex        =   17
      Top             =   3480
      Width           =   2655
   End
   Begin VB.Label Label1 
      Caption         =   "Gebannte User:"
      Height          =   255
      Left            =   240
      TabIndex        =   15
      Top             =   720
      Width           =   1935
   End
   Begin VB.Line Line20 
      BorderColor     =   &H80000010&
      X1              =   16
      X2              =   464
      Y1              =   359
      Y2              =   359
   End
End
Attribute VB_Name = "Form8"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private WithEvents Bannli As IconList
Attribute Bannli.VB_VarHelpID = -1
Private ChanKey As String
Private ModeList(6) As Boolean
Private ModeTypeList(6) As String * 1

Private Sub Bannli_DblClick()
  Command5_Click
End Sub

Private Sub Check1_Click(Index As Integer)
  If Index = 6 Then Text1.Enabled = Check1(Index).Value
End Sub

Private Sub ApplSettings(OKmode As Boolean)
  Dim I As Integer
  Dim OutBuff As String
  Dim OutBuff2 As String
  Dim KeyStr As String
  
  For I = 0 To 6
    If Not CBool(Check1(I).Value) = ModeList(I) Then
      If Check1(I).Value = 1 Then
        OutBuff = OutBuff + ModeTypeList(I)
        If I = 6 Then KeyStr = " " + Text1.Text
      Else
        OutBuff2 = OutBuff2 + ModeTypeList(I)
        If I = 6 Then KeyStr = " " + ChanKey
      End If
    ElseIf I = 6 Then
      If Check1(I).Value And ChanKey <> Text1.Text Then
        SellCFrame.IrcConn.SendLine "mode " + SellCFrame.Caption + " -k " + ChanKey
        OutBuff = OutBuff + ModeTypeList(I)
        KeyStr = " " + Text1.Text
      End If
    End If
  Next
  If Len(OutBuff) > 0 Then OutBuff = "+" + OutBuff
  If Len(OutBuff2) > 0 Then
    OutBuff = OutBuff + "-" + OutBuff2
  End If
  If Len(OutBuff) > 0 Then
    SellCFrame.IrcConn.SendLine "mode " + SellCFrame.Caption + " " + OutBuff + KeyStr
    If Not OKmode Then
      For I = 0 To 6
        Check1(I).Enabled = False
      Next
      Text1.Enabled = False
      SellCFrame.ModeRequesState = True
      SellCFrame.IrcConn.SendLine "mode " + SellCFrame.Caption
    End If
  End If
End Sub

Private Sub Command1_Click()
  ApplSettings False
End Sub

Private Sub Command2_Click()
  Unload Me
End Sub

Private Sub Command3_Click()
  ApplSettings True
  Unload Me
End Sub

Private Sub Command4_Click()
  If Bannli.ListCount > 0 Then
    SellCFrame.IrcConn.SendLine "mode " + SellCFrame.Caption + " -b " + Bannli.List(Bannli.ListIndex, 0)
  End If
End Sub

Private Sub Command5_Click()
  If Bannli.ListCount > 0 Then
    Load Form3
    Form3.Picture1.Visible = False
    Form3.Picture2.Visible = True
    Form3.Text3.TabIndex = 0
    Form3.Label3.Caption = TeVal.ls5Maske + ":"
    Form3.Text3.Text = Bannli.List(Bannli.ListIndex, 0)
    Form3.Text3.SelLength = Len(Form3.Text3.Text)
    Form3.Show 1
    If Len(Form3.Text3.Text) > 0 Then
      SellCFrame.IrcConn.SendLine "mode " + SellCFrame.Caption + " -b+b " + Bannli.List(Bannli.ListIndex, 0) + " " + Form3.Text3.Text
    End If
    Unload Form3
  End If
End Sub

Private Sub Command6_Click()
  Load Form3
  Form3.Picture1.Visible = False
  Form3.Picture2.Visible = True
  Form3.Text3.TabIndex = 0
  Form3.Label3.Caption = TeVal.ls5Maske + ":"
  Form3.Show 1
  If Len(Form3.Text3.Text) > 0 Then
    SellCFrame.IrcConn.SendLine "mode " + SellCFrame.Caption + " +b " + Form3.Text3.Text
  End If
  Unload Form3
End Sub

Private Sub Form_Load()
  Dim I As Integer
  
  InitFormFont Me
  SellCFrame.ChanValForm = True
  
  Set Bannli = New IconList
  Bannli.Inst Picture1, VScroll1, 1, 16
  Bannli.SetColum 0, TeVal.ls5Maske, 260
  Bannli.AddColum TeVal.ls5Von, 70
  Bannli.AddColum TeVal.ls5Vor
  LoadBannList
  Bannli.EnableRef
  Bannli.Resize
  LoadFormStrs Me
  
  ModeTypeList(0) = "t"
  ModeTypeList(1) = "n"
  ModeTypeList(2) = "i"
  ModeTypeList(3) = "m"
  ModeTypeList(4) = "p"
  ModeTypeList(5) = "s"
  ModeTypeList(6) = "k"
  
  For I = 0 To 6
    Check1(I).Enabled = False
  Next
  Text1.Enabled = False
  
  SellCFrame.ModeRequesState = True
  SellCFrame.IrcConn.SendLine "mode " + SellCFrame.Caption
  
  CapLabel.Caption = ChatNoSignGlobal(SellCFrame.Caption)
  Me.Caption = TeVal.menValues
  Command1.Caption = TeVal.butApply
  Command2.Caption = TeVal.butCancel
  Command3.Caption = TeVal.butOK
End Sub

Public Sub LoadBannList()
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim LIndex As Integer
  
  Bannli.Clear True
  I = -1
  Do
    I2 = InStr(I3 + 2, SellCFrame.BannList, " ")
    If I2 = 0 Then Exit Do
    LIndex = Bannli.Add(Mid(SellCFrame.BannList, I + 4, I2 - I - 4), 62, False)
    I3 = InStr(I2, SellCFrame.BannList, "@")
    I = InStr(I3, SellCFrame.BannList, vbNewLine)
    Bannli.SetListText LIndex, 1, Mid(SellCFrame.BannList, I2 + 1, I3 - I2 - 1), True
    If Val(Mid(SellCFrame.BannList, I3 + 1, I - I3 - 1)) < MAX_LNG And Val(Mid(SellCFrame.BannList, I3 + 1, I - I3 - 1)) > 0 Then
      Bannli.SetListText LIndex, 2, FormatSec(DateDiff("s", DateAdd("s", Val(Mid(SellCFrame.BannList, I3 + 1, I - I3 - 1)) + GMTDiff, START_DATE), Now)), True
    End If
  Loop
  Bannli.ListIndex = 0
  Bannli.Refresh
End Sub

Public Sub GetData(Modes As String, Passw As String)
  Dim I As Integer
  
  For I = 0 To 6
    Check1(I).Enabled = True
    Check1(I).Value = 0
    ModeList(I) = False
  Next
  
  For I = 2 To Len(Modes)
    Select Case Mid(Modes, I, 1)
      Case ModeTypeList(0)
        Check1(0).Value = 1
        ModeList(0) = True
      Case ModeTypeList(1)
        Check1(1).Value = 1
        ModeList(1) = True
      Case ModeTypeList(2)
        Check1(2).Value = 1
        ModeList(2) = True
      Case ModeTypeList(3)
        Check1(3).Value = 1
        ModeList(3) = True
      Case ModeTypeList(4)
        Check1(4).Value = 1
        ModeList(4) = True
      Case ModeTypeList(5)
        Check1(5).Value = 1
        ModeList(5) = True
      Case ModeTypeList(6)
        Check1(6).Value = 1
        ModeList(6) = True
        Text1.Text = Passw
        ChanKey = Passw
    End Select
  Next
End Sub

Private Sub Form_Unload(Cancel As Integer)
  SellCFrame.ChanValForm = False
End Sub

Private Sub Picture1_Click()
  Bannli.Click
End Sub

Private Sub Picture1_DblClick()
  Bannli.DblClick
End Sub

Private Sub Picture1_KeyDown(KeyCode As Integer, Shift As Integer)
  Bannli.KeyDown KeyCode, Shift
  If KeyCode = 46 Then Command4_Click
End Sub

Private Sub Picture1_KeyPress(KeyAscii As Integer)
  Bannli.KeyPress KeyAscii
End Sub

Private Sub Picture1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Bannli.MouseDown Button, Shift, X, Y
End Sub

Private Sub Picture1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Bannli.MouseMove Button, Shift, X, Y
End Sub

Private Sub Picture1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Bannli.MouseUp Button, Shift, X, Y
End Sub

Private Sub Picture1_Paint()
  Bannli.Refresh
End Sub

Private Sub VScroll1_Change()
  Bannli.Change
End Sub

Private Sub VScroll1_Scroll()
  Bannli.Scroll
End Sub
