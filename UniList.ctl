VERSION 5.00
Begin VB.UserControl UniList 
   Appearance      =   0  '2D
   BackColor       =   &H80000005&
   ClientHeight    =   3600
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4800
   BeginProperty Font 
      Name            =   "MS Shell Dlg"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ScaleHeight     =   3600
   ScaleWidth      =   4800
   Begin VB.PictureBox Picture1 
      Appearance      =   0  '2D
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   3135
      Left            =   0
      ScaleHeight     =   3105
      ScaleWidth      =   3945
      TabIndex        =   0
      Top             =   0
      Width           =   3975
      Begin VB.VScrollBar VScroll1 
         Height          =   1815
         Left            =   0
         TabIndex        =   1
         Top             =   0
         Width           =   255
      End
   End
End
Attribute VB_Name = "UniList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private WithEvents IcListfeld As IconList
Attribute IcListfeld.VB_VarHelpID = -1

Event MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
Event MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
Event KeyDown(KeyCode As Integer, Shift As Integer)
Event KeyPress(KeyAscii As Integer)
Event DblClick()
Event Click()

Public Sort As Boolean

Private Sub IcListfeld_Change()
  RaiseEvent Click
End Sub

Private Sub IcListfeld_Click()
  RaiseEvent Click
End Sub

Private Sub IcListfeld_DblClick()
  RaiseEvent DblClick
End Sub

Private Sub IcListfeld_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  RaiseEvent MouseDown(Button, Shift, X, Y)
End Sub

Private Sub IcListfeld_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  RaiseEvent MouseUp(Button, Shift, X, Y)
End Sub

Private Sub Picture1_Click()
  IcListfeld.Click
End Sub

Private Sub Picture1_DblClick()
  IcListfeld.DblClick
End Sub

Private Sub UserControl_Initialize()
  Set IcListfeld = New IconList
  IcListfeld.Inst Picture1, VScroll1, 2, 0, False, -1
  IcListfeld.ListIndex = -1
  IcListfeld.EnableRef
End Sub

Private Sub Picture1_KeyDown(KeyCode As Integer, Shift As Integer)
  RaiseEvent KeyDown(KeyCode, Shift)
  IcListfeld.KeyDown KeyCode, Shift
End Sub

Private Sub Picture1_KeyPress(KeyAscii As Integer)
  RaiseEvent KeyPress(KeyAscii)
  IcListfeld.KeyPress KeyAscii
End Sub

Private Sub Picture1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  IcListfeld.MouseDown Button, Shift, X, Y
End Sub

Private Sub Picture1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  IcListfeld.MouseMove Button, Shift, X, Y
End Sub

Private Sub Picture1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  IcListfeld.MouseUp Button, Shift, X, Y
End Sub

Private Sub Picture1_Paint()
  IcListfeld.Refresh -1, -1, 0, False, 1
End Sub

Private Sub UserControl_Resize()
  Picture1.Width = UserControl.ScaleWidth
  Picture1.Height = UserControl.ScaleHeight
  IcListfeld.Resize
End Sub

Private Sub VScroll1_Change()
  IcListfeld.Change
End Sub

Private Sub VScroll1_Scroll()
  IcListfeld.Scroll
End Sub

Public Sub AddColum(Caption As String, Optional Width As Integer = -1, Optional NoScrRef As Boolean)
  IcListfeld.AddColum Caption, Width, NoScrRef
End Sub

Public Sub SetColum(Cindex As Integer, Caption As String, Optional Width As Integer = -1)
  IcListfeld.SetColum Cindex, Caption, Width
End Sub

Public Sub RemColum(Cindex As Integer)
  IcListfeld.RemColum Cindex
End Sub

Public Sub GotoItem(Text As String)
  IcListfeld.GotoItem Text
End Sub

Public Property Let ListIndex(TempVal As Integer)
  IcListfeld.ListIndex = TempVal
  IcListfeld.ScrollToIndex
End Property
Public Property Get ListIndex() As Integer
  ListIndex = IcListfeld.ListIndex
End Property

Public Property Let List(ByVal Index As Integer, TempVal As String)
  IcListfeld.SetListText Index, 0, TempVal, False
End Property
Public Property Get List(ByVal Index As Integer) As String
  List = IcListfeld.GetListText(Index, 0)
End Property

Public Function Additem(Text As String, Optional Icon As Integer = -1, Optional Refresh As Boolean = True) As Integer
  IcListfeld.Add Text, Icon, Refresh, True, Sort
End Function

Public Sub RemoveItem(Index As Integer)
  IcListfeld.Remove Index
End Sub

Public Function ListCount() As Integer
  ListCount = IcListfeld.ListCount
End Function

Public Sub Clear(Optional NoRefr As Boolean)
  IcListfeld.Clear NoRefr
  IcListfeld.ListIndex = -1
End Sub

Public Sub ReSortList()
  IcListfeld.ReSortList
End Sub

