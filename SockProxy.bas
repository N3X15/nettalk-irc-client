Attribute VB_Name = "SockProxy"
Option Explicit

Public Sub SendProxyLogin(wSock As CSocket, HostName As String, IP As String, Port As Long, ProxyType As Long)
  Dim SocksHeader As String
  
  If ProxyType = 40 Or Len(IP) > 0 Then 'Sock v4
    If Len(IP) = 0 Then IP = AddrToIP(HostName)
    SocksHeader = Chr(&H4)                                    '1 - h04 - Socksversion 4
    SocksHeader = SocksHeader + Chr(&H1)                      '2 - h01 - CMD: Connect
    SocksHeader = SocksHeader + IntToStr(Port, 2)             '3,4 - Port des Zielhosts
    SocksHeader = SocksHeader + GetBinIP(IP)                  '5,6,7,8 - IP des Zielhosts
    SocksHeader = SocksHeader + Chr(&H0)                      'Letztes Byte - h00 - Abschluss
  Else  'Sock v4a
    SocksHeader = Chr(&H4)                                    '1 - h04 - Socksversion 4
    SocksHeader = SocksHeader + Chr(&H1)                      '2 - h01 - CMD: Connect
    SocksHeader = SocksHeader + IntToStr(Port, 2)             '3,4 - Port des Zielhosts
    SocksHeader = SocksHeader + Chr(0) + Chr(0) + Chr(0) + Chr(1) '5,6,7,8 - 0.0.0.1 ("IP")
    SocksHeader = SocksHeader + Chr(&H0)                      'Letztes Byte - h00 - Abschluss
    SocksHeader = SocksHeader + HostName
    SocksHeader = SocksHeader + Chr(&H0)                      'Letztes Byte - h00 - Abschluss
  End If
  
  wSock.SendData SocksHeader
End Sub

Private Function GetBinIP(IP As String) As String
  Dim I As Integer
  Dim I2 As Integer
  Dim PCount As Integer
  Dim Result As String
  Dim TVal As Long
  
  For PCount = 1 To 4
    I2 = I
    I = InStr(I + 1, IP, ".")
    If I = 0 Then I = Len(IP) + 1
    If I = I2 Or I - I2 > 4 Then Exit For
    TVal = Val(Mid(IP, I2 + 1, I - I2 - 1))
    If TVal < 256 Then Result = Result + Chr(TVal)
  Next
  GetBinIP = Result
End Function
