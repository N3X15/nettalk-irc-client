Attribute VB_Name = "FormSubClassing"
Option Explicit

Global Const GWLP_WNDPROC = -4

Public Const MYWM_CALLBACK = &H4000
Public MYWM_TASKBARCREATED As Long

Public Declare Function RegisterWindowMessage Lib "user32" _
Alias "RegisterWindowMessageA" (ByVal lpString As String) As Long

Private oldWinMain As Long
Private hwnd As Long

Public Sub StartListening(ByVal FormhWnd As Long)
  hwnd = FormhWnd
  oldWinMain = SetWindowLongA(hwnd, GWLP_WNDPROC, AddressOf WinMain)
End Sub

Public Sub StopListening()
  SetWindowLongA hwnd, GWLP_WNDPROC, oldWinMain
End Sub

Public Function WinMain(ByVal hwnd As Long, ByVal Msg As Long, ByVal WParam As Long, ByVal lParam As Long) As Long
  If hwnd = Form1.hwnd Then
    Select Case Msg
      Case MYWM_CALLBACK
        If WParam = 1 Then
          Select Case lParam
            Case WM_LBUTTONUP
              Form1.ShowMainWindow
            Case WM_RBUTTONUP
              SetForegroundWindow Form1.hwnd
              Form1.PopupMenuEx Form1.menH4, , , , Form1.menH4open
          End Select
        End If
        Exit Function
      Case MYWM_TASKBARCREATED
        Form1.AddTrayIcon
        Exit Function
      Case WM_ACTIVATE
        If WParam Then
          Unload Form4
          Form1.IsNowActiv
        End If
      Case wheel_msg_id
        MouseScrollProc hwnd, Msg, WParam, lParam
      Case 793 ' 4. und 5. Maustaste
        If lParam = -2147352576 Then
          Form1.ChangeFrontFrame 2
        ElseIf lParam = -2147418112 Then
          Form1.ChangeFrontFrame 3
        End If
    End Select
  End If
  WinMain = CallWindowProcA(oldWinMain, hwnd, Msg, WParam, lParam)
End Function
