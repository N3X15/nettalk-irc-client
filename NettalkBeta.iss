[Setup]
AppName=Nettalk
AppVerName=Nettalk 6.5.1 Beta
AppPublisher=Nicolas Kruse
AppPublisherURL=http://www.ntalk.de
AppSupportURL=http://www.ntalk.de
AppUpdatesURL=http://update.ntalk.de
DefaultDirName={userdocs}\Nettalk6Beta
DisableProgramGroupPage=yes
ShowLanguageDialog=yes
WizardImageFile=Setup.bmp
OutputBaseFilename=Nettalk65BetaSetup


[Languages]
Name: en; MessagesFile: "compiler:Default.isl"
Name: de; MessagesFile: "compiler:Languages\German.isl"
Name: nl; MessagesFile: "compiler:Languages\Dutch.isl"
Name: ru; MessagesFile: "compiler:Languages\Russian.isl"
Name: hu; MessagesFile: "compiler:Languages\Hungarian.isl"
Name: es; MessagesFile: "compiler:Languages\Spanish.isl"


[Tasks]
Name: "desktopicon"; Description: "Verkn�pfung auf dem Desktop erstellen"; Languages: de; GroupDescription: "Zus�tzliche Verkn�pfungen:"; MinVersion: 4,4
Name: "desktopicon"; Description: "Create a shortcut on the desktop"; Languages: en nl ru hu es; GroupDescription: "Additional shortcuts:"; MinVersion: 4,4

Name: "autoupdate"; Description: "W�chentlich nach Updates schauen"; Languages: de; GroupDescription: "Weitere Einstellungen:"; MinVersion: 4,4
Name: "autoupdate"; Description: "Check weekly for Updates"; Languages: en nl ru hu es; GroupDescription: "Additional preferences:"; MinVersion: 4,4


[Files]
Source: "Nettalk.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "PORT.DLL"; DestDir: "{app}"; Flags: ignoreversion; OnlyBelowVersion: 0,5.0
Source: "Deutsch.lng"; DestDir: "{app}"; Flags: ignoreversion
Source: "English.lng"; DestDir: "{app}"; Flags: ignoreversion
Source: "Nederlands.lng"; DestDir: "{app}"; Flags: ignoreversion
Source: "Russian.lng"; DestDir: "{app}"; Flags: ignoreversion
Source: "Spanish.lng"; DestDir: "{app}"; Flags: ignoreversion
Source: "Magyar.lng"; DestDir: "{app}"; Flags: ignoreversion
Source: "Simplified Chinese.lng"; DestDir: "{app}"; Flags: ignoreversion
Source: "Msg.wav"; DestDir: "{app}"; Flags: ignoreversion
Source: "Nettalkgb.jpg"; DestDir: "{app}"; Flags: ignoreversion
Source: "Words.dat"; DestDir: "{app}"; Flags: ignoreversion
Source: "servers.ini"; DestDir: "{app}"; Flags: ignoreversion
Source: "Servers.srv"; DestDir: "{app}"; Flags: ignoreversion
Source: "DarkGlass.skn"; DestDir: "{app}"; Flags: ignoreversion
Source: "GreenCrystal.skn"; DestDir: "{app}"; Flags: ignoreversion
Source: "Kontrast.skn"; DestDir: "{app}"; Flags: ignoreversion
Source: "Classic.skn"; DestDir: "{app}"; Flags: ignoreversion
Source: "msvbvm60.dll"; DestDir: "{sys}"; OnlyBelowVersion: 0,6; Flags: restartreplace uninsneveruninstall sharedfile regserver

Source: "Nettalk.ini"; DestDir: "{app}"

Source: "ShortCut.txt"; DestDir: "{app}"

Source: "Language-DE.ini"; DestName: "Language.ini"; Languages: de; DestDir: "{app}"
Source: "Language-EN.ini"; DestName: "Language.ini"; Languages: en; DestDir: "{app}"
Source: "Language-NL.ini"; DestName: "Language.ini"; Languages: nl; DestDir: "{app}"
Source: "Language-RU.ini"; DestName: "Language.ini"; Languages: ru; DestDir: "{app}"
Source: "Language-HU.ini"; DestName: "Language.ini"; Languages: hu; DestDir: "{app}"
Source: "Language-ES.ini"; DestName: "Language.ini"; Languages: es; DestDir: "{app}"

Source: "AutoUpdate\Update.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "AutoUpdate\Update.ini"; DestDir: "{app}"; Flags: ignoreversion

Source: "Runmode.ini"; DestDir: "{app}"; Flags: ignoreversion
Source: "script.txt"; DestDir: "{app}"; Flags: ignoreversion; Tasks: autoupdate


[UninstallDelete]
Type: filesandordirs; Name: "{app}\Preferences"
Type: files; Name: "{app}\script.txt"
Type: files; Name: "{app}\ScriptValues.ini"


[Icons]
Name: "{userprograms}\Nettalk Beta"; Filename: "{app}\Nettalk.exe"
Name: "{userdesktop}\Nettalk Beta"; Filename: "{app}\Nettalk.exe"; MinVersion: 4,4; Tasks: desktopicon


[Run]
Filename: "{app}\Nettalk.exe"; Parameters: "irc.ntalk.de"; Languages: de; Description: "Den offiziellen Nettalk-Channel betreten"; Flags: postinstall nowait unchecked
Filename: "{app}\Nettalk.exe"; Parameters: "irc.ntalk.de"; Languages: en nl ru hu es; Description: "Enter the official Nettalk channel"; Flags: postinstall nowait unchecked

