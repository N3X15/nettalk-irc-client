Attribute VB_Name = "LogModul"
Option Explicit

Public FilePPosPriv As Long
Public FilePPosPubl As Long
Public LogDateTrigger As String

Private oldDate As String
Private LastTime As String
Private LastDate As String
Private FrPubFile As Integer
Private FrPriFile As Integer

Private DateStrOrd(7) As Long

Const BUFFER_SIZE = 4096
Const CLUSTER_SIZE As Long = 65536  '64 KByte
Const END_CHAR As Byte = 46 'Asc(46) = "."
Const READ_LINE_COUNT = 3072 '3 KByte

Public Sub CloseLogFiles()
  If FrPriFile > 0 Then Close #FrPriFile
  If FrPubFile > 0 Then Close #FrPubFile
End Sub

Public Sub WriteLog(TextIn As String, CFrame As ChatFrame, Optional Wispered As String)
  Dim DAT As String
  Dim I As Double
  Dim Text As String
  
  On Error Resume Next
  If CFrame.ConnTyp = 2 Then
    Exit Sub
  ElseIf CFrame.ConnTyp = 0 Then  ' #IRC
    If CFrame.IrcConn.Silent = True Then Exit Sub
  End If

  Text = EncodeUTF8(TextIn)

  If CFrame.FrameType = cfPrivate Or Len(Wispered) > 0 Then
    If LogmodePrivate = False Then Exit Sub
    If FrPriFile = 0 Then
      FrPriFile = FreeFile
      Open UserPath + "PrivateLog.dat" For Binary As #FrPriFile
    End If
    If FilePPosPriv = 0 Then
      DAT = Space(10)
      Get FrPriFile, 1, DAT
      FilePPosPriv = Val(DAT)
      If FilePPosPriv = 0 Then
        DAT = vbNewLine
        Put FrPriFile, 11, DAT
        FilePPosPriv = 13
      End If
    ElseIf FilePPosPriv > LOF(FrPriFile) Then
      I = Int(FilePPosPriv / CLUSTER_SIZE + 1) * CLUSTER_SIZE
      If I > PrivLogMaxSize * CLng(1024) Then I = PrivLogMaxSize * CLng(1024)
      Put FrPriFile, I, END_CHAR
    End If
    If Len(Wispered) > 0 And Len(Text) > 1 Then
      DAT = Chr(182) + Wispered + ":#" + Text + vbNewLine
    Else
      DAT = Chr(182) + CFrame.Caption + ":" + Text + vbNewLine
    End If
    If Len(DAT) + FilePPosPriv - 1 > PrivLogMaxSize * CLng(1024) Then
      Put FrPriFile, FilePPosPriv, vbNewLine + Chr(27)
      FilePPosPriv = 13
    End If
    Put FrPriFile, FilePPosPriv, DAT
    FilePPosPriv = FilePPosPriv + Len(DAT)
    DAT = Space(10)
    Mid(DAT, 1) = CStr(FilePPosPriv)
    Put FrPriFile, 1, DAT
    If Err <> 0 Then
      CFrame.PrintText "- " + Err.Description + " (WriteLog Private)", CoVal.ErrorMSG, False, True
      Close #FrPriFile
      FrPriFile = FreeFile
      Open UserPath + "PrivateLog.dat" For Binary As #FrPriFile
      Put FrPriFile, 1, DAT
    End If
  Else
    If LogmodeRoom = False Then Exit Sub
    If FrPubFile = 0 Then
      FrPubFile = FreeFile
      Open UserPath + "PublicLog.dat" For Binary As #FrPubFile
    End If
    If FilePPosPubl = 0 Then
      DAT = Space(10)
      Get FrPubFile, 1, DAT
      FilePPosPubl = Val(DAT)
      If FilePPosPubl = 0 Then
        DAT = vbNewLine
        Put FrPubFile, 11, DAT
        FilePPosPubl = 13
      End If
    ElseIf FilePPosPubl > LOF(FrPubFile) Then
      I = Int(FilePPosPubl / CLUSTER_SIZE + 1) * CLUSTER_SIZE
      If I > PublLogMaxSize * CLng(1024) Then I = PublLogMaxSize * CLng(1024)
      Put FrPubFile, I, END_CHAR
    End If
    DAT = Chr(182) + CFrame.Caption + ":" + Text + vbNewLine
    If Len(DAT) + FilePPosPubl - 1 > PublLogMaxSize * CLng(1024) Then
      Put FrPubFile, FilePPosPubl, vbNewLine + Chr(27)
      FilePPosPubl = 13
    End If
    Put FrPubFile, FilePPosPubl, DAT
    FilePPosPubl = FilePPosPubl + Len(DAT)
    DAT = Space(10)
    Mid(DAT, 1) = CStr(FilePPosPubl)
    Put FrPubFile, 1, DAT
    If Err <> 0 Then
      CFrame.PrintText "- " + Err.Description + " (WriteLog Public)", CoVal.ErrorMSG, False, True
      Close #FrPubFile
      FrPubFile = FreeFile
      Open UserPath + "PublicLog.dat" For Binary As #FrPubFile
      Put FrPubFile, 1, DAT
    End If
  End If
End Sub

Public Sub LoadFromLog(Caption As String, Optional FrType As Integer = -1, Optional SMode As Boolean, Optional StartDate As String)
  Dim ChFrame As ChatFrame
  Dim I As Integer
  
  If Len(Trim(Caption)) = 0 Then Exit Sub
  
  For I = 1 To ChatFrames.Count
    If ChatFrames(I).FrameType = cfLog And ChatFrames(I).Caption = Caption Then
      Set ChFrame = ChatFrames(I)
      ChFrame.TextF.ClearText
      Exit For
    End If
  Next
  
  If ChFrame Is Nothing Then
    Set ChFrame = New ChatFrame
    If SMode = True Then
      ChFrame.Caption = TeVal.SearchResults
    Else
      ChFrame.Caption = Caption
    End If
    ChFrame.Inst cfLog, -1
    ChatFrames.Add ChFrame
  End If
  SetFrontFrame ChFrame
  
  oldDate = ""
  If FrType = -1 Then
    If ChatNoSignGlobal(Caption) = Caption Then FrType = 4 Else FrType = 1
  End If
  
  On Error Resume Next
  If FrType = 4 Or SMode = True Then
    If FrPriFile = 0 Then
      FrPriFile = FreeFile
      Open UserPath + "PrivateLog.dat" For Binary As #FrPriFile
    End If
    LogFLoad FrPriFile, FilePPosPriv, Caption, ChFrame, SMode, StartDate
  End If
  If FrType <> 4 Or SMode = True Then
    If FrPubFile = 0 Then
      FrPubFile = FreeFile
      Open UserPath + "PublicLog.dat" For Binary As #FrPubFile
    End If
    LogFLoad FrPubFile, FilePPosPubl, Caption, ChFrame, SMode, StartDate
  End If
  If Err > 0 Then ChFrame.PrintText "- " + Err.Description, CoVal.ErrorMSG, True, True, , , , True
  On Error GoTo 0
  
  If ChFrame.TextF.LineCount < 3 Then ChFrame.PrintText "- " + TeVal.NoLogs, CoVal.ErrorMSG, True, True, , , , True
  ChFrame.TextF.Scroll True
  ChFrame.TextF.RefText
End Sub

Public Sub LogFLoad(FrFile As Integer, FilePos As Long, Caption As String, ChFrame As ChatFrame, SMode As Boolean, Optional InStartDate As String)
  Dim I As Long
  Dim I2 As Long
  Dim I3 As Long
  Dim DAT As String
  Dim DAT2 As String
  Dim ReadBuff As String
  Dim BuffI As Long
  Dim LastBI As Long
  Dim LineCount As Long
  Dim LastLC As Long
  Dim TempList(READ_LINE_COUNT) As String
  Dim StartDate As String
  Dim LastDate As String
  Dim TempLDate As Date
  Dim tempDate As Date
  
  DateStrOrd(0) = 7
  DateStrOrd(1) = 8
  DateStrOrd(2) = 9
  DateStrOrd(3) = 10
  DateStrOrd(4) = 4
  DateStrOrd(5) = 5
  DateStrOrd(6) = 1
  DateStrOrd(7) = 2
  
  If Len(InStartDate) Then
    StartDate = Left(InStartDate, 10)
    If Mid(StartDate, 2, 1) = "." Or Len(StartDate) = 1 Then StartDate = "0" + StartDate
    If Mid(StartDate, 5, 1) = "." Or Len(StartDate) = 4 Then
      StartDate = Left(StartDate, 3) + "0" + Mid(StartDate, 4)
    ElseIf Len(StartDate) = 2 Then
      StartDate = Left(StartDate, 2) + Mid(Format(Now, "dd.mm.yyyy"), 3, 3)
    End If
    DAT = Mid(StartDate, 7)
    StartDate = Left(StartDate, 5) + Mid(Format(Now, "dd.mm.yyyy"), 6, 5 - Len(DAT)) + DAT
'    If IsDate(StartDate) Then
'      ChFrame.PrintText "< " + CStr(DateAdd("d", -1, StartDate)), CoVal.Link, True, True, 10, True, CStr(DateAdd("d", -1, StartDate)), True
'      ChFrame.PrintText " | ", CoVal.Link, True, False, , , , True
'      ChFrame.PrintText CStr(DateAdd("d", 1, StartDate)) + " >", CoVal.Link, True, False, 10, True, CStr(DateAdd("d", 1, StartDate)), True
'    End If
  End If
  
  ReadBuff = Space(10)
  Get FrFile, 1, ReadBuff
  FilePos = Val(ReadBuff)
  BuffI = FilePos
  ReadBuff = ""
  Do
    Do
      If Len(ReadBuff) <> BUFFER_SIZE Then ReadBuff = Space(BUFFER_SIZE)
      I2 = 0
      Do
        I = I2
        I2 = InStr(I2 + 1, DAT, vbNewLine)
      Loop Until I2 = 0 Or I2 > Len(DAT) - 2
      If I > 0 Then Exit Do
      BuffI = BuffI - Len(ReadBuff)
      If I3 = 1 Then I3 = 2: DAT = ""
      If BuffI < 1 Then
        ReadBuff = Space(Len(ReadBuff) + BuffI - 1)
        BuffI = 1
        If I3 = 0 Then I3 = 1
      End If
      Get FrFile, BuffI, ReadBuff
      If Right(ReadBuff, 1) = Chr(0) Then Exit Do
      DAT = ReadBuff + DAT
      If BuffI = 1 Then BuffI = LOF(FrFile)
    Loop
    If Right(ReadBuff, 1) = Chr(0) Then Exit Do
    
    If I3 = 2 And (BuffI + Len(ReadBuff) < FilePos Or Len(ReadBuff) > LOF(FrFile)) Then
      BuffI = FilePos
      Exit Do
    End If
          
    I2 = InStr(I + 1, DAT, vbNewLine)
    If I2 = 0 Then I2 = Len(DAT) + 1
    DAT2 = Mid(DAT, I + 2, I2 - I - 2)
    DAT = Left(DAT, I - 1)
    I = InStr(1, DAT2, ":")
    If I > 1 Then
      If SMode = False Then
        If LCase(Mid(DAT2, 2, I - 2)) = LCase(Caption) Then
          If Mid(DAT2, I + 11, 1) = " " Then
            LastDate = Mid(DAT2, I + 1, 10)
          Else
            LastDate = Mid(DAT2, I + 2, 10)
          End If
          If Len(StartDate) Then
            If DateStrComp(StartDate, LastDate) Then
              TempList(LineCount) = DAT2
              LineCount = LineCount + 1
            End If
          Else
            TempList(LineCount) = DAT2
            LineCount = LineCount + 1
          End If
        End If
      Else
        If InStr(1, DAT2, Caption, vbTextCompare) > 0 Then
          TempList(LineCount) = DAT2
          LineCount = LineCount + 1
        End If
      End If
    End If
  Loop Until LineCount > READ_LINE_COUNT

  If StartDate = "" Then StartDate = Format(Now, "dd.mm.yyyy")

  If ConvToDate(tempDate, StartDate) And ConvToDate(TempLDate, LastDate) And LineCount > 0 Then
    
    If tempDate > TempLDate Then ChFrame.PrintText "< " + CStr(TempLDate), CoVal.Link, True, True, 10, True, LastDate, True
    ChFrame.PrintText " | ", CoVal.Link, True, tempDate <= TempLDate, , , , True
    ChFrame.PrintText CStr(DateAdd("d", 1, tempDate)) + " >", CoVal.Link, True, False, 10, True, Format(DateAdd("d", 1, tempDate), "dd.mm.yyyy"), True

    For I = LineCount - 1 To 0 Step -1
      DrawFromLog DecodeUTF8(TempList(I)), ChFrame
    Next
  
    If tempDate > TempLDate Then ChFrame.PrintText "< " + CStr(TempLDate), CoVal.Link, True, True, 10, True, LastDate, True
    ChFrame.PrintText " | ", CoVal.Link, True, tempDate <= TempLDate, , , , True
    ChFrame.PrintText CStr(DateAdd("d", 1, tempDate)) + " >", CoVal.Link, True, False, 10, True, Format(DateAdd("d", 1, tempDate), "dd.mm.yyyy"), True
  Else
    For I = LineCount - 1 To 0 Step -1
      DrawFromLog DecodeUTF8(TempList(I)), ChFrame
    Next
  End If

        
'    LastLC = LineCount
'    ReadBuff = ""
'    Do
'      DAT = ReadBuff
'      ReadBuff = Space(BUFFER_SIZE)
'      Do
'        I = InStr(1, DAT, vbNewLine)
'        If I > 0 Then Exit Do
'        If BuffI = 0 Then BuffI = 1
'        Get FrFile, BuffI, ReadBuff
'        BuffI = BuffI + Len(ReadBuff)
'        If BuffI > LOF(FrFile) Then
'          BuffI = 1
'        End If
'        DAT = DAT + ReadBuff
'      Loop
'
'      ReadBuff = Mid(DAT, I + 2)
'      If Left(DAT, 1) = Chr(182) And Len(DAT) > 1 Then
'        DAT = Mid(DAT, 1, I - 1)
'        I = InStr(1, DAT, ":")
'        If I > 0 Then
'          If SMode = False Then
'            If LCase(Mid(DAT, 2, I - 2)) = LCase(Caption) Then
'              TestList(LineCount) = TestList(LineCount) + "  >" + CStr(Len(DAT))
'              LineCount = LineCount - 1
'              DrawFromLog DAT, ChFrame
'            End If
'          Else
'            If InStr(1, DAT, Caption, vbTextCompare) > 0 Then
'              LineCount = LineCount - 1
'              DrawFromLog DAT, ChFrame
'            End If
'          End If
'        End If
'      End If
'      If LastLC > 0 Then
'        Form1.UserInfoPic.Line (1, 1)- _
'        (Form1.UserInfoPic.ScaleWidth * (LastLC - LineCount) / LastLC - 1, Form1.UserInfoPic.ScaleHeight - 1), _
'        &H8000000D, BF
'        Form1.UserInfoPic.Refresh
'      End If
'    Loop Until LineCount < 1
End Sub

Private Function DateStrComp(Date1 As String, Date2 As String) As Boolean
  Dim I As Long
  Dim Byte1 As Byte
  Dim Byte2 As Byte
  
  DateStrComp = True
  For I = 0 To 7
    Byte1 = Asc(Mid(Date1, DateStrOrd(I), 1))
    Byte2 = Asc(Mid(Date2, DateStrOrd(I), 1))
    If Byte1 > Byte2 Then
      DateStrComp = True
      Exit For
    ElseIf Byte1 < Byte2 Then
      DateStrComp = False
      Exit For
    End If
  Next
End Function

Private Sub DrawFromLog(Text As String, ChFrame As ChatFrame)
  Dim lDate As String
  Dim lTime As String
  Dim lVon As String
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim TColor As Long
  Dim lastTD As Date
  Dim lTD As Date
  
  I = InStr(1, Text, ":")
  I2 = InStr(I + 1, Text, " ")
  If I2 = 0 Then Exit Sub
  lDate = Mid(Text, I + 1, I2 - I - 1)
  If Left(lDate, 1) = "#" And Len(lDate) > 1 Then
    TColor = CoVal.FlusterVon
    lDate = Mid(lDate, 2)
  Else
    TColor = CoVal.NormalVon
  End If
  I = InStr(I2 + 1, Text, " ")
  If I = 0 Then Exit Sub
  lTime = Mid(Text, I2 + 1, I - I2 - 1)
  
  If ConvToDate(lTD, lDate + " " + lTime) = False Then Exit Sub
  ConvToDate lastTD, LastDate + " " + LastTime
  
  If oldDate <> lDate Then
    If ChFrame.TextF.LineCount > 0 Then
      ChFrame.PrintText " ", 0, True, True, , , , True
      ChFrame.PrintText " ", 0, True, True, , , , True
    End If
    ChFrame.PrintText lDate, CoVal.ClientMSG, True, True, , , , True
    ChFrame.TextF.SetDivLine CoVal.ClientMSG
    ChFrame.PrintText " ", 0, True, True, , , , True
    oldDate = lDate
  ElseIf DateDiff("n", lastTD, lTD) > 5 Then
    ChFrame.PrintText "- " + FormatSec(DateDiff("s", lastTD, lTD)), CoVal.ClientMSG, True, True, , , , True
  End If
  
  LastDate = lDate
  LastTime = lTime
  
  If Mid(Text, I + 1, 1) = "<" Then
    I2 = InStr(I + 1, Text, ">")
    If I2 = 0 Then Exit Sub
    ChFrame.PrintText "(" + lTime + ")", CoVal.TimeStamp, True, True, , , , True
    ChFrame.PrintText Mid(Text, I + 1, I2 - I), TColor, True, False, , , , True
    ChFrame.PrintText Mid(Text, I2 + 2), CoVal.NormalText, True, False, , , , True
  ElseIf Mid(Text, I + 1, 1) = "*" Then
    ChFrame.PrintText "(" + lTime + ")", CoVal.TimeStamp, True, True, , , , True
    ChFrame.PrintText Mid(Text, I + 1), CoVal.NormalText, True, False, , , , True
  Else
    ChFrame.PrintText "(" + lTime + ")", CoVal.TimeStamp, True, True, , , , True
    ChFrame.PrintText Mid(Text, I + 1), CoVal.ClientMSG, True, False, , , , True
  End If
End Sub
