Attribute VB_Name = "Tools"
Option Explicit

Public Declare Function InitCommonControls Lib "comctl32.dll" () As Long

Public AppUnloading As Boolean

Public Function CutDirFromPath(Path As String, Optional Divider As String = "\", Optional RightPart As Boolean) As String
  Dim I As Integer
  Dim I2 As Integer
  Do
    I2 = I
    I = InStr(I + 1, Path, Divider)
  Loop Until I = 0
  If RightPart = True Then
    If Len(Path) > I2 Then CutDirFromPath = Mid(Path, I2 + 1)
  Else
    If I2 > 0 Then CutDirFromPath = Left(Path, I2 - 1)
  End If
End Function

Public Function FormatSec(ByVal Sec As Double) As String
  Dim outData As String
  Select Case Sec
    Case Is >= 60 ^ 2
      outData = CStr(Int(Sec / 60 ^ 2)) + " h, " + CStr(Int(Sec / 60) - Int(Sec / 60 ^ 2) * 60) + " min"
    Case Is >= 60
      outData = CStr(Int(Sec / 60)) + " min, " + CStr(Int(Sec) - Int(Sec / 60) * 60) + " sec"
    Case Else
      outData = CStr(Int(Sec)) + " sec"
  End Select
  FormatSec = outData
End Function

Public Function cVersions(NewVersion As String, OldVersion As String) As Boolean
  Dim I As Integer
  Dim I2 As Integer
  Dim J As Integer
  Dim J2 As Integer
  Dim ValI As Integer
  Dim ValJ As Integer
  Do
    I2 = I
    J2 = J
    I = InStr(I + 1, NewVersion, ".")
    J = InStr(J + 1, OldVersion, ".")
    If I = 0 Then I = Len(NewVersion) + 1
    If J = 0 Then J = Len(OldVersion) + 1
    If I * J = 0 Then cVersions = False: Exit Do
    If I - I2 > 0 And I - I2 < 6 Then ValI = Val(Mid(NewVersion, I2 + 1, I - I2 - 1)) Else ValI = 0
    If J - J2 > 0 And J - I2 < 6 Then ValJ = Val(Mid(OldVersion, J2 + 1, J - J2 - 1)) Else ValJ = 0
    If ValI > ValJ Then
      cVersions = True
      Exit Do
    ElseIf ValJ > ValI Then
      cVersions = False
      Exit Do
    End If
  Loop Until I = Len(NewVersion) + 1 Or J = Len(OldVersion) + 1
End Function
