VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  'Fest Einfach
   Caption         =   "Form1"
   ClientHeight    =   9705
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13365
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   647
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   891
   StartUpPosition =   3  'Windows-Standard
   Begin VB.PictureBox UpFrame 
      BackColor       =   &H80000005&
      BorderStyle     =   0  'Kein
      Height          =   4395
      Index           =   2
      Left            =   7320
      ScaleHeight     =   4395
      ScaleWidth      =   5010
      TabIndex        =   15
      TabStop         =   0   'False
      Top             =   4800
      Visible         =   0   'False
      Width           =   5010
      Begin VB.ListBox List3 
         ForeColor       =   &H80000011&
         Height          =   1035
         Left            =   240
         TabIndex        =   21
         Top             =   960
         Width           =   4095
      End
      Begin VB.PictureBox Picture3 
         AutoRedraw      =   -1  'True
         ForeColor       =   &H8000000D&
         Height          =   375
         Left            =   240
         ScaleHeight     =   315
         ScaleWidth      =   4035
         TabIndex        =   18
         TabStop         =   0   'False
         Top             =   2640
         Width           =   4095
      End
      Begin VB.PictureBox Picture2 
         AutoRedraw      =   -1  'True
         ForeColor       =   &H8000000D&
         Height          =   375
         Left            =   240
         ScaleHeight     =   315
         ScaleWidth      =   4035
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   480
         Width           =   4095
      End
      Begin VB.Label Label8 
         BackColor       =   &H80000005&
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   2400
         Width           =   3975
      End
      Begin VB.Label Label7 
         BackColor       =   &H80000005&
         Caption         =   "Over all:"
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   240
         Width           =   3975
      End
      Begin VB.Label Label5 
         BackColor       =   &H80000005&
         Height          =   1095
         Left            =   240
         TabIndex        =   16
         Top             =   3120
         Width           =   4095
      End
   End
   Begin VB.TextBox PluginData 
      Height          =   285
      Left            =   3840
      TabIndex        =   3
      Top             =   5520
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.PictureBox UpFrame 
      BackColor       =   &H80000005&
      BorderStyle     =   0  'Kein
      Height          =   4395
      Index           =   1
      Left            =   7200
      ScaleHeight     =   4395
      ScaleWidth      =   5010
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   5010
      Begin VB.PictureBox Picture4 
         BackColor       =   &H80000005&
         BorderStyle     =   0  'Kein
         Height          =   435
         Left            =   315
         ScaleHeight     =   435
         ScaleWidth      =   4005
         TabIndex        =   23
         Top             =   525
         Visible         =   0   'False
         Width           =   4005
      End
      Begin VB.CheckBox Check1 
         BackColor       =   &H80000005&
         Caption         =   "All (recommended)"
         Height          =   220
         Left            =   2640
         TabIndex        =   22
         Top             =   240
         Value           =   2  'Zwischenzustand
         Width           =   1695
      End
      Begin VB.ListBox List2 
         Height          =   1860
         Left            =   240
         Style           =   1  'Kontrollkästchen
         TabIndex        =   2
         Top             =   2280
         Width           =   4215
      End
      Begin VB.ListBox List1 
         Height          =   1410
         Left            =   240
         Style           =   1  'Kontrollkästchen
         TabIndex        =   1
         Top             =   480
         Width           =   4215
      End
      Begin VB.Label Label4 
         BackColor       =   &H80000005&
         Caption         =   "Additional available files:"
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   2040
         Width           =   4335
      End
      Begin VB.Label Label2 
         BackColor       =   &H80000005&
         Caption         =   "Available updated files:"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   240
         Width           =   4335
      End
   End
   Begin VB.PictureBox UpFrame 
      BackColor       =   &H80000005&
      BorderStyle     =   0  'Kein
      Height          =   4395
      Index           =   0
      Left            =   1920
      ScaleHeight     =   4395
      ScaleWidth      =   5010
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   5010
      Begin VB.TextBox Text1 
         Height          =   3285
         Left            =   240
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertikal
         TabIndex        =   0
         Top             =   480
         Width           =   4455
      End
      Begin VB.Label Label3 
         BackColor       =   &H80000005&
         ForeColor       =   &H80000008&
         Height          =   375
         Left            =   240
         TabIndex        =   13
         Top             =   3960
         Width           =   4455
      End
      Begin VB.Label Label1 
         BackColor       =   &H80000005&
         Caption         =   "Label1"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   240
         Width           =   4335
      End
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H80000005&
      BorderStyle     =   0  'Kein
      Height          =   4395
      Left            =   0
      ScaleHeight     =   4395
      ScaleWidth      =   1800
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   0
      Width           =   1800
   End
   Begin VB.CommandButton Command1 
      Caption         =   "< Back"
      Height          =   350
      Left            =   2595
      TabIndex        =   7
      Top             =   4520
      Width           =   1335
   End
   Begin VB.CommandButton Command3 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   350
      Left            =   5370
      TabIndex        =   6
      Top             =   4520
      Width           =   1335
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Next >"
      Default         =   -1  'True
      Height          =   350
      Left            =   3930
      TabIndex        =   5
      Top             =   4520
      Width           =   1335
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Complete"
      Height          =   350
      Left            =   3930
      TabIndex        =   4
      Top             =   4520
      Width           =   1335
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000010&
      X1              =   0
      X2              =   454
      Y1              =   293
      Y2              =   293
   End
   Begin VB.Line Line2 
      BorderColor     =   &H80000014&
      X1              =   0
      X2              =   454
      Y1              =   294
      Y2              =   294
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal WParam As Long, lParam As Any) As Long
Dim NtWindowID As Long
Dim WithEvents HttpT As HttpTranz
Attribute HttpT.VB_VarHelpID = -1

Dim Uindex As Long
Dim Pindex As Long
Dim CurentVersion As String
Dim NewVersion As String
Dim sUrl As String
Dim FileCIndex As Integer
Dim intNr As Integer
Dim UdFileList() As String
Dim PlFileList() As String

Private Sub SendPluginData(Text As String)
  Const WM_SETTEXT = &HC
  If NtWindowID = 0 Then Exit Sub
  SendMessage NtWindowID, WM_SETTEXT, -1, ByVal "UpdateApp " + Text
End Sub

Private Sub Check1_Click()
  Dim I As Integer
  For I = 0 To List1.ListCount - 1
    List1.Selected(I) = True
  Next
  List1.Enabled = Check1.Value = 0
  List1.ListIndex = -1
End Sub

Private Sub Command1_Click()
  ShFrame intNr - 1
End Sub

Private Sub Command2_Click()
  ShFrame intNr + 1
  If intNr = 2 Then
    CleanUPFiles
    Uindex = 0
    Pindex = 0
    FileCIndex = 0
    StartTimer Me, 1, 1
  End If
End Sub

Private Sub CleanUPFiles()
  Dim DAT As String
  DAT = Dir(App.Path + "\*.tempupdate")
  Do Until DAT = ""
    On Error Resume Next
    Kill App.Path + "\" + DAT
    On Error GoTo 0
    DAT = Dir
  Loop
End Sub

Private Sub Command3_Click()
  Unload Me
End Sub

Private Sub Command4_Click()
  Dim DAT As String
  Dim I As Integer
  Dim T As Long
  Me.Enabled = False
  If List1.SelCount > 0 And NtWindowID > 0 Then
    MsgBox uAppName + " will be quit now to update program files", vbInformation
    SendPluginData "NettalkUnload"
    T = Timer
    Do Until Timer > T + 2
      DoEvents
    Loop
  End If
  T = 0
  DAT = Dir(App.Path + "\*.tempupdate")
  Do Until DAT = ""
    If LCase(DAT) = "update.exe.tempupdate" Then
      On Error Resume Next
      Kill App.Path + "\" + CutDirFromPath(DAT, ".") + ".update"
      Name App.Path + "\" + DAT As App.Path + "\" + CutDirFromPath(DAT, ".") + ".update"
      On Error GoTo 0
      Err = 0
    Else
      Do
        On Error Resume Next
        Kill App.Path + "\" + CutDirFromPath(DAT, ".")
        On Error GoTo 0
        On Error Resume Next
        Name App.Path + "\" + DAT As App.Path + "\" + CutDirFromPath(DAT, ".")
        If Err > 0 Then
          On Error GoTo 0
          I = MsgBox("Couldn't replace " + CutDirFromPath(DAT, ".") + ", check if " + uAppName + " has been quit.", vbCritical + vbRetryCancel)
          If I = vbCancel Then T = 1: Exit Do
        Else
          On Error GoTo 0
          Exit Do
        End If
      Loop
      On Error Resume Next
      Kill App.Path + "\" + DAT
      On Error GoTo 0
    End If
    DAT = Dir
  Loop
  If T = 0 Then
    CurentVersion = NewVersion
    DAT = "Update has been finished"
  Else
    DAT = "Update couldn't fully finish"
  End If
'  If List1.SelCount > 0 And NtWindowID > 0 Then
'    I = MsgBox(DAT + ", soll " + uAppName + " jetzt gestartet werden?", vbQuestion + vbYesNo)
'    If I = vbYes Then
'      On Error Resume Next
'      ShellExecute 0&, vbNullString, App.Path + "\" + uAppName + ".exe", "", vbNullString, vbNormalFocus
'      If Err <> 0 Then MsgBox uAppName + " konnte nicht gestartet werden (" + Err.Description + ")", vbCritical
'      On Error GoTo 0
'    End If
'  Else
    MsgBox DAT, vbInformation
'  End If
  Unload Me
End Sub

Private Sub Form_Initialize()
  InitCommonControls
End Sub

Private Sub Form_Load()
  Dim DAT As String
  Dim I As Integer
  NtWindowID = Val(Trim(Command))
  SendPluginData "inst " + CStr(PluginData.hWnd)
  'InputBox "", "", "inst " + CStr(NtWindowID)
  Set HttpT = New HttpTranz
  
  On Error Resume Next
  If Len(Dir(App.Path + "\Update.ini")) = 0 Then
    uAppName = "Nettalk"
    CurentVersion = 0
    sUrl = "http://www.ntalk.de/Verkabelt/Nettalk/Update/Update.txt"
  Else
    I = FreeFile
    Open App.Path + "\Update.ini" For Input As #I
      Line Input #I, uAppName
      Line Input #I, CurentVersion
      Line Input #I, sUrl
    Close #I
    If Err <> 0 Then
      MsgBox "Couldn't open the file " + App.Path + "\Update.ini (" + Err.Description + ")", vbCritical
      Unload Me
      Exit Sub
    End If
  End If
  On Error GoTo 0
  
  Me.Height = 5415
  Me.Width = 6900
  For I = 0 To UpFrame.Count - 1
    UpFrame(I).Top = 0
    UpFrame(I).Left = 120
  Next
  Picture1.Picture = LoadResPicture(2, vbResBitmap)
  
  ShFrame 0
  List1.Enabled = False
  Check1.Value = 1
  
  Command2.Enabled = False
  Me.Caption = uAppName + "-Update"
  Label1.Caption = ""
  Me.Visible = True
  StartTimer Me, 1, 2
End Sub

Private Sub ShFrame(FrameIndex As Integer)
  Dim I As Integer
  intNr = FrameIndex
  For I = 0 To UpFrame.UBound
    If I = intNr Then UpFrame(I).Visible = True Else UpFrame(I).Visible = False
  Next
  Select Case intNr
    Case 0
      Command2.Visible = True
      Command2.Enabled = True
      Command4.Visible = False
      Command1.Enabled = False
    Case 1
      Command2.Visible = True
      Command2.Enabled = List1.SelCount + List2.SelCount > 0
      Command4.Visible = False
      Command1.Enabled = True
    Case 2
      Command2.Visible = False
      Command4.Visible = True
      Command4.Enabled = False
      Command1.Enabled = False
  End Select
End Sub


Private Sub Form_Unload(Cancel As Integer)
  Dim FreeF As Integer
  On Error Resume Next
  FreeF = FreeFile
  Open App.Path + "\Update.ini" For Output As #FreeF
    Print #FreeF, uAppName
    Print #FreeF, CurentVersion
    Print #FreeF, sUrl
  Close #FreeF
  If Err <> 0 Then MsgBox "Couldn't open the file " + App.Path + "\Update.ini (" + Err.Description + ")", vbCritical
  On Error GoTo 0
  SendPluginData "unload"
  KillAllTimers
End Sub

Private Sub HttpT_Connected()
  If intNr = 0 Then
    Label3.Caption = "Waiting for data..."
  ElseIf intNr = 2 Then
    Label5.Caption = "Waiting for data..."
  End If
End Sub

Private Sub HttpT_ConnectionClosed()
  If intNr = 0 Then
    Label3.Caption = Label3.Caption + "   Connection closed"
  ElseIf intNr = 2 Then
    Label5.Caption = "Connection closed"
    StartTimer Me, 1, 1
  End If
End Sub

Private Sub HttpT_DownloadCompleat()
  If intNr = 0 Then
    PhraseData HttpT.Buffer
    ShFrame 0
    Label1.Caption = "Change log for " + uAppName + " " + NewVersion + ":"
    Screen.MousePointer = 0
    Label3.ForeColor = &H80000011
  End If
  If intNr = 2 Then
    List3.List(List3.ListCount - 1) = List3.List(List3.ListCount - 1) + " OK"
    StartTimer Me, 1, 1
  End If
End Sub

Public Sub TimerEvent(TimerID As Integer)
  Dim DAT As String
  If TimerID = 1 Then
    If Uindex + 1 > List1.ListCount Then
      Do
        If Pindex + 1 > List2.ListCount Then
          Command4.Enabled = True
          Command1.Enabled = True
          Label5.Caption = "Download finished"
          Exit Sub
        End If
        DAT = PlFileList(1, Pindex)
        Pindex = Pindex + 1
      Loop Until List2.Selected(Pindex - 1) = True
    Else
      Do
        If Uindex + 1 > List1.ListCount Then StartTimer Me, 1, 1: Exit Sub
        DAT = UdFileList(1, Uindex)
        Uindex = Uindex + 1
      Loop Until List1.Selected(Uindex - 1) = True
    End If
    FileCIndex = FileCIndex + 1
    Label5.Caption = "Verbinden..."
    If LCase(Left(DAT, 7)) <> "http://" Then DAT = CutDirFromPath(sUrl, "/") + "/" + DAT
    Label8.Caption = CutDirFromPath(DAT, "/", True)
    List3.AddItem "Download " + Label8.Caption + "..."
    List3.ListIndex = List3.ListCount - 1
    HttpT.LoadFromURL DAT, App.Path + "\" + CutDirFromPath(DAT, "/", True) + ".tempupdate"
  ElseIf TimerID = 2 Then
    Label3.Caption = "Verbinden..."
    Screen.MousePointer = 13
    HttpT.LoadFromURL sUrl
  End If
End Sub


Private Sub PhraseData(Data As String)
  Dim I As Long
  Dim I2 As Long
  Dim I3 As Long
  Dim Plugin As Boolean
  Dim PhrasePart As Boolean
  Dim IfExist As Boolean
  ReDim UdFileList(2, 0)
  ReDim PlFileList(2, 0)
  
  Uindex = 0
  Pindex = 0
  
  Do
    I = InStr(I2 + 1, Data, "<updates", vbTextCompare)
    I2 = InStr(I2 + 1, Data, "<plugins", vbTextCompare)
    I3 = InStr(I2 + 1, Data, "<text>", vbTextCompare)
    If (I2 < I And I2 > 0) Or I = 0 Then I = I2: Plugin = True Else Plugin = False
    If (I3 < I And I3 > 0) Or I = 0 Then
      Text1.Text = Mid(Data, I3 + 8)
      Exit Do
    End If
    If I = 0 Then Exit Do
    I2 = InStr(I + 1, Data, ">")
    I = InStr(I + 1, Data, """")
    PhrasePart = True
    IfExist = False
    If I < I2 Then
      I3 = InStr(I + 1, Data, """")
      If I3 < I2 And I3 > 0 Then
        If LCase(Left(Trim(Mid(Data, I + 1, I3 - I - 1)), 2)) = "ex" Then
          PhrasePart = ComVersions(Trim(Mid(Data, I + 3, I3 - I - 3)), CurentVersion)
          IfExist = True
        Else
          PhrasePart = ComVersions(Trim(Mid(Data, I + 1, I3 - I - 1)), CurentVersion)
        End If
      End If
    End If
    If PhrasePart = True Then
      I = InStr(I2, Data, vbNewLine)
      Do
        I2 = I
        I = InStr(I + 2, Data, vbNewLine)
        If Mid(Data, I2 + 2, 1) = "<" Or I = 0 Then Exit Do
        I3 = InStr(I2 + 2, Data, ",")
        If Plugin = True Then
          If Len(Dir(App.Path + "\" + CutDirFromPath(Trim(Mid(Data, I3 + 2, I - I3 - 2)), "/", True))) = 0 Then
            ReDim Preserve PlFileList(2, Pindex)
            PlFileList(0, Pindex) = Trim(Mid(Data, I2 + 2, I3 - I2 - 2))
            PlFileList(1, Pindex) = Trim(Mid(Data, I3 + 2, I - I3 - 2))
            List2.AddItem PlFileList(0, Pindex)
            List2.ListIndex = -1
            Pindex = Pindex + 1
          End If
        Else
          If Len(Dir(App.Path + "\" + CutDirFromPath(Trim(Mid(Data, I3 + 2, I - I3 - 2)), "/", True))) > 0 Or IfExist = False Then
            ReDim Preserve UdFileList(2, Uindex)
            UdFileList(0, Uindex) = Trim(Mid(Data, I2 + 2, I3 - I2 - 2))
            UdFileList(1, Uindex) = Trim(Mid(Data, I3 + 2, I - I3 - 2))
            List1.AddItem UdFileList(0, Uindex)
            List1.Selected(Uindex) = True
            List1.ListIndex = -1
            Uindex = Uindex + 1
          End If
        End If
      Loop
    End If
  Loop
  Picture4.Visible = List1.ListCount = 0
  Check1.Enabled = List1.ListCount > 0
End Sub

Private Function ComVersions(Version1 As String, Version2 As String) As Boolean
  'Test...
  ComVersions = cVersions(Version1, Version2)
  If cVersions(Version1, NewVersion) Then NewVersion = Version1
End Function

Private Sub HttpT_Error(ErrorDescr As String)
  Dim I As Integer
  If intNr = 0 Then
    Label3 = "Fehler: " + ErrorDescr
    Screen.MousePointer = 0
    I = MsgBox("Couldn't established conncection to server", vbCritical + vbRetryCancel)
    If I = vbRetry Then
      StartTimer Me, 1, 2
    End If
  ElseIf intNr = 2 Then
    Label5.Caption = "Fehler: " + ErrorDescr
    List3.List(List3.ListCount - 1) = List3.List(List3.ListCount - 1) + " " + ErrorDescr
  End If
End Sub

Private Sub HttpT_RecPaket(Bytes As Long, Speed As Long)
  Dim I As Long
  If intNr = 0 Then
    If HttpT.FileSize > 0 Then
      Label3.Caption = HttpT.FileSize & " Bytes    (" & Int(Bytes / HttpT.FileSize * 100) & "%  -  " & Int(Speed / 1024 * 10) / 10 & " KB/s )"
    Else
      Label3.Caption = "(" & Bytes & " Bytes  -  " & Int(Speed / 1024 * 10) / 10 & " KB/s )"
    End If
  ElseIf intNr = 2 Then
    If HttpT.FileSize = 0 Then Exit Sub
    Picture2.Cls
    Picture3.Cls
    I = Bytes / HttpT.FileSize * 100
    Picture2.Line (0, 0)-(Picture2.ScaleWidth * ((FileCIndex - 1) + I / 100) / (List1.SelCount + List2.SelCount), Picture2.ScaleHeight), , BF
    Picture3.Line (0, 0)-(Picture3.ScaleWidth * I / 100, Picture3.ScaleHeight), , BF
    If Speed = 0 Then Exit Sub
    Label5.Caption = Int(Bytes / 1024) & " KByte / " & Int(HttpT.FileSize / 1024) & " KByte" _
    + vbNewLine + vbNewLine & Int(Speed / 1024 * 10) / 10 & " KB/s" + vbNewLine + vbNewLine + _
    "Remaining time: " + FormatSec((HttpT.FileSize - Bytes) / Speed)
  End If
End Sub

Private Sub List1_Click()
  Command2.Enabled = List1.SelCount + List2.SelCount > 0
End Sub

Private Sub List2_Click()
  Command2.Enabled = List1.SelCount + List2.SelCount > 0
End Sub

Private Sub Picture4_Paint()
  Picture4.Cls
  Picture4.Print "No updates avalibal,"
  Picture4.Print uAppName + " is up to date."
End Sub

Private Sub PluginData_Change()
  Dim I As Integer
  Dim I2 As Integer
  I = InStr(PluginData.Text, " ")
  If I = 0 Then I = Len(PluginData.Text) + 1
  
  Me.Print PluginData.Text
  Select Case Left(PluginData.Text, I - 1)
    Case "unload"
      SendPluginData "Unload"
      Unload Me
      Exit Sub
    Case "show"
      Me.Visible = True
      Form1.SetFocus
  End Select
  PluginData.Text = ""
End Sub

