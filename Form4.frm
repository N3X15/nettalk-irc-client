VERSION 5.00
Begin VB.Form Form4 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   0  'Kein
   ClientHeight    =   1545
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5415
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Shell Dlg"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form4"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   103
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   361
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows-Standard
   Begin VB.PictureBox BBuffer 
      Appearance      =   0  '2D
      AutoRedraw      =   -1  'True
      BackColor       =   &H00000000&
      BorderStyle     =   0  'Kein
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   3120
      ScaleHeight     =   33
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   65
      TabIndex        =   2
      Top             =   360
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.PictureBox Buffer2 
      Appearance      =   0  '2D
      AutoRedraw      =   -1  'True
      BackColor       =   &H00000000&
      BorderStyle     =   0  'Kein
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   240
      ScaleHeight     =   33
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   65
      TabIndex        =   1
      Top             =   240
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.PictureBox Buffer1 
      Appearance      =   0  '2D
      AutoRedraw      =   -1  'True
      BackColor       =   &H00000000&
      BorderStyle     =   0  'Kein
      ForeColor       =   &H80000008&
      Height          =   495
      Left            =   2040
      ScaleHeight     =   33
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   65
      TabIndex        =   0
      Top             =   360
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "Form4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim AniState As Double
Dim Lleft As Single
Dim AniMode As Integer
Dim TimeEx As Double
Dim LastWW As Long
Dim OldMode As Long

Public Speed As Integer
Public ChatFrs As ChatFrame

Private Const HWND_TOPMOST = -1
Private Const SWP_NOSIZE = &H1
Private Const SWP_NOMOVE = &H2
Private Const SWP_NOACTIVATE = &H10

Private Const GWL_EXSTYLE = (-20)
Private Const WS_EX_NOACTIVATE = &H8000000
Private Const SWP_NOOWNERZORDER = &H200

Private Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
Private Declare Function SystemParametersInfo Lib "user32" Alias "SystemParametersInfoA" (ByVal uAction As Long, ByVal uParam As Long, lpvParam As Any, ByVal fuWinIni As Long) As Long
Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long) As Long

Private Sub Form_Load()
  InitFormFont Me
  Speed = 12
  SetWindowLongA Me.hWnd, GWL_EXSTYLE, WS_EX_NOACTIVATE Or GetWindowLong(Me.hWnd, GWL_EXSTYLE)
  LastWW = 0
End Sub

Public Sub SetSize(Width As Single)
  Dim WA As RECT
  WA = GetWorkArea
  If Width = 0 Then Width = 400
  Me.Width = Width * Screen.TwipsPerPixelX
  Me.Height = 48 * Screen.TwipsPerPixelY
  Lleft = WA.Right * Screen.TwipsPerPixelX - Me.Width
  
  If WA.Top > (Screen.Height / Screen.TwipsPerPixelY - WA.Bottom) Then
    Me.Top = WA.Top * Screen.TwipsPerPixelY
  Else
    Me.Top = WA.Bottom * Screen.TwipsPerPixelY - Me.Height
  End If
  
  Me.Left = Lleft
End Sub

Public Sub IntFrontBuffer(Text As String, Von As String, Server As String, ChFrame As ChatFrame)
  Dim DAT As String
  Dim I As Long

  Buffer2.Width = Me.ScaleWidth
  Buffer2.Height = Me.ScaleHeight
  
  Buffer2.Line (0, 0)-(0, Me.ScaleHeight), &H80000003
  Buffer2.Line (0, 0)-(Me.ScaleWidth, 0), &H80000003
  Buffer2.Line (1, Me.ScaleHeight - 1)-(Me.ScaleWidth - 1, Me.ScaleHeight - 1), vbBlack
  Buffer2.Line (Me.ScaleWidth - 1, 1)-(Me.ScaleWidth - 1, Me.ScaleHeight), vbBlack
  DrawFlow Buffer2, 1, 1, Me.ScaleWidth - 2, 9, &H80000003, &H80000002
  DrawFlow Buffer2, 1, 11, Me.ScaleWidth - 2, Me.ScaleHeight - 13, &H80000002, &H80000003
  
  
  Buffer2.FontSize = 10
  Buffer2.ForeColor = &H80000009
  DAT = Text
  TextOutW Buffer2.hDC, 14, Form4.ScaleHeight - Form4.TextHeight(Text) - 8, StrPtr(DAT), Len(DAT)
  
  Buffer2.FontSize = 13
  TextOutW Buffer2.hDC, 10, 5, StrPtr(Von), Len(Von)
  
  I = TextWW(Von, Buffer2.hDC) + 16
  Buffer2.FontSize = 10
  Buffer2.ForeColor = &H80000013
  DAT = "( " + Server + " )"
  TextOutW Buffer2.hDC, I, 6, StrPtr(DAT), Len(DAT)
  Set ChatFrs = ChFrame
  
  TimeEx = Len(Text) * 70 + 4000
  If TimeEx > 20000 Then TimeEx = 20000
   
  SetWindowPos Me.hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOSIZE Or SWP_NOMOVE Or SWP_NOACTIVATE Or SWP_NOOWNERZORDER
End Sub

Public Sub IntBackBuffer()
  If LastWW <> Me.ScaleWidth Then
    Buffer1.Width = Me.ScaleWidth
    Buffer1.Height = Me.ScaleHeight
    BBuffer.Width = Me.ScaleWidth
    BBuffer.Height = Me.ScaleHeight
    Me.Visible = False
    DoEvents
    LastWW = Me.ScaleWidth
    BitBlt Buffer1.hDC, 0, 0, Me.ScaleWidth, Me.ScaleHeight, GetDC(0), Lleft / Screen.TwipsPerPixelX, Me.Top / Screen.TwipsPerPixelY, vbSrcCopy
    BitBlt BBuffer.hDC, 0, 0, Me.ScaleWidth, Me.ScaleHeight, Buffer1.hDC, 0, 0, vbSrcCopy
    BitBlt Me.hDC, 0, 0, Me.ScaleWidth, Me.ScaleHeight, BBuffer.hDC, 0, 0, vbSrcCopy
  End If
End Sub

Public Sub AnimateON()
  StartTimer Me, 10, 1
  If AniMode = 3 Then
    Speed = 1
    AniState = AltTimer - (3.141 - 0.3) / Speed
  Else
    Speed = 12
    AniState = AltTimer
  End If
  OldMode = AniMode
  AniMode = 1
  If TimeEx = 0 Then TimeEx = 2000
  StartTimer Me, TimeEx, 2
  'Me.ZOrder 0
End Sub

Public Sub AnimateOFF()
  If AniMode = 0 Then Exit Sub
  StartTimer Me, 10, 1
  AniState = AltTimer
  AniMode = 2
  Speed = 12
  'Me.ZOrder 0
End Sub

Public Sub TimerEvent(TimerID As Integer)
  Dim CosVal As Double
  Dim I As Long
  
  If TimerID = 1 Then
    If AniMode = 1 Then CosVal = AltTimeDiff(AniState) * Speed
    If AniMode = 2 Then CosVal = AltTimeDiff(AniState) * Speed / 3
    If AniMode = 1 And CosVal > 3.141 Then AniMode = 3: CosVal = 3.141
    If AniMode = 2 And CosVal > 3.141 Then AniMode = 0:  Unload Me
    If AniMode = 1 Or AniMode = 3 Then
      If OldMode = 3 Then
        BitBlt BBuffer.hDC, 0, 0, Me.ScaleWidth, Me.ScaleHeight, Buffer1.hDC, 0, 0, vbSrcCopy
        AlphBitBlt BBuffer.hDC, 0, 0, Me.ScaleWidth, Me.ScaleHeight, Buffer2.hDC, 0, 0, 115 * (1 - Cos(CosVal))
        BitBlt Me.hDC, 0, 0, Me.ScaleWidth, Me.ScaleHeight, BBuffer.hDC, 0, 0, vbSrcCopy
      Else
        I = (Cos(CosVal) * Me.ScaleWidth / 2 + Me.ScaleWidth / 2)
        BitBlt BBuffer.hDC, 0, 0, Me.ScaleWidth, Me.ScaleHeight, Buffer1.hDC, 0, 0, vbSrcCopy
        AlphBitBlt BBuffer.hDC, I, 0, Me.ScaleWidth - I, Me.ScaleHeight, Buffer2.hDC, 0, 0, 115 * (1 - Cos(CosVal))
        BitBlt Me.hDC, 0, 0, Me.ScaleWidth, Me.ScaleHeight, BBuffer.hDC, 0, 0, vbSrcCopy
      End If
    End If
    If AniMode = 2 Then
      BitBlt BBuffer.hDC, 0, 0, Me.ScaleWidth, Me.ScaleHeight, Buffer1.hDC, 0, 0, vbSrcCopy
      AlphBitBlt BBuffer.hDC, 0, 0, Me.ScaleWidth, Me.ScaleHeight, Buffer2.hDC, 0, 0, 115 * (1 - Cos(CosVal + 3.141))
      BitBlt Me.hDC, 0, 0, Me.ScaleWidth, Me.ScaleHeight, BBuffer.hDC, 0, 0, vbSrcCopy
    End If
    If AniMode <> 0 And AniMode <> 3 Then StartTimer Me, 10, 1
    If AniMode = 1 Or AniMode = 2 Then Me.Visible = True
  ElseIf TimerID = 2 Then
    AnimateOFF
  End If
End Sub

Private Function GetWorkArea() As RECT
  SystemParametersInfo 48, 0, GetWorkArea, 0
End Function

Private Sub Form_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  AnimateOFF
  If Button = 1 Then
    Form1.ShowMainWindow
    SetFrontFrame ChatFrs
  Else
    Form1.IsNowActiv
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  KillTimerByID 1, Me
  KillTimerByID 2, Me
End Sub

Private Sub AlphBitBlt(DhDC As Long, Dx As Long, Dy As Long, Dw As Long, Dh As Long, SchDC As Long, ScX As Long, ScY As Long, Optional ByVal AlphaBlF As Byte)
  Dim BlKonstr As Long
  
  If IsNt Then
    BlKonstr = AlphaBlF * 65536
    AlphaBlend DhDC, Dx, Dy, Dw, Dh, SchDC, ScX, ScY, Dw, Dh, BlKonstr
  Else
    BitBlt DhDC, Dx, Dy, Dw, Dh, SchDC, ScX, ScY, vbSrcCopy
  End If
End Sub

