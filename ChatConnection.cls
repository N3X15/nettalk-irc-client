VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IRCConnection"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Buffer As String
Public Server As String
Public Port As Long
Public Caption As String
Public State As Integer
Public Nick As String
Public StartCommands As String
Public Realname As String
Public ListID As Integer
Public ServerPass As String
Public UserID As String
Public JoinMode As Integer

Private ExLoop As Boolean
Private WaitLoop As Boolean
Private WithEvents wins As CSocket
Attribute wins.VB_VarHelpID = -1
Private WithEvents Timer1 As longTimer
Attribute Timer1.VB_VarHelpID = -1

Public Sub Connect()
  Dim i As Integer
  Dim I2 As Integer
  Dim ChatFrs As ChatFrame
  Dim T As Long
  Set wins = New CSocket
  State = 3
  For Each ChatFrs In ChatFrames
    If ChatFrs.IRCConn Is Me Then
      If ChatFrs.FrameType = 0 Then
        ChatFrs.QuitFrame
      End If
    End If
  Next
  For i = 1 To ChatFrames.Count
    Set ChatFrs = ChatFrames(i)
    If ChatFrs.IRCConn Is Me Then
      If ChatFrs.FrameType = 1 Then
        ChatFrs.Caption = Me.Server
        Form1.ColumRedraw
        ChatFrs.PrintText TeVal.ConStart, CoVal.ClientMSG, , True
        Exit For
      End If
    End If
  Next
  ConnList(ListID).State = 1
  RefreshList
  Form1.ColumRedraw
  wins.Connect Server, Port
  T = Timer + 6
  Do
    DoEvents
  Loop Until Timer > T Or wins.State <> 3
  If wins.State = 4 Then
    ChatFrs.PrintText "  " + TeVal.ConOK, CoVal.ClientMSG, , False
    State = 4
    If ServerPass <> "" Then SendLine "PASS " + ServerPass
    SendLine "NICK " + Nick
    If UserID = "" Then
      SendLine "USER " + Nick + " " + Chr(34) + Chr(34) + " " + Chr(34) + Realname + Chr(34) + " :-"
    Else
      SendLine "USER " + UserID + " " + Chr(34) + Chr(34) + " " + Chr(34) + Realname + Chr(34) + " :-"
    End If
    If JoinMode <> 0 Then SendLine "MODE +i"
    ConnList(ListID).State = 2
    RefreshList
    Form1.ColumRedraw
  Else
    ChatFrs.PrintText "  " + TeVal.ConErr, CoVal.ErrorMSG, , False
    CloseConnection
  End If
End Sub

Private Sub Class_Initialize()
  Dim ChFrame As New ChatFrame
  ChFrame.Caption = "Open..."
  ChFrame.Inst 1, 0
  Set ChFrame.IRCConn = Me
  ChatFrames.Add ChFrame
  SetFrontFrame ChFrame
End Sub

Public Sub ExitLoops()
  ExLoop = True
End Sub

Private Sub wins_Closed()
  CloseConnection
End Sub

Public Sub CloseConnection()
  Dim ChatFrs As ChatFrame
  Dim i As Integer
  Dim I2 As Integer
  Dim DAT As String
  Do Until wins.State = 1
    wins.Disconnect
  Loop
  i = -1
  Do
    I2 = i
    i = InStr(i + 2, StartCommands, vbNewLine)
    If i = 0 Then i = Len(StartCommands) + 1
    If Mid(StartCommands, I2 + 2, 1) <> "$" And i - I2 > 2 Then
      DAT = DAT + Mid(StartCommands, I2 + 2, i - I2 - 2) + vbNewLine
    End If
  Loop Until i = Len(StartCommands) + 1
  If DAT = vbNewLine Then DAT = ""
  StartCommands = DAT
  DAT = ""
  For Each ChatFrs In ChatFrames
    If ChatFrs.IRCConn Is Me Then
      Select Case ChatFrs.FrameType
        Case 0
          DAT = DAT + "$join " + ChatFrs.Caption + vbNewLine
          ChatFrs.PrintText TeVal.ConClose, CoVal.ClientMSG, , True
        Case 2
          DAT = DAT + "$list" + vbNewLine
        Case 1, 4
          ChatFrs.PrintText TeVal.ConClose, CoVal.ClientMSG, , True
      End Select
    End If
  Next
  StartCommands = DAT + StartCommands
  ConnList(ListID).Commands = StartCommands
  ConnList(ListID).Nick = Nick
  ConnList(ListID).State = 3
  RefreshList
  State = 1
  Form1.ColumRedraw
End Sub

Public Sub Quit()
  Dim ChatFrs As ChatFrame
  Dim i As Integer
  If State = 3 Then Exit Sub
  CloseConnection
  For i = ChatFrames.Count To 1 Step -1
    Set ChatFrs = ChatFrames(i)
    If ChatFrs.IRCConn Is Me Then
      ChatFrs.QuitFrame
    End If
  Next
  For i = IRCconns.Count To 1 Step -1
    If IRCconns(i) Is Me Then
      IRCconns.Remove i
    End If
  Next
  ConnList(ListID).State = 0
  RefreshList
  SetFrontFrame ChatFrames(1)
End Sub

Public Sub SendComm(data As String, ChatFrs As ChatFrame)
  Dim sDAT(0 To 9) As String
  Dim i As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim I4 As Integer
  Dim Iind As Integer
  Dim OutStr As String
  Dim OutStr2 As String
  '##################### Extern ###########################
  i = 0
  Do
    I2 = i
    i = InStr(i + 1, data, " ")
    If i = 0 Or Iind > 8 Then Exit Do
    sDAT(Iind) = Mid(data, I2 + 1, i - I2 - 1)
    Iind = Iind + 1
  Loop
  sDAT(Iind) = Mid(data, I2 + 1)
  Iind = 0
  For i = 0 To UBound(ComTranzList)
    If UCase(ComTranzList(i, 0)) = UCase(sDAT(0)) Then
      Iind = 1
      I2 = -1
      OutStr = ""
      Do
        I3 = I2 + 1
        I2 = InStr(I2 + 2, ComTranzList(i, 1), "$")
        If I2 = 0 Then Exit Do
        OutStr = OutStr + Mid(ComTranzList(i, 1), I3 + 1, I2 - I3 - 1)
        If Mid(ComTranzList(i, 1), I2 + 1, 1) = ">" Then
          For I4 = Val(Mid(ComTranzList(i, 1), I2 + 2, 1)) + 1 To 9
            OutStr = OutStr + sDAT(I4)
            If I4 < 9 Then If sDAT(I4 + 1) <> "" Then OutStr = OutStr + " "
          Next
        ElseIf Mid(ComTranzList(i, 1), I2 + 1, 1) = "<" Then
          For I4 = 1 To Val(Mid(ComTranzList(i, 1), I2 + 2, 1)) - 1
            If I4 > 1 Then OutStr = OutStr + " "
            OutStr = OutStr + sDAT(I4)
          Next
        ElseIf Val(Mid(ComTranzList(i, 1), I2 + 1, 1)) > 0 Then
          OutStr = OutStr + sDAT(Val(Mid(ComTranzList(i, 1), I2 + 1, 1)))
        ElseIf Mid(ComTranzList(i, 1), I2 + 1, 1) = "(" Then
          I4 = InStr(I2 + 2, ComTranzList(i, 1), ")")
          If I4 <> 0 Then
            Select Case LCase(Mid(ComTranzList(i, 1), I2 + 2, I4 - I2 - 2))
              Case "roomname"
                OutStr = OutStr + ChatFrs.Caption
              Case "localport"
                OutStr = OutStr + wins.LocalPort
              Case "remoteport"
                OutStr = OutStr + wins.RemotePort
            End Select
            I2 = I4 - 1
          End If
        End If
      Loop
      OutStr = OutStr + Mid(ComTranzList(i, 1), I3 + 2)
      Exit For
    End If
  Next
  If Iind = 0 Then
    OutStr = ""
    For I4 = 0 To 9
      OutStr = OutStr + sDAT(I4)
      If I4 < 9 Then If sDAT(I4 + 1) <> "" Then OutStr = OutStr + " "
    Next
  End If
  '######################### Intern ###############################
  i = 0
  Iind = 0
  Do
    I2 = i
    i = InStr(i + 1, OutStr, " ")
    If i = 0 Or Iind > 8 Then Exit Do
    sDAT(Iind) = Mid(OutStr, I2 + 1, i - I2 - 1)
    Iind = Iind + 1
  Loop
  sDAT(Iind) = Mid(OutStr, I2 + 1)
  Select Case LCase(sDAT(0))
    Case "me"
      If ChatFrs.FrameType = 0 Or ChatFrs.FrameType = 4 Then
        OutStr2 = "privmsg " + ChatFrs.Caption + " :" + Chr(1) + "ACTION " + Mid(OutStr, Len(sDAT(0)) + 2) + Chr(1)
        ChatFrs.PrintText "* " + Nick + " " + Mid(OutStr, Len(sDAT(0)) + 2), CoVal.NormalText, , True
      End If
    Case "msg"
      OutStr2 = "PRIVMSG " + sDAT(1) + " :" + Mid(OutStr, Len(sDAT(0)) + Len(sDAT(1)) + 3)
      If ChatFrs.FrameType = 0 Or ChatFrs.FrameType = 1 Or ChatFrs.FrameType = 4 Then
        ChatFrs.PrintText "<PRIVATE:" + Nick + ">", CoVal.Notice, True, True
        ChatFrs.PrintText Mid(OutStr, Len(sDAT(0)) + Len(sDAT(1)) + 3), CoVal.NormalText, , False
      End If
    Case "notice"
      OutStr2 = "notice " + sDAT(1) + " :" + Mid(OutStr, Len(sDAT(0)) + Len(sDAT(1)) + 3)
      If ChatFrs.FrameType = 0 Or ChatFrs.FrameType = 1 Or ChatFrs.FrameType = 4 Then
        ChatFrs.PrintText "- " + Nick + ": " + Mid(OutStr, Len(sDAT(0)) + Len(sDAT(1)) + 3), CoVal.Notice, , True
      End If
    Case "query"
      If Left(sDAT(1), 1) = "#" Or Left(sDAT(1), 1) = "+" Or Left(sDAT(1), 1) = "&" Then
        OutStr2 = "join " + sDAT(1)
      Else
        OutStr2 = ""
        ChatFrs.PrivateFrame sDAT(1)
      End If
    Case "dcc"
      OpenDCC Nick, sDAT(2), "D:\Netzwerk-Downloads\mp3s\X-perience.MP3", _
      sDAT(1), wins
      OutStr2 = ""
      If ChatFrs.FrameType = 0 Or ChatFrs.FrameType = 1 Or ChatFrs.FrameType = 4 Then
        ChatFrs.PrintText "<DCC:" + Nick + ">", CoVal.Notice, True, True
        ChatFrs.PrintText Mid(OutStr, Len(sDAT(0)) + Len(sDAT(1)) + 3), CoVal.NormalText, , False
      End If
    Case Else
      OutStr2 = OutStr
  End Select
  '########################## Send #############################
  If OutStr2 <> "" Then SendLine OutStr2
End Sub

Public Sub SendLine(data As String)
  wins.SendData data + Chr(10)
End Sub

Private Sub PraseCommand(data As String)
  Dim ChatFrs As ChatFrame
  Dim DAT As String
  Dim i As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim PIndex As Integer
  Dim PData(4) As String
  Dim Joined As Integer
  Dim PrivMSG As Integer
  Dim ListRooms As Integer
  Dim sNotice As Integer
  Dim sDCC As Integer
  Dim mVon As String
  
  Do
    If PIndex = 1 And I3 <> i Then PIndex = PIndex + 1
    I2 = i
    i = InStr(I2 + 1, data, " ")
    I3 = InStr(I2 + 1, data, "!")
    If I3 < i And I3 > 0 Then
      i = I3
    End If
    If i = 0 Then i = Len(data) + 1
    PData(PIndex) = Mid(data, I2 + 1, i - I2 - 1)
    PIndex = PIndex + 1
  Loop Until PIndex > 3 Or i = Len(data) + 1
  If Len(data) > i Then PData(4) = Right(data, Len(data) - i)
  If Len(PData(0)) > 0 Then
    If Left(PData(0), 1) = ":" Then mVon = Right(PData(0), Len(PData(0)) - 1) Else mVon = PData(0)
  End If
  
  For Each ChatFrs In ChatFrames
    If ChatFrs.IRCConn Is Me Then
      
      Select Case Val(PData(2))
        Case 322, 321, 353, 376, 332, 366, 323
          'Do Nothing Not(1, 2, 375, 372, 401, 280, 291, 412, 292, 421, 311, 319, 312, 318, 403)
        Case Is > 0
          If ChatFrs.FrameType = 1 Then
            If Left(PData(4), 1) = ":" Then
              ChatFrs.PrintText Right(PData(4), Len(PData(4)) - 1), CoVal.NormalText, , True
            Else
              ChatFrs.PrintText "- " & PData(4), CoVal.ServerMSG
            End If
          End If
          If ChatFrs.FrameType = 4 And Val(PData(2)) = 401 Then
            i = InStr(1, PData(4), " ") - 1
            If i < 0 Then i = 0
            If UCase(Left(PData(4), i)) = UCase(ChatFrs.Caption) And i <> 0 Then
              ChatFrs.PrintText "- " & PData(4), CoVal.ServerMSG
            End If
          End If
          'If ChatFrs.FrameType = 1 And PData(2) <> "322" Then ChatFrs.PrintText vbNewLine + data, vbGreen
        Case Else
          'If ChatFrs.FrameType = 1 And PData(2) <> "322" Then ChatFrs.PrintText vbNewLine + data, vbBlue
      End Select
      
      If ChatFrs.FrameType = 1 And PData(0) = "ERROR" Then
        ChatFrs.PrintText "- " + data, CoVal.ErrorMSG, , True
      End If
      
      'Mode
      If UCase(PData(2)) = "MODE" Or UCase(PData(3)) = "MODE" Then
        If ChatFrs.FrameType = 1 And UCase(mVon) = UCase(Nick) Then
          ChatFrs.PrintText "- " + mVon + " " + TeVal.UserRank + " " + PData(4), CoVal.ServerMSG, , True
        End If
        If ChatFrs.FrameType = 0 Then
          i = 0
          For i = 0 To ChatFrs.UListF.ListCount - 1
            If UCase(ChatNoS(ChatFrs.UListF.List(i))) = UCase(Mid(PData(4), 4)) Then
              ChatFrs.PrintText "- " + mVon + " " + TeVal.UserRank + " " + PData(4), CoVal.ServerMSG, , True
              If ChatFrs.UListF.List(i) = ChatNoS(ChatFrs.UListF.List(i)) Then
                DAT = ""
              Else
                DAT = Left(ChatFrs.UListF.List(i), 1)
              End If
              Select Case LCase(Left(PData(4), 2))
                ' -- + --
                Case "+o"
                  If Left(ChatFrs.UListF.List(i), 1) <> "!" Then DAT = "@"
                Case "+h"
                  If Left(ChatFrs.UListF.List(i), 1) <> "@" And Left(ChatFrs.UListF.List(i), 1) <> "!" Then DAT = "%"
                Case "+v"
                  If Left(ChatFrs.UListF.List(i), 1) <> "@" And Left(ChatFrs.UListF.List(i), 1) <> "%" And Left(ChatFrs.UListF.List(i), 1) <> "!" Then DAT = "+"
                ' -- - --
                Case "-o"
                  If Left(ChatFrs.UListF.List(i), 1) <> "!" Then DAT = ""
                Case "-h"
                  If Left(ChatFrs.UListF.List(i), 1) <> "@" And Left(ChatFrs.UListF.List(i), 1) <> "!" Then DAT = ""
                Case "-v"
                  If Left(ChatFrs.UListF.List(i), 1) <> "@" And Left(ChatFrs.UListF.List(i), 1) <> "%" And Left(ChatFrs.UListF.List(i), 1) <> "!" Then DAT = ""
              End Select
              ChatFrs.UListF.RemoveByText ChatFrs.UListF.List(i)
              If ChatFrs Is FrontCFrame Then
                ChatFrs.UListF.Add DAT + Mid(PData(4), 4)
              Else
                ChatFrs.UListF.Add DAT + Mid(PData(4), 4), , False
              End If
              Exit For
            End If
          Next
        End If
      End If
            
      'Server Notice
      If UCase(PData(0)) = "NOTICE" Then
        If ChatFrs.FrameType = 1 Then
          ChatFrs.PrintText TimeDbt + "*** ", CoVal.ServerMSG, True, True
          ChatFrs.PrintText PData(4), CoVal.ServerMSG, True, False, "SMM:" + PData(4)
          ChatFrs.PrintText " " + PData(4), CoVal.ServerMSG, False, False
        End If
      End If
      
      'Show Topic
      If UCase(PData(2)) = "332" Then
        i = InStr(PData(4), " ")
        If i > 0 And ChatFrs.FrameType = 0 Then
          If UCase(ChatFrs.Caption) = UCase(Left(PData(4), i - 1)) Then
            ChatFrs.PrintText Mid(PData(4), i + 2), CoVal.ServerMSG
          End If
        End If
      End If
      
      'Join
      If UCase(PData(2)) = "JOIN" Then
        If ":" + UCase(ChatFrs.Caption) = UCase(PData(3)) And ChatFrs.FrameType = 0 Then
          ChatFrs.PrintText TimeDbt + "- ", CoVal.ServerMSG, True, True
          ChatFrs.PrintText mVon, CoVal.ServerMSG, True, False, "SMM:" + mVon
          ChatFrs.PrintText " " + TeVal.Join, CoVal.ServerMSG, False, False
          If ChatFrs Is FrontCFrame Then ChatFrs.UListF.Add mVon Else ChatFrs.UListF.Add mVon, , False
          Joined = 1
        Else
          If Joined = 0 Then Joined = 2
        End If
      End If
    
      'PART
      If UCase(PData(2)) = "PART" Then
        If UCase(ChatFrs.Caption) = UCase(PData(3)) And ChatFrs.FrameType = 0 Then
          If mVon = Nick Then ChatFrs.QuitFrame
          ChatFrs.PrintText TimeDbt + "- ", CoVal.ServerMSG, True, True
          ChatFrs.PrintText mVon, CoVal.ServerMSG, True, False, "SMM:" + mVon
          ChatFrs.PrintText " " + TeVal.Leave, CoVal.ServerMSG, False, False
          ChatFrs.UListF.RemoveByText mVon
        End If
      End If
      
      'QUIT
      If UCase(PData(2)) = "QUIT" Then
        If ChatFrs.FrameType = 4 And UCase(ChatFrs.Caption) = UCase(mVon) Then
          ChatFrs.PrintText TimeDbt + "- ", CoVal.ServerMSG, True, True
          ChatFrs.PrintText mVon, CoVal.ServerMSG, True, False, "SMM:" + mVon
          ChatFrs.PrintText " " + TeVal.Quit + " (" + Mid(PData(3) + " " + PData(4), 2) + ")", CoVal.ServerMSG, False, False
        End If
        If ChatFrs.FrameType = 0 Then
          For i = 0 To ChatFrs.UListF.ListCount - 1
            If UCase(ChatNoS(ChatFrs.UListF.List(i))) = UCase(mVon) Then
              ChatFrs.PrintText TimeDbt + "- ", CoVal.ServerMSG, True, True
              ChatFrs.PrintText mVon, CoVal.ServerMSG, True, False, "SMM:" + mVon
              ChatFrs.PrintText " " + TeVal.Quit + " (" + Mid(PData(3) + " " + PData(4), 2) + ")", CoVal.ServerMSG, False, False
              ChatFrs.UListF.RemoveByText ChatNoS(ChatFrs.UListF.List(i))
              Exit For
            End If
          Next
        End If
      End If
    
      'PRIVMSG
      If UCase(PData(2)) = "PRIVMSG" Then
        If Left(PData(4), 6) = ":" + Chr(1) + "DCC " Then
          If UCase(ChatFrs.Caption) = UCase(mVon) And FrontCFrame Is ChatFrs And ChatFrs.FrameType = 4 Then
            DCCConnRequest mVon, PData(4), ChatFrs, Nick
            SetAl ChatFrs
            sDCC = 1
          Else
            If sDCC = 0 Then sDCC = 2
          End If
        Else
          If (UCase(ChatFrs.Caption) = UCase(PData(3)) And ChatFrs.FrameType = 0) Or (UCase(ChatFrs.Caption) = UCase(mVon) And ChatFrs.FrameType = 4 And UCase(PData(3)) = UCase(Nick)) Then
            If Left(PData(4), 9) = ":" + Chr(1) + "ACTION " Then 'Me-MSG
              ChatFrs.PrintText TimeDbt + "* ", CoVal.NormalText, True, True
              ChatFrs.PrintText mVon, CoVal.NormalText, True, False, "SMM:" + mVon
              ChatFrs.PrintText " " + Mid(PData(4), 10, Len(PData(4)) - 10), CoVal.NormalText, False, False
              If ChatFrs.FrameType = 4 Then SetAl ChatFrs
            Else
              ChatFrs.PrintText TimeDat + "<" + mVon + ">", CoVal.NormalVon, True, True, "ANN:" + mVon
              ChatFrs.PrintText Mid(PData(4), 2), CoVal.NormalText, , False
              If ChatFrs.FrameType = 4 Then SetAl ChatFrs
            End If
            PrivMSG = 1
          Else
            If PrivMSG = 0 Then PrivMSG = 2
          End If
        End If
      End If
      
      'NOTICE
      If UCase(PData(2)) = "NOTICE" And Left(PData(4), 11) <> ":DCC Chat (" And Left(PData(4), 10) <> ":DCC Send " Then
        If ChatFrs.FrameType = 0 And sNotice <> 1 And FrontCFrame Is ChatFrs Then
          i = 0
          For i = 0 To ChatFrs.UListF.ListCount - 1
            If UCase(ChatNoS(ChatFrs.UListF.List(i))) = UCase(mVon) Then
              ChatFrs.PrintText TimeDat + "<" + mVon + " whispers>", CoVal.FlusterVon, True, True, "ANN:" + mVon
              ChatFrs.PrintText Right(PData(4), Len(PData(4)) - 1), CoVal.NormalText, , False
              SetAl ChatFrs
              sNotice = 1
              Exit For
            End If
          Next
        End If
        If sNotice = 0 Then sNotice = 2
      End If
      
      'NICK
      If UCase(PData(2)) = "NICK" Then
        If ChatFrs.FrameType = 4 And UCase(ChatFrs.Caption) = UCase(mVon) Then
          ChatFrs.PrintText TimeDbt + "- " + mVon + " " + TeVal.Nick + " ", CoVal.ServerMSG, True, True
          ChatFrs.PrintText Right(PData(3), Len(PData(3)) - 1), CoVal.ServerMSG, False, False, "SMM:" + Right(PData(3), Len(PData(3)) - 1)
          ChatFrs.Caption = Mid(PData(3), 2)
          Form1.ColumRedraw
        End If
        If ChatFrs.FrameType = 1 And UCase(mVon) = UCase(Nick) Then
          Nick = Mid(PData(3), 2)
          ChatFrs.PrintText TimeDbt + "- " + mVon + " " + TeVal.Nick + " ", CoVal.ServerMSG, True, False
          ChatFrs.PrintText Mid(PData(3), 2), CoVal.ServerMSG, False, False, "SMM:" + Mid(PData(3), 2)
        End If
        If ChatFrs.FrameType = 0 Then
          i = 0
          For i = 0 To ChatFrs.UListF.ListCount - 1
            If UCase(ChatNoS(ChatFrs.UListF.List(i))) = UCase(mVon) Then
              ChatFrs.PrintText TimeDbt + "- " + mVon + " " + TeVal.Nick + " ", CoVal.ServerMSG, True, True
              ChatFrs.PrintText Mid(PData(3), 2), CoVal.ServerMSG, False, False, "SMM:" + Mid(PData(3), 2)
              If ChatFrs.UListF.List(i) = ChatNoS(ChatFrs.UListF.List(i)) Then DAT = "" Else DAT = Left(ChatFrs.UListF.List(i), 1)
              ChatFrs.UListF.RemoveByText ChatFrs.UListF.List(i)
              If ChatFrs Is FrontCFrame Then
                ChatFrs.UListF.Add DAT + Mid(PData(3), 2)
              Else
                ChatFrs.UListF.Add DAT + Mid(PData(3), 2), , False
              End If
              Exit For
            End If
          Next
        End If
      End If
      
      'StartCommands
      If UCase(PData(2)) = "376" Then
        If ChatFrs.FrameType = 1 Then
          i = -1
          Do
            I2 = i
            i = InStr(i + 2, StartCommands, vbNewLine)
            If i = 0 Then i = Len(StartCommands) + 1
            If Mid(StartCommands, I2 + 2, 1) = "/" Or Mid(StartCommands, I2 + 2, 1) = "$" Then I2 = I2 + 1
            If i - I2 > 2 Then SendLine Mid(StartCommands, I2 + 2, i - I2 - 2)
          Loop Until i = Len(StartCommands) + 1
        End If
      End If
    
      'UserListe
      If UCase(PData(2)) = "353" Then
        i = InStr(3, PData(4), " ")
        If UCase(ChatFrs.Caption) = UCase(Mid(PData(4), 3, i - 3)) And ChatFrs.FrameType = 0 Then
          i = i + 1
          Do
            I2 = i
            i = InStr(i + 1, PData(4), " ")
            If i = 0 Then i = Len(PData(4)) + 1
            If ChatFrs Is FrontCFrame Then
              ChatFrs.UListF.Add Mid(PData(4), I2 + 1, i - I2 - 1)
            Else
              ChatFrs.UListF.Add Mid(PData(4), I2 + 1, i - I2 - 1), , False
          End If
          Loop Until i = Len(PData(4)) + 1
        End If
      End If
      
      'StartRoomListe
      If UCase(PData(2)) = "321" Then
        If ChatFrs.FrameType = 2 Then
          ChatFrs.UListF.Clear
          ListRooms = 1
        Else
          If ListRooms = 0 Then ListRooms = 2
        End If
      End If
      
      'RoomListe
      If UCase(PData(2)) = "322" Then
        If ChatFrs.FrameType = 2 Then
          DAT = ""
          i = InStr(1, PData(4), " ")
          If i < 1 Then Exit Sub
          If Mid(PData(4), 1, 1) <> "*" Then
            DAT = DAT + Mid(PData(4), 1, i - 1) + Chr(0)
            I2 = i
            i = InStr(i + 1, PData(4), " ")
            If i - I2 > 4 Or Not i > 0 Then Exit Sub
            DAT = DAT + Space(5 - i + I2) + Mid(PData(4), I2 + 1, i - I2 - 1) + Chr(0)
            DAT = DAT + Right(PData(4), Len(PData(4)) - i - 1) + Chr(0)
            If ChatFrs Is FrontCFrame Then
              ChatFrs.UListF.Add DAT
            Else
              ChatFrs.UListF.Add DAT, , False
            End If
          End If
        End If
      End If
    
      'Ping
      If UCase(PData(0)) = "PING" Then
        If ChatFrs.FrameType = 1 Then
          SendLine "PONG :" + Right(data, Len(data) - 6)
          ChatFrs.PrintText "- Ping/Pong", &H800000, , True
        End If
      End If
      
      If UCase(PData(2)) = "PONG" Then
        If ChatFrs.FrameType = 1 Then
          ChatFrs.PrintText "- Pong", &H800000, , True
        End If
      End If
    
    End If
  Next
  If Joined = 2 Then
    NewFrame Me, Right(PData(3), Len(PData(3)) - 1), 0, 0
  End If
  If ListRooms = 2 Then
    Set ChatFrs = NewFrame(Me, "Rooms", 2, 0)
    ChatFrs.UListF.SetColum 0, "Name", 120
    ChatFrs.UListF.AddColum "User", 50
    ChatFrs.UListF.AddColum "Topic", , True
  End If
  If PrivMSG = 2 Then
    Set ChatFrs = NewFrame(Me, mVon, 4, 0, False)
    If Left(PData(4), 9) = ":" + Chr(1) + "ACTION " Then
      ChatFrs.PrintText TimeDbt + "* ", CoVal.NormalText, True, True
      ChatFrs.PrintText mVon, CoVal.NormalText, True, False, "SMM:" + mVon
      ChatFrs.PrintText " " + Mid(PData(4), 10, Len(PData(4)) - 10), CoVal.NormalText, False, False
      SetAl ChatFrs
    Else
      ChatFrs.PrintText TimeDat + "<" + mVon + ">", CoVal.NormalVon, , True
      ChatFrs.PrintText Right(PData(4), Len(PData(4)) - 1), CoVal.NormalText, , False
      SetAl ChatFrs
    End If
  End If
  If sNotice = 2 Then
    sNotice = 1
    For Each ChatFrs In ChatFrames
      If ChatFrs.IRCConn Is Me And ChatFrs.FrameType = 0 Then
        i = 0
        For i = 0 To ChatFrs.UListF.ListCount - 1
          If UCase(ChatNoS(ChatFrs.UListF.List(i))) = UCase(mVon) Then
            ChatFrs.PrintText TimeDat + "<" + mVon + " whispers>", CoVal.FlusterVon, True, True, "ANN:" + mVon
            ChatFrs.PrintText Right(PData(4), Len(PData(4)) - 1), CoVal.NormalText, , False
            SetAl ChatFrs
            sNotice = 2
            Exit For
          End If
        Next
        If sNotice = 2 Then Exit For
      End If
    Next
    If sNotice = 1 Then
      For Each ChatFrs In ChatFrames
        If ChatFrs.IRCConn Is Me And ChatFrs.FrameType = 1 Then
          ChatFrs.PrintText TimeDbt + "- ", CoVal.Notice, True, True
          ChatFrs.PrintText mVon, CoVal.Notice, True, False, "SMM:" + mVon
          ChatFrs.PrintText " " + Right(PData(4), Len(PData(4)) - 1), CoVal.Notice, False, False
          SetAl ChatFrs
          Exit For
        End If
      Next
    End If
  End If
  If sDCC = 2 Then
    sDCC = 1
    For Each ChatFrs In ChatFrames
      If ChatFrs.IRCConn Is Me And ChatFrs.FrameType = 0 Then
        i = 0
        For i = 0 To ChatFrs.UListF.ListCount - 1
          If UCase(ChatNoS(ChatFrs.UListF.List(i))) = UCase(mVon) Then
            DCCConnRequest mVon, PData(4), ChatFrs, Nick
            SetAl ChatFrs
            sDCC = 2
            Exit For
          End If
        Next
        If sDCC = 2 Then Exit For
      End If
    Next
    If sDCC = 1 Then
      For Each ChatFrs In ChatFrames
        If ChatFrs.IRCConn Is Me And ChatFrs.FrameType = 1 Then
          DCCConnRequest mVon, PData(4), ChatFrs, Nick
          SetAl ChatFrs
          Exit For
        End If
      Next
    End If
  End If
End Sub

Private Sub WinS_DataArrival(BinData() As Byte, RemoteHostIP As String)
  Dim i As Long
  Dim I2 As Long
  Buffer = Buffer + StrConv(BinData, vbUnicode)
  If WaitLoop = True Then Exit Sub
  WaitLoop = True
  ExLoop = False
  i = 0
  Do Until ExLoop = True
    DoEvents
    I2 = i
    If I2 = 0 Then I2 = -1
    i = InStr(i + 2, Buffer, Chr(10))
    If i = 0 Then Exit Do
    If Mid(Buffer, i - 1, 1) = Chr(13) Then
      PraseCommand Mid(Buffer, I2 + 2, i - I2 - 3)
      i = i - 1
    Else
      PraseCommand Mid(Buffer, I2 + 2, i - I2 - 2)
      i = i - 1
    End If
  Loop
  Buffer = Right(Buffer, Len(Buffer) - I2 - 1)
  WaitLoop = False
End Sub


