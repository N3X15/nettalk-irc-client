Attribute VB_Name = "PublicData"
Option Explicit

Public Type TextStrings
  Join As String
  Leave As String
  Quit As String
  Nick As String
  ConStart As String
  ConClose As String
  ConErr As String
  ConOK As String
  ConCancel As String
  Connected As String
  Closed As String
  Connecting As String
  UserRank As String
  CUserRank As String
  DCCquestion As String
  DCCok As String
  Kick As String
  Kick2 As String
  RoomEnter As String
  WisperTo As String
  Wispers As String
  WispersTo As String
  msgDel As String
  msgNoDel As String
  dccFileNotFound As String
  FileExist As String
  AutoUpdate1 As String
  AutoUpdate2 As String
  ExtWorning As String
  DccCaption As String
  DccConnCancel As String
  DccFinished As String
  Caption1 As String
  Caption2 As String
  NoLogs As String
  lsYes As String
  lsNo As String
  lsUnknowen As String
  butOK As String
  butCancel As String
  butApply As String
  menNew As String
  menDel As String
  menValues As String
  menCut As String
  menCopy As String
  menPast As String
  menConn As String
  menDis As String
  menRooms As String
  menSend As String
  menColors As String
  menOpt As String
  menFrinds As String
  menHelp As String
  menSymbol As String
  menMenue As String
  menScriptStart As String
  menScriptStop As String
  menScript As String
  ls1Caption As String
  ls1Server As String
  ls1Used As String
  ls1State As String
  ls1Date As String
  ls1Auto As String
  lsServers As String
  lsRooms As String
  lsFrinds As String
  lsScript As String
  ls2Name As String
  ls2User As String
  ls2Topic As String
  ls3FileName As String
  ls3Progres As String
  ls3Speed As String
  ls3Time As String
  ls3Size As String
  ls3User As String
  ls3Path As String
  ls4Nick As String
  ls4State As String
  ls4Server As String
  ls4Online As String
  ls5Maske As String
  ls5Von As String
  ls5Vor As String
  cClientMsg As String
  cErrMsg As String
  cNickW As String
  cLink As String
  cText As String
  cNickN As String
  cNotice As String
  cMSG As String
  chMSG As String
  ccHG As String
  ctUserL As String
  cULHG As String
  cTextF As String
  cTextFHG As String
  cTimeStamp As String
  cOwnNick As String
  oGeneral As String
  oMsgs As String
  oConnections As String
  oShortcuts As String
  oApp As String
  oPlugins As String
  opPos1 As String
  opPos2 As String
  NewCTitel As String
  fFunction As String
  fOn As String
  fOff As String
  FrindOnline As String
  scrTimeout As String
  SearchResults As String
  TimeOut As String
  TopicSet As String
  Ignore As String
  UnIgnore As String
  scReload As String
  ConRemove As String
  Away As String
  WhoisRealName As String
  WhoisHostMask As String
  WhoisList As String
  WhoisRegedAs As String
  WhoisIn As String
  WhoisServer As String
  WhoisIdle As String
  WhoisSO As String
  WhoisReged As String
  TopicSeter As String
  Invite As String
  bezChannel As String
  bezUser As String
  bezGlobal As String
End Type

Public Type ColorValues
  NormalText As Long
  NormalVon As Long
  FlusterVon As Long
  ClientMSG As Long
  ServerMSG As Long
  UserMSG As Long
  ErrorMSG As Long
  Notice As Long
  Link As Long
  bColumDisc As Long
  bColumBusy As Long
  TimeStamp As Long
  OwnNick As Long
  ButtonShadow As Long
  Light3D As Long
  BackColor As Long
  MenuText As Long
  GrayText As Long
  Shadow3D As Long
  Highlight3D  As Long
  ButtonText  As Long
End Type

Public Type ConnValues
  Server As String
  Caption As String
  Pass As String
  Port As Long
  Commands As String
  LastUsed As Date
  Nick As String
  Name As String
  State As Integer
  ServerMode As Integer
  UserID As String
  ServerPass As String
  CreateDate As Date
  CreateType As Integer
  CTCKey As String
  CTCOn As Integer
  IgnoreList As String
  Silent As Integer
  Temp As Integer
  ConnectedIndex As Integer
  UTF8 As Integer
  UTF8dyn As Integer
  Codepage As Long
  UseSSL As Integer
End Type

Public Type WinButton
  Hide As Boolean
  Disable As Boolean
  SymbolIndex As Integer
  Text As String
  Down As Boolean
End Type

Public Type RECT
  Left As Long
  Top As Long
  Right As Long
  Bottom As Long
End Type

Public Type WINDOWPLACEMENT
  Length           As Long
  FLAGS            As Long
  showCmd          As Long
  ptMinPosition    As POINTAPI
  ptMaxPosition    As POINTAPI
  rcNormalPosition As RECT
End Type

Public Type NOTIFYICONDATA
  cbSize As Long
  hWnd As Long
  uID As Long
  uFlags As Long
  uCallbackMessage As Long
  hIcon As Long
  szTip As String * 64
End Type

Private Type SHITEMID
  cb As Long
  abID As Byte
End Type

Private Type ITEMIDLIST
  mkid As SHITEMID
End Type


Private Type SYSTEMTIME
  wYear As Integer
  wMonth As Integer
  wDayOfWeek As Integer
  wDay As Integer
  wHour As Integer
  wMinute As Integer
  wSecond As Integer
  wMilliseconds As Integer
End Type

Public Type MENUITEMINFO
  cbSize As Long
  fMask As Long
  fType As Long
  fState As Long
  wID As Long
  hSubMenu As Long
  hbmpChecked As Long
  hbmpUnchecked As Long
  dwItemData As Long
  dwTypeData As String
  ccH As Long
End Type

Private Declare Sub GetSystemTime Lib "kernel32" ( _
  lpSystemTime As SYSTEMTIME)

Public Declare Function ShellExecute Lib "shell32.dll" _
Alias "ShellExecuteA" (ByVal hWnd As Long, _
ByVal lpOperation As String, ByVal lpFile As String, _
ByVal lpParameters As String, ByVal lpDirectory As String, _
ByVal nShowCmd As Long) As Long

Public Declare Function ClientToScreen Lib "user32" (ByVal hWnd As Long, lpPoint As POINTAPI) As Long
Public Declare Function GetMenuItemCount Lib "user32" (ByVal hMenu As Long) As Long
Public Declare Function GetMenu Lib "user32" (ByVal hWnd As Long) As Long
Public Declare Function GetSubMenu Lib "user32" (ByVal hMenu As Long, ByVal nPos As Long) As Long
Public Declare Function GetMenuItemInfo Lib "user32" Alias "GetMenuItemInfoA" (ByVal hMenu As Long, ByVal un As Long, ByVal b As Boolean, lpmii As MENUITEMINFO) As Long
Public Declare Function TrackPopupMenu Lib "user32" (ByVal hMenu As Long, ByVal wFlags As Long, ByVal X As Long, ByVal Y As Long, ByVal nReserved As Long, ByVal hWnd As Long, ByVal lprc As Any) As Long

Public Declare Function LoadImage Lib "user32" Alias "LoadImageA" _
  (ByVal hInst As Long, ByVal lpsz As Long, ByVal dwImageType As Long, _
  ByVal dwDesiredWidth As Long, ByVal dwDesiredHeight As Long, ByVal dwFlags As Long) As Long
Public Declare Function Shell_NotifyIcon Lib "shell32.dll" Alias "Shell_NotifyIconA" (ByVal dwMessage As Long, lpData As NOTIFYICONDATA) As Long
Public Declare Function ReleaseCapture Lib "user32" () As Long
Public Declare Function CreateCompatibleDC Lib "gdi32" (ByVal hDC As Long) As Long
Public Declare Function GetDC Lib "user32" (ByVal hWnd As Long) As Long
Public Declare Function GetDeviceCaps Lib "gdi32" (ByVal hDC As Long, ByVal nIndex As Long) As Long
Public Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal X As Long, ByVal Y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal dwRop As Long) As Long
Public Declare Function CreateCompatibleBitmap Lib "gdi32" (ByVal hDC As Long, ByVal nWidth As Long, ByVal nHeight As Long) As Long
Public Declare Function DeleteDC Lib "gdi32" (ByVal hDC As Long) As Long
Public Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long
Public Declare Function CreateFont Lib "gdi32" Alias "CreateFontA" (ByVal nHeight As Long, ByVal nWidth As Long, ByVal nEscapement As Long, ByVal nOrientation As Long, ByVal fnWeight As Long, ByVal fdwItalic As Boolean, ByVal fdwUnderline As Boolean, ByVal fdwStrikeOut As Boolean, ByVal fdwCharSet As Long, ByVal fdwOutputPrecision As Long, ByVal fdwClipPrecision As Long, ByVal fdwQuality As Long, ByVal fdwPitchAndFamily As Long, ByVal lpszFace As String) As Long
Public Declare Function SelectObject Lib "gdi32" (ByVal hDC As Long, ByVal hObject As Long) As Long
Public Declare Function MulDiv Lib "kernel32" (ByVal nNumber As Long, ByVal nNumerator As Long, ByVal nDenominator As Long) As Long
Public Declare Function SetBkMode Lib "gdi32" (ByVal hDC As Long, ByVal nBkMode As Long) As Long
Public Declare Function GetSysColorBrush Lib "user32" (ByVal nIndex As Long) As Long
Public Declare Function FillRect Lib "user32" (ByVal hDC As Long, lpRect As RECT, ByVal hBrush As Long) As Long
Public Declare Function SetRect Lib "user32" (lpRect As RECT, ByVal X1 As Long, ByVal Y1 As Long, ByVal X2 As Long, ByVal Y2 As Long) As Long
Public Declare Function CreateSolidBrush Lib "gdi32" (ByVal crColor As Long) As Long
Public Declare Function TranslateColor Lib "olepro32.dll" Alias "OleTranslateColor" (ByVal clr As Long, ByVal palet As Long, col As Long) As Long
Public Declare Function SetBkColor Lib "gdi32" (ByVal hDC As Long, ByVal crColor As Long) As Long
Public Declare Function SetTextColor Lib "gdi32" (ByVal hDC As Long, ByVal crColor As Long) As Long
Public Declare Function ReleaseDC Lib "user32" (ByVal hWnd As Long, ByVal hDC As Long) As Long

Public Declare Function SHGetSpecialFolderLocation Lib "shell32.dll" (ByVal hwndOwner As Long, ByVal nFolder As Long, pidl As ITEMIDLIST) As Long
Public Declare Function SHGetPathFromIDList Lib "shell32.dll" Alias "SHGetPathFromIDListA" (ByVal pidl As Long, ByVal pszPath As String) As Long
Public Declare Function SetForegroundWindow Lib "user32" (ByVal hWnd As Long) As Long
Public Declare Function GetTickCount Lib "kernel32" () As Long
Public Declare Function GetWindowPlacement Lib "user32" (ByVal hWnd As Long, ByRef lpwndpl As WINDOWPLACEMENT) As Long
Public Declare Function GetForegroundWindow Lib "user32" () As Long
Public Declare Function FlashWindow Lib "user32" (ByVal hWnd As Long, ByVal bInvert As Long) As Long

' Clipboard functions:
Const CF_UNICODETEXT = 13

Private Declare Function OpenClipboard Lib "user32" (ByVal hWnd As Long) As Long
Private Declare Function CloseClipboard Lib "user32" () As Long
Private Declare Function SetClipboardData Lib "user32" (ByVal wFormat As Long, ByVal hMem As Long) As Long
Private Declare Function EmptyClipboard Lib "user32" () As Long


' Memory functions:
Private Const GMEM_DDESHARE = &H2000

Private Declare Function GlobalAlloc Lib "kernel32" (ByVal wFlags As Long, ByVal dwBytes As Long) As Long
Private Declare Function GlobalLock Lib "kernel32" (ByVal hMem As Long) As Long
Private Declare Function GlobalUnlock Lib "kernel32" (ByVal hMem As Long) As Long

Public Declare Function TextOutW Lib "gdi32" (ByVal hDC As Long, ByVal lX As Long, ByVal lY As Long, ByVal pText As Long, ByVal lTextCharCount As Long) As Long

Global T As NOTIFYICONDATA
Global Const NIM_ADD = &H0
Global Const NIM_MODIFY = &H1
Global Const NIM_DELETE = &H2

Global Const NIF_MESSAGE = &H1
Global Const NIF_ICON = &H2
Global Const NIF_TIP = &H4

Public Const WM_KEYPRESS = &H102
Public Const WM_KEYDOWN = &H100
Public Const WM_KEYUP = &H101
Public Const WM_LBUTTONDOWN = &H201
Public Const WM_LBUTTONUP = &H202
Public Const WM_LBUTTONDBLCLK = &H203
Public Const WM_RBUTTONUP = &H205
Public Const WM_SETTEXT = &HC
Public Const WM_GETTEXT = &HD
Public Const WM_ACTIVATE = &H6

Public Const MAX_LNG = &H7FFFFFFF

Public Const cfRoom = 0
Public Const cfStatus = 1
Public Const cfRoomList = 2
Public Const cfServerList = 3
Public Const cfPrivate = 4
Public Const cfScript = 5
Public Const cfDCC = 6
Public Const cfLog = 7
Public Const cfFriends = 8
Public Const cfDebug = 9
Public Const cfWindow = 10

Public Const SW_NORMAL = 1
Public Const SW_MAXIMIZE = 3
Public Const SW_MINIMIZE = 6

Public Const GWL_WNDPROC = (-4)

Public Const START_DATE = 25569 ' = 01.01.1970

Public Const LINE_MAX_LEN = 460

Public Const DAY_SEC = 86400 '24*3600

'LoadImage Konstanten
Public Const IMAGE_ICON = 1
Public Const LR_COPYFROMRESOURCE = &H4000

Public LastUsedRealName As String
Public LastUsedNick As String
Public LastUsedUserID As String
Public UModeOSD As Boolean
Public UModeBalken As Boolean
Public HideFormWithMin As Boolean
Public NoQuitWithX As Boolean
Public AutoReJoin As Boolean
Public RetryTime As Integer
Public RemaindRooms As Boolean
Public RemaindConns As Boolean
Public RemaindList As Boolean
Public StartVisible As Boolean
Public OpFrameIndex As Integer
Public LogmodePrivate As Boolean
Public LogmodeRoom As Boolean
Public ShowTimeStampText As Boolean
Public PlayWav As Boolean
Public PlayBeep As Boolean
Public WavePath As String
Public EaOSDPriv As Boolean
Public EaOSDRoom As Boolean
Public OsdRoomList As String
Public SoundStumm As Boolean
Public LastSPath As String
Public LastLPath As String
Public LastServers As String
Public HGBildPath As String
Public HGDrawMode As Integer
Public FontTypeName As String
Public FontTypeSize As Integer
Public FontTypeBold As Integer
Public StaticDCCPort As Long
Public DCCServerPort As Long
Public IRCTimeOut As Integer
Public PlayWav2 As Boolean
Public PlayBeep2 As Boolean
Public WavePath2 As String
Public EaOSDPriv2 As Boolean
Public UseSecIP As Boolean
Public dccMaxUpSpeed As Long
Public dccPaketSize As Long
Public dccAutoAcc As Boolean
Public PrivLogMaxSize As Long
Public PublLogMaxSize As Long
Public BeepArr As String
Public DCCPath As String
Public SockProxyServerGlob As String
Public SockProxyPortGlob As Long
Public SockProxyTypeGlob As Long
Public QuitMsg As String
Public FingerData As String
Public LastScSize As Long
Public UTF8on As Boolean
Public CurrentCodepage As Long
Public InputCodepage As Long
Public BasicCodepage As Long
Public LastUsedChans As String

Public SSLIsAvailable As Boolean

Public OpFormIsLoaded As Boolean
Public LastWispMsg As String
Public SendedBytes As Double
Public RecevedBytes As Double

Public IRCconns As New Collection
Public DCCconns As New Collection
Public ChatFrames As New Collection
Public FrontCFrame As ChatFrame
Public LastFrontFrame As ChatFrame
Public SellCFrame As ChatFrame
Public RListW As Long
Public ConnList() As ConnValues
Public ConnListCount As Integer
Public CoVal As ColorValues
Public TeVal As TextStrings
Public AppUnloading  As Boolean
Public TrayIconState As Integer

Public TabList() As Long
Public RefrMini As Boolean
Public ComIndex() As String
Public ComTranzList() As String
Public WisperTo As String
Public AppPath As String
Public UserPath As String
Public DisIcon As Boolean

Public OnScr As OSDisplay

Public Function SetClipboardText(Text As String, hWnd As Long) As Boolean
  Dim lSize As Long
  Dim lPtr As Long
  Dim hMem As Long
    
  If IsNt Then
    lSize = Len(Text) * 2 + 2
    hMem = GlobalAlloc(GMEM_DDESHARE, lSize)
    If hMem Then
      lPtr = GlobalLock(hMem)
      MemCopy ByVal lPtr, StrPtr(Text), lSize
      GlobalUnlock hMem
      OpenClipboard hWnd
      EmptyClipboard
      SetClipboardText = SetClipboardData(CF_UNICODETEXT, hMem)
      CloseClipboard
    End If
  Else
    Clipboard.Clear
    Clipboard.SetText Text
  End If
End Function

Public Function GMTDiff() As Long
  Dim SysTime As SYSTEMTIME
  
  On Error Resume Next
  GetSystemTime SysTime
  GMTDiff = DateDiff("s", DateSerial(SysTime.wYear, SysTime.wMonth, SysTime.wDay) + TimeSerial(SysTime.wHour, SysTime.wMinute, SysTime.wSecond), Now)
End Function

Public Function AltTimer() As Single
  AltTimer = GetTickCount / 1000
End Function

Public Function AltTimeDiff(ByVal fTime As Single) As Single
  If fTime * CDbl(1000) - &H7FFFFFFF > GetTickCount Then
    AltTimeDiff = (GetTickCount + &H7FFFFFFF * CDbl(2)) / 1000 - fTime
  Else
    AltTimeDiff = GetTickCount / CDbl(1000) - fTime
  End If
End Function

Public Sub FlashMainWindow()
  If Form1.Visible And IsAfterVista Then FlashWindow Form1.hWnd, 0
End Sub

Public Function AddRemOsdRL(Caption As String, Optional CheckOnly As Boolean) As Boolean
  Dim I As Integer
  I = InStr(1, " " + OsdRoomList + " ", " " + Caption + " ", vbTextCompare)
  If CheckOnly Then
    AddRemOsdRL = I > 0 And EaOSDRoom
  Else
    If I = 0 Then
      OsdRoomList = Trim(Trim(OsdRoomList) + " " + Caption)
      EaOSDRoom = True
      AddRemOsdRL = True
    Else
      If EaOSDRoom Then
        OsdRoomList = Trim(Trim(Left(OsdRoomList, I - 1)) + " " + Trim(Mid(OsdRoomList, I + Len(Caption))))
        AddRemOsdRL = False
      Else
        EaOSDRoom = True
        AddRemOsdRL = True
      End If
    End If
  End If
End Function

Public Function GetAppPath() As String
  Dim nItemList As ITEMIDLIST
  Dim nPath As String
  Const NOERROR = 0
  If SHGetSpecialFolderLocation(0, &H1A, nItemList) = NOERROR Then
    nPath = Space$(260)
    If SHGetPathFromIDList(nItemList.mkid.cb, nPath) <> 0 Then
      GetAppPath = Left$(nPath, InStr(nPath, vbNullChar) - 1)
    End If
  End If
  'If App.EXEName = "Projekt1" Then GetAppPath = AppPath + "Test"
End Function

Public Sub RefreshBGP()
  Dim DAT As String
  On Error Resume Next
  DAT = HGBildPath
  If InStr(DAT, "\") = 0 Then
    DAT = AppPath + Dir(AppPath + DAT)
  End If
  Form1.BgPicture.Picture = LoadPicture(DAT)
  If Err <> 0 Then
    Form1.BgPicture.Picture = LoadPicture("")
    Form1.BgPicture.Height = 32
    Form1.BgPicture.Width = 640
    Form1.BgPicture.FontName = "Arial"
    Form1.BgPicture.FontSize = 12
    Form1.BgPicture.ForeColor = RGB(255, 20, 20)
    Form1.BgPicture.Picture = Nothing
    Form1.BgPicture.Line (0, 0)-(640, 32), RGB(128, 128, 128), BF
    Form1.BgPicture.CurrentX = 4
    Form1.BgPicture.CurrentY = 4
    Form1.BgPicture.Print Err.Description
  Else
    If Form1.BgPicture.Picture.Height / 26.45 < 2000 And Form1.BgPicture.Picture.Width / 26.45 < 2200 Then
      Form1.BgPicture.Height = Form1.BgPicture.Picture.Height / 26.45
      Form1.BgPicture.Width = Form1.BgPicture.Picture.Width / 26.45
    End If
  End If
End Sub

Public Sub ShowChatText(Text As String, Optional Von As String, Optional Server As String, Optional ChFrame As ChatFrame)
  Dim I As Single
  Dim DAT As String
  
  For I = 0 To Forms.Count - 1
    If Forms(I).Name <> "Form1" And Forms(I).Name <> "Form4" Then Exit Sub
  Next
  
  Load Form4
  
  Form4.Buffer2.FontSize = 10

  If TextWW("-> " + Text, Form4.Buffer2.hDC) + 24 + 50 < 300 Then
    Form4.SetSize 300
  Else
    Form4.SetSize TextWW("-> " + Text, Form4.Buffer2.hDC) + 24 + 50
  End If
  Form4.IntBackBuffer
  Form4.IntFrontBuffer Text, Von, Server, ChFrame
  
  Form4.AnimateON
End Sub

'Public Function GetBrowser() As String
'  Dim hKey As Long
'  Dim nBytes As Long
'  Dim nStart As Long
'  Dim nEnd As Long
'  Static RetVal As String
'
'  Const HttpAssoc As String = "http\shell\open\command"
'
'  If Len(RetVal) = 0 Then
'    If RegOpenKey(HKEY_CLASSES_ROOT, HttpAssoc, hKey) = ERROR_SUCCESS Then
'      If RegQueryValueEx(hKey, vbNullString, 0, ByVal 0, ByVal RetVal, nBytes) = ERROR_SUCCESS Then
'        RetVal = Space(nBytes)
'        If RegQueryValueEx(hKey, vbNullString, 0, ByVal 0, ByVal RetVal, nBytes) = ERROR_SUCCESS Then
'          RetVal = Left(RetVal, nBytes - 1)
'        End If
'      End If
'      RegCloseKey hKey
'    End If
'
'    nStart = InStr(RetVal, """")
'    nEnd = InStr(nStart + 1, RetVal, """")
'    If nStart = 1 And nEnd > 1 Then
'      RetVal = Mid(RetVal, nStart + 1, nEnd - nStart - 1)
'    Else
'      nEnd = Len(CutDirFromPath(RetVal, "\"))
'      If nEnd Then
'        nEnd = InStr(nEnd, RetVal, " ")
'        If nEnd Then
'          RetVal = Left(RetVal, nEnd - 1)
'        End If
'      End If
'    End If
'  End If
'  GetBrowser = RetVal
'End Function

Public Function CallWebSite(ByVal URL As String, Optional NewWindow As Boolean, Optional ByVal Mode As Long = vbNormalFocus) As Long
  'If NewWindow Then
  '  CallWebSite = ShellExecute(0&, vbNullString, GetBrowser, URL, vbNullString, Mode)
  'Else
    CallWebSite = ShellExecute(0&, vbNullString, URL, vbNullString, vbNullString, Mode)
  'End If
End Function

Public Sub DrawTLine(CFrame As ChatFrame, TextMask As String, Color As Long, Optional P1 As String, Optional P2 As String, Optional P3 As String)
  Dim I  As Integer
  Dim I2 As Integer
  Dim I3 As Single
  Dim SetInStr As String
  Dim LastChar As Integer
  Dim Trigger As Long
  
  I = 0
  Do
    I = InStr(I + 1, TextMask, "%")
    If I > 0 Then
      I2 = I
      If Mid(TextMask, I + 1, 1) = "n" Then
        Trigger = 5
        I = I + 1
      Else
        Trigger = 0
      End If
      I3 = Val(Mid(TextMask, I + 1, 1))
      Select Case I3
        Case 1: SetInStr = P1
        Case 2: SetInStr = P2
        Case 3: SetInStr = P3
        Case Else: SetInStr = ""
      End Select
      If I2 - LastChar - 1 > 0 Then
        CFrame.PrintText Mid(TextMask, LastChar + 1, I2 - LastChar - 1), Color, True, LastChar = 0
      End If
      If Len(SetInStr) Then CFrame.PrintText SetInStr, Color, True, I2 - LastChar - 1 < 1 And LastChar = 0, Trigger
    Else
      CFrame.PrintText Mid(TextMask, LastChar + 1), Color, True, LastChar = 0
    End If
    LastChar = I + 1
    
  Loop Until I = 0
End Sub

Public Function SetInVals(TextMask As String, Optional P1 As String, Optional P2 As String, Optional P3 As String) As String
  Dim I  As Integer
  Dim I2 As Integer
  Dim I3 As Single
  Dim DAT As String
  Dim SetInStr As String
  Dim LastChar As Integer
  
  I = 0
  Do
    I = InStr(I + 1, TextMask, "%")
    If I > 0 Then
      I2 = I
      If Mid(TextMask, I + 1, 1) = "n" Then I = I + 1
      I3 = Val(Mid(TextMask, I + 1, 1))
      Select Case I3
        Case 1: SetInStr = P1
        Case 2: SetInStr = P2
        Case 3: SetInStr = P3
        Case Else: SetInStr = ""
      End Select
    Else
      SetInStr = ""
      I2 = Len(TextMask) + 1
    End If
    DAT = DAT + Mid(TextMask, LastChar + 1, I2 - LastChar - 1) + SetInStr
    LastChar = I + 1
  Loop Until I = 0
  SetInVals = DAT
End Function

Public Sub DrawSLine(CFrame As ChatFrame, IsComm As Byte, Text As String, Color As Long, _
Optional Text2 As String, Optional Trigger As Long, Optional TrStr As String, _
Optional Text3 As String, Optional Keyed As Boolean)

  Dim TColor As Long
  
  If Keyed Then CFrame.TextF.SetLineIcon 65
  
  If Len(Text3) > 0 Or IsComm = 2 Then
    TColor = Color
    CFrame.PrintText Text, Color, True, True
  Else
    TColor = CoVal.NormalText
    CFrame.PrintText Text, Color, True, True, Trigger, , TrStr
  End If
  If Len(Text2) > 0 Then
    If Len(Text3) > 0 Or IsComm = 2 Then
      CFrame.PrintText Text2, TColor, True, False, Trigger, , TrStr
      CFrame.PrintText Text3, TColor, True, False
    Else
      CFrame.PrintText Text2, TColor, True, False
    End If
  End If
End Sub

Public Sub SetGlobalTimeShow(Visible As Boolean)
  Dim I As Integer
  
  For I = 1 To ChatFrames.Count
    Select Case ChatFrames(I).FrameType
      Case cfRoom, cfStatus, cfPrivate, cfDebug
        ChatFrames(I).TextF.SetShowTime Visible, False   '  True
    End Select
  Next
End Sub

Public Function TextChrs(Text As String, ByVal MaxPixWidth As Long, opObject As Object) As String
  Dim I As Integer
  Dim I2 As Integer
  Dim nOb As Integer
  Dim nUn As Integer
  Dim nMit As Integer
  If MaxPixWidth < 1 Then TextChrs = "": Exit Function
  nUn = 0
  nOb = Len(Text)
  Do
    nMit = (nOb - nUn) / 2 + nUn
    If nMit = nUn Or nMit = nOb Then
      TextChrs = Left(Text, nUn + 1)
      Exit Do
    End If
    If TextWW(Left(Text, nMit), opObject.hDC) < MaxPixWidth Then
      nUn = nMit
    Else
      nOb = nMit
    End If
  Loop
  I = InStr(Text, vbNewLine)
  If I - 3 < nUn And I <> 0 Then TextChrs = Left(Text, I + 1)
End Function

Public Sub GiveSortedC(CArray() As Integer)
  Dim I As Integer
  Dim I2 As Integer
  Dim Zindex As Integer
  Dim TCFrame As ChatFrame
  Dim TCFrame2 As ChatFrame
  Dim TempArray() As Integer
  
  ReDim CArray(ChatFrames.Count)
  ReDim TempArray(ChatFrames.Count)
  
  For I = 1 To ChatFrames.Count
    Set TCFrame = ChatFrames(I)
    I2 = 0
    If TCFrame.FrameType <> cfRoom And TCFrame.FrameType <> cfPrivate And TCFrame.FrameType <> cfRoomList Then
      If TCFrame.FrameType = 1 Then
        TempArray(I) = 1
        Zindex = Zindex + 1
        CArray(Zindex) = I
      End If
      For Each TCFrame2 In ChatFrames
        I2 = I2 + 1
        If TCFrame2.FrameType <> 1 Then
          If TempArray(I2) = 0 Then
            If TCFrame2.ConnTyp = -1 Then TempArray(I2) = 1
            If TCFrame2.ConnTyp = 0 Then
              If TCFrame.IrcConn Is TCFrame2.IrcConn Then TempArray(I2) = 1
            End If
            If TCFrame.ConnTyp = 2 Then
              If TCFrame.DCCConn Is TCFrame2.DCCConn Then TempArray(I2) = 1
            End If
            If TempArray(I2) = 1 Then
              Zindex = Zindex + 1
              CArray(Zindex) = I2
            End If
          End If
        End If
      Next
    End If
  Next
End Sub

Public Sub StartConnection(Optional ByVal cIDint As Integer = -1)
  Dim IrcConn As IRCConnection
  Dim cId As Integer
  Dim I2 As Integer
  Dim I As Integer
  Dim StFrame As New ChatFrame
  
  If cIDint = -1 Then
    If SellCFrame Is Nothing Then
      For I = 1 To ChatFrames.Count
        Set StFrame = ChatFrames(I)
        If StFrame.ConnTyp = -1 Then Exit For
      Next
      If I > ChatFrames.Count Then Exit Sub
      I = StFrame.UListF.ListIndex
      If I > -1 And I < StFrame.UListF.ListCount Then
        cId = ConnID(I)
      End If
    Else
      If SellCFrame.ConnTyp = 0 Then
        If SellCFrame.IrcConn.State > 1 Then
          SetFrontFrame SellCFrame
          Exit Sub
        End If
        cId = SellCFrame.IrcConn.ListID
      End If
    End If
    For I = 1 To ChatFrames.Count
      Set StFrame = ChatFrames(I)
      If StFrame.ConnTyp = 0 Then
        If StFrame.IrcConn.ListID = cId Then
          StFrame.IrcConn.Connect
          Exit Sub
        End If
      End If
    Next
  Else
    cId = cIDint
  End If
  If Len(ConnList(cId).Nick) = 0 Then
    ConnList(cId).Nick = InputBox("Nick:")
    If Len(ConnList(cId).Nick) > 0 Then
      ConnList(cId).UserID = MakeUserID(ConnList(cId).Nick)
      ConnList(cId).Name = ConnList(cId).Nick
      LastUsedNick = ConnList(cId).Nick
    End If
  End If
  If Len(ConnList(cId).Nick) > 0 Then
    Set IrcConn = New IRCConnection
    ChangeConnValues IrcConn, cId
    IRCconns.Add IrcConn
    IrcConn.Connect
  End If
End Sub

Public Sub ChangeConnValues(Connection As Object, cId As Integer)
  Connection.Caption = ConnList(cId).Caption
  Connection.Port = ConnList(cId).Port
  Connection.Server = ConnList(cId).Server
  Connection.Nick = ConnList(cId).Nick
  Connection.StartCommands = ConnList(cId).Commands
  Connection.ListID = cId
  Connection.Realname = ConnList(cId).Name
  Connection.ServerPass = ConnList(cId).ServerPass
  Connection.NickPass = ConnList(cId).Pass
  Connection.UserID = ConnList(cId).UserID
  Connection.JoinMode = ConnList(cId).ServerMode
  Connection.CTCKey = ConnList(cId).CTCKey
  Connection.CTCAktiv = CBool(ConnList(cId).CTCOn)
  Connection.IgnoreList = ConnList(cId).IgnoreList
  Connection.Silent = CBool(ConnList(cId).Silent)
  Connection.Temp = CBool(ConnList(cId).Temp)
  Connection.UTF8 = CBool(ConnList(cId).UTF8)
  Connection.UTF8dyn = ConnList(cId).UTF8dyn
  Connection.Codepage = ConnList(cId).Codepage
  Connection.UseSSL = ConnList(cId).UseSSL
End Sub


Public Sub RefreshList()
  Dim I As Integer
  Dim I2 As Integer
  Dim DAT As String
  Dim AltIndex As Integer
  Dim StFrame As ChatFrame
  Dim Icon As Integer
  
  For I = 1 To ChatFrames.Count
    Set StFrame = ChatFrames(I)
    If StFrame.ConnTyp = -1 Then Exit For
  Next
  If I > ChatFrames.Count Then Exit Sub
  AltIndex = StFrame.UListF.ListIndex
  StFrame.UListF.Clear True
  For I = 0 To ConnListCount - 1
    I2 = StFrame.UListF.Add(ConnList(I).Caption, 68, False)
    StFrame.UListF.SetListText I2, 1, ConnList(I).Server, True
    If CLng(ConnList(I).LastUsed) > 0 Then
      StFrame.UListF.SetListText I2, 3, "\\-->" + Format(ConnList(I).LastUsed, "yyyymmddhhnnss"), True
    Else
      StFrame.UListF.SetListText I2, 3, "", True
    End If
    If CLng(ConnList(I).CreateDate) > 0 Then
      StFrame.UListF.SetListText I2, 4, "\\-->" + Format(ConnList(I).CreateDate, "yyyymmddhhnnss"), True
    Else
      StFrame.UListF.SetListText I2, 4, TeVal.lsUnknowen, True
    End If
    If ConnList(I).CreateType = 1 Then
      StFrame.UListF.SetListText I2, 5, TeVal.lsYes, True
    Else
      StFrame.UListF.SetListText I2, 5, TeVal.lsNo, True
    End If
    Select Case ConnList(I).State
      Case 0
        DAT = ""
        Icon = 74
      Case 1
        DAT = TeVal.Connecting
        Icon = 68
      Case 2
        DAT = TeVal.Connected
        Icon = 67
      Case 3
        DAT = TeVal.Closed
        Icon = 68
    End Select
    StFrame.UListF.SetListText I2, -1, CStr(Icon), True
    StFrame.UListF.SetListText I2, 2, DAT, True
  Next
  StFrame.UListF.ReSortList
  If StFrame.UListF.ListCount - 1 < AltIndex Then AltIndex = StFrame.UListF.ListCount - 1
  StFrame.UListF.ListIndex = AltIndex
  StFrame.UListF.Refresh
End Sub

Public Function StartDCCConnection(ParaMs As String, Optional NoWindow As Boolean, Optional ByVal cMode As Integer, Optional sbFrame As ChatFrame) As Integer
  Dim Werte(8) As String
  Dim I As Integer
  Dim P1 As Integer
  Dim P2 As Integer
  Dim DAT As String
  Dim hIndex As Integer
  Dim DCCConn As DCCConnection
  Dim FTrans As FileTransfer
  
  For I = 0 To 8
    P2 = P1
    P1 = InStr(P1 + 1, ParaMs, " ")
    If P1 = 0 Then P1 = Len(ParaMs) + 1
    Werte(I) = Mid(ParaMs, P2 + 1, P1 - P2 - 1)
    If P1 = Len(ParaMs) + 1 Then Exit For
  Next
  
  If Len(Werte(3)) > 7 Then Exit Function
  If Len(Werte(4)) > 16 Then Exit Function
  
  If UCase(Right(Werte(0), Len(Werte(0)) - 4)) = "SEND" Then
    hIndex = sbFrame.RequestDCCFile(Werte(1), Werte(5), Werte(2), Val(Werte(3)), Val(Werte(4)))
    If hIndex = -1 Then Exit Function
    
    I = 0
    If dccAutoAcc = True Then
      On Error Resume Next
      If Len(DCCPath) = 0 Or Len(Dir(DCCPath, vbDirectory)) = 0 Then I = 1
      If Err > 0 Then I = 1
      If I = 1 Then DCCPath = ""
      On Error GoTo 0
    End If
    
    If NoWindow = False Or I = 1 Then
      I = 0
      Do
        ShowSave DAT, "*.*" + Chr(0) + "*.*", LastSPath, Werte(5), I, Form1.hWnd
        If DAT = "" Then Exit Function
        If Mid(DAT, Len(DAT) - 3, 1) <> "." And Mid(DAT, Len(DAT) - 4, 1) <> "." Then
          DAT = DAT + "." + CutDirFromPath(Werte(5), ".", True)
        End If
        On Error Resume Next
        If Dir(DAT) <> "" Then
          On Error GoTo 0
          If MsgBox(TeVal.FileExist, vbQuestion + vbYesNo) = vbYes Then Exit Do
        Else
          On Error GoTo 0
          Exit Do
        End If
      Loop
      LastSPath = CutDirFromPath(DAT)
      If dccAutoAcc = True And Len(DCCPath) = 0 Then DCCPath = LastSPath
    Else
      DAT = Werte(5)
      If Right(DCCPath, 1) = "\" Then DCCPath = Left(DCCPath, Len(DCCPath) - 1)
      On Error Resume Next
      Do Until Len(Dir(DCCPath + "\" + DAT)) = 0
        DAT = "(2)" + DAT
      Loop
      If Err > 0 Then Exit Function
      On Error GoTo 0
      DAT = DCCPath + "\" + DAT
    End If
  End If

  If UCase(Right(Werte(0), Len(Werte(0)) - 4)) = "SEND" Then
    If hIndex > 0 Then
      Set FTrans = DCCFileConns(hIndex)
    Else
      Set FTrans = New FileTransfer
    End If
    FTrans.FileSize = Val(Werte(4))
    FTrans.User = Werte(1)
    FTrans.DestPath = DAT
    FTrans.PeerIP = Werte(2)
    FTrans.SetNoteServerPort sbFrame
    FTrans.PeerPort = Val(Werte(3))
    FTrans.ByteCount = 0
    FTrans.SenderFN = Werte(5)
    ShowDCCFrame True
    If hIndex > 0 Then
      ChangeDCCFileName FTrans, CheckTList(Werte(5))
      FTrans.Connect
    Else
      FTrans.FileName = CheckTList(Werte(5))
      DCCFileConns.Add FTrans
      FTrans.Connect
      AddDCCFileList FTrans
    End If
    SaveDCCValues
    ntScript_Call "Dcc_Created", FTrans.User, FTrans.FileName, CStr(FTrans.FileSize), CStr(FTrans.NoteCaption), CStr(FTrans.DccID)
    StartDCCConnection = FTrans.DccID
  Else
    Set DCCConn = New DCCConnection
    DCCConn.ConnID = getDccID
    DCCConn.Nick = Werte(6)
    DCCConn.Server = Werte(2)
    DCCConn.Port = Val(Werte(3))
    DCCConn.User = Werte(1)
    DCCConn.Mode = 1
    DCCConn.GetMode = cMode
    If cMode < 2 Then DCCConn.OpenWindow
    DCCConn.Connect
    DCCconns.Add DCCConn
    StartDCCConnection = DCCConn.ConnID
  End If
End Function

Public Sub DccConnRequest(sVon As String, sString As String, sChatFrs As ChatFrame, LocalNick As String)
  Dim DCCType As String
  Dim FileName As String
  Dim IP As String
  Dim Port As String
  Dim FileSize As String
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim hIndex As Integer

  I = InStr(1, sString, " ")
  If I = 0 Then Exit Sub
  I2 = InStr(I + 1, sString, " ")
  If I2 <= I Then Exit Sub
  DCCType = UCase(Mid(sString, I + 1, I2 - I - 1))
  I = I2
  
  If Mid(sString, I + 1, 1) = """" Then
    I = I + 1
    I2 = InStr(I, sString, """ ")
  Else
    I2 = InStr(I + 1, sString, " ")
  End If
  If I2 <= I Then Exit Sub
  FileName = Replace(CutDirFromPath(CutDirFromPath(Mid(sString, I + 1, I2 - I - 1), "\", True), "/", True), " ", "_")
  I = I2
  I2 = InStr(I + 2, sString, " ")
  If I2 <= I Or I2 - I > 32 Then Exit Sub
  IP = Trim(Mid(sString, I + 1, I2 - I - 1))
  I = I2
  I2 = InStr(I + 1, sString, " ")
  If I2 = 0 Then I2 = Len(sString)
  If I2 <= I Or I2 - I > 32 Then Exit Sub
  Port = Mid(sString, I + 1, I2 - I - 1)
  If I2 = Len(sString) Then
    FileSize = "-1"
  Else
    I = I2
    I2 = InStr(I + 1, sString, " ")
    If I2 = 0 Then I2 = Len(sString)
    If I2 <= I Then Exit Sub
    FileSize = Mid(sString, I + 1, I2 - I - 1)
    If Len(FileSize) > 11 Or InStr(1, FileSize, "&") > 0 Then FileSize = "-1"
  End If
  
  I = sChatFrs.IrcConn.ListID
  
  If ntScript_Call("Serv_DccRequest", sVon, ConvIPtoStr(Val(IP)) + " " + Port + " " + FileSize + " " + FileName + " " + LocalNick, DCCType, CStr(sChatFrs.FrameID), CStr(I)) = False Then Exit Sub

  If DCCType = "CHAT" Then
    DrawTLine sChatFrs, "* " + TeVal.DCCquestion + "  ", CoVal.ClientMSG, sVon
    If dccAutoAcc = True Then
      StartDCCConnection "DCC:" + DCCType + " " + sVon + " " + ConvIPtoStr(Val(IP)) + " " + Port + " " + FileSize + " " + FileName + " " + LocalNick, True, , sChatFrs
    Else
      sChatFrs.PrintText TeVal.DCCok, CoVal.Link, True, False, 7, , "DCC:" + DCCType + " " + sVon + " " + ConvIPtoStr(Val(IP)) + " " + Port + " " + FileSize + " " + FileName + " " + LocalNick
    End If
    DrawSLine sChatFrs, 2, "  (Host: " + ConvIPtoStr(Val(IP)) + ":" + Port + ")", CoVal.ClientMSG
    SetAl sChatFrs, 4, sVon, SetInVals(TeVal.DCCquestion, sVon)
  ElseIf DCCType = "SEND" Then
    DrawTLine sChatFrs, "* " + TeVal.DCCquestion + "  ", CoVal.ClientMSG, sVon
    If dccAutoAcc = True Then
      StartDCCConnection "DCC:" + DCCType + " " + sVon + " " + ConvIPtoStr(Val(IP)) + " " + Port + " " + FileSize + " " + FileName + " " + LocalNick, True, , sChatFrs
    Else
      sChatFrs.PrintText TeVal.DCCok, CoVal.Link, True, False, 7, , "DCC:" + DCCType + " " + sVon + " " + ConvIPtoStr(Val(IP)) + " " + Port + " " + FileSize + " " + FileName + " " + LocalNick
    End If
    If Len(FileName) > 43 Then FileName = "..." + Right(FileName, 40)
    DrawSLine sChatFrs, 2, "  (" + FileName + "  " + FormatBytes(Val(FileSize)) + "  Host: " + ConvIPtoStr(Val(IP)) + ":" + Port + ")", CoVal.ClientMSG
    Select Case LCase(CutDirFromPath(FileName, ".", True))
      Case "exe", "scr", "vbs", "com", "bat", "cmd", "js", "lnk", "shs", "pif", "mrc", "inf"
        DrawSLine sChatFrs, 2, "  " + TeVal.ExtWorning, vbRed
    End Select
    SetAl sChatFrs, 4, sVon, SetInVals(TeVal.DCCquestion, sVon)
  ElseIf DCCType = "RESUME" Then
    If Len(IP) < 8 And Len(Port) < 16 Then
      DrawSLine sChatFrs, 2, "* " + sVon + " -> RESUME", CoVal.ClientMSG
      sChatFrs.AnswerDCCRequest sVon, FileName, Val(IP), Val(Port)
    End If
  ElseIf DCCType = "ACCEPT" Then
    If Len(IP) < 8 And Len(Port) < 16 Then
      I3 = 0
      For I = 1 To DCCFileConns.Count
        If DCCFileConns(I).SendGetMode > 0 And DCCFileConns(I).State = 1 Then
          I2 = 0
          If LCase(DCCFileConns(I).SenderFN) = LCase(FileName) Then I2 = I2 + 3
          If DCCFileConns(I).PeerIP = Port Then I2 = I2 + 2
          If LCase(DCCFileConns(I).User) = LCase(sVon) Then I2 = I2 + 2
          If I2 > I3 Then I3 = I2: hIndex = I
        End If
      Next
      If I3 > 4 Then
        DrawSLine sChatFrs, 2, "* " + sVon + " -> RESUME ACCEPT", CoVal.ClientMSG
        DCCFileConns(hIndex).Connect
      End If
    End If
  End If
End Sub

Public Function CSendFile(Nick As String, User As String, IrcConn As IRCConnection, ConnType As Integer) As Boolean
  Dim File As String
  Dim I As Integer
  
  If Trim(User) = "" Then
    Load Form3
    Form3.Label3.Caption = "User:"
    Form3.Text3.Visible = False
    Form3.Combo1.Visible = True
    Form3.Picture1.Visible = False
    Form3.Picture2.Visible = True
    Form3.Text3.TabIndex = 0
    If FrontCFrame.FrameType = cfRoom Then
      For I = 0 To FrontCFrame.UListF.ListCount - 1
        Form3.Combo1.Additem IrcConn.ChatNoSign(FrontCFrame.UListF.List(I))
      Next
    End If
    Form3.Show 1
    User = Form3.Combo1.Text
    Form3.Combo1.Clear
    Unload Form3
  End If
  If Trim(User) = "" Then Exit Function
  On Error Resume Next
  Do
    File = ShowOpen("*.*", Form1.hWnd, LastLPath)
    If File = "" Then Exit Function
  Loop While Dir(File) = ""
  On Error GoTo 0
  LastLPath = CutDirFromPath(File)
  OpenDCC Nick, User, File, "SEND", IrcConn, ConnType, FrontCFrame
  CSendFile = True
End Function

Public Function GetLocalDCCIP(LocalIP As Double, RooterIP As Double, ServerIP As Double) As Double
  If RooterIP = 0 Or UseSecIP = False Then
    GetLocalDCCIP = LocalIP
  Else
    If Left(ConvIPtoStr(RooterIP), 5) = "192.1" And RooterIP = LocalIP Then
      GetLocalDCCIP = ServerIP
    Else
      GetLocalDCCIP = RooterIP
    End If
  End If
End Function

Public Function OpenDCC(Nick As String, User As String, File As String, _
  CType As String, IrcConn As IRCConnection, ConnType As Integer, _
  Optional sbFrame As ChatFrame, Optional KillAtEnd As Boolean) As Integer
  
  Dim Werte(8) As String
  Dim DCCConn As DCCConnection
  Dim FTrans As FileTransfer
  Dim DAT As String
  Dim DAT2 As String
  Dim OutStr As String
  Dim I As Integer
  Dim I2 As Integer
  Dim LocalIP As Double
  Dim LocalPort As Long
  
  LocalIP = GetLocalDCCIP(ConvIPtoDouble(GetLocalIP(IrcConn.GetSeocket.SocketHandle)), _
  IrcConn.GetSeocket.LocalRooterIP, ConvIPtoDouble(GetPeerHostByAddr(IrcConn.GetSeocket.SocketHandle), True))
     
  If UCase(CType) = "SEND" Then
    Set FTrans = New FileTransfer
    
    FTrans.FileSize = extGetFileSize(File)
    If FTrans.FileSize = 0 Then Exit Function
    
    DAT = CutDirFromPath(File, "\", True)
    I = 0
    Do
      I = InStr(I + 1, DAT, " ")
      If I = 0 Then Exit Do
      Mid(DAT, I, 1) = "_"
    Loop
    FTrans.User = User
    FTrans.DestPath = File
    FTrans.FileName = CheckTList(DAT)
    FTrans.SenderFN = DAT
    FTrans.LocalIP = LocalIP
    FTrans.KillAtEnd = KillAtEnd
    FTrans.SetNoteServerPort sbFrame
    ShowDCCFrame True
    DCCFileConns.Add FTrans
    FTrans.OpenPort
    AddDCCFileList FTrans
    SaveDCCValues
    LocalPort = FTrans.LocalPort
    DAT2 = " " + Format(FTrans.FileSize, "0")
  Else
    Set DCCConn = New DCCConnection
    DCCConn.ConnID = getDccID
    DCCConn.Nick = Nick
    DCCConn.Port = 0
    DCCConn.Server = ""
    DCCConn.User = User
    DCCConn.Mode = 0
    DCCConn.OpenWindow
    DCCConn.OpenPort
    LocalPort = DCCConn.Port
    OpenDCC = DCCConn.ConnID
    DCCconns.Add DCCConn
    DAT = "chat"
    DAT2 = ""
  End If

  If ConnType = 0 Then
    If UCase(CType) = "CHAT" Or UCase(CType) = "SEND" Then
      IrcConn.SendLine "PRIVMSG " + User + " :" + Chr(1) + "DCC " + UCase(CType) + " " _
      + DAT + " " + CStr(LocalIP) _
      + " " + CStr(LocalPort) + DAT2 + Chr(1)
    End If
  End If
End Function

'Public Function OpenNewWindow(Caption As String) As Integer
'  Dim DCCConn As DCCConnection
'  Set DCCConn = New DCCConnection
'  DCCConn.ListID = getDccID
'  DCCConn.Nick = ""
'  DCCConn.Server = ""
'  DCCConn.User = ""
'  DCCConn.Mode = 0
'  DCCConn.SetCaption Caption
'  DCCconns.Add DCCConn
'  OpenNewWindow = DCCConn.ListID
'End Function

Public Function OpenNewWindow(Caption As String) As Integer
  Dim ChFrame As New ChatFrame
 
  ChFrame.Caption = Caption
  ChFrame.Inst cfWindow, -1
  ChatFrames.Add ChFrame
  Form1.ColumRedraw True
  OpenNewWindow = ChFrame.FrameID
End Function

Public Sub CloseConb(ListID As Integer)
  Dim I As Integer
  For I = 1 To ChatFrames.Count
    If ChatFrames(I).ConnTyp = 0 Then
      If ChatFrames(I).IrcConn.ListID = ListID Then ChatFrames(I).IrcConn.Quit: Exit For
    ElseIf ChatFrames(I).ConnTyp = 2 Then
      If ChatFrames(I).DCCConn.ConnID = ListID Then ChatFrames(I).DCCConn.Quit: Exit For
    End If
  Next
End Sub

Public Function ConnID(ListIndex As Integer) As Integer
  Dim I As Integer
  Dim StFrame As ChatFrame
  
  For I = 1 To ChatFrames.Count
    Set StFrame = ChatFrames(I)
    If StFrame.ConnTyp = -1 Then Exit For
  Next
  If I > ChatFrames.Count Then Exit Function
  If StFrame.UListF.ListCount < ListIndex + 1 Then Exit Function
  For I = 0 To ConnListCount - 1
    If ConnList(I).Caption = StFrame.UListF.GetListText(ListIndex, 0) Then
      ConnID = I
      Exit Function
    End If
  Next
End Function

Public Function ConnRevID(ConnIndex As Integer) As Integer
  Dim I As Integer
  Dim StFrame As ChatFrame
  For I = 1 To ChatFrames.Count
    Set StFrame = ChatFrames(I)
    If StFrame.ConnTyp = -1 Then Exit For
  Next
  If I > ChatFrames.Count Then Exit Function
  If ConnListCount < ConnIndex + 1 Then Exit Function
  For I = 0 To StFrame.UListF.ListCount - 1
    If ConnList(ConnIndex).Caption = StFrame.UListF.GetListText(I, 0) Then
      ConnRevID = I
      Exit Function
    End If
  Next
End Function

Public Sub SetFrontFrame(CFrame As ChatFrame)
  Dim I As Integer
  Dim SortedC() As Integer
  
  If FrontCFrame Is CFrame Then Exit Sub
  If Not FrontCFrame Is Nothing Then ntScript_Call "FrameChange", CStr(FrontCFrame.FrameID), CStr(CFrame.FrameID)
  If Not CFrame Is LastFrontFrame Then Set LastFrontFrame = FrontCFrame
  If Form1.UserInfoPic.Visible = True Then ReleaseCapture
  
  Form1.pMain.Visible = False
  
  If Not FrontCFrame Is Nothing Then
    FrontCFrame.TempWisperTo = WisperTo
    FrontCFrame.Wisp = Form1.Check1.Value = 1
    If Not FrontCFrame.TextF Is Nothing Then
      If Form1.VScroll1 = Form1.VScroll1.Max Then
        FrontCFrame.TextF.ScrollState = -1
      Else
        FrontCFrame.TextF.ScrollState = Form1.VScroll1
      End If
    End If
    If Not FrontCFrame.UListF Is Nothing Then
      If Form1.UlistScr = Form1.UlistScr.Max And Form1.UlistScr.Enabled = True Then
        FrontCFrame.UListF.ScrollState = -1
      Else
        FrontCFrame.UListF.ScrollState = Form1.UlistScr
      End If
      FrontCFrame.UListF.DisableRef
    End If
  End If
  
  WisperTo = ""
  Form1.Check1.Value = Abs(CInt(CFrame.Wisp))
  
  Select Case CFrame.FrameType
    Case cfScript
      ListTriggers Form1.Combo1
      Form1.pText.Visible = True
      Form1.pTBox.Visible = False
      Form1.pDiff.Visible = False
      Form1.pUList.Visible = False
      Set FrontCFrame = CFrame
    Case cfRoomList, cfServerList, cfDCC, cfFriends
      Form1.pText.Visible = False
      Form1.pTBox.Visible = False
      Form1.pDiff.Visible = False
      Form1.pUList.Visible = True
      Form1.pUList.OLEDropMode = 0
      Set FrontCFrame = CFrame
    Case Else
      Form1.pText.Visible = False
      If CFrame.FrameType = cfRoom Then
        Form1.pUList.Visible = True
        Form1.pUList.Width = RListW
        Form1.pDiff.Visible = True
        If CFrame.UListF.ListIndex > -1 And CFrame.UListF.ListCount > 0 Then
          WisperTo = CFrame.TempWisperTo
          Form1.Check1.Visible = True
        End If
        Form1.pUList.OLEDropMode = 1
      Else
        WisperTo = CFrame.Caption
        Form1.pUList.Visible = False
        Form1.pDiff.Visible = False
        Form1.pUList.OLEDropMode = 0
      End If
      Form1.pTBox.Visible = True
      Set FrontCFrame = CFrame
      FrontCFrame.TextF.Resize
      If FrontCFrame.TextF.ScrollState > Form1.VScroll1.Max Or FrontCFrame.TextF.ScrollState = -1 Then
        If Form1.VScroll1 = Form1.VScroll1.Max Then
          If Form1.Visible = True And Form1.WindowState <> 1 Then FrontCFrame.TextF.Refresh
        Else
          Form1.VScroll1.Value = Form1.VScroll1.Max
        End If
        FrontCFrame.TextF.LastScrPos = Form1.VScroll1
      Else
        If FrontCFrame.TextF.ScrollState = -1 Then FrontCFrame.TextF.ScrollState = Form1.VScroll1.Max
        If Form1.VScroll1 = FrontCFrame.TextF.ScrollState Then
          FrontCFrame.TextF.Refresh
        Else
          Form1.VScroll1 = FrontCFrame.TextF.ScrollState
        End If
      End If
  End Select
  
  If Not FrontCFrame.UListF Is Nothing Then
    FrontCFrame.UListF.EnableRef
    FrontCFrame.UListF.Resize
    If FrontCFrame.UListF.ScrollState > Form1.UlistScr.Max Or FrontCFrame.UListF.ScrollState = -1 Then
      Form1.UlistScr = Form1.UlistScr.Max
    Else
      Form1.UlistScr = FrontCFrame.UListF.ScrollState
    End If
  End If
  Form1.StopFlash FrontCFrame
  FrontCFrame.Changed = 0
  Form1.pMain_Resize
  Form1.TfRedraw
  Form1.pStateIntRefresh
  Form1.MoMenue
  DrawTitel
  Form1.pMain.Visible = True
End Sub

Public Sub DrawTitel()
  Dim TitelOut As String
  If FrontCFrame.ConnTyp = 0 Then
    TitelOut = "IRC://" + LCase(FrontCFrame.IrcConn.RealServer)
    If FrontCFrame.FrameType <> cfStatus Then TitelOut = TitelOut + "/" + LCase(FrontCFrame.Caption)
    If FrontCFrame.FrameType = cfRoom Or FrontCFrame.FrameType = cfRoomList Then
      TitelOut = TitelOut + " (" + CStr(FrontCFrame.UListF.ListCount) + ")"
    End If
  ElseIf FrontCFrame.ConnTyp = 2 Then
    TitelOut = "DCC://" + FrontCFrame.DCCConn.Server + "    (" + FrontCFrame.DCCConn.User + ")"
  Else
    TitelOut = FrontCFrame.Caption
  End If
  TitelOut = TitelOut + "  -  " + App.Title + Chr(0)
  DefWindowProc Form1.hWnd, WM_SETTEXT, &H0&, TitelOut
End Sub

Public Function CopyTitel(Optional PastTitel As Boolean = True)
  Dim DAT As String
  Dim I As Integer
  
  If FrontCFrame.TextF Is Nothing Then Exit Function
  If FrontCFrame.ConnTyp = 0 Then
    DAT = "IRC://" + LCase(FrontCFrame.IrcConn.RealServer)
    If FrontCFrame.IrcConn.Port <> 6667 Then DAT = DAT + ":" + CStr(FrontCFrame.IrcConn.Port)
    If FrontCFrame.FrameType <> cfStatus Then DAT = DAT + "/" + LCase(FrontCFrame.Caption)
  ElseIf FrontCFrame.ConnTyp = 2 Then
    DAT = "DCC://" + FrontCFrame.DCCConn.Server
  Else
    DAT = Form1.Caption
  End If
  If PastTitel Then
    I = Form1.Text1.SelStart
    Form1.Text1.Text = Left(Form1.Text1.Text, Form1.Text1.SelStart) + DAT + Mid(Form1.Text1.Text, Form1.Text1.SelStart + Form1.Text1.SelLength + 1)
    Form1.Text1.SelStart = I + Len(DAT)
    Form1.TextChanged
  End If
  CopyTitel = DAT
End Function

Public Sub PastCaption()
  Dim I As Integer

  I = Form1.Text1.SelStart
  Form1.Text1.Text = Left(Form1.Text1.Text, Form1.Text1.SelStart) + FrontCFrame.Caption + Mid(Form1.Text1.Text, Form1.Text1.SelStart + Form1.Text1.SelLength + 1)
  Form1.Text1.SelStart = I + Len(FrontCFrame.Caption)
  Form1.TextChanged
End Sub

Public Function AreStringsIncl(SearchStrList As String, Text As String, Optional Divider As String = " ") As Boolean
  Dim I As Long
  Dim I2 As Long
  Dim I3 As Long
  
  Do
    I2 = I
    I = InStr(I + 1, SearchStrList, Divider)
    If I = 0 Then I = Len(SearchStrList) + 1
    
    I3 = InStr(1, Text, Mid(SearchStrList, I2 + 1, I - I2 - 1))
  Loop Until I = Len(SearchStrList) + 1 Or I3 = 0
  
  AreStringsIncl = (I3 > 0)
End Function

Public Function NewFrame(IrcConn As IRCConnection, Caption As String, Optional FrType As Integer = 0, Optional SetFront As Boolean = True) As ChatFrame
  Dim ChFrame As New ChatFrame
  
  Set ChFrame.IrcConn = IrcConn
  ChFrame.Inst FrType, 0
  ChFrame.Caption = Caption
  ChatFrames.Add ChFrame
  If SetFront = True Then
    SetFrontFrame ChFrame
  Else
    Form1.ColumRedraw
  End If
  Set NewFrame = ChFrame
End Function

Public Function GetCharValue(Char As String, cvPrefixSign As String) As Byte
  Dim I As Integer
  
  For I = 1 To Len(cvPrefixSign)
    If Mid(cvPrefixSign, I, 1) = Char Then
      GetCharValue = I
      Exit For
    End If
  Next
  If I > Len(cvPrefixSign) Then GetCharValue = 255
End Function

Public Function ChatNoSignGlobal(ChaterName As String) As String
  Dim I As Long
  
  For I = 1 To Len(ChaterName)
    Select Case Mid(ChaterName, I, 1)
      Case "@", "%", "+", "!", "*", "#", "~", Chr(176), "&", Chr(167), "$", "?"
      Case Else
        Exit For
    End Select
  Next
  ChatNoSignGlobal = Mid(ChaterName, I)
End Function

Public Function GetIRCColor(ByVal ColorNumber As Integer, Optional ByVal ErrColor As Long) As Long
  GetIRCColor = ErrColor
  If ColorNumber = 0 Then GetIRCColor = vbWhite
  If ColorNumber = 1 Then GetIRCColor = vbBlack
  If ColorNumber = 2 Then GetIRCColor = RGB(0, 0, 100)    'Blau dunkel
  If ColorNumber = 3 Then GetIRCColor = RGB(0, 100, 0)    'Gr�n dunkel
  If ColorNumber = 4 Then GetIRCColor = vbRed
  If ColorNumber = 5 Then GetIRCColor = RGB(128, 20, 20)  'Braun
  If ColorNumber = 6 Then GetIRCColor = RGB(128, 0, 128)  'Pink dunkel
  If ColorNumber = 7 Then GetIRCColor = RGB(255, 100, 0)  'Orange
  If ColorNumber = 8 Then GetIRCColor = vbYellow
  If ColorNumber = 9 Then GetIRCColor = vbGreen
  If ColorNumber = 10 Then GetIRCColor = RGB(0, 80, 150)  'T�rkis dunkel
  If ColorNumber = 11 Then GetIRCColor = RGB(0, 255, 255) 'T�rkis
  If ColorNumber = 12 Then GetIRCColor = vbBlue
  If ColorNumber = 13 Then GetIRCColor = RGB(255, 0, 255) 'Magenta
  If ColorNumber = 14 Then GetIRCColor = QBColor(8)       'Grau
  If ColorNumber = 15 Then GetIRCColor = QBColor(7)       'Grau dunkel
End Function

Public Function GetIRCColorName(ByVal ColorNumber As Integer) As String
  If ColorNumber = 0 Then GetIRCColorName = "White"
  If ColorNumber = 1 Then GetIRCColorName = "Black"
  If ColorNumber = 2 Then GetIRCColorName = "Blue2"
  If ColorNumber = 3 Then GetIRCColorName = "Green2"
  If ColorNumber = 4 Then GetIRCColorName = "Red"
  If ColorNumber = 5 Then GetIRCColorName = "Brown"
  If ColorNumber = 6 Then GetIRCColorName = "Lilac"
  If ColorNumber = 7 Then GetIRCColorName = "Orange"
  If ColorNumber = 8 Then GetIRCColorName = "Yellow"
  If ColorNumber = 9 Then GetIRCColorName = "Green"
  If ColorNumber = 10 Then GetIRCColorName = "Turquoise2"
  If ColorNumber = 11 Then GetIRCColorName = "Turquoise"
  If ColorNumber = 12 Then GetIRCColorName = "Blue"
  If ColorNumber = 13 Then GetIRCColorName = "Pink"
  If ColorNumber = 14 Then GetIRCColorName = "Grey2"
  If ColorNumber = 15 Then GetIRCColorName = "Grey"
End Function

Public Function GetIRCColorNumber(ByVal ColorName As String) As Integer
  Dim DAT As String
  GetIRCColorNumber = -1
  DAT = Trim(LCase(ColorName))
  If DAT = "white" Then GetIRCColorNumber = 0
  If DAT = "black" Then GetIRCColorNumber = 1
  If DAT = "blue2" Then GetIRCColorNumber = 2
  If DAT = "green2" Then GetIRCColorNumber = 3
  If DAT = "red" Then GetIRCColorNumber = 4
  If DAT = "brown" Then GetIRCColorNumber = 5
  If DAT = "lilac" Then GetIRCColorNumber = 6
  If DAT = "orange" Then GetIRCColorNumber = 7
  If DAT = "yellow" Then GetIRCColorNumber = 8
  If DAT = "green" Then GetIRCColorNumber = 9
  If DAT = "turquoise2" Then GetIRCColorNumber = 10
  If DAT = "turquoise" Then GetIRCColorNumber = 11
  If DAT = "blue" Then GetIRCColorNumber = 12
  If DAT = "pink" Then GetIRCColorNumber = 13
  If DAT = "grey2" Then GetIRCColorNumber = 14
  If DAT = "grey" Then GetIRCColorNumber = 15
End Function

Public Function ConvIPtoStr(ByVal StrIP As Double) As String
  Dim St1 As Byte
  Dim St2 As Byte
  Dim St3 As Byte
  Dim St4 As Byte
  Dim doIP As Double
  doIP = Abs(StrIP)
  If Fix(doIP / 256 ^ 3) > 255 Then Exit Function
  St1 = Fix(doIP / 256 ^ 3)
  doIP = doIP - St1 * 256 ^ 3
  St2 = Fix(doIP / 256 ^ 2)
  doIP = doIP - St2 * 256 ^ 2
  St3 = Fix(doIP / 256)
  doIP = doIP - St3 * 256 ^ 1
  St4 = Fix(doIP)
  ConvIPtoStr = CStr(St1) + "." + CStr(St2) + "." + CStr(St3) + "." + CStr(St4)
End Function

Public Function ConvIPtoDouble(StrIP As String, Optional Rev As Boolean) As Double
  Dim EVal As Double
  Dim I As Integer
  Dim I2 As Integer
  Dim CountI As Integer
  I = 0
  CountI = IIf(Rev, 3, 0)
  Do
    I2 = I
    I = InStr(I + 1, StrIP, ".")
    If I = 0 Then I = Len(StrIP) + 1
    If Val(Mid(StrIP, I2 + 1, I - I2 - 1)) > 255 Then Exit Function
    EVal = EVal + Val(Mid(StrIP, I2 + 1, I - I2 - 1)) * 256 ^ CountI
    CountI = CountI + IIf(Rev, -1, 1)
  Loop Until I = Len(StrIP) + 1 Or CountI > 3
  ConvIPtoDouble = EVal
End Function

Public Function IntToStr(ByVal Zahl As Double, Optional OutLen As Integer = 4) As String
  Dim BinStr As String
  Dim doIP As Double
  Dim I As Integer
  Dim I2 As Single
  
  doIP = Zahl
  For I = OutLen - 1 To 0 Step -1
    I2 = Int(doIP / 256 ^ I)
    doIP = doIP - I2 * 256 ^ I
    If I2 > 255 Then I2 = 255
    BinStr = BinStr + Chr(I2)
  Next
  IntToStr = BinStr
End Function

Public Function BinToHex(BinStr As String) As String
  Dim I As Integer

  For I = 1 To Len(BinStr)
    BinToHex = BinToHex + Format(Hex(AscB(Mid(BinStr, I, 1))), "00") + " "
  Next
End Function

Public Function StrToInt(StrIP As String) As Double
  Dim EVal As Double
  Dim I As Integer
  Dim I2 As Integer
  Dim CountI As Integer
  For CountI = 3 To 0 Step -1
    If I = 0 Then I = Len(StrIP) + 1
    EVal = EVal + AscB(Mid(StrIP, 4 - CountI, 1)) * 256 ^ CountI
  Next
  StrToInt = EVal
End Function

Public Sub SetAl(WinF As ChatFrame, Optional sNumber As Integer = 4, Optional textVon As String, Optional Text As String, Optional MeMsg As Boolean)
  Dim DAT As String
  
  If AppUnloading = True Or Left(Text, 2) = "**" Then Exit Sub
  
  If MeMsg Then
    DAT = " *" + textVon + " "
  Else
    DAT = " <" + textVon + "> "
  End If
  
  If WinF.FrameType < 2 And WinF.ConnTyp <> 2 Then
    WriteLog Format(Now, "dd.mm.yyyy hh:mm:ss") + DAT + Text, WinF, textVon
  Else
    WriteLog Format(Now, "dd.mm.yyyy hh:mm:ss") + DAT + Text, WinF
  End If
  If Not WinF Is FrontCFrame Then
    LastWispMsg = "<" + textVon + "> " + Text
    If Form1.Visible = True Then Form1.pStateRefresh
  End If
  If WinF.Changed < sNumber And Not WinF Is FrontCFrame Then
    WinF.Changed = sNumber
    Form1.ColumRedraw
    Form1.StartFlash WinF
  End If
  If Text <> "" Then
    If WinF.ConnTyp = 0 Then
      If WinF.IrcConn.Silent = True Then Exit Sub
    End If
    If IsNotifyNess And TrayIconState < 5 Then
      T.hIcon = Form1.TrayIcon(6)
      Shell_NotifyIcon NIM_MODIFY, T
      TrayIconState = 5
    End If
    If EaOSDPriv = True Then ShowOSDmsg WinF, textVon, Text
    If IsNotifyNess Then
      FlashMainWindow
      Form1.StartTrayFlash: MakeSignal
    End If
  End If
End Sub

Public Sub SetAlPriv(WinF As ChatFrame, Optional textVon As String, Optional Text As String, Optional OwnNick As String, Optional MeMsg As Boolean)
  Dim I As Integer
  Dim IsNick As Boolean
  Dim DAT As String
  
  If AppUnloading = True Then Exit Sub
  
  If MeMsg Then
    DAT = " *" + textVon + " "
  Else
    DAT = " <" + textVon + "> "
  End If
  
  WriteLog Format(Now, "dd.mm.yyyy hh:mm:ss") + DAT + Text, WinF
  If EaOSDRoom = True Then
    If InStr(1, " " + OsdRoomList + " ", " " + WinF.Caption + " ", vbTextCompare) > 0 Then
      ShowOSDmsg WinF, textVon, Text
      If Not WinF Is FrontCFrame Then
        LastWispMsg = "<" + textVon + "> " + Text
        If Form1.Visible = True Then Form1.pStateRefresh
      End If
    End If
  End If
  If OwnNick <> "" Then
    Do
      IsNick = True
      I = InStr(I + 1, Text, OwnNick, vbTextCompare)
      If I = 0 Then Exit Do
      If I > 1 Then
        Select Case AscB(Mid(Text, I - 1, 1))
          Case 65 To 90, 97 To 122, 196, 214, 228, 246, 252, 220, 223, 48 To 57 'Alle Buchstaben & Zahlen
            IsNick = False
        End Select
      End If
      If I + Len(OwnNick) <= Len(Text) Then
        Select Case AscB(Mid(Text, I + Len(OwnNick), 1))
          Case 65 To 90, 97 To 122, 196, 214, 228, 246, 252, 220, 223, 48 To 57 'Alle Buchstaben & Zahlen
            IsNick = False
        End Select
      End If
      If IsNick = True Then
        If Not WinF Is FrontCFrame Then
          LastWispMsg = "<" + textVon + "> " + Text
          If Form1.Visible = True Then Form1.pStateRefresh
        End If
        If WinF.Changed < 3 And Not WinF Is FrontCFrame Then
          WinF.Changed = 3
          Form1.ColumRedraw True
          Form1.StartFlash WinF
        End If
        If WinF.ConnTyp = 0 Then
          If WinF.IrcConn.Silent = True Then Exit Sub
        End If
        If IsNotifyNess And TrayIconState < 4 Then
          T.hIcon = Form1.TrayIcon(5)
          Shell_NotifyIcon NIM_MODIFY, T
          TrayIconState = 4
        End If
        WinF.TextF.SetLineIcon 69
        If EaOSDPriv2 = True Then ShowOSDmsg WinF, textVon, Text
        If IsNotifyNess Then
          FlashMainWindow
          Form1.StartTrayFlash: MakeSignal2
        End If
        Exit Do
      End If
    Loop
  End If
End Sub

Public Sub DisconnectMsg()
  If (Form1.Visible = False Or Form1.WindowState = 1) And TrayIconState < 3 Then
    T.hIcon = Form1.TrayIcon(7)
    Shell_NotifyIcon NIM_MODIFY, T
    TrayIconState = 3
    DisIcon = True
  End If
End Sub

Public Function IsNotifyNess() As Boolean
  Dim TempFWindow As Long
  Dim lWP As WINDOWPLACEMENT
  
  If Form1.Visible = False Or Form1.WindowState = 1 Then
    IsNotifyNess = True
  Else
    TempFWindow = GetForegroundWindow
    If TempFWindow <> Form1.hWnd Then
      lWP.Length = Len(lWP)
      GetWindowPlacement TempFWindow, lWP
      If lWP.showCmd = SW_MAXIMIZE Then
        IsNotifyNess = True
      ElseIf lWP.showCmd = SW_NORMAL Then
        With lWP.rcNormalPosition
          If Form1.Top \ Screen.TwipsPerPixelY + 32 < .Bottom _
            And (Form1.Top + Form1.Height) \ Screen.TwipsPerPixelY > .Top + 32 Then
            If Form1.Left \ Screen.TwipsPerPixelX + 32 < .Right _
              And (Form1.Left + Form1.Width) \ Screen.TwipsPerPixelX > .Left + 32 Then
              IsNotifyNess = Form1.Height / Screen.TwipsPerPixelY / 2.2 < .Bottom - .Top _
              And Form1.Width / Screen.TwipsPerPixelX / 3 < .Right - .Left
            End If
          End If
        End With
      End If
    End If
  End If
End Function

Public Sub NewTextAdded(ChatFrs As ChatFrame)
  Dim I As Boolean
  
  If ChatFrs.ConnTyp = 0 Then
    If ChatFrs.IrcConn.Silent = False Then I = True
  End If
  If I Then
    If ChatFrs.Changed < 2 And Not ChatFrs Is FrontCFrame Then
      ChatFrs.Changed = 2
      Form1.ColumRedraw True
    End If
    'If (Form1.Visible = False Or Form1.WindowState = 1) And (ChatFrs.ConnTyp <> 0 Or ChatFrs.FrameType <> cfStatus) Then
    If IsNotifyNess And ChatFrs.FrameType <> cfStatus And ChatFrs.ConnTyp = 0 Then
      If ChatFrs Is FrontCFrame And TrayIconState < 3 Then
        T.hIcon = Form1.TrayIcon(4)
        Shell_NotifyIcon NIM_MODIFY, T
        TrayIconState = 3
      ElseIf TrayIconState < 2 Then
        T.hIcon = Form1.TrayIcon(3)
        Shell_NotifyIcon NIM_MODIFY, T
        TrayIconState = 2
      End If
    End If
  End If
End Sub

Public Sub ShowOSDmsg(WinF As ChatFrame, Optional textVon As String, Optional Text As String, Optional ShowIT As Boolean)
  If UModeOSD = True And Not OnScr Is Nothing Then
    OnScr.AddLine textVon, Text
    If OnScr.Running = True Then Exit Sub
  End If
  If UModeBalken = True And (IsNotifyNess Or ShowIT) Then
    If WinF.ConnTyp = 0 Then ' #IRC
      ShowChatText Text, textVon, WinF.IrcConn.Caption + "/" + WinF.Caption, WinF
    ElseIf WinF.ConnTyp = 2 Then ' #DCC
      ShowChatText Text, textVon, WinF.DCCConn.Server, WinF
    Else
      ShowChatText Text, textVon, WinF.Caption, WinF
    End If
  End If
End Sub

Public Function FormatBytes(ByVal Bytes As Double) As String
  Dim outData As String
  Select Case Bytes
    Case Is > 1024 ^ 4
      outData = CStr(Int(Bytes / 1024 ^ 4 * 100) / 100) + " TB"
    Case Is > 1024 ^ 3
      outData = CStr(Int(Bytes / 1024 ^ 3 * 100) / 100) + " GB"
    Case Is > 1024 ^ 2
      outData = CStr(Int(Bytes / 1024 ^ 2 * 100) / 100) + " MB"
    Case Is > 1024
      outData = CStr(Int(Bytes / 1024 * 100) / 100) + " KB"
    Case Else
      outData = CStr(Bytes) + " Byte"
  End Select
  FormatBytes = outData
End Function

Public Function FormatSec(ByVal Sec As Double) As String
  Dim outData As String
  Select Case Sec
    Case Is >= 86400 ' Ein Tag in sec (60*60*24)
      outData = CStr(Int(Sec / 86400)) + " d, " + CStr(Int(Sec / 3600) - Int(Sec / 86400) * 24) + " h"
    Case Is >= 3600
      outData = CStr(Int(Sec / 3600)) + " h, " + CStr(Int(Sec / 60) - Int(Sec / 3600) * 60) + " min"
    Case Is >= 60
      outData = CStr(Int(Sec / 60)) + " min, " + CStr(Int(Sec) - Int(Sec / 60) * 60) + " sec"
    Case Else
      outData = CStr(Int(Sec)) + " sec"
  End Select
  FormatSec = outData
End Function

Public Function CutDirFromPath(Path As String, Optional Divider As String = "\", Optional RightPart As Boolean) As String
  Dim I As Integer
  Dim I2 As Integer
  Do
    I2 = I
    I = InStr(I + 1, Path, Divider)
  Loop Until I = 0
  If RightPart = True Then
    If Len(Path) > I2 Then CutDirFromPath = Mid(Path, I2 + 1)
  Else
    If I2 > 0 Then CutDirFromPath = Left(Path, I2 - 1)
  End If
End Function

Public Function Parse(Data As String, Char As String) As String
  Dim I As Integer
  I = InStr(Data, Char)
  If I = 0 Then
    Parse = Data
    Data = ""
  Else
    Parse = Left$(Data, I - 1)
    Data = Mid$(Data, I + Len(Char))
  End If
End Function

Public Function getDccID() As Integer
  Dim I As Integer
  Dim I2 As Integer
  Dim TempF() As Integer
  I2 = 500
  Do
    For I = 1 To DCCconns.Count
      If DCCconns(I).ConnID = I2 Then Exit For
    Next
    If I > DCCconns.Count Then Exit Do
    I2 = I2 + 1
  Loop
  getDccID = I2
End Function

Public Function cVersions(NewVersion As String, OldVersion As String) As Boolean
  Dim I As Integer
  Dim I2 As Integer
  Dim J As Integer
  Dim J2 As Integer
  Dim ValI As Integer
  Dim ValJ As Integer
  Do
    I2 = I
    J2 = J
    I = InStr(I + 1, NewVersion, ".")
    J = InStr(J + 1, OldVersion, ".")
    If I = 0 Then I = Len(NewVersion) + 1
    If J = 0 Then J = Len(OldVersion) + 1
    If I * J = 0 Then cVersions = False: Exit Do
    If I - I2 > 0 And I - I2 < 6 Then ValI = Val(Mid(NewVersion, I2 + 1, I - I2 - 1)) Else ValI = 0
    If J - J2 > 0 And J - I2 < 6 Then ValJ = Val(Mid(OldVersion, J2 + 1, J - J2 - 1)) Else ValJ = 0
    If ValI > ValJ Then
      cVersions = True
      Exit Do
    ElseIf ValJ > ValI Then
      cVersions = False
      Exit Do
    End If
  Loop Until I = Len(NewVersion) + 1 Or J = Len(OldVersion) + 1
End Function

Public Function CheckConns(NameStr As String) As String
  Dim I As Integer
  Dim I2 As Integer
  Dim AddZ As Integer
  Dim ConnName As String
  ConnName = NameStr
  Do
    For I = 0 To ConnListCount - 1
      If UCase(ConnName) = UCase(ConnList(I).Caption) Then Exit For
    Next
    If I > ConnListCount - 1 Then Exit Do
    For I = Len(ConnName) To Len(ConnName) - 3 Step -1
      If I < 1 Then Exit For
      If Mid(ConnName, I, 1) <> CStr(Val(Mid(ConnName, I, 1))) Then Exit For
    Next
    AddZ = Val(Mid(ConnName, I + 1)) + 1
    ConnName = Left(ConnName, I) + CStr(AddZ)
  Loop
  CheckConns = ConnName
End Function

Public Function InputBox(Text As String) As String
  Load Form3
  Form3.Label3.Caption = Text
  Form3.Caption = "Nettalk"
  Form3.Picture1.Visible = False
  Form3.Picture2.Visible = True
  Form3.Text3.TabIndex = 0
  Form3.Show 1
  InputBox = Form3.Text3.Text
  Unload Form3
End Function

Public Function DateFormatEn(Datum As Date) As String
  Dim DD As String
  Dim DDD As String
  Dim MMM As String
  Dim YY As String
  Dim DAYTIME As String

  Select Case Weekday(Datum)
    Case 1
      DDD = "Mon"
    Case 2
      DDD = "Thu"
    Case 3
      DDD = "Wed"
    Case 4
      DDD = "Thu"
    Case 5
      DDD = "Fri"
    Case 6
      DDD = "Sat"
    Case 7
      DDD = "Sun"
  End Select
  
  Select Case Month(Datum)
    Case 1
      MMM = "Jan"
    Case 2
      MMM = "Feb"
    Case 3
      MMM = "Mar"
    Case 4
      MMM = "Apr"
    Case 5
      MMM = "May"
    Case 6
      MMM = "Jun"
    Case 7
      MMM = "Jul"
    Case 8
      MMM = "Aug"
    Case 9
      MMM = "Sep"
    Case 10
      MMM = "Oct"
    Case 11
      MMM = "Nov"
    Case 12
      MMM = "Dec"
  End Select
  
  YY = Format(Datum, "yyyy")
  DD = Format(Datum, "DD")
  DAYTIME = Format(Datum, "hh:nn:ss")

  DateFormatEn = DDD & " " & MMM & " " & DD & " " & DAYTIME & " " & YY
End Function


'###########################################

'Public Sub ListValues(Frm As Form)
'  Dim Contr As Control
'  Dim Frf As Integer
'
'  Frf = FreeFile
'  Open App.Path + "\ger.txt" For Append As #Frf
'    For Each Contr In Frm.Controls
'      On Error Resume Next
'      If Len(Contr.Caption) = 0 Or Contr.Caption = "-" Then
'        On Error GoTo 0
'      Else
'        On Error GoTo 0
'        'List1.AddItem Frm.Name + "." + Contr.Name + " = " + Contr.Caption
'        Print #Frf, Frm.Name + "." + Contr.Name + " = " + Contr.Caption
'      End If
'    Next
'  Close #Frf
'End Sub

'Public Sub PrintLog(Text As String)
'  Dim Frf As Integer
'
'  Frf = FreeFile
'  Open App.Path + "\LogFile.txt" For Append As #Frf
'    Print #Frf, Date, Time, Text
'  Close #Frf
'End Sub
