Attribute VB_Name = "FriendsList"
Option Explicit

Private Type FriendEntry
  Nick As String * 32
  Server As String * 64
  LastOnline As Date
  State As Integer
  OldState As Integer
End Type

Private FriendsList() As FriendEntry
Private FriendsListCound As Integer

Public Sub WatchNewUser(Nick As String, Server As String)
  Dim I As Integer
  
  If Len(Trim(Nick)) = 0 Or Len(Trim(Server)) = 0 Then Exit Sub
  If FriendsListCound = 0 Then ReDim FriendsList(1 To 4)
  For I = 1 To FriendsListCound
    If LCase(Trim(FriendsList(I).Server)) = LCase(Server) And _
    LCase(Trim(FriendsList(I).Nick)) = LCase(Nick) Then
      Exit For
    End If
  Next
  If I > FriendsListCound Then
    AddToFList Nick, Server, 0
  End If
  RequestWUState Server
End Sub

Public Sub RemWatchedUser(Nick As String, Server As String)
  Dim I As Integer
  Dim I2 As Integer
  
  If FriendsListCound = 0 Then ReDim FriendsList(1 To 4)
  For I = 1 To FriendsListCound
    If LCase(Trim(FriendsList(I).Server)) = LCase(Server) And _
    LCase(Trim(FriendsList(I).Nick)) = LCase(Nick) Then
      FriendsList(I).Nick = ""
      FriendsList(I).Server = ""
      If FriendsList(I).State = 1 Then
        For I2 = 1 To IRCconns.Count
          If LCase(IRCconns(I2).Server) = LCase(Server) And IRCconns(I2).State = 4 Then
            IRCconns(I2).SendLine "watch -" + Nick
          End If
        Next
      End If
    End If
  Next
  If I = FriendsListCound Then FriendsListCound = I - 1

  For I = 1 To ChatFrames.Count
    If ChatFrames(I).FrameType = cfFriends Then
      For I2 = 0 To ChatFrames(I).UListF.ListCount - 1
        If (LCase(ChatFrames(I).UListF.GetListText(I2, 0)) = LCase(Nick) Or Len(Nick) = 0) And _
        LCase(ChatFrames(I).UListF.GetListText(I2, 2)) = LCase(Server) Then
          ChatFrames(I).UListF.Remove I2
        End If
      Next
      ChatFrames(I).UListF.Refresh
      Exit For
    End If
  Next
End Sub

Public Function RequestWUState(Server As String) As Boolean
  Dim I As Integer
  Dim I2 As Integer
  Dim DAT As String
  Dim OnL As Boolean
  
  If FriendsListCound = 0 Then Exit Function
  For I = 1 To FriendsListCound
    If LCase(Trim(FriendsList(I).Server)) = LCase(Server) Then
      DAT = DAT + " +" + Trim(FriendsList(I).Nick)
    End If
  Next
  If Len(DAT) > 0 Then
    For I = 1 To IRCconns.Count
      If LCase(IRCconns(I).Server) = LCase(Server) And IRCconns(I).State = 4 Then
        IRCconns(I).SendLine "watch C" + DAT
        RequestWUState = True
      End If
    Next
  End If
End Function

Private Sub AddToFList(Nick As String, Server As String, LastOnline As Date)
  Dim I As Integer
  Dim I2 As Integer
  
  If FriendsListCound = 0 Then ReDim FriendsList(1 To 4)
  
  For I = 1 To FriendsListCound
    If Len(Trim(FriendsList(I).Nick)) = 0 Then Exit For
  Next
  If I > FriendsListCound Then
    FriendsListCound = I
  End If
  If FriendsListCound > UBound(FriendsList) Then
    ReDim Preserve FriendsList(1 To FriendsListCound + 3)
  End If
  FriendsList(I).Nick = Nick
  FriendsList(I).Server = Server
  FriendsList(I).LastOnline = LastOnline
  FriendsList(I).State = 0
    
  For I = 1 To ChatFrames.Count
    If ChatFrames(I).FrameType = cfFriends Then
      I2 = ChatFrames(I).UListF.Add(Nick, 63, False)
      ChatFrames(I).UListF.SetListText I2, 2, Server, True
      Exit For
    End If
  Next
  ChatFrames(I).UListF.ReSortList
  ChatFrames(I).UListF.Refresh
End Sub

Public Function SetFList(Nick As String, Server As String, State As Integer, Optional CompOld As Boolean) As Boolean
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim test As ChatFrame
  
  I2 = 0
  If FriendsListCound = 0 Then Exit Function
  For I = 1 To FriendsListCound
    If (LCase(Trim(FriendsList(I).Nick)) = LCase(Nick) Or Len(Nick) = 0) And _
    LCase(Trim(FriendsList(I).Server)) = LCase(Server) Then
      I3 = FriendsList(I).State
      If I3 <> State Then
        If I3 = 1 Then FriendsList(I).LastOnline = Now
        FriendsList(I).State = State
        I2 = I2 + 1
        If FriendsList(I).OldState <> State And CompOld Then SetFList = True
      End If
      FriendsList(I).OldState = I3
    End If
  Next
  If I2 > 0 Then
    For I = 1 To ChatFrames.Count
      If ChatFrames(I).FrameType = cfFriends Then
        For I2 = 0 To ChatFrames(I).UListF.ListCount - 1
          If (LCase(ChatFrames(I).UListF.GetListText(I2, 0)) = LCase(Nick) Or Len(Nick) = 0) And _
          LCase(ChatFrames(I).UListF.GetListText(I2, 2)) = LCase(Server) Then
            UserListState ChatFrames(I), I2, State, 0
          End If
        Next
        ChatFrames(I).UListF.ReSortList
        ChatFrames(I).UListF.Refresh
        Exit For
      End If
    Next
  End If
End Function

Private Sub UserListState(CFrame As ChatFrame, ListIndex As Integer, State As Integer, lTime As Date)
  Dim DAT As String
  Dim I As Double
  If State = 1 Then
    CFrame.UListF.SetListText ListIndex, 1, "+ Online", True
    CFrame.UListF.SetListText ListIndex, -1, 62, True
  ElseIf State = 2 Then
    CFrame.UListF.SetListText ListIndex, 1, "-  Offline", True
    CFrame.UListF.SetListText ListIndex, -1, 64, True
  Else
    CFrame.UListF.SetListText ListIndex, 1, TeVal.lsUnknowen, True
    CFrame.UListF.SetListText ListIndex, -1, 63, True
  End If
  If lTime > 0 Then
    If State <> 1 Then
      DAT = Format(Now, "short date")
      I = DateDiff("s", lTime, Now)
      If I > 900 Then DAT = DAT + "      (" + FormatSec(I) + ")"
    End If
    CFrame.UListF.SetListText ListIndex, 3, DAT, True
  End If
End Sub

Public Sub ShowFriedsList()
  Dim FrFile As Integer
  Dim I As Long
  Dim I2 As Integer
  Dim ChFrame As New ChatFrame
  
  For I = 1 To ChatFrames.Count
    If ChatFrames(I).FrameType = cfFriends Then
      If ChatFrames(I) Is FrontCFrame Then
        ChatFrames(I).CloseFrame
      Else
        SetFrontFrame ChatFrames(I)
      End If
      Exit Sub
    End If
  Next
  
  ChFrame.Caption = TeVal.lsFrinds
  ChFrame.Inst cfFriends, -1
  
  ChFrame.UListF.SetColum 0, TeVal.ls4Nick, 150
  ChFrame.UListF.AddColum TeVal.ls4State, 80
  ChFrame.UListF.AddColum TeVal.ls4Server, 120
  ChFrame.UListF.AddColum TeVal.ls4Online
  
  If FriendsListCound > 0 Then
    For I = 1 To FriendsListCound
      If Len(Trim(FriendsList(I).Nick)) > 0 Then
        I2 = ChFrame.UListF.Add(Trim(FriendsList(I).Nick), 63, False)
        ChFrame.UListF.SetListText I2, 0, Trim(FriendsList(I).Nick), True
        ChFrame.UListF.SetListText I2, 2, Trim(FriendsList(I).Server), True
        UserListState ChFrame, I2, FriendsList(I).State, FriendsList(I).LastOnline
      End If
    Next
    ChFrame.UListF.CSortIndex = 1
    ChFrame.UListF.ReSortList
  End If

  ChatFrames.Add ChFrame
  SetFrontFrame ChFrame
End Sub

Public Sub SaveFriendsValues()
  Dim OFi As Integer
  Dim I As Integer
  Dim DAT As String
  
  If FriendsListCound = 0 Then Exit Sub
  OFi = FreeFile
  On Error Resume Next
  Open UserPath + "FriendsValues.ini" For Output As #OFi
    For I = 1 To FriendsListCound
      If Len(Trim(FriendsList(I).Nick)) > 0 Then
        Print #OFi, ">Entry " + CStr(I)
        Print #OFi, "Nick=" + FriendsList(I).Nick
        Print #OFi, "Server=" + FriendsList(I).Server
        Print #OFi, "LastOnlineDate=" + CStr(CDbl(FriendsList(I).LastOnline) * DAY_SEC)
        Print #OFi, "State=" + CStr(FriendsList(I).State)
        Print #OFi, ""
      End If
    Next
  Close #OFi
  If Err <> 0 Then MsgBox Err.Description + " (Friendsvalues)", vbCritical
  On Error GoTo 0
End Sub

Public Sub LoadFriendsValues()
  Dim OFi As Integer
  Dim InpData As String
  Dim I As Integer
  Dim Path As String
  
  If FriendsListCound = 0 Then ReDim FriendsList(1 To 4)
  FriendsListCound = 0
  OFi = FreeFile
  Path = UserPath + "FriendsValues.ini"
  On Error Resume Next
  If Dir(Path) <> "" Then
    Open Path For Input As #OFi
      Do Until EOF(OFi) = True
        Line Input #OFi, InpData
        If Left(InpData, 1) = ">" Then
          FriendsListCound = FriendsListCound + 1
          If FriendsListCound > UBound(FriendsList) Then
            ReDim Preserve FriendsList(1 To FriendsListCound + 3)
          End If
          FriendsList(FriendsListCound).State = 0
        End If
        I = InStr(InpData, "=")
        If I > 0 Then
          Select Case LCase(Left(InpData, I - 1))
            Case "nick"
              FriendsList(FriendsListCound).Nick = Trim(Right(InpData, Len(InpData) - I))
            Case "server"
              FriendsList(FriendsListCound).Server = Trim(Right(InpData, Len(InpData) - I))
            Case "lastonlinedate"
              FriendsList(FriendsListCound).LastOnline = Val(Right(InpData, Len(InpData) - I)) / DAY_SEC
            Case "lastonline"
              FriendsList(FriendsListCound).LastOnline = CDate(Right(InpData, Len(InpData) - I)) 'Abw�rtskompatiblit�t
          End Select
        End If
      Loop
    Close #OFi
  End If
  If Err <> 0 Then MsgBox Err.Description + " (Friendsvalues)", vbCritical
  On Error GoTo 0
End Sub


