VERSION 5.00
Begin VB.Form Form1 
   AutoRedraw      =   -1  'True
   Caption         =   "Form1"
   ClientHeight    =   3420
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6960
   LinkTopic       =   "Form1"
   ScaleHeight     =   228
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   464
   StartUpPosition =   3  'Windows-Standard
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   1920
      Top             =   1320
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  Dim Xs As Long, Ys As Long
  Dim x As Long, y As Long
  Dim i As Long
  Dim Path As String
  Dim FLen As Long
  Dim DAT(3) As Byte
  Dim Col As Long
  
  Path = Command
  Xs = 321
  Ys = 80
  
  Open Path For Binary As #1
  
  FLen = LOF(1)
  
  For x = 0 To Xs - 1
    For y = 0 To Ys - 1
      i = FLen - (Xs * Ys * 4) + (x + y * Xs) * 4 + 1
      Get 1, i, DAT
      Me.PSet (x + 40, 100 - y), DAT(3)
      For Col = 0 To 2
        DAT(Col) = DAT(Col) * CLng(DAT(3)) \ 255
      Next
      Put 1, i, DAT
    Next
  Next
  
  Close #1
End Sub

Private Sub Timer1_Timer()
  Unload Me
End Sub
