Attribute VB_Name = "SoundOut"
Option Explicit

Private Const SND_ASYNC = &H1 ' Stoppt die Wiedergabe aller Sounddateien, um diese abzuspielen
Private Const SND_FILENAME = &H20000 ' der angegebene Name ist ein Pfad zu einer
Private Const SND_NODEFAULT = &H2 ' spielt keinen Standardsound ab wenn die

Private Declare Sub OUTPORT Lib "PORT.DLL" (ByVal Adr As Integer, ByVal DAT As Integer)
Private Declare Function INPORT Lib "PORT.DLL" (ByVal Adr As Integer) As Integer
Private Declare Sub DELAY Lib "PORT.DLL" (ByVal Zeit As Integer)
Private Declare Function BeeP2000 Lib "kernel32" Alias "Beep" (ByVal dwFreq As Long, ByVal dwDuration As Long) As Long
Private Declare Function PlaySound Lib "winmm.dll" Alias "PlaySoundA" (lpszName As Any, ByVal hModule As Long, ByVal dwFlags As Long) As Long

Private BeepLoopTime As Long

Public Sub Beep2(Optional StrToPlay As String)
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim BeepTArr As String
  
  If Len(StrToPlay) Then
    BeepTArr = StrToPlay
  Else
    BeepTArr = BeepArr
  End If
  
  On Error Resume Next
  If IsNt Then
    Do
      I3 = I2
      I = InStr(I3 + 1, BeepTArr, "/")
      If I = 0 Then Exit Do
      I2 = InStr(I + 1, BeepTArr, " ")
      If I2 = 0 Then I2 = Len(BeepTArr) + 1
      If I - I3 - 1 < 5 And I2 - I - 1 < 4 Then
        BeeP2000 Val(Mid(BeepTArr, I3 + 1, I - I3 - 1)), Val(Mid(BeepTArr, I + 1, I2 - I - 1))
      End If
    Loop Until I2 = Len(BeepTArr) + 1
  Else
    Beep9x
  End If
End Sub

Public Sub MakeSignal(Optional NoBreak As Boolean)
  Dim DAT As String
  
  If SoundStumm = True Then Exit Sub
  If AltTimeDiff(BeepLoopTime) < 4 And NoBreak = False Then Exit Sub
  If PlayWav = True And WavePath <> "" Then
    On Error Resume Next
    DAT = WavePath
    If InStr(DAT, "\") = 0 Then
      DAT = AppPath + Dir(AppPath + DAT)
    End If
    PlaySound ByVal DAT, 0&, SND_ASYNC Or SND_FILENAME Or SND_NODEFAULT
    On Error GoTo 0
  End If
  If PlayBeep = True Then
    Beep2
  End If
  BeepLoopTime = AltTimer
End Sub

Public Sub MakeSignal2(Optional WaveFilePath As String)
  Dim DAT As String
  
  If SoundStumm = True And Len(WaveFilePath) = 0 Then Exit Sub
  If AltTimeDiff(BeepLoopTime) < 4 Then Exit Sub
  If Len(WaveFilePath) Then
    DAT = WaveFilePath
  Else
    DAT = WavePath2
  End If
  If (PlayWav2 = True Or Len(WaveFilePath) > 0) And Len(DAT) > 0 Then
    On Error Resume Next
    If InStr(DAT, "\") = 0 Then
      DAT = AppPath + Dir(AppPath + DAT)
    End If
    PlaySound ByVal DAT, 0&, SND_ASYNC Or SND_FILENAME Or SND_NODEFAULT
    On Error GoTo 0
  End If
  If PlayBeep2 = True And Len(WaveFilePath) = 0 Then
    Beep2
  End If
  BeepLoopTime = AltTimer
End Sub

Private Sub Beep9x()
  Dim n As Integer
  For n = 1 To 200
     OUTPORT 97, (INPORT(97) Or 2)
     DELAY 1
     OUTPORT 97, (INPORT(97) And 253)
     DELAY 1
  Next n
End Sub



