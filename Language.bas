Attribute VB_Name = "Language"
Option Explicit

Public LanguageName As String

Public Sub SaveNextLangw(LFile As String)
  Dim I As Integer
  
  On Error Resume Next
  I = FreeFile
  Open UserPath + "Language.ini" For Output As #I
    Print #I, LFile
  Close #I
  If Err <> 0 Then MsgBox Err.Description + " (SaveNextLangw)", vbCritical
  On Error GoTo 0
End Sub

Public Sub ListAllLangws()
  Dim DAT As String
  Dim LastPath As String
  Dim I As Integer
  
  On Error Resume Next
  I = FreeFile
  If Len(Dir(UserPath + "Language.ini")) > 0 Then
    Open UserPath + "Language.ini" For Input As #I
      Line Input #I, DAT
      LanguageName = Trim(DecodeUTF8(DAT))
    Close #I
  ElseIf Len(Dir(AppPath + "Language.ini")) > 0 Then
    Open AppPath + "Language.ini" For Input As #I
      Line Input #I, DAT
      LanguageName = Trim(DecodeUTF8(DAT))
    Close #I
  End If
  LanguageName = Replace(LanguageName, ".lng", ".ulng") 'Switch to Unicode file
  If Err <> 0 Then MsgBox Err.Description + " (ListAllLangws)", vbCritical
  On Error GoTo 0
  
  I = 0
  DAT = Dir(AppPath + "*.ulng")
  Do Until Len(DAT) = 0 Or I > 64
    If I > 0 Then Load Form1.menLListe(I): Form1.menLListe(I).Checked = False
    Form1.menLListe(I).Caption = Left(DAT, Len(DAT) - 5)
    LastPath = DAT
    If DAT = LanguageName Then Form1.menLListe(I).Checked = True
    DAT = Dir
    I = I + 1
  Loop
  If Len(Dir(AppPath + LanguageName)) = 0 Or Len(LanguageName) = 0 Then
    LanguageName = LastPath
    If I > 0 Then Form1.menLListe(I - 1).Checked = True
  End If
End Sub

Public Sub LoadFormStrs(Frm As Form, Optional LoadTeVal As Boolean)
  Dim Frif As Integer
  Dim DAT As String
  Dim RawDAT As String
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim I4 As Integer
  Dim Ind As Integer
  Dim ErrLine As Long
  Dim CurrCharSet As Long
  
  CurrCharSet = GetCharSetByCP(BasicCodepage)
  
  On Error Resume Next
  Frif = FreeFile
  If Len(Dir(AppPath + LanguageName)) > 0 And Len(LanguageName) > 0 Then
    Open AppPath + LanguageName For Input As #Frif
      Do Until EOF(Frif)
        If Err > 0 Then Exit Do
        ErrLine = ErrLine + 1
        Line Input #Frif, RawDAT
        DAT = DecodeUTF8(RawDAT)
        If Len(DAT) > 0 And Left(DAT, 1) <> "#" Then
          If AscW(Left(DAT, 1)) > 47 And AscW(Left(DAT, 1)) < 58 Then
            If LoadTeVal = True Then
              ReDim Preserve ComIndex(I3) As String
              ComIndex(I3) = DAT
              I3 = I3 + 1
            End If
            I = 0
          Else
            I = InStr(1, DAT, ".")
          End If
          If I > 0 Then
            I2 = InStr(I + 1, DAT, "=")
            If I2 > 0 Then
              If LoadTeVal = True And Left(DAT, I - 1) = "global" Then
                Select Case LCase(Trim(Mid(DAT, I + 1, I2 - I - 1)))
                  Case "autoupdate1"
                    TeVal.AutoUpdate1 = Trim(Mid(DAT, I2 + 1))
                  Case "autoupdate2"
                    TeVal.AutoUpdate2 = Trim(Mid(DAT, I2 + 1))
                  Case "butapply"
                    TeVal.butApply = Trim(Mid(DAT, I2 + 1))
                  Case "butcancel"
                    TeVal.butCancel = Trim(Mid(DAT, I2 + 1))
                  Case "butok"
                    TeVal.butOK = Trim(Mid(DAT, I2 + 1))
                  Case "caption1"
                    TeVal.Caption1 = Trim(Mid(DAT, I2 + 1))
                  Case "caption2"
                    TeVal.Caption2 = Trim(Mid(DAT, I2 + 1))
                  Case "closed"
                    TeVal.Closed = Trim(Mid(DAT, I2 + 1))
                  Case "concancel"
                    TeVal.ConCancel = Trim(Mid(DAT, I2 + 1))
                  Case "conclose"
                    TeVal.ConClose = Trim(Mid(DAT, I2 + 1))
                  Case "conerr"
                    TeVal.ConErr = Trim(Mid(DAT, I2 + 1))
                  Case "connected"
                    TeVal.Connected = Trim(Mid(DAT, I2 + 1))
                  Case "connecting"
                    TeVal.Connecting = Trim(Mid(DAT, I2 + 1))
                  Case "conok"
                    TeVal.ConOK = Trim(Mid(DAT, I2 + 1))
                  Case "constart"
                    TeVal.ConStart = Trim(Mid(DAT, I2 + 1))
                  Case "dcccaption"
                    TeVal.DccCaption = Trim(Mid(DAT, I2 + 1))
                  Case "dccconncancel"
                    TeVal.DccConnCancel = Trim(Mid(DAT, I2 + 1))
                  Case "dccfilenotfound"
                    TeVal.dccFileNotFound = Trim(Mid(DAT, I2 + 1))
                  Case "dccfinished"
                    TeVal.DccFinished = Trim(Mid(DAT, I2 + 1))
                  Case "dccok"
                    TeVal.DCCok = Trim(Mid(DAT, I2 + 1))
                  Case "dccquestion"
                    TeVal.DCCquestion = Trim(Mid(DAT, I2 + 1))
                  Case "extworning"
                    TeVal.ExtWorning = Trim(Mid(DAT, I2 + 1))
                  Case "fileexist"
                    TeVal.FileExist = Trim(Mid(DAT, I2 + 1))
                  Case "join"
                    TeVal.Join = Trim(Mid(DAT, I2 + 1))
                  Case "kick"
                    TeVal.Kick = Trim(Mid(DAT, I2 + 1))
                  Case "kick2"
                    TeVal.Kick2 = Trim(Mid(DAT, I2 + 1))
                  Case "leave"
                    TeVal.Leave = Trim(Mid(DAT, I2 + 1))
                  Case "lsno"
                    TeVal.lsNo = Trim(Mid(DAT, I2 + 1))
                  Case "lsunknowen"
                    TeVal.lsUnknowen = Trim(Mid(DAT, I2 + 1))
                  Case "lsyes"
                    TeVal.lsYes = Trim(Mid(DAT, I2 + 1))
                  Case "msgdel"
                    TeVal.msgDel = Trim(Mid(DAT, I2 + 1))
                  Case "msgnodel"
                    TeVal.msgNoDel = Trim(Mid(DAT, I2 + 1))
                  Case "nick"
                    TeVal.Nick = Trim(Mid(DAT, I2 + 1))
                  Case "nologs"
                    TeVal.NoLogs = Trim(Mid(DAT, I2 + 1))
                  Case "quit"
                    TeVal.Quit = Trim(Mid(DAT, I2 + 1))
                  Case "roomenter"
                    TeVal.RoomEnter = Trim(Mid(DAT, I2 + 1))
                  Case "userrank"
                    TeVal.UserRank = Trim(Mid(DAT, I2 + 1))
                  Case "cuserrank"
                    TeVal.CUserRank = Trim(Mid(DAT, I2 + 1))
                  Case "wispers"
                    TeVal.Wispers = Trim(Mid(DAT, I2 + 1))
                  Case "wispersto"
                    TeVal.WispersTo = Trim(Mid(DAT, I2 + 1))
                  Case "wisperto"
                    TeVal.WisperTo = Trim(Mid(DAT, I2 + 1))
                  Case "mennew"
                    TeVal.menNew = Trim(Mid(DAT, I2 + 1))
                  Case "mendel"
                    TeVal.menDel = Trim(Mid(DAT, I2 + 1))
                  Case "menvalues"
                    TeVal.menValues = Trim(Mid(DAT, I2 + 1))
                  Case "mencut"
                    TeVal.menCut = Trim(Mid(DAT, I2 + 1))
                  Case "mencopy"
                    TeVal.menCopy = Trim(Mid(DAT, I2 + 1))
                  Case "menpast"
                    TeVal.menPast = Trim(Mid(DAT, I2 + 1))
                  Case "menconn"
                    TeVal.menConn = Trim(Mid(DAT, I2 + 1))
                  Case "mendis"
                    TeVal.menDis = Trim(Mid(DAT, I2 + 1))
                  Case "menrooms"
                    TeVal.menRooms = Trim(Mid(DAT, I2 + 1))
                  Case "mensend"
                    TeVal.menSend = Trim(Mid(DAT, I2 + 1))
                  Case "mencolors"
                    TeVal.menColors = Trim(Mid(DAT, I2 + 1))
                  Case "menopt"
                    TeVal.menOpt = Trim(Mid(DAT, I2 + 1))
                  Case "menfrinds"
                    TeVal.menFrinds = Trim(Mid(DAT, I2 + 1))
                  Case "menhelp"
                    TeVal.menHelp = Trim(Mid(DAT, I2 + 1))
                  Case "mensymbol"
                    TeVal.menSymbol = Trim(Mid(DAT, I2 + 1))
                  Case "menmenue"
                    TeVal.menMenue = Trim(Mid(DAT, I2 + 1))
                  Case "menscriptstart"
                    TeVal.menScriptStart = Trim(Mid(DAT, I2 + 1))
                  Case "menscriptstop"
                    TeVal.menScriptStop = Trim(Mid(DAT, I2 + 1))
                  Case "menscript"
                    TeVal.menScript = Trim(Mid(DAT, I2 + 1))
                  Case "ls1caption"
                    TeVal.ls1Caption = Trim(Mid(DAT, I2 + 1))
                  Case "ls1server"
                    TeVal.ls1Server = Trim(Mid(DAT, I2 + 1))
                  Case "ls1used"
                    TeVal.ls1Used = Trim(Mid(DAT, I2 + 1))
                  Case "ls1state"
                    TeVal.ls1State = Trim(Mid(DAT, I2 + 1))
                  Case "ls1date"
                    TeVal.ls1Date = Trim(Mid(DAT, I2 + 1))
                  Case "ls1auto"
                    TeVal.ls1Auto = Trim(Mid(DAT, I2 + 1))
                  Case "lsservers"
                    TeVal.lsServers = Trim(Mid(DAT, I2 + 1))
                  Case "lsrooms"
                    TeVal.lsRooms = Trim(Mid(DAT, I2 + 1))
                  Case "lsfrinds"
                    TeVal.lsFrinds = Trim(Mid(DAT, I2 + 1))
                  Case "lsscript"
                    TeVal.lsScript = Trim(Mid(DAT, I2 + 1))
                  Case "ls2name"
                    TeVal.ls2Name = Trim(Mid(DAT, I2 + 1))
                  Case "ls2user"
                    TeVal.ls2User = Trim(Mid(DAT, I2 + 1))
                  Case "ls2topic"
                    TeVal.ls2Topic = Trim(Mid(DAT, I2 + 1))
                  Case "ls3filename"
                    TeVal.ls3FileName = Trim(Mid(DAT, I2 + 1))
                  Case "ls3progres"
                    TeVal.ls3Progres = Trim(Mid(DAT, I2 + 1))
                  Case "ls3speed"
                    TeVal.ls3Speed = Trim(Mid(DAT, I2 + 1))
                  Case "ls3time"
                    TeVal.ls3Time = Trim(Mid(DAT, I2 + 1))
                  Case "ls3size"
                    TeVal.ls3Size = Trim(Mid(DAT, I2 + 1))
                  Case "ls3user"
                    TeVal.ls3User = Trim(Mid(DAT, I2 + 1))
                  Case "ls3path"
                    TeVal.ls3Path = Trim(Mid(DAT, I2 + 1))
                  Case "ls4nick"
                    TeVal.ls4Nick = Trim(Mid(DAT, I2 + 1))
                  Case "ls4state"
                    TeVal.ls4State = Trim(Mid(DAT, I2 + 1))
                  Case "ls4server"
                    TeVal.ls4Server = Trim(Mid(DAT, I2 + 1))
                  Case "ls4online"
                    TeVal.ls4Online = Trim(Mid(DAT, I2 + 1))
                  Case "ls5maske"
                    TeVal.ls5Maske = Trim(Mid(DAT, I2 + 1))
                  Case "ls5von"
                    TeVal.ls5Von = Trim(Mid(DAT, I2 + 1))
                  Case "ls5vor"
                    TeVal.ls5Vor = Trim(Mid(DAT, I2 + 1))
                  Case "cclientmsg"
                    TeVal.cClientMsg = Trim(Mid(DAT, I2 + 1))
                  Case "cerrmsg"
                    TeVal.cErrMsg = Trim(Mid(DAT, I2 + 1))
                  Case "cnickw"
                    TeVal.cNickW = Trim(Mid(DAT, I2 + 1))
                  Case "clink"
                    TeVal.cLink = Trim(Mid(DAT, I2 + 1))
                  Case "ctext"
                    TeVal.cText = Trim(Mid(DAT, I2 + 1))
                  Case "cnickn"
                    TeVal.cNickN = Trim(Mid(DAT, I2 + 1))
                  Case "cnotice"
                    TeVal.cNotice = Trim(Mid(DAT, I2 + 1))
                  Case "cmsg"
                    TeVal.cMSG = Trim(Mid(DAT, I2 + 1))
                  Case "chmsg"
                    TeVal.chMSG = Trim(Mid(DAT, I2 + 1))
                  Case "cchg"
                    TeVal.ccHG = Trim(Mid(DAT, I2 + 1))
                  Case "ctuserl"
                    TeVal.ctUserL = Trim(Mid(DAT, I2 + 1))
                  Case "culhg"
                    TeVal.cULHG = Trim(Mid(DAT, I2 + 1))
                  Case "ctextf"
                    TeVal.cTextF = Trim(Mid(DAT, I2 + 1))
                  Case "ctextfhg"
                    TeVal.cTextFHG = Trim(Mid(DAT, I2 + 1))
                  Case "ctimestamp"
                    TeVal.cTimeStamp = Trim(Mid(DAT, I2 + 1))
                  Case "cownnick"
                    TeVal.cOwnNick = Trim(Mid(DAT, I2 + 1))
                  Case "ogeneral"
                    TeVal.oGeneral = Trim(Mid(DAT, I2 + 1))
                  Case "omsgs"
                    TeVal.oMsgs = Trim(Mid(DAT, I2 + 1))
                  Case "oconnections"
                    TeVal.oConnections = Trim(Mid(DAT, I2 + 1))
                  Case "oshortcuts"
                    TeVal.oShortcuts = Trim(Mid(DAT, I2 + 1))
                  Case "oapp"
                    TeVal.oApp = Trim(Mid(DAT, I2 + 1))
                  Case "oplugins"
                    TeVal.oPlugins = Trim(Mid(DAT, I2 + 1))
                  Case "oppos1"
                    TeVal.opPos1 = Trim(Mid(DAT, I2 + 1))
                  Case "oppos2"
                    TeVal.opPos2 = Trim(Mid(DAT, I2 + 1))
                  Case "newctitel"
                    TeVal.NewCTitel = Trim(Mid(DAT, I2 + 1))
                  Case "ffunction"
                    TeVal.fFunction = Trim(Mid(DAT, I2 + 1))
                  Case "fon"
                    TeVal.fOn = Trim(Mid(DAT, I2 + 1))
                  Case "foff"
                    TeVal.fOff = Trim(Mid(DAT, I2 + 1))
                  Case "frindonline"
                    TeVal.FrindOnline = Trim(Mid(DAT, I2 + 1))
                  Case "scrtimeout"
                    TeVal.scrTimeout = Trim(Mid(DAT, I2 + 1))
                  Case "searchresults"
                    TeVal.SearchResults = Trim(Mid(DAT, I2 + 1))
                  Case "timeout"
                    TeVal.TimeOut = Trim(Mid(DAT, I2 + 1))
                  Case "topicset"
                    TeVal.TopicSet = Trim(Mid(DAT, I2 + 1))
                  Case "ignore"
                    TeVal.Ignore = Trim(Mid(DAT, I2 + 1))
                  Case "unignore"
                    TeVal.UnIgnore = Trim(Mid(DAT, I2 + 1))
                  Case "screload"
                    TeVal.scReload = Trim(Mid(DAT, I2 + 1))
                  Case "conremove"
                    TeVal.ConRemove = Trim(Mid(DAT, I2 + 1))
                  Case "away"
                    TeVal.Away = Trim(Mid(DAT, I2 + 1))
                  Case "whoisrealname"
                    TeVal.WhoisRealName = Trim(Mid(DAT, I2 + 1))
                  Case "whoishostmask"
                    TeVal.WhoisHostMask = Trim(Mid(DAT, I2 + 1))
                  Case "whoislist"
                    TeVal.WhoisList = Trim(Mid(DAT, I2 + 1))
                  Case "whoisregedas"
                    TeVal.WhoisRegedAs = Trim(Mid(DAT, I2 + 1))
                  Case "whoisin"
                    TeVal.WhoisIn = Trim(Mid(DAT, I2 + 1))
                  Case "whoisserver"
                    TeVal.WhoisServer = Trim(Mid(DAT, I2 + 1))
                  Case "whoisidle"
                    TeVal.WhoisIdle = Trim(Mid(DAT, I2 + 1))
                  Case "whoisso"
                    TeVal.WhoisSO = Trim(Mid(DAT, I2 + 1))
                  Case "whoisreged"
                    TeVal.WhoisReged = Trim(Mid(DAT, I2 + 1))
                  Case "founded"
                    TeVal.TopicSeter = Trim(Mid(DAT, I2 + 1))
                  Case "invite"
                    TeVal.Invite = Trim(Mid(DAT, I2 + 1))
                  Case "bezchannel"
                    TeVal.bezChannel = Trim(Mid(DAT, I2 + 1))
                  Case "bezuser"
                    TeVal.bezUser = Trim(Mid(DAT, I2 + 1))
                  Case "bezglobal"
                    TeVal.bezGlobal = Trim(Mid(DAT, I2 + 1))
                End Select
              ElseIf Left(DAT, I - 1) = Frm.Name Then
                If Mid(DAT, I2 - 2, 1) = ")" Then
                  I4 = InStr(1, DAT, "(")
                Else
                  I4 = 0
                End If
                For Ind = 0 To Frm.Controls.Count - 1
                  If I4 Then
                    If Frm.Controls(Ind).Name = Mid(DAT, I + 1, I4 - I - 1) Then
                      If Frm.Controls(Ind).Index = Val(Mid(DAT, I4 + 1, 2)) Then
                        Frm.Controls(Ind).Caption = Trim(Mid(DAT, I2 + 1))
                        If (TypeOf Frm.Controls(Ind) Is Menu) = False Then Frm.Controls(Ind).Font.CharSet = CurrCharSet
                      End If
                    End If
                  Else
                    If Frm.Controls(Ind).Name = Trim(Mid(DAT, I + 1, I2 - I - 1)) Then
                      Frm.Controls(Ind).Caption = Trim(Mid(DAT, I2 + 1))
                      If (TypeOf Frm.Controls(Ind) Is Menu) = False Then Frm.Controls(Ind).Font.CharSet = CurrCharSet
                    End If
                  End If
                Next
              End If
            End If
          End If
        End If
      Loop
    Close #Frif
  End If
  If Err <> 0 Then MsgBox Err.Description + " (LoadFormStrs, in line: " + CStr(ErrLine) + " of " + LanguageName + ")", vbCritical
  On Error GoTo 0
  If I3 = 0 And LoadTeVal = True Then ReDim Preserve ComIndex(0)
End Sub
