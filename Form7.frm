VERSION 5.00
Begin VB.Form Form7 
   BorderStyle     =   3  'Fester Dialog
   Caption         =   "Eigenschaften"
   ClientHeight    =   5700
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5505
   BeginProperty Font 
      Name            =   "MS Shell Dlg"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Form7.frx":0000
   LinkTopic       =   "Form7"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   380
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   367
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'Fenstermitte
   Begin VB.PictureBox Picture2 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'Kein
      Height          =   330
      Left            =   210
      ScaleHeight     =   330
      ScaleWidth      =   330
      TabIndex        =   23
      Top             =   105
      Width           =   330
   End
   Begin VB.TextBox Text9 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'Kein
      BeginProperty Font 
         Name            =   "MS Shell Dlg"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C00000&
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   22
      Top             =   4200
      Width           =   3585
   End
   Begin VB.PictureBox Picture1 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'Kein
      Height          =   915
      Left            =   2940
      ScaleHeight     =   61
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   165
      TabIndex        =   20
      Top             =   630
      Width           =   2475
   End
   Begin VB.TextBox Text8 
      Height          =   285
      Left            =   1680
      MaxLength       =   5
      TabIndex        =   18
      Top             =   2310
      Width           =   1275
   End
   Begin VB.TextBox Text7 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'Kein
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   16
      Top             =   3885
      Width           =   3585
   End
   Begin VB.TextBox Text6 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'Kein
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   14
      Top             =   3360
      Width           =   3690
   End
   Begin VB.TextBox Text5 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'Kein
      Height          =   285
      Left            =   1995
      Locked          =   -1  'True
      TabIndex        =   12
      Top             =   1785
      Width           =   1590
   End
   Begin VB.TextBox Text4 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'Kein
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   10
      Top             =   1260
      Width           =   1170
   End
   Begin VB.TextBox Text3 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'Kein
      Height          =   285
      Left            =   1680
      Locked          =   -1  'True
      TabIndex        =   8
      Top             =   735
      Width           =   1170
   End
   Begin VB.TextBox Text2 
      BackColor       =   &H8000000F&
      BorderStyle     =   0  'Kein
      Height          =   600
      Left            =   1680
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   6
      Top             =   4515
      Width           =   3585
   End
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   1680
      MaxLength       =   5
      TabIndex        =   4
      Top             =   2835
      Width           =   1275
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Übernehmen"
      Height          =   345
      Left            =   4200
      TabIndex        =   2
      Top             =   5250
      Width           =   1215
   End
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Caption         =   "Abbrechen"
      Height          =   345
      Left            =   2835
      TabIndex        =   1
      Top             =   5250
      Width           =   1215
   End
   Begin VB.CommandButton Command3 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   345
      Left            =   1470
      TabIndex        =   0
      Top             =   5250
      Width           =   1215
   End
   Begin VB.Label Label21 
      Caption         =   "Dateiname"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   525
      TabIndex        =   24
      Top             =   105
      Width           =   4755
   End
   Begin VB.Label Label10 
      Caption         =   "t"
      BeginProperty Font 
         Name            =   "Marlett"
         Size            =   11.25
         Charset         =   2
         Weight          =   500
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   1680
      TabIndex        =   21
      Top             =   1770
      Width           =   225
   End
   Begin VB.Label Label9 
      Caption         =   "0 %"
      Height          =   225
      Left            =   3885
      TabIndex        =   19
      Top             =   1575
      Width           =   540
   End
   Begin VB.Label Label8 
      Caption         =   "Max KByte/sec:"
      Height          =   225
      Left            =   210
      TabIndex        =   17
      Top             =   2325
      Width           =   1380
   End
   Begin VB.Label Label7 
      Caption         =   "Peer:"
      Height          =   225
      Left            =   210
      TabIndex        =   15
      Top             =   3900
      Width           =   1380
   End
   Begin VB.Label Label6 
      Caption         =   "Verbleidende Zeit:"
      Height          =   225
      Left            =   210
      TabIndex        =   13
      Top             =   3360
      Width           =   1380
   End
   Begin VB.Label Label5 
      Caption         =   "Geschwindigkeit:"
      Height          =   225
      Left            =   210
      TabIndex        =   11
      Top             =   1800
      Width           =   1380
   End
   Begin VB.Label Label4 
      Caption         =   "Übertragen:"
      Height          =   225
      Left            =   210
      TabIndex        =   9
      Top             =   1275
      Width           =   1380
   End
   Begin VB.Label Label3 
      Caption         =   "Dateigröße:"
      Height          =   225
      Left            =   210
      TabIndex        =   7
      Top             =   750
      Width           =   1380
   End
   Begin VB.Label Label2 
      Caption         =   "Dateipfad:"
      Height          =   225
      Left            =   210
      TabIndex        =   5
      Top             =   4530
      Width           =   1380
   End
   Begin VB.Label Label1 
      Caption         =   "Paketgröße:(Byte)"
      Height          =   225
      Left            =   210
      TabIndex        =   3
      Top             =   2850
      Width           =   1380
   End
   Begin VB.Line Line19 
      BorderColor     =   &H80000010&
      X1              =   14
      X2              =   357
      Y1              =   32
      Y2              =   32
   End
   Begin VB.Line Line20 
      BorderColor     =   &H80000010&
      X1              =   14
      X2              =   360
      Y1              =   343
      Y2              =   343
   End
End
Attribute VB_Name = "Form7"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function Pie Lib "gdi32" (ByVal hDC As _
        Long, ByVal X1 As Long, ByVal Y1 As Long, ByVal _
        X2 As Long, ByVal Y2 As Long, ByVal X3 As Long, _
        ByVal Y3 As Long, ByVal X4 As Long, ByVal Y4 _
        As Long) As Long

Const pi = 3.14159265358979

Dim LastIcon As Integer

Private Sub DrawCookeDg(hDC As Long, Size As Long, Value As Single)
  If Value < 0.005 Then Value = 0.005
  Picture1.ForeColor = CoVal.GrayText
  Picture1.FillStyle = 1
  Pie hDC, 5, 5, Size * 2 - 5, Size / 3 * 2 - 5, _
  Size + Cos(2 * 0.4 * pi) * Size, Size / 3 + Sin(2 * 0.4 * pi) * Size / 3, _
  Size + Cos(2 * ((Value) + 0.4) * pi) * Size, Size / 3 + Sin(2 * ((Value) + 0.4) * pi) * Size / 3
  Picture1.FillColor = &H8000000D
  Picture1.FillStyle = 0
  Pie hDC, 5, 5, Size * 2 - 5, Size / 3 * 2 - 5, _
  Size + Cos(2 * (Value + 0.4) * pi) * Size, Size / 3 + Sin(2 * (Value + 0.4) * pi) * Size / 3, _
  Size + Cos(2 * 0.4 * pi) * Size, Size / 3 + Sin(2 * 0.4 * pi) * Size / 3
End Sub

Public Sub FormRefresh(Optional FirstRef As Boolean)
  Dim I As Integer
  If DCCFileConns(SelledFC).FileSize < 1 Then Exit Sub
  Select Case DCCFileConns(SelledFC).State
    Case 4
      If DCCFileConns(SelledFC).SendGetMode = 1 Then
        I = 10
      Else
        I = 11
      End If
    Case 3, 2
      I = 13
    Case Else
      If DCCFileConns(SelledFC).FileSize = DCCFileConns(SelledFC).ByteCount Then
        I = 12
      Else
        I = 14
      End If
  End Select
  If LastIcon <> I Then
    Picture2.Cls
    PrintIcon Picture2, 2, 4, I
    LastIcon = I
  End If
  
  If Len(DCCFileConns(SelledFC).LastError) = 0 Then
    Label21.ForeColor = &H80000012
    Label21.Caption = DCCFileConns(SelledFC).FileName
  Else
    Label21.ForeColor = vbRed
    Label21.Caption = DCCFileConns(SelledFC).LastError
  End If
  Text3.Text = FormatBytes(DCCFileConns(SelledFC).FileSize)
  Text4.Text = FormatBytes(DCCFileConns(SelledFC).ByteCount)
  Label9.Caption = Format(DCCFileConns(SelledFC).ByteCount / DCCFileConns(SelledFC).FileSize, "0.0%")
  Text5.Text = CStr(Int(DCCFileConns(SelledFC).Speed * 100) / 100) + " KByte/sec"
  If DCCFileConns(SelledFC).SendGetMode = 0 Then
    Label10.Caption = "t"
    If FirstRef = True Then
      Text8.Text = DCCFileConns(SelledFC).MaxSpeedUp
      Text1.Text = DCCFileConns(SelledFC).PaketSize
    End If
  Else
    Label10.Caption = "u"
    Text8.Text = ""
    Text1.Text = DCCFileConns(SelledFC).PaketSize
  End If
  If DCCFileConns(SelledFC).Speed > 0.01 Then
    Text6.Text = FormatSec((DCCFileConns(SelledFC).FileSize - DCCFileConns(SelledFC).ByteCount) _
    / 1024 / DCCFileConns(SelledFC).Speed)
  Else
    If DCCFileConns(SelledFC).FileSize = DCCFileConns(SelledFC).ByteCount Then
      Text6.Text = FormatSec(0)
    Else
      Text6.Text = ""
    End If
  End If
  Text7.Text = DCCFileConns(SelledFC).User + "    ( " + DCCFileConns(SelledFC).PeerIP _
  + ":" + CStr(DCCFileConns(SelledFC).PeerPort) + " )"
  
  Text9.Text = DCCFileConns(SelledFC).NoteCType + DCCFileConns(SelledFC).NoteServer + _
  ":" + CStr(DCCFileConns(SelledFC).NotePort) + "/" + DCCFileConns(SelledFC).NoteRoom
  
  Text2.Text = DCCFileConns(SelledFC).DestPath
  Picture1.Cls
  DrawCookeDg Picture1.hDC, 80, DCCFileConns(SelledFC).ByteCount / DCCFileConns(SelledFC).FileSize
End Sub

Private Sub Command1_Click()
  If DCCFileConns(SelledFC).SendGetMode = 0 Then
    If Val(Text1.Text) > 100000 Then Text1.Text = "100000"
    If Val(Text8.Text) > 1000000 Then Text8.Text = "1000000"
    If Val(Text1.Text) < 1 Then Text1.Text = "0"
    If Val(Text8.Text) < 1 Then Text8.Text = "0"
    DCCFileConns(SelledFC).MaxSpeedUp = Val(Text8.Text)
    DCCFileConns(SelledFC).PaketSize = Val(Text1.Text)
    dccMaxUpSpeed = Val(Text8.Text)
    dccPaketSize = Val(Text1.Text)
  End If
End Sub

Private Sub Command2_Click()
  Unload Me
End Sub

Private Sub Command3_Click()
  Command1_Click
  Unload Me
End Sub

Private Sub Form_Load()
  InitFormFont Me

  ValWindVisible = True
  Me.Caption = TeVal.menValues
  If DCCFileConns(SelledFC).SendGetMode > 0 Then
    Text1.Locked = True
    Text1.BackColor = Me.BackColor
    Text8.Locked = True
    Text8.BackColor = Me.BackColor
  Else
    Text1.Locked = False
    Text1.BackColor = &H80000005
    Text8.Locked = False
    Text8.BackColor = &H80000005
  End If
  LastIcon = 0
  Text9.MouseIcon = LoadResPicture(11, vbResCursor)
  Text9.MousePointer = 99
  LoadFormStrs Me
  FormRefresh True
  Command1.Caption = TeVal.butApply
  Command2.Caption = TeVal.butCancel
  Command3.Caption = TeVal.butOK
End Sub

Private Sub Form_Unload(Cancel As Integer)
  ValWindVisible = False
End Sub

Private Sub Text9_Click()
  If Text9.SelLength > 0 Then Exit Sub
  LastCommandLine = Text9.Text
  PhraseCLine
End Sub
