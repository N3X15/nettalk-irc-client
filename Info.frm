VERSION 5.00
Begin VB.Form Form6 
   BorderStyle     =   3  'Fester Dialog
   Caption         =   "Form6"
   ClientHeight    =   3885
   ClientLeft      =   45
   ClientTop       =   540
   ClientWidth     =   6255
   BeginProperty Font 
      Name            =   "MS Shell Dlg"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Info.frx":0000
   LinkTopic       =   "Form6"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   259
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   417
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'Fenstermitte
   Begin VB.PictureBox Picture1 
      BackColor       =   &H80000005&
      BorderStyle     =   0  'Kein
      FillColor       =   &H80000016&
      Height          =   3360
      Left            =   0
      ScaleHeight     =   224
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   417
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   0
      Width           =   6255
      Begin VB.TextBox Text1 
         Height          =   1695
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertikal
         TabIndex        =   6
         Text            =   "Info.frx":000C
         Top             =   1560
         Width           =   6015
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Shell Dlg"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   3840
         MousePointer    =   99  'Benutzerdefiniert
         TabIndex        =   5
         Top             =   1080
         Width           =   2175
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   1080
         Width           =   3375
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   720
         Width           =   2535
      End
      Begin VB.Label Label1 
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Nettalk"
         BeginProperty Font 
            Name            =   "MS Shell Dlg"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   450
         TabIndex        =   2
         Top             =   120
         Width           =   4215
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   330
      Left            =   4950
      TabIndex        =   0
      Top             =   3480
      Width           =   1215
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000010&
      X1              =   0
      X2              =   416
      Y1              =   224
      Y2              =   224
   End
   Begin VB.Line Line2 
      BorderColor     =   &H80000014&
      X1              =   0
      X2              =   416
      Y1              =   225
      Y2              =   225
   End
End
Attribute VB_Name = "Form6"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
  Unload Me
End Sub

Private Sub Form_Load()
  InitFormFont Me
  
  On Error Resume Next
  Me.Caption = App.Title & "-Info"
  Label2.Caption = "Version: " & App.Major & "." & App.Minor & "." & App.Revision
  Label3 = "Copyright: " & App.LegalCopyright
  Label5 = "Webseite: www.ntalk.de"
  Command1.Caption = TeVal.butOK
End Sub

Private Sub Label5_Click()
  CallWebSite "http://www.ntalk.de", True
End Sub

Private Sub Picture1_Paint()
  DrawFlow Picture1, 0, 0, Picture1.ScaleWidth, Picture1.ScaleHeight / 2, &H8000000F, &H80000005
  DrawFlow Picture1, 0, Picture1.ScaleHeight / 2, Picture1.ScaleWidth, Picture1.ScaleHeight / 2, &H80000005, &H8000000F
  Label5.MouseIcon = LoadResPicture(11, vbResCursor)
  PrintIcon Picture1, 10, 15, 1
End Sub
