Attribute VB_Name = "MixColors"
Option Explicit

Private Type POINTAPI
    X As Long
    Y As Long
End Type

'Private Declare Function TranslateColor Lib "olepro32.dll" Alias "OleTranslateColor" (ByVal clr As OLE_COLOR, ByVal palet As Long, col As Long) As Long
Private Declare Function MoveToEx Lib "gdi32" (ByVal hDC As Long, ByVal X As Long, ByVal Y As Long, lpPoint As POINTAPI) As Long
Private Declare Function LineTo Lib "gdi32" (ByVal hDC As Long, ByVal X As Long, ByVal Y As Long) As Long


Public Sub DrawFlow(PicBox As Object, ByVal Left As Long, ByVal Top As Long, ByVal Width As Long, ByVal Height As Long, ByVal Color1 As Long, ByVal Color2 As Long, Optional Turn As Boolean, Optional RealLen As Long)
  Dim I As Long
  Dim POINT As POINTAPI
  Dim RGB_Farbe As Long, r As Long, g As Long, b As Long
  Dim RGB_Farbe2 As Long, r2 As Long, g2 As Long, b2 As Long
  Dim r3 As Long, g3 As Long, b3 As Long
  Dim TempFC As Long

  TempFC = PicBox.ForeColor

  TranslateColor Color2, 0, RGB_Farbe
  If RGB_Farbe < 1 Then RGB_Farbe = 1
  r = RGB_Farbe Mod 256
  RGB_Farbe = (RGB_Farbe - r) / 256
  g = RGB_Farbe Mod 256
  b = (RGB_Farbe - g) / 256

  TranslateColor Color1, 0, RGB_Farbe2
  If RGB_Farbe2 < 1 Then RGB_Farbe2 = 1
  r2 = RGB_Farbe2 Mod 256
  RGB_Farbe2 = (RGB_Farbe2 - r2) / 256
  g2 = RGB_Farbe2 Mod 256
  b2 = (RGB_Farbe2 - g2) / 256

  If Turn = False Then
    If RealLen = 0 Then RealLen = Height + 16
    For I = 0 To Height
      If I > PicBox.ScaleHeight Or I > RealLen Then Exit For
      r3 = (r * I + r2 * (Height - I)) / Height
      g3 = (g * I + g2 * (Height - I)) / Height
      b3 = (b * I + b2 * (Height - I)) / Height
      
      PicBox.ForeColor = RGB(r3, g3, b3)
      
      POINT.X = Left
      POINT.Y = I + Top
      MoveToEx PicBox.hDC, Left, I + Top, POINT
      LineTo PicBox.hDC, Left + Width, I + Top
    Next
  Else
    If RealLen = 0 Then RealLen = Width + 16
    For I = 0 To Width
      If I > PicBox.ScaleWidth Or I > RealLen Then Exit For
      r3 = (r * I + r2 * (Width - I)) / Width
      g3 = (g * I + g2 * (Width - I)) / Width
      b3 = (b * I + b2 * (Width - I)) / Width
      
      PicBox.ForeColor = RGB(r3, g3, b3)
      
      POINT.X = Left
      POINT.Y = I + Left
      MoveToEx PicBox.hDC, I + Left, Top, POINT
      LineTo PicBox.hDC, I + Left, Top + Height
    Next
  End If
  PicBox.ForeColor = TempFC
End Sub

Public Function GetColorLightnes(Color1 As Long) As Byte
  Dim RGB_Farbe As Long, r As Long, g As Long, b As Long
  
  TranslateColor Color1, 0, RGB_Farbe
  If RGB_Farbe < 1 Then RGB_Farbe = 1
  r = RGB_Farbe Mod 256
  RGB_Farbe = (RGB_Farbe - r) / 256
  g = RGB_Farbe Mod 256
  b = (RGB_Farbe - g) / 256
  
  GetColorLightnes = (r + g + b) \ 3
End Function

Public Function GetMixedColor(Color1 As Long, Color2 As Long, ByVal Percent As Integer) As Long
  Dim RGB_Farbe As Long, r As Long, g As Long, b As Long
  Dim RGB_Farbe2 As Long, r2 As Long, g2 As Long, b2 As Long
  
  TranslateColor Color1, 0, RGB_Farbe
  If RGB_Farbe < 1 Then RGB_Farbe = 1
  r = RGB_Farbe Mod 256
  RGB_Farbe = (RGB_Farbe - r) / 256
  g = RGB_Farbe Mod 256
  b = (RGB_Farbe - g) / 256

  TranslateColor Color2, 0, RGB_Farbe2
  If RGB_Farbe2 < 1 Then RGB_Farbe2 = 1
  r2 = RGB_Farbe2 Mod 256
  RGB_Farbe2 = (RGB_Farbe2 - r2) / 256
  g2 = RGB_Farbe2 Mod 256
  b2 = (RGB_Farbe2 - g2) / 256
  
  
  GetMixedColor = RGB( _
  r * Percent / 100 + r2 * (100 - Percent) / 100, _
  g * Percent / 100 + g2 * (100 - Percent) / 100, _
  b * Percent / 100 + b2 * (100 - Percent) / 100)
End Function

