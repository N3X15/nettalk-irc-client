VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "OSDisplay"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'Private Declare Function GetDeviceCaps Lib "gdi32" (ByVal hDC As Long, ByVal nIndex As Long) As Long
Private Declare Function GetWindowDC Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function GetWindowRect Lib "user32" (ByVal hwnd As Long, lpRect As RECT) As Long
Private Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As Long, ByVal lpString As String, ByVal ccH As Long) As Long

Private LCount As Integer
Private MsgList(1, 10) As String
Private TextTimeOut As Long
Private CheckTimeOut As Long
Private DC As Long
Private TempH As Long

Public Running As Boolean

Const HORZRES = &H8
Const VERTRES = &HA


Public Sub AddLine(Von As String, Text As String)
  Dim I As Integer
  If Running = True Then
    For I = 0 To 9
      MsgList(0, I) = MsgList(0, I + 1)
      MsgList(1, I) = MsgList(1, I + 1)
    Next
    If LCount < 11 Then LCount = LCount + 1
  Else
    LCount = 1
  End If
  If Running = False Then
    DetectFullScr
    If Running = True Then TextTimeOut = AltTimer
  End If
  If Running = True Then
    MsgList(0, 10) = "<" + Von + ">"
    MsgList(1, 10) = Text
    CheckTimeOut = AltTimer
    TimerEvent 1
  End If
End Sub

Public Sub TimerEvent(TimerID As Integer)
  Dim I As Integer
  Dim TeOut As String
  Dim TempVal As Single
  
  If AltTimeDiff(CheckTimeOut) > 1 Then DetectFullScr: CheckTimeOut = AltTimer
  
  If Running = True Then
    If AltTimeDiff(TextTimeOut) > 8 - (LCount ^ 2 / 15) Then
      MsgList(0, 11 - LCount) = ""
      MsgList(1, 11 - LCount) = ""
      LCount = LCount - 1
      TextTimeOut = AltTimer
    End If
    SetBkMode DC, 0
    For I = 11 - LCount To 10
      TeOut = MsgList(0, I) + " " + MsgList(1, I)
'      If I = 11 - LCount And AltTimeDiff(TextTimeOut) > 6 - (LCount ^ 2 / 15) Then
'        TempVal = Screen.Height / Screen.TwipsPerPixelY / 2 - (10 - I) * 22 - _
'        (AltTimeDiff(TextTimeOut + 6 - LCount ^ 2 / 15) * 5) ^ 3
'        If TempVal > 0 And TempVal < 32000 Then DrawInDC TeOut, 12, CLng(TempVal)
'      Else
'        DrawInDC TeOut, 12, Screen.Height / Screen.TwipsPerPixelY / 2 - (10 - I) * 22
        DrawInDC TeOut, 12, 10 * 22 - (10 - I) * 22
'      End If
    Next
    If LCount > 0 Then
      StartTimer Me, 1, 1
    Else
      Running = False
      TempH = 0
    End If
  End If
End Sub

Private Sub DrawInDC(TeOut As String, X As Long, Y As Long)
  If SetTextColor(DC, RGB(0, 50, 0)) = 0 Then Exit Sub
  TextOutW DC, X + 2, Y + 2, StrPtr(TeOut), Len(TeOut)
  SetTextColor DC, RGB(0, 240, 0)
  TextOutW DC, X, Y, StrPtr(TeOut), Len(TeOut)
End Sub

Private Sub DetectFullScr()
  Dim WinRect As RECT
  Dim TeOut As String
  Dim OldWhd As Long

  OldWhd = TempH
  TempH = GetForegroundWindow
  
  If OldWhd <> TempH Then
    If DC <> 0 Then ReleaseDC OldWhd, DC: DC = 0
    GetWindowRect TempH, WinRect
    DC = GetDC(TempH)
  'Running = True
  'Exit Sub
    Running = False
    If WinRect.Bottom = GetDeviceCaps(DC, VERTRES) And _
    WinRect.Right = GetDeviceCaps(DC, HORZRES) And _
    WinRect.Left = 0 And WinRect.Top = 0 Then
      TeOut = String(16, Chr(0))
      GetWindowText TempH, TeOut, 16
      If "Program Manager" <> Left(TeOut, 15) Then Running = True
    End If
  End If
End Sub

Public Sub StopOSD()
  KillTimerByID 1, Me
  LCount = 0
End Sub

Private Sub Class_Terminate()
  KillTimerByID 1, Me
  If DC <> 0 Then ReleaseDC TempH, DC
End Sub
