VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ISubClass"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

  ' öffentliche Enums

  Public Enum ProcessOrderConstants
    poProcessAfterOriginal
    poProcessBeforeOriginal
    poProcessInsteadOfOriginal
    poProcessOriginalOnly
  End Enum


' öffentliche Methoden

Public Function GetProcessingOrder(ByVal hWnd As Long, ByVal MSG As Long, ByVal wParam As Long, ByVal lParam As Long) As ProcessOrderConstants
  '
End Function

Public Function WinMain(ByVal iListener As Long, ByVal oldWinMain As Long, ByVal oldRetVal As Long, ByVal hWnd As Long, ByVal MSG As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
  '
End Function
