VERSION 5.00
Begin VB.Form Form2 
   BorderStyle     =   3  'Fester Dialog
   ClientHeight    =   4965
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6255
   BeginProperty Font 
      Name            =   "MS Shell Dlg"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Form2.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   331
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   417
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'Fenstermitte
   Begin VB.PictureBox Picture2 
      BackColor       =   &H80000005&
      BorderStyle     =   0  'Kein
      Height          =   4395
      Left            =   1560
      ScaleHeight     =   4395
      ScaleWidth      =   4455
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   240
      Width           =   4455
      Begin VB.CheckBox Check5 
         BackColor       =   &H80000005&
         Caption         =   "Alle Daten zum Server in UTF-8 codieren"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   240
         TabIndex        =   31
         Top             =   3960
         Value           =   1  'Aktiviert
         Width           =   4095
      End
      Begin VB.CheckBox Check4 
         BackColor       =   &H80000005&
         Caption         =   "SSL"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   2160
         TabIndex        =   30
         Top             =   3120
         Value           =   1  'Aktiviert
         Width           =   1695
      End
      Begin VB.CheckBox Check3 
         BackColor       =   &H80000005&
         Caption         =   "UTF-8 f�r Nachrichten nutzen"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   3600
         Value           =   1  'Aktiviert
         Width           =   4095
      End
      Begin VB.TextBox Text3 
         Height          =   285
         Left            =   600
         MaxLength       =   50
         TabIndex        =   2
         Text            =   "Verbindung1"
         Top             =   1200
         Width           =   2655
      End
      Begin VB.OptionButton Option1 
         BackColor       =   &H80000005&
         Caption         =   "Bezeichnung des Netzes verwenden"
         Height          =   255
         Left            =   240
         TabIndex        =   0
         Top             =   480
         Width           =   3615
      End
      Begin VB.OptionButton Option2 
         BackColor       =   &H80000005&
         Caption         =   "Eigende Bezeichnung festlegen"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   840
         Value           =   -1  'True
         Width           =   3615
      End
      Begin VB.ComboBox Text1 
         Height          =   315
         Left            =   600
         TabIndex        =   3
         Top             =   2160
         Width           =   3285
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   600
         MaxLength       =   10
         TabIndex        =   4
         Text            =   "6667"
         Top             =   3120
         Width           =   855
      End
      Begin VB.Label Label5 
         BackColor       =   &H80000005&
         Caption         =   "Bezeichnung f�r die Verbindung:"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   240
         TabIndex        =   29
         Top             =   120
         Width           =   3375
      End
      Begin VB.Label Label1 
         BackColor       =   &H80000005&
         Caption         =   "IP oder Name des Servers:"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   1800
         Width           =   2295
      End
      Begin VB.Label Label3 
         BackColor       =   &H80000005&
         Caption         =   "Port des Servers:"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   2760
         Width           =   1575
      End
   End
   Begin VB.PictureBox Picture3 
      BackColor       =   &H80000005&
      BorderStyle     =   0  'Kein
      Height          =   4395
      Left            =   2880
      ScaleHeight     =   4395
      ScaleWidth      =   4455
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   4455
      Begin VB.TextBox Text12 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   600
         MaxLength       =   50
         PasswordChar    =   "*"
         TabIndex        =   13
         Top             =   3840
         Width           =   2415
      End
      Begin VB.TextBox Text5 
         Height          =   285
         Left            =   600
         MaxLength       =   50
         TabIndex        =   12
         Top             =   3000
         Width           =   2415
      End
      Begin VB.TextBox Text7 
         Height          =   285
         Left            =   600
         MaxLength       =   50
         TabIndex        =   11
         Top             =   2160
         Width           =   2415
      End
      Begin VB.TextBox Text6 
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   600
         MaxLength       =   50
         TabIndex        =   9
         Top             =   480
         Width           =   2415
      End
      Begin VB.TextBox Text4 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   600
         MaxLength       =   50
         PasswordChar    =   "*"
         TabIndex        =   10
         Top             =   1320
         Width           =   2415
      End
      Begin VB.Label Label13 
         BackColor       =   &H80000005&
         Caption         =   "Server-Passwort*:"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   240
         TabIndex        =   28
         Top             =   3480
         Width           =   2415
      End
      Begin VB.Label Label6 
         BackColor       =   &H80000005&
         Caption         =   "Richtiger Name*:"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   240
         TabIndex        =   27
         Top             =   2640
         Width           =   2415
      End
      Begin VB.Label Label8 
         BackColor       =   &H80000005&
         Caption         =   "User-ID*:"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   240
         TabIndex        =   24
         Top             =   1800
         Width           =   2415
      End
      Begin VB.Label Label9 
         BackColor       =   &H80000005&
         Caption         =   "Nick-Name:"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   240
         TabIndex        =   23
         Top             =   120
         Width           =   3375
      End
      Begin VB.Label Label7 
         BackColor       =   &H80000005&
         Caption         =   "Passwort*:"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   240
         TabIndex        =   22
         Top             =   960
         Width           =   2415
      End
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Fertig stellen"
      Height          =   350
      Left            =   3370
      TabIndex        =   6
      Top             =   4520
      Width           =   1335
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Weiter >"
      Height          =   350
      Left            =   3370
      TabIndex        =   16
      Top             =   4520
      Width           =   1335
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Abbrechen"
      Height          =   350
      Left            =   4800
      TabIndex        =   8
      Top             =   4520
      Width           =   1335
   End
   Begin VB.CommandButton Command1 
      Caption         =   "< Zur�ck"
      Height          =   350
      Left            =   2040
      TabIndex        =   7
      Top             =   4520
      Width           =   1335
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H80000005&
      BorderStyle     =   0  'Kein
      Height          =   4395
      Left            =   0
      ScaleHeight     =   4395
      ScaleWidth      =   1800
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   0
      Width           =   1800
   End
   Begin VB.PictureBox Picture4 
      BackColor       =   &H80000005&
      BorderStyle     =   0  'Kein
      Height          =   4395
      Left            =   750
      ScaleHeight     =   4395
      ScaleWidth      =   4455
      TabIndex        =   25
      TabStop         =   0   'False
      Top             =   300
      Visible         =   0   'False
      Width           =   4455
      Begin VB.CheckBox Check2 
         BackColor       =   &H80000005&
         Caption         =   "Unsichtbare Verbindung"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   240
         TabIndex        =   32
         Top             =   3480
         Value           =   1  'Aktiviert
         Width           =   3615
      End
      Begin VB.CheckBox Check1 
         BackColor       =   &H80000005&
         Caption         =   "Verbindung gleich herstellen"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   3840
         Value           =   1  'Aktiviert
         Width           =   3735
      End
      Begin VB.TextBox Text10 
         Height          =   2685
         Left            =   600
         MaxLength       =   1000
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertikal
         TabIndex        =   14
         Top             =   600
         Width           =   3375
      End
      Begin VB.Label Label12 
         BackColor       =   &H80000005&
         Caption         =   "Sonstige Befehle nach dem Login*:"
         ForeColor       =   &H00000000&
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   240
         Width           =   3135
      End
   End
   Begin VB.Line Line2 
      BorderColor     =   &H80000014&
      X1              =   0
      X2              =   418
      Y1              =   294
      Y2              =   294
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000010&
      X1              =   0
      X2              =   417
      Y1              =   293
      Y2              =   293
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Check5_Click()
  If Me.Visible = True Then
    If Check5.Value = 1 Then
      Check3.Value = 1
      Check3.Enabled = False
    Else
      Check3.Value = 1
      Check3.Enabled = True
    End If
  End If
End Sub

Private Sub Command1_Click()
  If Picture4.Visible = True Then
    ShFrame 1
  Else
    ShFrame 0
  End If
End Sub

Private Sub Command2_Click()
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  
  If Picture3.Visible = True Then
    ShFrame 2
  Else
    ShFrame 1

    'Contert URI to Server and Port
    If LCase(Left(Text1.Text, 6)) = "irc://" Then I2 = 6 Else I2 = 0
    If Len(Text1.Text) > I2 Then
      I = InStr(I2 + 1, Text1.Text, "/")
      If I = 0 Then I = Len(Text1.Text) + 1
      
      I3 = InStr(I2 + 1, Text1.Text, ":")
      If I3 > 0 Then
        Text2.Text = Val(Mid(Text1.Text, I3 + 1, I - I3 - 1))
      Else
        If I3 = 0 Then I3 = I
      End If
      Text1.Text = Mid(Text1.Text, I2 + 1, I3 - I2 - 1)
    End If
  End If
End Sub

Private Sub ShFrame(intNr As Integer)
  Select Case intNr
    Case 0
      Command2.Visible = True
      Command4.Visible = False
      Command1.Enabled = False
      Command2.Default = True
      Picture2.Visible = True
      Picture3.Visible = False
      Picture4.Visible = False
    Case 1
      Command2.Visible = True
      Command4.Visible = False
      Command1.Enabled = True
      Command2.Default = True
      Picture2.Visible = False
      Picture3.Visible = True
      Picture4.Visible = False
      Text6.SetFocus
    Case 2
      Command4.Visible = True
      Command1.Enabled = True
      Command4.Default = True
      Command2.Visible = False
      Picture2.Visible = False
      Picture3.Visible = False
      Picture4.Visible = True
      Text10.SetFocus
  End Select
End Sub

Private Sub Command3_Click()
  CloseForm
End Sub

Private Sub CloseForm()
  If InStr(1, LastServers, Text1.Text, vbTextCompare) = 0 Then
    LastServers = Text1.Text + ":" + Text2.Text + LastServers
    If Len(LastServers) > 512 Then LastServers = Left(LastServers, 512)
  End If
  Text3.Text = ""
  Me.Visible = False
End Sub

Private Sub Command4_Click()
  LastUsedUserID = Text7.Text
  LastUsedRealName = Text5.Text
  LastUsedNick = Text6.Text
  Me.Visible = False
End Sub


Private Sub Form_Load()
'  Me.Height = 331 * Screen.TwipsPerPixelY + (Me.Height - Me.ScaleHeight * Screen.TwipsPerPixelY)
'  Me.Width = 417 * Screen.TwipsPerPixelX + (Me.Width - Me.ScaleWidth * Screen.TwipsPerPixelX)
'  Debug.Print Me.Height ' -> 5445
'  Debug.Print Me.Width  ' -> 6345
  
  InitFormFont Me
  
  Picture2.Top = 0
  Picture3.Top = 0
  Picture4.Top = 0
  Picture2.Left = 120
  Picture3.Left = 120
  Picture4.Left = 120
  Command3.Caption = TeVal.butCancel
  Picture1.Picture = LoadResPicture(10, vbResBitmap)
  LoadFormStrs Me
  Check4.Visible = SSLIsAvailable
  ShFrame 0
End Sub

Public Sub LoadList()
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim Frif As Integer
  Dim DAT As String
  
  Do
    I2 = I
    I = InStr(I + 1, LastServers, ",")
    If I = 0 Then I = Len(LastServers) + 1
    DAT = Trim(Mid(LastServers, I2 + 1, I - I2 - 1))
    Text1.Additem DAT
  Loop Until I > Len(LastServers) - 2
  
  On Error Resume Next
  If Len(Dir(AppPath + "servers.ini")) > 0 Then
    Frif = FreeFile
    Open AppPath + "servers.ini" For Input As #Frif
      Do Until EOF(Frif)
        Line Input #Frif, DAT
        If Left(Trim(DAT), 1) <> ";" And Len(Trim(DAT)) > 0 And Left(Trim(DAT), 1) <> "[" Then
          I = InStr(1, DAT, "SERVER:")
          I2 = InStr(I + 7, DAT, ":")
          For I3 = I2 + 1 To Len(DAT)
            If AscB(Mid(DAT, I3, 1)) < 48 Or AscB(Mid(DAT, I3, 1)) > 57 Then Exit For
          Next
          Text1.Additem Mid(DAT, I + 7, I3 - I - 7)
        End If
      Loop
    Close #I
  End If
  On Error GoTo 0
  Text1.Additem Text1.Text, 0
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If UnloadMode = 0 Then
    CloseForm
    Cancel = 1
  End If
End Sub

Private Sub Option1_Click()
  Text3.Enabled = False
End Sub

Private Sub Option2_Click()
  Text3.Enabled = True
End Sub

Private Sub Text1_Click()
  Dim I As Integer
  Dim I2 As Integer
  If Text1.ListIndex = 0 Then Exit Sub
  I = InStr(1, Text1.Text, "(")
  If I = 0 Then I = Len(Text1.Text) + 1
  If LCase(Left(Text1.Text, 7)) = "http://" Then I2 = 7 Else I2 = 1
  I2 = InStr(I2, Text1.Text, ":")
  If I2 > 0 Then
    Text2.Text = Val(Mid(Text1.Text, I2 + 1, I - I2 - 1))
    I = I2
  End If
  If Text1.ListCount > 1 Then If InStr(Text1.List(0), "(") = 0 Then Text1.RemoveItem 0
  Text1.Additem Trim(Left(Text1.Text, I - 1)), 0
  Text1.ListIndex = 0
End Sub

Private Sub Text10_GotFocus()
  Command4.Default = False
End Sub

Private Sub Text10_LostFocus()
  Command4.Default = True
End Sub

