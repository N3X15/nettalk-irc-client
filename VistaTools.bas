Attribute VB_Name = "VistaTools"
Option Explicit

Public Sub InitFormFont(Frm As Form)
  Dim I As Integer
  
  If getVersion >= 2060 Then 'Vista or newer
    On Error Resume Next
    For I = 0 To Frm.Controls.Count - 1
      If Frm.Controls(I).Font.Name = "MS Shell Dlg" Then Frm.Controls(I).Font.Name = "Segoe UI"
      If Frm.Controls(I).Font.Size = 8.25 And Frm Is Form1 Then Frm.Controls(I).Font.Size = 9
    Next
    On Error GoTo 0
  End If
End Sub
