VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "NettalkConnection"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Buffer As String
Public Server As String
Public RealServer As String
Public Port As Long
Public Caption As String
Public State As Integer
Public Nick As String
Public StartCommands As String
Public ListID As Integer
Public Pass As String
Public Room As String
Public CTCKey As String
Public CTCAktiv As Boolean
Public IgnoreList As String
Public Silent As Boolean
Public Temp As Boolean

Private ctcAk As Boolean
Private TimePoint As Long
Private LIndex As Boolean
Private Nline As Boolean
Private ReConn As Integer
Private ConTimeOut As Integer
Private DataWaitMode As Byte
Private ProxyState As Integer
Private ServerIP As String

Private WithEvents wins As CSocket
Attribute wins.VB_VarHelpID = -1
Private ResIP As URLtoIP

Public Sub Connect()
  Dim I As Integer
  Dim I2 As Integer
  If State > 1 Then Exit Sub
  State = 3
  For I = 1 To ChatFrames.Count
    If ChatFrames(I).NetConn Is Me Then
      If ChatFrames(I).FrameType = cfRoom Or ChatFrames(I).FrameType = cfPrivate Then
        ChatFrames(I).PrintText TeVal.ConStart, CoVal.ClientMSG, , True
      ElseIf ChatFrames(I).FrameType = cfStatus Then
        ChatFrames(I).Caption = Me.Caption
        ChatFrames(I).PrintText TeVal.ConStart, CoVal.ClientMSG, , True
      End If
    End If
  Next
  ConnList(ListID).State = 1
  RefreshList
  Form1.ColumRedraw
  If LCase(Left(Server, 7)) = "http://" Then
    Set ResIP = New URLtoIP
    ResIP.ConvertToIP Me, Server
  Else
    RealServer = Server
    FinishConnection
  End If
End Sub

Public Sub FinishConnection()
  DnsResolve Me
End Sub

Public Sub DnsResEvent(IP As String)
  If State = 3 Then
    Set ResIP = Nothing
    ServerIP = IP
    If SockProxyPort > 0 Then
      wins.Connect SockProxyServer, SockProxyPort
      ProxyState = 1
    Else
      wins.Connect IP, Port
      ProxyState = 0
    End If
    TimePoint = AltTimer + 10
    StartTimer Me, 500, 1
  End If
End Sub

Public Function GetSeocket() As CSocket
  Set GetSeocket = wins
End Function

Private Sub Class_Initialize()
  Dim ChFrame As New ChatFrame
  ChFrame.Caption = "Open..."
  ChFrame.Inst cfStatus, 1
  Set ChFrame.NetConn = Me
  ChatFrames.Add ChFrame
  SetFrontFrame ChFrame
  Set wins = New CSocket
End Sub

Public Sub TimerEvent(TimerID As Integer)
  Dim ChatFrs As ChatFrame
  Dim I As Integer
  If TimerID = 1 Then
    If (State <> 3 And State > 1) Or AltTimeDiff(TimePoint) > 0 Then
      For Each ChatFrs In ChatFrames
        If ChatFrs.NetConn Is Me Then
          If ChatFrs.FrameType = cfStatus Or ChatFrs.FrameType = cfPrivate Then
            ChatFrs.PrintText "  " + TeVal.ConErr, CoVal.ErrorMSG, , False
          End If
        End If
      Next
      CloseConnection
      ReConn = RetryTime
      StartTimer Me, 1000, 3
    Else
      StartTimer Me, 500, 1
    End If
  ElseIf TimerID = 2 Then
    DataWaitMode = 0
    WinS_DataArrival
  ElseIf TimerID = 3 Then
    If RetryTime > 0 And State < 2 Then
      If ReConn < 1 Then
        Connect
      End If
      If ReConn > -4 Then
        ReConn = ReConn - 1
        StartTimer Me, 1000, 3
      End If
    End If
  ElseIf TimerID = 4 Then
    If State = 4 Then
      If ConTimeOut > NettalkTimeOut / 20 Then
        For Each ChatFrs In ChatFrames
          If ChatFrs.NetConn Is Me Then
            If ChatFrs.FrameType = cfRoom Or ChatFrs.FrameType = cfPrivate Or ChatFrs.FrameType = cfStatus Then
              DrawSLine ChatFrs, 2, "- " + TeVal.TimeOut, CoVal.ErrorMSG
            End If
          End If
        Next
        CloseConnection
        Connect
      Else
        StartTimer Me, 20000, 4
        ConTimeOut = ConTimeOut + 1
        If ConTimeOut Mod 2 = 0 Then
          SendLine "X"
        End If
      End If
    End If
  End If
End Sub

Private Sub Class_Terminate()
  If State > 1 Then CloseConnection
  KillTimerByID 1, Me
  KillTimerByID 2, Me
  KillTimerByID 3, Me
  KillTimerByID 4, Me
  If Not ResIP Is Nothing Then ResIP.CanceldRes
End Sub

Private Sub wins_Closed()
  CloseConnection
  If ReConn > -5 Then
    StartTimer Me, 3000, 3
    ReConn = RetryTime
  Else
    StartTimer Me, 1000, 3
    ReConn = 0
  End If
End Sub

Public Sub CloseConnection()
  Dim ChatFrs As ChatFrame
  Dim I As Integer
  Dim I2 As Integer
  Dim DAT As String
  KillTimerByID 1, Me
  If Not ResIP Is Nothing Then ResIP.CanceldRes
  If wins.State > 1 Then
    SendLine "Mail" & Chr(13) & Chr(10) & "all" & Chr(13) & Chr(10) & Nick & Chr(13) & Chr(10) & "/Quit"
    wins.Disconnect
  End If
  For Each ChatFrs In ChatFrames
    If ChatFrs.NetConn Is Me Then
      If ChatFrs.FrameType = cfRoom Or ChatFrs.FrameType = cfPrivate Then
        ChatFrs.PrintText TeVal.ConClose, CoVal.ClientMSG, , True
      End If
      If ChatFrs.FrameType = cfStatus Then
        ChatFrs.UListF.Clear
        If ChatFrs Is FrontCFrame Then DrawTitel
      End If
    End If
  Next
  
  I = -1
  Do
    I2 = I
    I = InStr(I + 2, StartCommands, vbNewLine)
    If I = 0 Then I = Len(StartCommands) + 1
    If Mid(StartCommands, I2 + 2, 1) <> "$" And I - I2 > 2 Then
      DAT = DAT + Mid(StartCommands, I2 + 2, I - I2 - 2) + vbNewLine
    End If
  Loop Until I = Len(StartCommands) + 1
  If DAT = vbNewLine Then DAT = ""
  StartCommands = DAT
  ConnList(ListID).Commands = StartCommands
  ConnList(ListID).State = 3
  ConnList(ListID).Nick = Nick
  ConnList(ListID).Name = Room
  ConnList(ListID).Pass = Pass
  ConnList(ListID).CTCKey = CTCKey
  ConnList(ListID).CTCOn = Abs(CInt(CTCAktiv))
  ConnList(ListID).IgnoreList = IgnoreList
  ConnList(ListID).Silent = CInt(Silent)
  RefreshList
  State = 1
  If FrontCFrame.NetConn Is Me Then Form1.pStateIntRefresh
  Form1.ColumRedraw
  DisconnectMsg
  SetFList "", Server, 0, 1
End Sub

Public Sub Quit()
  Dim I As Integer
  KillTimerByID 2, Me
  KillTimerByID 3, Me
  CloseConnection
  For I = ChatFrames.Count To 1 Step -1
    If ChatFrames(I).NetConn Is Me Then
      ChatFrames(I).QuitFrame
    End If
  Next
  For I = NETconns.Count To 1 Step -1
    If NETconns(I) Is Me Then
      NETconns.Remove I
    End If
  Next
  ConnList(ListID).State = 0
  If ConnList(ListID).Temp Then
    If MsgBox(TeVal.ConRemove, vbYesNo + vbQuestion, ConnList(ListID).Caption) = vbNo Then
      Form1.RemoveServer ListID
    Else
      ConnList(ListID).Temp = 0
    End If
  End If
  RefreshList
End Sub

Public Function SendComm(Data As String, ChatFrs As ChatFrame, Optional scIndex As Integer = -1, Optional mAn As String = "ALLE") As Boolean
  Dim sDAT(0 To 9) As String
  Dim I As Long
  Dim I2 As Integer
  Dim Iind As Integer
  Dim OutStr As String
  Dim OutStr2 As String
  Dim LIndex As Integer
  Dim Sendable As Boolean
  
  If Left(Data, 1) = "/" Then Data = Mid(Data, 2)
  Do
    OutStr = CharReplace(Data, ChatFrs, wins, scIndex, LIndex)
    If Len(OutStr) Then
      I = 0
      Iind = 0
      Do
        I2 = I
        I = InStr(I + 1, OutStr, " ")
        If I = 0 Or Iind > 8 Then Exit Do
        sDAT(Iind) = Mid(OutStr, I2 + 1, I - I2 - 1)
        Iind = Iind + 1
      Loop
      sDAT(Iind) = Mid(OutStr, I2 + 1)
      Select Case LCase(sDAT(0))
        Case "query"
          ChatFrs.PrivateFrame sDAT(1)
          If sDAT(2) <> "" Then OutStr2 = "PRIVATE " + sDAT(1) + " " + Mid(OutStr, Len(sDAT(0)) + Len(sDAT(1)) + 3)
        Case "dcc"
          If sDAT(1) <> "" Then
            If sDAT(2) <> "" Then
              OutStr2 = Mid(OutStr, Len(sDAT(0) + sDAT(1) + sDAT(2)) + 4)
              If UCase(sDAT(2)) = "SEND" Then
                On Error Resume Next
                If Dir(OutStr2) = "" Then
                  On Error GoTo 0
                  DrawSLine ChatFrs, 2, "- " + TeVal.dccFileNotFound, CoVal.ErrorMSG
                Else
                  On Error GoTo 0
                  OpenDCC Nick, sDAT(1), OutStr2, sDAT(2), wins, 1, ChatFrs
                End If
              Else
                OpenDCC Nick, sDAT(1), OutStr2, sDAT(2), wins, 1, ChatFrs
              End If
            Else
              OpenDCC Nick, sDAT(1), "", "CHAT", wins, 1
            End If
            OutStr2 = ""
          End If
        Case "reg"
          OutStr2 = "REG " + sDAT(1)
          Pass = Trim(sDAT(1))
        Case "rereg"
          OutStr2 = "REREG " + sDAT(1) + " " + sDAT(2)
          If Pass = Trim(sDAT(1)) Then Pass = Trim(sDAT(2))
        Case "connect"
          If State < 2 Then Connect
        Case "disconnect"
          If State > 1 Then CloseConnection
        Case "sendtext"
          SendLine "Mail" & Chr(13) & Chr(10) & "all" & Chr(13) & Chr(10) & Nick & Chr(13) & Chr(10) & _
          Mid(OutStr, Len(sDAT(0)) + 2)
        Case "notice"
          SendLine "Mail" & Chr(13) & Chr(10) & sDAT(1) & Chr(13) & Chr(10) & Nick & Chr(13) & Chr(10) & _
          Mid(OutStr, Len(sDAT(0)) + Len(sDAT(1)) + 3)
        Case "private"
          DrawSLine ChatFrs, 0, "<PRIVATE:" + Nick + ">", CoVal.ServerMSG, Mid(OutStr, Len(sDAT(0)) + Len(sDAT(1)) + 3)
          OutStr2 = OutStr
        Case "watch"
          ShowFriedsList
          WatchNewUser sDAT(1), Me.Server, 1
        Case "coding"
          CTCAktiv = CTCAktiv = False
          DrawSLine ChatFrs, 2, "- " + TeVal.fFunction + " " + IIf(CTCAktiv, TeVal.fOn, TeVal.fOff), CoVal.ClientMSG
          Form1.pStateIntRefresh
        Case "setcode"
          CTCKey = sDAT(1)
        Case "ignore"
          If Len(IgnoreList) = 0 Then IgnoreList = " "
          UnIgnore IgnoreList, sDAT(1)
          IgnoreList = IgnoreList + sDAT(1) + "@" + CStr(Date) + " "
          DrawSLine ChatFrs, 2, "- " + sDAT(1) + " " + TeVal.Ignore, CoVal.ClientMSG
        Case "unignore"
          If UnIgnore(IgnoreList, sDAT(1)) Then
            DrawSLine ChatFrs, 2, "- " + sDAT(1) + " " + TeVal.UnIgnore, CoVal.ClientMSG
          End If
        Case Else
          If CCommands(ChatFrs, sDAT, OutStr, OutStr2) = False Then
            OutStr2 = OutStr
          End If
      End Select
      '########################## Send #############################
      If OutStr2 = "" Then
        Sendable = True
      Else
        If State = 4 Then
          SendLine "Mail" & Chr(13) & Chr(10) & _
          mAn & Chr(13) & Chr(10) & Nick & Chr(13) & Chr(10) & _
          "/" + OutStr2
          Sendable = True
        End If
      End If
    End If
    LIndex = LIndex + 1
  Loop While LIndex
  SendComm = Sendable
End Function

Public Sub SendLine(Data As String)
  wins.SendData Data + Chr(27)
  SendedBytes = SendedBytes + Len(Data) + 1
  If (FrontCFrame.FrameType = cfServerList Or FrontCFrame.FrameType = cfScript Or FrontCFrame.FrameType = cfDCC) And _
  Form1.Visible = True And Form1.WindowState <> 1 Then Form1.pStateRefresh
End Sub

Private Sub NTPrint(DAT As String, ChatFrs As ChatFrame, Keyed As Boolean)
  Dim I As Long
  Dim I2 As Long
  Dim I3 As Long
  Dim I4 As Long
  Dim I5 As Long
  Dim TColor As Long
  Dim Uline As Boolean
  Dim Trigger As Long
  Dim ShowTime As Boolean
  
  If Keyed Then ChatFrs.TextF.SetLineIcon 65
  I = -1
  Do
    I2 = I + 2
    I = InStr(I2, DAT, Chr(13))
    If I = 0 Then I = Len(DAT) + 2
    If LIndex = True Then Nline = True: LIndex = False
    Uline = False
    Trigger = 0
    If I > Len(DAT) Then
      LIndex = True
      TColor = CoVal.NormalText
    Else
      Select Case Mid(DAT, I + 1, 1)
        Case Chr(10)
          If I + 1 < Len(DAT) Then LIndex = True
          TColor = CoVal.NormalText
        Case "N"
          TColor = CoVal.ServerMSG
          ShowTime = ShowTimeStampComm
        Case "M"
          TColor = CoVal.NormalVon
          Trigger = 3
          I5 = InStr(I2, DAT, "<")
          I3 = InStr(I5 + 1, DAT, ">")
          I4 = InStr(I5 + 1, DAT, " ")
          If I4 > I3 Or I4 = 0 Then I4 = I3
          SetAlPriv ChatFrs, Mid(DAT, I5 + 1, I4 - I5 - 1), Mid(DAT, I3 + 3), Nick
          I2 = I5
          ShowTime = ShowTimeStampText
        Case "F"
          TColor = CoVal.FlusterVon
          Trigger = 3
          I5 = InStr(I2, DAT, "<")
          I3 = InStr(I5 + 1, DAT, ">")
          I4 = InStr(I5 + 1, DAT, " ")
          If I4 > I3 Or I4 = 0 Then I4 = I3
          If I4 > 0 Then
            If Mid(DAT, I4, 2) <> "  " Then
              SetAl ChatFrs, 4, Mid(DAT, I5 + 1, I4 - I5 - 1), Mid(DAT, I3 + 3)
            End If
          End If
          I2 = I5
          ShowTime = ShowTimeStampText
        Case "X"
          TColor = CoVal.NormalText
        Case "E"
          TColor = CoVal.ErrorMSG
          ShowTime = ShowTimeStampComm
        Case "P"
          TColor = CoVal.Notice
          ShowTime = ShowTimeStampComm
        Case "U"
          TColor = CoVal.Link
        Case "G"
          TColor = CoVal.Link
          Trigger = 1
        Case Else
          TColor = CoVal.NormalText
          Trigger = 1
      End Select
    End If
    If I2 > 0 Then
      If ShowTime = True Then
        ChatFrs.PrintText "(" & Left(Time, 5) & ")", CoVal.TimeStamp, , Nline
        ChatFrs.PrintText Mid(DAT, I2, I - I2), TColor, , False, Trigger, Uline
      Else
        ChatFrs.PrintText Mid(DAT, I2, I - I2), TColor, , Nline, Trigger, Uline
      End If
    End If
    
    Nline = False
    ShowTime = False
  Loop Until I = Len(DAT) + 2
End Sub

Private Sub PraseCommand(Data As String)
  Dim ChatFrs As ChatFrame
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim PrivMSG As Integer
  Dim Room As Integer
  Dim DAT As String
  Dim DecodedText As String
   
  If ntScript_Call("GetData", Data, CStr(Me.ListID), "NET") = False Then Exit Sub
   
  For Each ChatFrs In ChatFrames
    If ChatFrs.NetConn Is Me Then
      
      'MSG
      If Left(Data, 1) = " " Or Left(Data, 1) = "M" Then
        
        'Get Local Rooter IP
        If Left(Data, 15) = " - your IP is: " And Right(Data, 2) = Chr(13) + "N" Then
          wins.LocalRooterIP = ConvIPtoDouble(Mid(Data, 16, Len(Data) - 17), True)
        End If
        I = InStr(1, Data, "<") + 1
        If I > 1 Then I2 = InStr(I, Data, ">")
        
        DecodedText = DecodeText(Mid(Data, I2 + 3), ctcAk, CTCKey)
        If Mid(Data, I2 + 3, 10) = "\\--!>DCC " Then
          If ChatFrs.FrameType = cfStatus Then
            I3 = InStr(I, Data, " ")
            DAT = Mid(Data, I2 + 9)
            If Right(DAT, 2) = vbNewLine Then DAT = Left(DAT, Len(DAT) - 2)
            If Mid(Data, I, I3 - I) <> Nick Then
              If IsIgnored(IgnoreList, Mid(Data, I, I3 - I)) Then Exit For
              DccConnRequest Mid(Data, I, I3 - I), DAT + " ", ChatFrs, Nick
            End If
          End If
        ElseIf UCase(Mid(Data, I, 8)) = "PRIVATE:" Then
          If I2 > I + 8 Then
            If IsIgnored(IgnoreList, Mid(Data, I + 8, I2 - I - 8)) Then Exit For
            If PrivMSG = 0 Then
              If ntScript_Call("GetText", DecodedText, Mid(Data, I + 8, I2 - I - 8), Me.ListID, "PRIVATE", "NET") = False Then
                PrivMSG = 1
                Exit For
              Else
                PrivMSG = 3
              End If
            End If
            If UCase(Mid(Data, I + 8, I2 - I - 8)) <> UCase(Me.Nick) Then
              If UCase(ChatFrs.Caption) = UCase(Mid(Data, I + 8, I2 - I - 8)) And ChatFrs.FrameType = cfPrivate Then
                DrawSLine ChatFrs, 0, "<" + Mid(Data, I + 8, I2 - I - 8) + ">", CoVal.NormalVon, DecodedText, 5, , , ctcAk
                If ChatFrs Is FrontCFrame Then ChatFrs.TextF.RefText
                SetAl ChatFrs, 4, Mid(Data, I + 8, I2 - I - 8), DecodedText
                PrivMSG = 1
              Else
                If PrivMSG = 0 Or PrivMSG = 3 Then PrivMSG = 2
              End If
            End If
          End If
        Else
          If UCase(Mid(Data, I, 4)) = "MSG:" And ChatFrs.FrameType = cfStatus Then
            If I2 > I + 4 Then
              If IsIgnored(IgnoreList, Mid(Data, I + 4, I2 - I - 4)) Then Exit For
              SetAl ChatFrs, 4, Mid(Data, I + 4, I2 - I - 4), DecodedText
            End If
          End If
          I3 = InStr(I, Data, " ")
          If I3 = 0 Then I3 = I2
          If I2 > 1 Then
            If IsIgnored(IgnoreList, Mid(Data, I, I3 - I)) Then Exit For
            If ntScript_Call("GetText", DecodedText, Mid(Data, I, I3 - I), Me.ListID, IIf(I3 = I2, "PUBLIC", "NOTICE"), "NET") = True Then
              ChatFrs.LastWisperer = Mid(Data, I, I3 - I)
              If ChatFrs.FrameType = cfStatus Then
                NTPrint Mid(Data, 2, I2 + 1) + DecodedText, ChatFrs, ctcAk
                NewTextAdded ChatFrs
              End If
            End If
          Else
            If ChatFrs.FrameType = cfStatus Then NTPrint Mid(Data, 2, I2 + 1) + DecodedText, ChatFrs, ctcAk
          End If
        End If
      End If

      'UserListe
      If Left(Data, 1) = "L" Then
        If ChatFrs.FrameType = cfStatus Then
          SetFList "", Server, 2, 1
          DAT = ChatFrs.UListF.GetListText(ChatFrs.UListF.ListIndex, 0)
          I3 = ChatFrs.UListF.ListCount
          ChatFrs.UListF.Clear
          I = 2
          Do
            I2 = I
            I = InStr(I + 2, Data, Chr(13) + Chr(10)) + 2
            If I = 2 Then Exit Do
            ChatFrs.UListF.Add Trim(Mid(Data, I2, I - I2 - 4 + 2)), , False, True
            If SetFList(ChatNoSign(Trim(Mid(Data, I2, I - I2 - 4 + 2))), Server, 1, 1, True) And I3 Then
              SetAl ChatFrs, 4, ChatNoSign(Trim(Mid(Data, I2, I - I2 - 4 + 2))), _
              ChatNoSign(Trim(Mid(Data, I2, I - I2 - 4 + 2))) + " " + TeVal.FrindOnline
              DrawSLine ChatFrs, 2, "- ", CoVal.ClientMSG, ChatNoSign(Trim(Mid(Data, I2, I - I2 - 4 + 2))), 5, , " " + TeVal.FrindOnline
            End If
          Loop
          If Len(DAT) > 0 Then ChatFrs.UListF.GotoItem DAT
          ChatFrs.UListF.Refresh
          If ChatFrs Is FrontCFrame Then DrawTitel
        End If
      End If
      
      'Join
      If Left(Data, 1) = "J" Then
        If ChatFrs.FrameType = cfStatus Then
          Me.Room = Mid(Data, 2)
        End If
      End If
      
      'Nickchange
      If Left(Data, 1) = "C" Then
        If ChatFrs.FrameType = cfStatus Then
          If InStr(1, Data, " ") + InStr(1, Data, Chr(13)) = 0 Then
            Nick = Mid(Data, 2)
            LastUsedNick = Nick
            Form1.pStateIntRefresh
          End If
        End If
      End If
      
      'RoomListe
      If Left(Data, 1) = "R" Then
        If ChatFrs.FrameType = cfRoomList Then
          ChatFrs.UListF.Clear
          I = 2
          Do
            I2 = I
            I = InStr(I + 2, Data, Chr(13) + Chr(10))
            If I = 0 Then Exit Do
            ChatFrs.UListF.Add Mid(Data, I2 + 2, I - I2 - 2)
          Loop
          For I = 1 To ChatFrames.Count
            If ChatFrames(I).FrameType = cfRoomList And ChatFrames(I).NetConn Is Me Then
              SetFrontFrame ChatFrames(I)
              If ChatFrs Is FrontCFrame Then DrawTitel
              Exit For
            End If
          Next
          Room = 1
        Else
          If Room = 0 Then Room = 2
        End If
      End If
    
      'Ping
      If Left(Data, 1) = "P" Then
        If ChatFrs.FrameType = cfStatus Then
          I = InStr(1, Data, Chr(13) + Chr(10))
          If I = 0 Then I = Len(Data) + 1
          SendLine "Ping" & Chr(13) & Chr(10) & Chr(13) & Chr(10) & Mid(Data, 2, I - 2) & Chr(13) & Chr(10) & Mid(Data, I + 2, Len(Data) - I + 1)
        End If
      End If

      'GetVersion
      If Left(Data, 1) = "V" Then
        If ChatFrs.FrameType = cfStatus Then
          SendLine "Version" & Chr(13) & Chr(10) & App.Major & "." & App.Minor & "." & App.Revision & Chr(13) & Chr(10) & Right(Data, Len(Data) - 1) & Chr(13) & Chr(10) & Chr(13) & Chr(10)
        End If
      End If
    
      'Update
      If Left(Data, 1) = "U" Then
        If ChatFrs.FrameType = cfStatus Then
          I = InStr(1, Data, Chr(13) + Chr(10))
          If I = 0 Then I = Len(Data) + 1
          If cVersions(Mid(Data, 2, I - 2), App.Major & "." & App.Minor & "." & App.Revision) Then
            ChatFrs.PrintText "- " + TeVal.AutoUpdate1 + " (" + Mid(Data, 2, I - 2) + ") ", CoVal.ServerMSG, True, True
            ChatFrs.PrintText "[" + TeVal.AutoUpdate2 + "]", CoVal.Link, False, False, 8
          End If
        End If
      End If
    
    End If
  Next
  
  If PrivMSG = 2 Then
    DecodedText = DecodeText(Mid(Data, I2 + 3), ctcAk, CTCKey)
    Set ChatFrs = NewNetFrame(Me, Mid(Data, I + 8, I2 - I - 8), 4, 1, False)
    DrawSLine ChatFrs, 0, "<" + Mid(Data, I + 8, I2 - I - 8) + ">", CoVal.NormalVon, DecodedText, 5, , , CTCAktiv
    SetAl ChatFrs, 4, Mid(Data, I + 8, I2 - I - 8), DecodedText
  End If
  If Room = 2 Then
    Set ChatFrs = NewNetFrame(Me, TeVal.lsRooms, 2, 1, True)
    ChatFrs.UListF.SetColum 0, TeVal.ls2Name, 500
    ChatFrs.UListF.Clear
    I = 2
    Do
      I2 = I
      I = InStr(I + 2, Data, Chr(13) + Chr(10))
      If I = 0 Then Exit Do
      ChatFrs.UListF.Add Mid(Data, I2 + 2, I - I2 - 2)
    Loop
    DrawTitel
  End If
End Sub

Private Sub wins_Connected()
  Dim I As Integer
  Dim I2 As Integer
  Dim ConIndex As Integer
  Dim ChatFrs As ChatFrame
  
  If SockProxyPort > 0 Then SendProxyLogin wins, ServerIP, Port
  State = 4
  For I = 1 To ChatFrames.Count
    If ChatFrames(I).NetConn Is Me Then
      If ChatFrames(I).FrameType = cfStatus Or ChatFrames(I).FrameType = cfPrivate Or ChatFrames(I).FrameType = cfStatus Then
        ChatFrames(I).PrintText "  " + TeVal.ConOK, CoVal.ClientMSG, , False
        ConIndex = I
      End If
      If ChatFrames(I) Is FrontCFrame Then
        Form1.pStateIntRefresh
        If ChatFrs Is FrontCFrame Then DrawTitel
      End If
    End If
  Next
  KillTimerByID 1, Me
  LIndex = True
  SendLine "Name" & Chr(13) & Chr(10) & Nick & Chr(13) & Chr(10) & Pass & Chr(13) & Chr(10) & Room

  If UseSecIP = True Then
    SendComm "/myip", ChatFrames(ConIndex)
  End If
  
  I = -1
  Do
    I2 = I
    I = InStr(I + 2, StartCommands, vbNewLine)
    If I = 0 Then I = Len(StartCommands) + 1
    If Mid(StartCommands, I2 + 2, 1) = "/" Or Mid(StartCommands, I2 + 2, 1) = "$" Then I2 = I2 + 1
    If I - I2 > 2 Then
      SendComm CReplace("", Mid(StartCommands, I2 + 2, I - I2 - 2), ChatFrames(ConIndex), wins), ChatFrames(ConIndex)
    End If
  Loop Until I = Len(StartCommands) + 1
  ConnList(ListID).State = 2
  RefreshList
  Form1.ColumRedraw
  ConTimeOut = 0
  StartTimer Me, 20000, 4
  If TrayIconState > 0 Then
    T.hIcon = Form1.TrayIcon(0).Picture
    Shell_NotifyIcon NIM_MODIFY, T
    TrayIconState = 0
  End If
End Sub

Private Sub WinS_DataArrival()
  Dim iStart As Long
  Dim iLen As Long
  Dim BinData() As Byte

  ConTimeOut = 0
  
  If DataWaitMode = 0 Then
    iStart = wins.Recive(BinData)
    If iStart = 0 Then
      DataWaitMode = 0
    Else
      DataWaitMode = 1
      RecevedBytes = RecevedBytes + iStart
      Buffer = Buffer + StrConv(BinData, vbUnicode)
      If ProxyState = 1 Then
        Buffer = Mid(Buffer, 9)
        ProxyState = 2
      End If
      iStart = 0
      Do
        iLen = iStart
        iStart = InStr(iStart + 1, Buffer, Chr(27))
        If iStart > 0 Then
          PraseCommand Mid(Buffer, iLen + 1, iStart - iLen - 1)
          If Len(Buffer) < iStart - 1 Then Exit Do
        Else
          Buffer = Right(Buffer, Len(Buffer) - iLen)
          Exit Do
        End If
      Loop
      If FrontCFrame.FrameType = cfServerList And Form1.Visible = True Then Form1.pStateRefresh
      StartTimer Me, 1, 2
    End If
  End If
End Sub


Private Sub wins_Error(Error As Long, Description As String)
  Dim I As Integer
  For I = 1 To ChatFrames.Count
    If ChatFrames(I).NetConn Is Me Then
      If ChatFrames(I).FrameType = cfPrivate Or ChatFrames(I).FrameType = cfStatus Then
        If State = 3 Then
          ChatFrames(I).PrintText "  " + TeVal.ConErr, CoVal.ErrorMSG, , False
        End If
      End If
    End If
  Next
  If State > 1 Then
    CloseConnection
    ReConn = RetryTime
    StartTimer Me, 1000, 3
  End If
End Sub
