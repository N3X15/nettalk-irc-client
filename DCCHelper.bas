Attribute VB_Name = "DCCHelper"
Option Explicit

Public DCCFileConns As New Collection
Public DCCFileFrame As ChatFrame
Public LastScrRef As Single
Public SelledFC As Integer
Public ValWindVisible As Boolean

Public Function ExDccConnExisting(xdccCommand As String, FileName As String, Server As String, Port As Long, Room As String) As Boolean
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  
  For I = 1 To DCCFileConns.Count
    If DCCFileConns(I).SendGetMode > 0 And DCCFileConns(I).State = 1 Then
      I2 = 0
      If DCCFileConns(I).RequCommand = xdccCommand Then I2 = I2 + 5
      If DCCFileConns(I).NoteServer = LCase(Server) Then I2 = I2 + 8
      'If DCCFileConns(I).NotePort = Port Then I2 = I2 + 1
      If DCCFileConns(I).NoteRoom = Room Then I2 = I2 + 4
      If CompSChars(DCCFileConns(I).FileName, FileName) > 30 Then I2 = I2 + 4
      If DCCFileConns(I).FileSize = DCCFileConns(I).ByteCount Then I2 = 0
      If I2 > I3 Then I3 = I2
    End If
  Next
  ExDccConnExisting = I3 > 12
End Function

Public Function DccConnExisting(User As String, FileName As String, PeerIP As String, Port As Long, FileSize As Currency, NoteServer As String, NoteRoom As String, hIndex As Integer) As Integer
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  
  For I = 1 To DCCFileConns.Count
    If DCCFileConns(I).SendGetMode > 0 And DCCFileConns(I).State = 1 And DCCFileConns(I).ByteCount < DCCFileConns(I).FileSize Then
      I2 = 0
      If DCCFileConns(I).ByteCount Then
        If DCCFileConns(I).PeerIP = PeerIP Then I2 = I2 + 1
        If DCCFileConns(I).User = User Then I2 = I2 + 2
        If LCase(DCCFileConns(I).SenderFN) = LCase(FileName) Then I2 = I2 + 5
        If DCCFileConns(I).FileSize = FileSize Then I2 = I2 + 10
        If DCCFileConns(I).FileSize = DCCFileConns(I).ByteCount Then I2 = 0
      Else
        If InStr(1, DCCFileConns(I).RequCommand, User, vbTextCompare) > 0 Then I2 = I2 + 4
        If DCCFileConns(I).NoteServer = NoteServer Then I2 = I2 + 5
        If ChatNoSignGlobal(DCCFileConns(I).NoteRoom) = ChatNoSignGlobal(NoteRoom) Then I2 = I2 + 5
        If CompSChars(DCCFileConns(I).FileName, FileName) > 60 Then I2 = I2 + 5
      End If
      If I2 > I3 Then I3 = I2: hIndex = I
    End If
  Next
  DccConnExisting = I3
End Function

Public Function CompSChars(Text1 As String, Text2 As String) As Integer
  Dim I As Integer
  Dim I2 As Integer
  
  If Len(Text1) = 0 Then Exit Function
  For I = 1 To Len(Text1)
    If Mid(Text1, I, 1) = Mid(Text2, I, 1) Then I2 = I2 + 1
  Next
  CompSChars = Int(I2 * 100 / Len(Text1))
End Function

Public Function CheckTList(NameStr As String) As String
  Dim I As Integer
  Dim I2 As Integer
  Dim AddZ As Integer
  Dim ConnName As String
  ConnName = NameStr
  Do
    For I = 1 To DCCFileConns.Count
      If UCase(ConnName) = UCase(DCCFileConns(I).FileName) Then Exit For
    Next
    If I > DCCFileConns.Count Then Exit Do
    For I = Len(ConnName) - 1 To Len(ConnName) - 3 Step -1
      If I < 1 Then Exit For
      If Mid(ConnName, I, 1) <> CStr(Val(Mid(ConnName, I, 1))) Then Exit For
    Next
    AddZ = Val(Mid(ConnName, I + 1)) + 1
    If Mid(ConnName, I, 1) = "(" Then
      ConnName = Left(ConnName, I) + CStr(AddZ) + ")"
    Else
      If Right(ConnName, 1) <> ")" Then I = I + 1
      ConnName = Left(ConnName, I) + "  (" + CStr(AddZ) + ")"
    End If
  Loop
  CheckTList = ConnName
End Function

Public Sub AddDCCFileList(cModul As FileTransfer)
  Dim I As Integer
  
  If DCCFileFrame Is Nothing Then Exit Sub
  I = DCCFileFrame.UListF.Add(cModul.FileName, 14, False)
  DCCFileFrame.UListF.SetListText I, 4, FormatBytes(cModul.FileSize)
  DCCFileFrame.UListF.SetListText I, 5, cModul.User
  DCCFileFrame.UListF.SetListText I, 6, cModul.DestPath
  RefrDCCFileList cModul, True
End Sub

Public Sub ChangeDCCFileName(cModul As FileTransfer, NewFileName As String)
  Dim I As Integer
  
  If DCCFileFrame Is Nothing Then Exit Sub
  For I = 0 To DCCFileFrame.UListF.ListCount - 1
    If DCCFileFrame.UListF.List(I, 0) = cModul.FileName Then
      DCCFileFrame.UListF.SetListText I, 0, NewFileName, True
      DCCFileFrame.UListF.SetListText I, 4, FormatBytes(cModul.FileSize)
      DCCFileFrame.UListF.SetListText I, 5, cModul.User
      DCCFileFrame.UListF.SetListText I, 6, cModul.DestPath
      cModul.FileName = NewFileName
      RefrDCCFileList cModul, True
    End If
  Next
End Sub

Public Sub RemDCCFileList(cModul As FileTransfer)
  Dim I As Integer
  If DCCFileFrame Is Nothing Then Exit Sub
  For I = 0 To DCCFileFrame.UListF.ListCount - 1
    If DCCFileFrame.UListF.List(I, 0) = cModul.FileName Then
      DCCFileFrame.UListF.Remove I
      Exit For
    End If
  Next
End Sub

Public Sub RefrDCCFileList(cModul As FileTransfer, Optional LastRef As Boolean)
  Dim I As Integer
  Dim I2 As Integer
  Dim J As Integer
  If Not cModul Is Nothing Then cModul.DataChanged = True
  If AltTimeDiff(LastScrRef) > 0.5 Or LastRef = True Then
    For J = 1 To DCCFileConns.Count
      If DCCFileConns(J).DataChanged = True Or cModul Is Nothing Then
        DCCFileConns(J).DataChanged = False
        For I = 0 To DCCFileFrame.UListF.ListCount - 1
          If DCCFileFrame.UListF.List(I, 0) = DCCFileConns(J).FileName Then
            I2 = -1
            Select Case DCCFileConns(J).State
              Case 4
                If DCCFileConns(J).SendGetMode = 1 Then
                  DCCFileFrame.UListF.SetListText I, -1, 10, True
                Else
                  DCCFileFrame.UListF.SetListText I, -1, 11, True
                  DCCFileFrame.UListF.SetListText I, 5, DCCFileConns(J).User, True
                End If
                I2 = 1
              Case 3, 2
                DCCFileFrame.UListF.SetListText I, -1, 13, True
              Case Else
                If DCCFileConns(J).FileSize = DCCFileConns(J).ByteCount Then
                  DCCFileFrame.UListF.SetListText I, -1, 12, True
                Else
                  DCCFileFrame.UListF.SetListText I, -1, 14, True
                End If
            End Select
            If DCCFileConns(J).FileSize > 0 Then
              DCCFileFrame.UListF.SetListText I, 1, "%" + Format(DCCFileConns(J).ByteCount / DCCFileConns(J).FileSize * I2, "000.0%"), True
            End If
            DCCFileFrame.UListF.SetListText I, 2, Int(DCCFileConns(J).Speed + 0.999), True
            If DCCFileConns(J).Speed > 0.01 Then
              DCCFileFrame.UListF.SetListText I, 3, FormatSec((DCCFileConns(J).FileSize - DCCFileConns(J).ByteCount) / 1024 / DCCFileConns(J).Speed), True
            Else
              DCCFileFrame.UListF.SetListText I, 3, "", True
            End If
            If DCCFileFrame Is FrontCFrame And Form1.Visible = True And Form1.WindowState <> 1 Then
              DCCFileFrame.UListF.Refresh , , , , 10 + I
            End If
            Exit For
          End If
        Next
      End If
    Next
    If (FrontCFrame.FrameType = cfServerList Or FrontCFrame.FrameType = cfScript Or FrontCFrame.FrameType = cfDCC) _
    And Form1.Visible = True And Form1.WindowState <> 1 Then Form1.pStateRefresh
    If ValWindVisible = True Then Form7.FormRefresh
    LastScrRef = AltTimer
  End If
End Sub

Public Sub ShowDCCFrame(Optional SetToFront As Boolean)
  Dim I As Integer
  Dim CFrame As ChatFrame
  If DCCFileFrame Is Nothing Then
    Set CFrame = New ChatFrame
    
    CFrame.Caption = TeVal.DccCaption
    CFrame.Inst cfDCC, -1
    
    CFrame.UListF.SetColum 0, TeVal.ls3FileName, 210
    CFrame.UListF.AddColum TeVal.ls3Progres, 150
    CFrame.UListF.AddColum TeVal.ls3Speed, 35
    CFrame.UListF.AddColum TeVal.ls3Time, 80
    CFrame.UListF.AddColum TeVal.ls3Size, 60
    CFrame.UListF.AddColum TeVal.ls3User, 160
    CFrame.UListF.AddColum TeVal.ls3Path
    
    ChatFrames.Add CFrame
    If SetToFront Then
      SetFrontFrame CFrame
    Else
      Form1.pMain_Resize
    End If
    Set DCCFileFrame = CFrame
  Else
    If DCCFileFrame Is FrontCFrame Then Exit Sub
    If SetToFront = False Then
      Form1.pMain_Resize
    Else
      For I = 1 To ChatFrames.Count
        If ChatFrames(I).FrameType = cfDCC Then
          SetFrontFrame ChatFrames(I)
          Exit For
        End If
      Next
    End If
  End If
End Sub

Public Sub ShowDCCTMenue(ListIndex As Integer, Optional DblClick As Boolean)
  Dim I As Integer
  If DCCFileFrame Is Nothing Then Exit Sub
  Form1.menH6brake.Enabled = False
  Form1.menH6del.Enabled = False
  Form1.menH6alldel.Enabled = True
  Form1.menH6disconn.Enabled = False
  Form1.menH6retry.Enabled = False
  Form1.menH6values.Enabled = True
  Form1.menH6open.Enabled = False
  Form1.menH6opendir.Enabled = True
  
  For I = 1 To DCCFileConns.Count
    If DCCFileFrame.UListF.List(ListIndex, 0) = DCCFileConns(I).FileName Then
      Exit For
    End If
  Next
  If I <= DCCFileConns.Count Then
    SelledFC = I
    If DblClick = True Then
      Form7.Show 1
      Exit Sub
    End If
    Form1.menH6brake.Checked = DCCFileConns(I).SlowMode
    If DCCFileConns(I).ByteCount = DCCFileConns(I).FileSize Then Form1.menH6open.Enabled = True
    If DCCFileConns(I).SendGetMode = 0 Then
      If DCCFileConns(I).State = 4 Then
        Form1.menH6disconn.Enabled = True
        Form1.menH6brake.Enabled = True
      ElseIf DCCFileConns(I).State = 2 Then
        Form1.menH6disconn.Enabled = True
      ElseIf DCCFileConns(I).State = 1 Then
        If DCCFileConns(I).ByteCount < DCCFileConns(I).FileSize Then Form1.menH6retry.Enabled = True
        Form1.menH6del.Enabled = True
      End If
    Else
      If DCCFileConns(I).State = 4 Then
        Form1.menH6disconn.Enabled = True
        Form1.menH6brake.Enabled = True
      ElseIf DCCFileConns(I).State = 3 Then
        Form1.menH6disconn.Enabled = True
      ElseIf DCCFileConns(I).State = 1 Then
        If DCCFileConns(I).ByteCount < DCCFileConns(I).FileSize Then
          If Len(DCCFileConns(I).RequCommand) Then Form1.menH6retry.Enabled = True
        End If
        Form1.menH6del.Enabled = True
      End If
    End If
    Form1.PopupMenuEx Form1.menH6, , , , Form1.menH6values
    If Form1.Visible = True Then Form1.Text1.SetFocus
  End If
End Sub

Public Sub SaveDCCValues()
  Dim OFi As Integer
  Dim I As Integer
  Dim DAT As String
  
  On Error Resume Next
  If DCCFileConns.Count > 0 Or Len(Dir(UserPath + "DCCValues.ini")) Then
    OFi = FreeFile
    Open UserPath + "DCCValues.ini" For Output As #OFi
      For I = 1 To DCCFileConns.Count
        'If Terminate = True Then DCCFileConns(I).CloseConnection
        If DCCFileConns(I).ByteCount <> DCCFileConns(I).FileSize _
        And (DCCFileConns(I).ByteCount > 0 Or DCCFileConns(I).PeerPort = 0) Then
          Print #OFi, ">" + DCCFileConns(I).FileName
          Print #OFi, "Filename=" + DCCFileConns(I).SenderFN
          Print #OFi, "DestPath=" + DCCFileConns(I).DestPath
          Print #OFi, "User=" + DCCFileConns(I).User
          Print #OFi, "FileSize=" + Format(DCCFileConns(I).FileSize, "0")
          Print #OFi, "ByteCount=" + Format(DCCFileConns(I).ByteCount, "0")
          Print #OFi, "PaketSize=" + CStr(DCCFileConns(I).PaketSize)
          Print #OFi, "PeerIP=" + DCCFileConns(I).PeerIP
          Print #OFi, "PeerPort=" + CStr(DCCFileConns(I).PeerPort)
          Print #OFi, "SendGetMode=" + CStr(DCCFileConns(I).SendGetMode)
          Print #OFi, "MaxSpeedUp=" + CStr(DCCFileConns(I).MaxSpeedUp)
          Print #OFi, "LocalPort=" + CStr(DCCFileConns(I).LocalPort)
          Print #OFi, "SlowMode=" + CStr(CInt(DCCFileConns(I).SlowMode))
          Print #OFi, "LocalIP=" + Format(DCCFileConns(I).LocalIP, "0")
          Print #OFi, "NoteServer=" + DCCFileConns(I).NoteServer
          Print #OFi, "NotePort=" + CStr(DCCFileConns(I).NotePort)
          Print #OFi, "NoteCaption=" + DCCFileConns(I).NoteCaption
          Print #OFi, "NoteCType=" + DCCFileConns(I).NoteCType
          Print #OFi, "NoteRoom=" + DCCFileConns(I).NoteRoom
          Print #OFi, "SenderFN=" + DCCFileConns(I).SenderFN
          Print #OFi, "RequCommand=" + DCCFileConns(I).RequCommand
          Print #OFi, ""
        End If
      Next
    Close #OFi
    If Err Then MsgBox Err.Description + " (DCC values)", vbCritical
  End If
  On Error GoTo 0
End Sub

Public Sub LoadDCCValues()
  Dim OFi As Integer
  Dim InpData As String
  Dim I As Integer
  Dim FTrans As FileTransfer
  Dim Path As String
  
  OFi = FreeFile
  Path = UserPath + "DCCValues.ini"
  On Error Resume Next
  If Dir(Path) <> "" Then
    Open Path For Input As #OFi
      Do Until EOF(OFi) = True
        Line Input #OFi, InpData
        If Left(InpData, 1) = ">" Then
          If Not FTrans Is Nothing Then
            If Len(FTrans.SenderFN) = 0 Then FTrans.SenderFN = FTrans.FileName
            AddDCCFileList FTrans
          End If
          Set FTrans = New FileTransfer
          ShowDCCFrame
          DCCFileConns.Add FTrans
          FTrans.FileName = CheckTList(Right(InpData, Len(InpData) - 1))
        End If
        I = InStr(InpData, "=")
        If I > 0 And Not FTrans Is Nothing Then
          Select Case LCase(Left(InpData, I - 1))
            Case "filename"
              FTrans.SenderFN = Trim(Right(InpData, Len(InpData) - I))
            Case "destpath"
              FTrans.DestPath = Trim(Right(InpData, Len(InpData) - I))
            Case "user"
              FTrans.User = Trim(Right(InpData, Len(InpData) - I))
            Case "filesize"
              FTrans.FileSize = Val(Right(InpData, Len(InpData) - I))
            Case "bytecount"
              FTrans.ByteCount = Val(Right(InpData, Len(InpData) - I))
            Case "paketsize"
              FTrans.PaketSize = Val(Right(InpData, Len(InpData) - I))
            Case "peerip"
              FTrans.PeerIP = Trim(Right(InpData, Len(InpData) - I))
            Case "peerport"
              FTrans.PeerPort = Val(Right(InpData, Len(InpData) - I))
            Case "sendgetmode"
              FTrans.SendGetMode = Val(Right(InpData, Len(InpData) - I))
              If FTrans.SendGetMode > 0 Then
                On Error Resume Next
                If Len(Dir(FTrans.DestPath + ".dcc")) = 0 Then
                  FTrans.ByteCount = 0
                Else
                  FTrans.ByteCount = extGetFileSize(FTrans.DestPath + ".dcc")
                End If
                'If FTrans.ByteCount > 0 Then FTrans.ByteCount = FTrans.ByteCount
                On Error GoTo 0
              End If
            Case "maxspeedup"
              FTrans.MaxSpeedUp = Val(Right(InpData, Len(InpData) - I))
            Case "localport"
              FTrans.LocalPort = Val(Right(InpData, Len(InpData) - I))
            Case "slowmode"
              FTrans.SlowMode = CBool(Val(Right(InpData, Len(InpData) - I)))
            Case "localip"
              FTrans.LocalIP = Val(Right(InpData, Len(InpData) - I))
            Case "noteserver"
              FTrans.NoteServer = Trim(Right(InpData, Len(InpData) - I))
            Case "noteport"
              FTrans.NotePort = Val(Right(InpData, Len(InpData) - I))
            Case "notecaption"
              FTrans.NoteCType = Trim(Right(InpData, Len(InpData) - I))
            Case "notectype"
              FTrans.NoteCType = Trim(Right(InpData, Len(InpData) - I))
            Case "noteroom"
              FTrans.NoteRoom = Trim(Right(InpData, Len(InpData) - I))
            Case "senderfn"
              FTrans.SenderFN = Trim(Right(InpData, Len(InpData) - I))
            Case "requcommand"
              FTrans.RequCommand = Trim(Right(InpData, Len(InpData) - I))
          End Select
        End If
      Loop
    Close #OFi
    If Not FTrans Is Nothing Then
      AddDCCFileList FTrans
      If Len(FTrans.SenderFN) = 0 Then FTrans.SenderFN = FTrans.FileName
    End If
  End If
  If Err <> 0 Then MsgBox Err.Description + " (DCC values)", vbCritical
  On Error GoTo 0
End Sub



