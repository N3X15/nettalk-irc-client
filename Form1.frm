VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Nettalk"
   ClientHeight    =   7500
   ClientLeft      =   165
   ClientTop       =   855
   ClientWidth     =   10710
   FillColor       =   &H80000014&
   BeginProperty Font 
      Name            =   "MS Shell Dlg"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Form1.frx":0000
   KeyPreview      =   -1  'True
   ScaleHeight     =   500
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   714
   StartUpPosition =   3  'Windows-Standard
   Begin Nettalk.UniList comList 
      Height          =   870
      Left            =   3840
      TabIndex        =   22
      TabStop         =   0   'False
      Top             =   4800
      Visible         =   0   'False
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   1535
   End
   Begin Nettalk.UniList txtList 
      Height          =   1530
      Left            =   480
      TabIndex        =   21
      TabStop         =   0   'False
      Top             =   4080
      Visible         =   0   'False
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   2699
   End
   Begin VB.PictureBox BgPicture 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00000000&
      BorderStyle     =   0  'Kein
      Height          =   615
      Left            =   6360
      ScaleHeight     =   41
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   41
      TabIndex        =   18
      TabStop         =   0   'False
      Top             =   720
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox DataPort 
      BorderStyle     =   0  'Kein
      Height          =   270
      Left            =   8040
      LinkTimeout     =   100
      MaxLength       =   1024
      TabIndex        =   17
      TabStop         =   0   'False
      Top             =   5160
      Visible         =   0   'False
      Width           =   2655
   End
   Begin VB.PictureBox IconStaw 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BackColor       =   &H00000000&
      BorderStyle     =   0  'Kein
      Height          =   960
      Left            =   360
      ScaleHeight     =   64
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   320
      TabIndex        =   13
      TabStop         =   0   'False
      Top             =   720
      Visible         =   0   'False
      Width           =   4800
   End
   Begin VB.PictureBox UserInfoPic 
      Appearance      =   0  '2D
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000018&
      BorderStyle     =   0  'Kein
      ForeColor       =   &H80000013&
      Height          =   255
      Left            =   0
      ScaleHeight     =   17
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   201
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   3015
   End
   Begin VB.PictureBox pDiff2 
      BorderStyle     =   0  'Kein
      Height          =   75
      Left            =   360
      MousePointer    =   7  'Gr��en�nderung N S
      ScaleHeight     =   5
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   661
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   6720
      Width           =   9915
   End
   Begin VB.PictureBox pTexthg 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'Kein
      Height          =   1215
      Left            =   0
      ScaleHeight     =   81
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   713
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   5880
      Width           =   10695
      Begin Nettalk.UniEdit Text1 
         Height          =   255
         Left            =   1080
         TabIndex        =   0
         Top             =   240
         Width           =   375
         _ExtentX        =   661
         _ExtentY        =   450
      End
      Begin VB.CheckBox Check1 
         Height          =   195
         Left            =   7920
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   90
         Width           =   195
      End
   End
   Begin VB.PictureBox pState 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'Kein
      Height          =   300
      Left            =   0
      ScaleHeight     =   20
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   721
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   7320
      Width           =   10815
   End
   Begin VB.PictureBox pMain 
      BackColor       =   &H8000000C&
      BorderStyle     =   0  'Kein
      FillColor       =   &H8000000D&
      Height          =   4455
      Left            =   0
      ScaleHeight     =   297
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   713
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   435
      Width           =   10695
      Begin VB.PictureBox pTapStrip 
         AutoRedraw      =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   0  'Kein
         DragIcon        =   "Form1.frx":11D05
         FillStyle       =   0  'Ausgef�llt
         Height          =   600
         Left            =   6300
         ScaleHeight     =   40
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   207
         TabIndex        =   20
         TabStop         =   0   'False
         Top             =   3990
         Width           =   3105
      End
      Begin VB.PictureBox pText 
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'Kein
         Height          =   1815
         Left            =   360
         ScaleHeight     =   121
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   505
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   1440
         Visible         =   0   'False
         Width           =   7575
         Begin VB.ComboBox Combo1 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Left            =   1200
            Sorted          =   -1  'True
            Style           =   2  'Dropdown-Liste
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   60
            Width           =   4815
         End
         Begin VB.TextBox Text2 
            BorderStyle     =   0  'Kein
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   975
            Left            =   120
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Beides
            TabIndex        =   15
            TabStop         =   0   'False
            Top             =   720
            Width           =   3975
         End
         Begin VB.Label Label1 
            BackStyle       =   0  'Transparent
            Caption         =   "Label1"
            Height          =   255
            Left            =   120
            TabIndex        =   19
            Top             =   120
            Width           =   1095
         End
      End
      Begin VB.PictureBox pDiff 
         BorderStyle     =   0  'Kein
         Height          =   3015
         Left            =   9120
         MousePointer    =   9  'Gr��en�nderung W O
         ScaleHeight     =   201
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   5
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   720
         Width           =   75
      End
      Begin VB.PictureBox pUList 
         BackColor       =   &H80000005&
         BorderStyle     =   0  'Kein
         Height          =   3135
         Left            =   8520
         ScaleHeight     =   3135
         ScaleWidth      =   2055
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   150
         Width           =   2055
         Begin VB.VScrollBar UlistScr 
            Height          =   2055
            Left            =   1560
            TabIndex        =   10
            TabStop         =   0   'False
            Top             =   240
            Width           =   255
         End
      End
      Begin VB.PictureBox pTBox 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'Kein
         FillColor       =   &H80000010&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3615
         Left            =   120
         MouseIcon       =   "Form1.frx":12147
         ScaleHeight     =   241
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   553
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   120
         Visible         =   0   'False
         Width           =   8295
         Begin VB.VScrollBar VScroll1 
            Height          =   2775
            Left            =   7920
            TabIndex        =   5
            TabStop         =   0   'False
            Top             =   240
            Width           =   255
         End
         Begin VB.PictureBox pScriptNotify 
            AutoRedraw      =   -1  'True
            BackColor       =   &H80000005&
            BorderStyle     =   0  'Kein
            DragIcon        =   "Form1.frx":12299
            FillStyle       =   0  'Ausgef�llt
            Height          =   300
            Left            =   4680
            ScaleHeight     =   20
            ScaleMode       =   3  'Pixel
            ScaleWidth      =   207
            TabIndex        =   23
            TabStop         =   0   'False
            Top             =   840
            Visible         =   0   'False
            Width           =   3105
         End
      End
   End
   Begin VB.PictureBox pMenu 
      BackColor       =   &H00C0C0C0&
      BorderStyle     =   0  'Kein
      ForeColor       =   &H80000011&
      Height          =   420
      Left            =   0
      ScaleHeight     =   28
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   713
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   0
      Width           =   10695
   End
   Begin VB.Menu mDatei 
      Caption         =   "&Datei"
      Begin VB.Menu menSaveText 
         Caption         =   "Text &Speichern unter..."
      End
      Begin VB.Menu menExConns 
         Caption         =   "Verbindungen &exportieren..."
      End
      Begin VB.Menu menImConns 
         Caption         =   "Verbindungen &imporieren..."
      End
      Begin VB.Menu menStrich1 
         Caption         =   "-"
      End
      Begin VB.Menu menOEdit 
         Caption         =   "Einstellungen"
      End
      Begin VB.Menu menStrich2 
         Caption         =   "-"
      End
      Begin VB.Menu menQuit 
         Caption         =   "&Beenden"
         Shortcut        =   ^Q
      End
   End
   Begin VB.Menu mBearbeiten 
      Caption         =   "&Bearbeiten"
      Begin VB.Menu menBcut 
         Caption         =   "Ausschneiden"
         Shortcut        =   ^X
      End
      Begin VB.Menu menBcopy 
         Caption         =   "Kopieren"
         Shortcut        =   ^C
      End
      Begin VB.Menu menBpast 
         Caption         =   "Einf�gen"
         Shortcut        =   ^V
      End
      Begin VB.Menu menBdel 
         Caption         =   "L�schen"
         Shortcut        =   {DEL}
      End
      Begin VB.Menu menBstrich1 
         Caption         =   "-"
      End
      Begin VB.Menu menBsearch 
         Caption         =   "Suchen..."
         Shortcut        =   ^F
      End
   End
   Begin VB.Menu mVerbindungen 
      Caption         =   "&Verbindungen"
      Begin VB.Menu mVnewIRC 
         Caption         =   "Neue IRC-Verbindng..."
      End
      Begin VB.Menu mVnewtpc 
         Caption         =   "TCP/IP-Verbindung aufbauen..."
      End
   End
   Begin VB.Menu menScript 
      Caption         =   "S&cript"
      Begin VB.Menu menSEdit 
         Caption         =   "Bearbeiten"
      End
      Begin VB.Menu menSStrich1 
         Caption         =   "-"
      End
      Begin VB.Menu menSStart 
         Caption         =   "Script neustarten"
      End
      Begin VB.Menu menSStop 
         Caption         =   "Script anhalten"
      End
      Begin VB.Menu menSStrich2 
         Caption         =   "-"
      End
      Begin VB.Menu menSImport 
         Caption         =   "Importieren"
      End
      Begin VB.Menu menSExport 
         Caption         =   "Expotieren"
      End
   End
   Begin VB.Menu menLangw 
      Caption         =   "S&prache"
      Begin VB.Menu menLListe 
         Caption         =   "#"
         Index           =   0
      End
   End
   Begin VB.Menu menHilfe 
      Caption         =   "&?"
      Begin VB.Menu menHilfeHilfe 
         Caption         =   "&Hilfe"
      End
      Begin VB.Menu menHilfeUpdate 
         Caption         =   "&Updates..."
      End
      Begin VB.Menu menHilfeStrich1 
         Caption         =   "-"
      End
      Begin VB.Menu menHilfeInfo 
         Caption         =   "Inf&o"
      End
   End
   Begin VB.Menu menH 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu menHtrennen 
         Caption         =   "Trennen"
      End
      Begin VB.Menu menHverbinden 
         Caption         =   "Verbinden"
      End
      Begin VB.Menu menHstrich1 
         Caption         =   "-"
      End
      Begin VB.Menu menHneu 
         Caption         =   "Neu..."
      End
      Begin VB.Menu menHcopy 
         Caption         =   "Kopie erstellen"
      End
      Begin VB.Menu menHedit 
         Caption         =   "Bearbeiten"
      End
      Begin VB.Menu menHdel 
         Caption         =   "L�schen"
      End
      Begin VB.Menu menHStrich2 
         Caption         =   "-"
      End
      Begin VB.Menu menHprop 
         Caption         =   "Eigenschaften"
      End
      Begin VB.Menu menHStrich4 
         Caption         =   "-"
      End
      Begin VB.Menu menHlog 
         Caption         =   "Log anzeigen"
      End
      Begin VB.Menu menHpublicnotice 
         Caption         =   "Nachrichten einblenden"
      End
      Begin VB.Menu menHsilent 
         Caption         =   "Inaktiv"
      End
      Begin VB.Menu menHStrich3 
         Caption         =   "-"
      End
      Begin VB.Menu menHclose 
         Caption         =   "Schlie�en"
      End
   End
   Begin VB.Menu menH2 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu menH2copy 
         Caption         =   "Kopieren"
      End
      Begin VB.Menu menH2copytime 
         Caption         =   "Kopieren mit Uhrzeit"
      End
      Begin VB.Menu menH2past 
         Caption         =   "Makierten Text einf�gen"
      End
      Begin VB.Menu menH2strich2 
         Caption         =   "-"
      End
      Begin VB.Menu menH2prop 
         Caption         =   "Eigenschaften"
      End
      Begin VB.Menu menH2strich 
         Caption         =   "-"
      End
      Begin VB.Menu menH2shortcut 
         Caption         =   "#"
         Index           =   0
      End
      Begin VB.Menu menH2Sub 
         Caption         =   "Weitere Funktionen"
         Begin VB.Menu menH2Subsc 
            Caption         =   "#"
            Index           =   0
         End
      End
      Begin VB.Menu menH2strich3 
         Caption         =   "-"
      End
      Begin VB.Menu menH2scroll 
         Caption         =   "Runter scrollen"
      End
      Begin VB.Menu menH2edit 
         Caption         =   "Text �ffnen"
      End
      Begin VB.Menu menH2show 
         Caption         =   "Text im Browser anzeigen"
      End
   End
   Begin VB.Menu menH3 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu menH3private 
         Caption         =   "Privat-Chat"
      End
      Begin VB.Menu menH3wisper 
         Caption         =   "Fl�stern"
      End
      Begin VB.Menu menH3strich1 
         Caption         =   "-"
      End
      Begin VB.Menu menH3shortcut 
         Caption         =   "#"
         Index           =   0
      End
      Begin VB.Menu menH3Sub 
         Caption         =   "Weitere Funktionen"
         Begin VB.Menu menH3Subsc 
            Caption         =   "#"
            Index           =   0
         End
      End
      Begin VB.Menu menH3strich2 
         Caption         =   "-"
      End
      Begin VB.Menu menH3dccc 
         Caption         =   "DCC Privat-Chat"
      End
      Begin VB.Menu menH3file 
         Caption         =   "DCC Datei senden..."
      End
      Begin VB.Menu menH3filepast 
         Caption         =   "DCC Bild senden"
      End
      Begin VB.Menu menH3strich3 
         Caption         =   "-"
      End
      Begin VB.Menu menH3copyname 
         Caption         =   "Name kopieren"
      End
   End
   Begin VB.Menu menH4 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu menH4open 
         Caption         =   "Fenster &�ffnen"
      End
      Begin VB.Menu menH4strich1 
         Caption         =   "-"
      End
      Begin VB.Menu menH4ibalken 
         Caption         =   "&Infobalken aktivieren"
         Checked         =   -1  'True
      End
      Begin VB.Menu menH4oncreeen 
         Caption         =   "&OSD aktivieren"
         Checked         =   -1  'True
      End
      Begin VB.Menu menH4stumm 
         Caption         =   "&Stumm"
      End
      Begin VB.Menu menH4strich2 
         Caption         =   "-"
      End
      Begin VB.Menu menH4options 
         Caption         =   "Einstellungen"
      End
      Begin VB.Menu menH4script 
         Caption         =   "Script bearbeiten"
      End
      Begin VB.Menu menH4strich3 
         Caption         =   "-"
      End
      Begin VB.Menu menH4quit 
         Caption         =   "&Beenden"
      End
   End
   Begin VB.Menu menH5 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu menH5list 
         Caption         =   "#"
         Index           =   0
      End
   End
   Begin VB.Menu menH6 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu menH6retry 
         Caption         =   "Fortsetzen"
      End
      Begin VB.Menu menH6disconn 
         Caption         =   "Abbrechen"
      End
      Begin VB.Menu menH6brake 
         Caption         =   "Slowmode"
      End
      Begin VB.Menu menH6strich1 
         Caption         =   "-"
      End
      Begin VB.Menu menH6open 
         Caption         =   "�ffnen"
      End
      Begin VB.Menu menH6opendir 
         Caption         =   "Ordner �ffnen"
      End
      Begin VB.Menu menH6alldel 
         Caption         =   "Alle getrennten entfernen"
      End
      Begin VB.Menu menH6del 
         Caption         =   "Entfernen"
      End
      Begin VB.Menu menH6strich2 
         Caption         =   "-"
      End
      Begin VB.Menu menH6values 
         Caption         =   "Eigenschaften"
      End
   End
   Begin VB.Menu menH7 
      Caption         =   ""
      Visible         =   0   'False
      Begin VB.Menu menH7private 
         Caption         =   "Privat-Chat"
      End
      Begin VB.Menu menH7refresh 
         Caption         =   "Aktualisieren"
      End
      Begin VB.Menu menH7strich1 
         Caption         =   "-"
      End
      Begin VB.Menu menH7del 
         Caption         =   "L�schen"
      End
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Copyright 2009 Nicolas Kruse
'
'   Licensed under the Apache License, Version 2.0 (the "License");
'   you may not use this file except in compliance with the License.
'   You may obtain a copy of the License at
'
'       http://www.apache.org/licenses/LICENSE-2.0
'
'   Unless required by applicable law or agreed to in writing, software
'   distributed under the License is distributed on an "AS IS" BASIS,
'   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
'   See the License for the specific language governing permissions and
'

Option Explicit

Const TsrHeight As Integer = 24

Public UInfoMode As Integer
Public LastTrigger As String
Public TextFeld As Object
Public TrayIcon As New Collection

Private PopUpTablist As Boolean
Private KeyOffFlag As Boolean
Private ScriptIn As Long
Private ComListMode As Integer
Private LastVisIndex As Integer
Private FirstVisIndex As Integer
Private LastWState As Integer
Private TOutSt(5) As String
Private ColClose As WinButton
Private SpCount As Integer
Private CoursorMode As Integer
Private TryBlink As Integer
Private LiteUpLM As Boolean
Private StateRef As Boolean
Private LastTextLen As Long
Private DragPos As Long
Private StartDrag As Integer
Private RealDrag As Long
Private LastColorTextPos As Integer
Private ListedType As String
Private UseHelpIndex As Boolean

Private WithEvents MenuBar As plusMenu
Attribute MenuBar.VB_VarHelpID = -1

Public Sub StartFlash(CFrame As ChatFrame)
  If Me.WindowState <> 1 And Me.Visible = True Then
    StartTimer Me, 500, 1
    CFrame.ntBlink = 2
    LiteUpLM = True
    pStateRefresh
  End If
End Sub

Public Sub StartTrayFlash()
  StartTimer Me, 500, 2
  TryBlink = 2
End Sub

Public Sub StopFlash(CFrame As ChatFrame)
  Dim I As Integer
  Dim I2 As Integer
  
  If CFrame.ntBlink Mod 2 = 1 Then
    CFrame.ntBlink = 0
    ColumRedraw True
  Else
    CFrame.ntBlink = 0
  End If
  For I = 1 To ChatFrames.Count
    If ChatFrames(I).ntBlink > 0 Then I2 = I2 + 1
  Next
  If I2 = 0 Then KillTimerByID 1, Me
  LiteUpLM = False
End Sub

Public Sub TimerEvent(TimerID As Integer)
  Dim I As Integer
  Dim I2 As Integer
  
  If TimerID = 1 Then
    For I = 1 To ChatFrames.Count
      If ChatFrames(I).ntBlink > 0 Then
        If ChatFrames(I).ntBlink < 40 Then
          ChatFrames(I).ntBlink = ChatFrames(I).ntBlink + 1
          ColumRedraw True
          I2 = I2 + 1
        Else
          StopFlash ChatFrames(I)
          pStateRefresh
        End If
      End If
    Next
    If I2 > 0 Then StartTimer Me, 500, 1
  ElseIf TimerID = 2 Then
    If TrayIconState > 3 Then
      TryBlink = TryBlink + 1
      If TryBlink Mod 2 = 0 Then
        T.hIcon = TrayIcon(1)
      Else
        T.hIcon = TrayIcon(TrayIconState + 1)
      End If
      Shell_NotifyIcon NIM_MODIFY, T
      If TryBlink < 39 Then StartTimer Me, 500, 2
    End If
  ElseIf TimerID = 3 Then
    StateRef = False
    pStateIntRefresh
  ElseIf TimerID = 4 Then
    LoadFromLog FrontCFrame.Caption, -1, 0, LogDateTrigger
  ElseIf TimerID > 9 Then
    ntScript_TimerEvent TimerID - 10
  End If
End Sub

Public Sub StartScriptTimer(Interval As Long, ID As Integer)
  StartTimer Me, Interval, ID + 10
End Sub

Public Sub StopScriptTimer(ID As Integer)
  KillTimerByID ID + 10, Me
End Sub

Public Sub MoMenue()
  Dim FrTyp  As Integer
  Dim CoTyp  As Integer
  
  FrTyp = FrontCFrame.FrameType
  CoTyp = FrontCFrame.ConnTyp
  If MenuBar Is Nothing Then Exit Sub
  MenuBar.SetEnable 2, False
  MenuBar.SetEnable 3, False
  MenuBar.SetEnable 7, False
  MenuBar.SetEnable 8, False
  MenuBar.SetEnable 9, False
  MenuBar.SetEnable 14, False
  MenuBar.ButtonVisible 18, False
  MenuBar.ButtonVisible 19, False
   
  If CoTyp = 0 Then
    MenuBar.SetEnable 3, True
    If FrontCFrame.IrcConn.State < 2 Then
      MenuBar.SetEnable 7, True
    ElseIf FrontCFrame.IrcConn.State = 4 Then
      MenuBar.SetEnable 8, True
      MenuBar.SetEnable 9, True
      MenuBar.SetEnable 14, True
    Else
      MenuBar.SetEnable 8, True
    End If
  End If
    
  If CoTyp = 2 Then
    If FrontCFrame.DCCConn.State < 2 Then
      MenuBar.SetEnable 7, True
    Else
      MenuBar.SetEnable 8, True
    End If
  End If
  
  If CoTyp = -1 Then
    If FrTyp = 3 Then
      If FrontCFrame.UListF.ListIndex >= 0 Then
        MenuBar.SetEnable 2, True
        MenuBar.SetEnable 3, True
      End If
    ElseIf FrTyp = 5 Then
      MenuBar.ButtonVisible 18, True
      MenuBar.ButtonVisible 19, True
      If ntScriptEnabled = True Then
        MenuBar.SetEnable 19, True
      Else
        MenuBar.SetEnable 19, False
      End If
      MenuBar.SetEnable 12, True
    End If
  End If
  
  MenuBar.Refresh
End Sub

Public Sub LoadScriptFile(Optional AddFilePath As String)
  Dim I As Integer
  Dim DAT As String
  
  If Len(AddFilePath) Then
    DAT = AddFilePath
    ntScript = ntScript + vbNewLine + vbNewLine
  Else
    DAT = UserPath + "script.txt"
    ntScript = ""
  End If
  
  On Error Resume Next
  I = FreeFile
  Open DAT For Binary As #I
    DAT = Space(LOF(I))
    Get I, 1, DAT
  Close #I
  On Error GoTo 0
  ntScript = ntScript + DAT
  For I = 1 To ChatFrames.Count
    If ChatFrames(I).FrameType = cfScript Then
      SetScriptText ntScript
      Exit For
    End If
  Next
End Sub

Public Sub SetScriptText(Text As String)
  SendMessage Text2.hWnd, WM_SETTEXT, Len(Text), Text
End Sub

Public Sub SetScriptSelection(Start As Long, SelLen As Long)
  SendMessageA Text2.hWnd, EM_SETSEL, Start, Start + SelLen
  SendMessageA Text2.hWnd, EM_SCROLLCARET, 0&, 0&
End Sub

Public Function GetScriptSelStart()
  Dim sStart As Long
  SendMessageA Text2.hWnd, EM_GETSEL, VarPtr(sStart), 0&
  GetScriptSelStart = sStart
End Function

Public Sub SaveScriptFile()
  Dim I As Integer
  On Error Resume Next
  I = FreeFile
  If Len(Dir(UserPath + "script.txt")) > 0 Then Kill UserPath + "script.txt"
  Open UserPath + "script.txt" For Binary As #I
    Put I, 1, ntScript
  Close #I
  If Err <> 0 Then MsgBox Err.Description, vbCritical
  On Error GoTo 0
End Sub

Public Sub SaveMainValues(Optional PlUnload As Boolean = True)
  Dim lWP As WINDOWPLACEMENT
  Dim OFi As Integer
  Dim I As Integer
  
  lWP.Length = Len(lWP)
  
  On Error Resume Next
  OFi = FreeFile
  Open UserPath + "Nettalk.ini" For Output As #OFi
    Print #OFi, "lastusedrealname = " + EncodeUTF8(LastUsedRealName)
    Print #OFi, "lastusednick = " + EncodeUTF8(LastUsedNick)
    Print #OFi, "lastuseduserid = " + EncodeUTF8(LastUsedUserID)
    Print #OFi, "osd = " + CStr(CInt(UModeOSD))
    Print #OFi, "textpopup = " + CStr(CInt(UModeBalken))
    Print #OFi, "hideformwithmin = " + CStr(CInt(HideFormWithMin))
    Print #OFi, "noquitwithx = " + CStr(CInt(NoQuitWithX))
    Print #OFi, "autorejoin = " + CStr(CInt(AutoReJoin))
    Print #OFi, "retrytime = " + CStr(CInt(RetryTime))
    Print #OFi, "OpFrameIndex = " + CStr(CInt(OpFrameIndex))
    Print #OFi, "logmodeprivate = " + CStr(CInt(LogmodePrivate))
    Print #OFi, "logmoderoom = " + CStr(CInt(LogmodeRoom))
    Print #OFi, "PrivLogMaxSize = " + CStr(PrivLogMaxSize)
    Print #OFi, "PublLogMaxSize = " + CStr(PublLogMaxSize)
    Print #OFi, "timestamptext = " + CStr(CInt(ShowTimeStampText))
    Print #OFi, "retrytime = " + CStr(CInt(RetryTime))
    Print #OFi, "playbeep = " + CStr(CInt(PlayBeep))
    Print #OFi, "playwav = " + CStr(CInt(PlayWav))
    Print #OFi, "wavepath = " + WavePath
    Print #OFi, "showprivtext = " + CStr(CInt(EaOSDPriv))
    Print #OFi, "showroomtext = " + CStr(CInt(EaOSDRoom))
    Print #OFi, "remaindrooms = " + CStr(CInt(RemaindRooms))
    Print #OFi, "remaindconns = " + CStr(CInt(RemaindConns))
    Print #OFi, "remaindlists = " + CStr(CInt(RemaindList))
    Print #OFi, "windowvisible = " + CStr(CInt(Form1.Visible))
    Print #OFi, "windowstate = " + CStr(Form1.WindowState)
    Print #OFi, "lastwstate = " + CStr(LastWState)
    If CBool(GetWindowPlacement(Form1.hWnd, lWP)) Then
      Print #OFi, "windowtop = " + CStr(lWP.rcNormalPosition.Top * Screen.TwipsPerPixelY)
      Print #OFi, "windowleft = " + CStr(lWP.rcNormalPosition.Left * Screen.TwipsPerPixelX)
      Print #OFi, "windowheight = " + CStr((lWP.rcNormalPosition.Bottom - lWP.rcNormalPosition.Top) * Screen.TwipsPerPixelY)
      Print #OFi, "windowwidth = " + CStr((lWP.rcNormalPosition.Right - lWP.rcNormalPosition.Left) * Screen.TwipsPerPixelX)
    End If
    Print #OFi, "TextboxHeight = " + CStr(pTexthg.Height)
    Print #OFi, "UserlistWidth = " + CStr(RListW)
    Print #OFi, "bColumBusy = " + CStr(CoVal.bColumBusy)
    Print #OFi, "menvisible = " + CStr(CInt(Form1.mDatei.Visible))
    Print #OFi, "beeprooms = " + EncodeUTF8(OsdRoomList)
    Print #OFi, "stumm = " + CStr(CInt(SoundStumm))
    Print #OFi, "ClientMSG = " + CStr(CoVal.ClientMSG)
    Print #OFi, "ErrorMSG = " + CStr(CoVal.ErrorMSG)
    Print #OFi, "FlusterVon = " + CStr(CoVal.FlusterVon)
    Print #OFi, "Link = " + CStr(CoVal.Link)
    Print #OFi, "NormalText = " + CStr(CoVal.NormalText)
    Print #OFi, "NormalVon = " + CStr(CoVal.NormalVon)
    Print #OFi, "Notice = " + CStr(CoVal.Notice)
    Print #OFi, "ServerMSG = " + CStr(CoVal.ServerMSG)
    Print #OFi, "UserMSG = " + CStr(CoVal.UserMSG)
    Print #OFi, "bColumDisc = " + CStr(CoVal.bColumDisc)
    Print #OFi, "bColumBusy = " + CStr(CoVal.bColumBusy)
    Print #OFi, "KonBackColor = " + CStr(Form1.pTBox.BackColor)
    Print #OFi, "UListForeColor = " + CStr(Form1.pUList.ForeColor)
    Print #OFi, "UListBackColor = " + CStr(Form1.pUList.BackColor)
    Print #OFi, "TextForeColor = " + CStr(Form1.Text1.ForeColor)
    Print #OFi, "TextBackColor = " + CStr(Form1.Text1.BackColor)
    Print #OFi, "TimeStamp = " + CStr(CoVal.TimeStamp)
    Print #OFi, "OwnNick = " + CStr(CoVal.OwnNick)
    Print #OFi, "ButtonShadow = " + CStr(CoVal.ButtonShadow)
    Print #OFi, "Light3D = " + CStr(CoVal.Light3D)
    Print #OFi, "BackColor = " + CStr(CoVal.BackColor)
    Print #OFi, "MenuText = " + CStr(CoVal.MenuText)
    Print #OFi, "GrayText = " + CStr(CoVal.GrayText)
    Print #OFi, "Shadow3D = " + CStr(CoVal.Shadow3D)
    Print #OFi, "Highlight3D = " + CStr(CoVal.Highlight3D)
    Print #OFi, "ButtonText = " + CStr(CoVal.ButtonText)
    Print #OFi, "LastslPath = " + LastSPath
    Print #OFi, "LastOpenPath = " + LastLPath
    Print #OFi, "LastServers = " + LastServers
    Print #OFi, "ScriptEnabled = " + CStr(CInt(ntScriptEnabled))
    Print #OFi, "PicturePath = " + HGBildPath
    Print #OFi, "PictureMode = " + CStr(HGDrawMode)
    Print #OFi, "FontName = " + FontTypeName
    Print #OFi, "FontSize = " + CStr(FontTypeSize)
    Print #OFi, "FontBold = " + CStr(CInt(FontTypeBold))
    Print #OFi, "StaticDCCPort = " + CStr(StaticDCCPort)
    Print #OFi, "DCCServerPort = " + CStr(DCCServerPort)
    Print #OFi, "IRCTimeOut = " + CStr(IRCTimeOut)
    Print #OFi, "playbeep2 = " + CStr(CInt(PlayBeep2))
    Print #OFi, "playwav2 = " + CStr(CInt(PlayWav2))
    Print #OFi, "wavepath2 = " + WavePath2
    Print #OFi, "showprivtext2 = " + CStr(CInt(EaOSDPriv2))
    Print #OFi, "UseSecIP = " + CStr(CInt(UseSecIP))
    Print #OFi, "DccMaxUpSpeed = " + CStr(dccMaxUpSpeed)
    Print #OFi, "DccPaketSize = " + CStr(dccPaketSize)
    Print #OFi, "DccAutoAcc = " + CStr(CInt(dccAutoAcc))
    Print #OFi, "BeepArr = " + BeepArr
    Print #OFi, "DCCPath = " + DCCPath
    Print #OFi, "SockProxyServer = " + CStr(SockProxyServerGlob)
    Print #OFi, "SockProxyPort = " + CStr(SockProxyPortGlob)
    Print #OFi, "SockProxyType = " + CStr(SockProxyTypeGlob)
    Print #OFi, "QuitMsg = " + EncodeUTF8(QuitMsg)
    Print #OFi, "Finger = " + EncodeUTF8(FingerData)
    Print #OFi, "LastScSize = " + CStr(LastScSize)
    Print #OFi, "UTF8 = " + CStr(CInt(UTF8on))
    Print #OFi, "LastUsedChans = " + LastUsedChans
    Print #OFi, UnloadPlugins(PlUnload = False)
  Close #OFi
  
  OFi = FreeFile
  Open UserPath + "ShortCut.txt" For Output As #OFi
    For I = 0 To UBound(ComTranzList, 2)
      If (Len(ComTranzList(0, I)) > 0 Or Len(ComTranzList(2, I)) > 0) _
      And InStr(1, ComTranzList(3, I), "s") = 0 Then
        Print #OFi, ComTranzList(0, I) 'Kurzbefehl
        Print #OFi, ComTranzList(1, I) 'Befehl
        Print #OFi, ComTranzList(2, I) 'Men�titel
        Print #OFi, ComTranzList(3, I) '10(0) -> User, 11(1) -> Raum, 12(2) -> Algemein
        Print #OFi, "-"
      End If
    Next
  Close #OFi
  If Err Then MsgBox Err.Description + " (basic values)", vbCritical
  On Error GoTo 0
End Sub

Private Sub LoadMainValues()
  Dim OFi As Integer
  Dim InpData As String
  Dim I As Integer
  Dim I2 As Integer
  Dim StartScript As Boolean
  
  On Error Resume Next
  If Dir(UserPath + "Nettalk.ini") = "" Then Exit Sub
  OFi = FreeFile
  Open UserPath + "Nettalk.ini" For Input As #OFi
    Do Until EOF(OFi) = True
      Line Input #OFi, InpData
      I = InStr(InpData, "=")
      If I > 0 Then
        Select Case Trim(LCase(Left(InpData, I - 1)))
          Case "lastusedrealname"
            LastUsedRealName = DecodeUTF8(Trim(Right(InpData, Len(InpData) - I)))
          Case "lastusednick"
            LastUsedNick = DecodeUTF8(Trim(Right(InpData, Len(InpData) - I)))
          Case "lastuseduserid"
            LastUsedUserID = DecodeUTF8(Trim(Right(InpData, Len(InpData) - I)))
          Case "osd"
            UModeOSD = CBool(Val(Right(InpData, Len(InpData) - I))) = False
          Case "textpopup"
            UModeBalken = CBool(Val(Right(InpData, Len(InpData) - I))) = False
          Case "hideformwithmin"
            HideFormWithMin = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "noquitwithx"
            NoQuitWithX = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "autorejoin"
            AutoReJoin = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "retrytime"
            RetryTime = Val(Right(InpData, Len(InpData) - I))
          Case "windowvisible"
            StartVisible = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "windowstate"
            Me.WindowState = Val(Right(InpData, Len(InpData) - I))
          Case "lastwstate"
            LastWState = Val(Right(InpData, Len(InpData) - I))
          Case "opframeindex"
            OpFrameIndex = Val(Right(InpData, Len(InpData) - I))
          Case "logmodeprivate"
            LogmodePrivate = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "logmoderoom"
            LogmodeRoom = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "privlogmaxsize"
            PrivLogMaxSize = Val(Right(InpData, Len(InpData) - I))
          Case "publlogmaxsize"
            PublLogMaxSize = Val(Right(InpData, Len(InpData) - I))
          Case "timestamptext"
            ShowTimeStampText = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "retrytime"
            RetryTime = Val(Right(InpData, Len(InpData) - I))
          Case "playbeep"
            PlayBeep = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "playwav"
            PlayWav = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "wavepath"
            WavePath = Trim(Right(InpData, Len(InpData) - I))
          Case "showprivtext"
            EaOSDPriv = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "showroomtext"
            EaOSDRoom = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "remaindrooms"
            RemaindRooms = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "remaindconns"
            RemaindConns = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "remaindlists"
            RemaindList = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "windowtop"
            Form1.Top = Val(Val(Right(InpData, Len(InpData) - I)))
          Case "windowleft"
            Form1.Left = Val(Right(InpData, Len(InpData) - I))
          Case "windowheight"
            Form1.Height = Val(Right(InpData, Len(InpData) - I))
          Case "windowwidth"
            Form1.Width = Val(Right(InpData, Len(InpData) - I))
          Case "menvisible"
            HideShowMenue CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "beeprooms"
            OsdRoomList = DecodeUTF8(Trim(Right(InpData, Len(InpData) - I)))
          Case "stumm"
            SoundStumm = Val(Right(InpData, Len(InpData) - I)) = False
          Case "textboxheight"
            pTexthg.Height = Val(Right(InpData, Len(InpData) - I))
          Case "userlistwidth"
            RListW = Val(Right(InpData, Len(InpData) - I))
          Case "clientmsg"
            CoVal.ClientMSG = Val(Right(InpData, Len(InpData) - I))
          Case "errormsg"
            CoVal.ErrorMSG = Val(Right(InpData, Len(InpData) - I))
          Case "flustervon"
            CoVal.FlusterVon = Val(Right(InpData, Len(InpData) - I))
          Case "link"
            CoVal.Link = Val(Right(InpData, Len(InpData) - I))
          Case "normaltext"
            CoVal.NormalText = Val(Right(InpData, Len(InpData) - I))
          Case "normalvon"
            CoVal.NormalVon = Val(Right(InpData, Len(InpData) - I))
          Case "notice"
            CoVal.Notice = Val(Right(InpData, Len(InpData) - I))
          Case "servermsg"
            CoVal.ServerMSG = Val(Right(InpData, Len(InpData) - I))
          Case "usermsg"
            CoVal.UserMSG = Val(Right(InpData, Len(InpData) - I))
          Case "bcolumdisc"
            CoVal.bColumDisc = Val(Right(InpData, Len(InpData) - I))
          Case "bcolumbusy"
            CoVal.bColumBusy = Val(Right(InpData, Len(InpData) - I))
          Case "konbackcolor"
            Form1.pTBox.BackColor = Val(Right(InpData, Len(InpData) - I))
          Case "ulistforecolor"
            Form1.pUList.ForeColor = Val(Right(InpData, Len(InpData) - I))
          Case "ulistbackcolor"
            Form1.pUList.BackColor = Val(Right(InpData, Len(InpData) - I))
          Case "textforecolor"
            Form1.Text1.ForeColor = Val(Right(InpData, Len(InpData) - I))
          Case "textbackcolor"
            Form1.Text1.BackColor = Val(Right(InpData, Len(InpData) - I))
          Case "timestamp"
            CoVal.TimeStamp = Val(Right(InpData, Len(InpData) - I))
          Case "ownnick"
            CoVal.OwnNick = Val(Right(InpData, Len(InpData) - I))
          Case "buttonshadow"
            CoVal.ButtonShadow = Val(Right(InpData, Len(InpData) - I))
          Case "light3d"
            CoVal.Light3D = Val(Right(InpData, Len(InpData) - I))
          Case "backcolor"
            CoVal.BackColor = Val(Right(InpData, Len(InpData) - I))
          Case "menutext"
            CoVal.MenuText = Val(Right(InpData, Len(InpData) - I))
          Case "graytext"
            CoVal.GrayText = Val(Right(InpData, Len(InpData) - I))
          Case "shadow3d"
            CoVal.Shadow3D = Val(Right(InpData, Len(InpData) - I))
          Case "highlight3d"
            CoVal.Highlight3D = Val(Right(InpData, Len(InpData) - I))
          Case "buttontext"
            CoVal.ButtonText = Val(Right(InpData, Len(InpData) - I))
          Case "lastslpath"
            LastSPath = Trim(Right(InpData, Len(InpData) - I))
          Case "lastopenpath"
            LastLPath = Trim(Right(InpData, Len(InpData) - I))
          Case "lastservers"
            LastServers = Trim(Right(InpData, Len(InpData) - I))
          Case "scriptenabled"
            StartScript = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "picturepath"
            HGBildPath = Trim(Right(InpData, Len(InpData) - I))
          Case "picturemode"
            HGDrawMode = Val(Right(InpData, Len(InpData) - I))
          Case "fontname"
            FontTypeName = Trim(Right(InpData, Len(InpData) - I))
          Case "fontsize"
            FontTypeSize = Val(Right(InpData, Len(InpData) - I))
          Case "fontbold"
            FontTypeBold = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "staticdccport"
            StaticDCCPort = Val(Right(InpData, Len(InpData) - I))
          Case "dccserverport"
            DCCServerPort = Val(Right(InpData, Len(InpData) - I))
          Case "irctimeout"
            IRCTimeOut = Val(Right(InpData, Len(InpData) - I))
          Case "playbeep2"
            PlayBeep2 = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "playwav2"
            PlayWav2 = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "wavepath2"
            WavePath2 = Trim(Right(InpData, Len(InpData) - I))
          Case "showprivtext2"
            EaOSDPriv2 = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "usesecip"
            UseSecIP = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "dccmaxupspeed"
            dccMaxUpSpeed = Val(Right(InpData, Len(InpData) - I))
          Case "dccpaketsize"
            dccPaketSize = Val(Right(InpData, Len(InpData) - I))
          Case "dccautoacc"
            dccAutoAcc = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "beeparr"
            BeepArr = Trim(Right(InpData, Len(InpData) - I))
          Case "dccpath"
            DCCPath = Trim(Right(InpData, Len(InpData) - I))
          Case "sockproxyserver"
            SockProxyServerGlob = Trim(Right(InpData, Len(InpData) - I))
          Case "sockproxyport"
            SockProxyPortGlob = Val(Right(InpData, Len(InpData) - I))
          Case "sockproxytype"
            SockProxyTypeGlob = Val(Right(InpData, Len(InpData) - I))
          Case "quitmsg"
            QuitMsg = DecodeUTF8(Trim(Right(InpData, Len(InpData) - I)))
          Case "finger"
            FingerData = DecodeUTF8(Trim(Right(InpData, Len(InpData) - I)))
          Case "lastscsize"
            LastScSize = Val(Right(InpData, Len(InpData) - I))
          Case "utf8"
            UTF8on = CBool(Val(Right(InpData, Len(InpData) - I)))
          Case "lastusedchans"
            LastUsedChans = Trim(Right(InpData, Len(InpData) - I))
          Case "loadedplugins"
            LoadAllPlugins Trim(Right(InpData, Len(InpData) - I))
        End Select
      End If
    Loop
  Close #OFi

  If Len(Dir(AppPath + "ShortCut.txt")) Then
    I = FileLen(AppPath + "ShortCut.txt")
  Else
    I = LastScSize
  End If
  InpData = UserPath
  If I <> LastScSize And AppPath <> UserPath Then
    I2 = MsgBox(TeVal.scReload, vbQuestion + vbYesNo)
    If I2 = vbYes Then InpData = AppPath
  End If
  LastScSize = I
  I2 = 0
  If Len(Dir(InpData + "ShortCut.txt")) Then
    OFi = FreeFile
    Open InpData + "ShortCut.txt" For Input As #OFi
      ReDim ComTranzList(0 To 3, 0 To 7) As String
      I = 0
      Do Until EOF(OFi)
        Line Input #OFi, InpData
        If I2 < 4 Then ComTranzList(I2, I) = InpData
        I2 = I2 + 1
        If I2 > 4 Then
          I2 = 0
          I = I + 1
          If I > UBound(ComTranzList, 2) Then ReDim Preserve ComTranzList(0 To 3, 0 To I + 7) As String
        End If
      Loop
    Close #OFi
  End If
  If StartScript Then ntScript_Reset
  If Err <> 0 Then MsgBox Err.Description + " (basic values)", vbCritical
End Sub

Private Sub Check1_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Text1.SetFocus
End Sub

Private Sub Combo1_Click()
  Dim I As Long
  Dim I2 As Long
  Dim DAT As String
  
  If Combo1.ListIndex < 0 Then Exit Sub
  
  I = GetScriptSelStart - 2
  If I < 1 Then I = 1
  Do
    I2 = I
    I = InStr(I + 2, Text2.Text, vbNewLine)
    If I = 0 Then I = Len(Text2.Text) + 1
    If I - I2 > 1 Then DAT = LCase(Trim(Mid(Text2.Text, I2 + 2, I - I2 - 2)))
    If DAT = "end sub" Or DAT = "end function" Then Exit Do
    If Left(DAT, 4) = "sub " Or Left(DAT, 9) = "function " Or Left(DAT, 15) = "#newscriptbegin" Then
      I = I2 - 2
      Exit Do
    End If
  Loop Until I = Len(Text2.Text) + 1
  If I < 1 Then I = 1
  
  DAT = vbNewLine + vbNewLine + "Sub " + Combo1.List(Combo1.ListIndex) + vbNewLine + "  " + vbNewLine + "End Sub"
  SetScriptText Left(Text2.Text, I - 1) + DAT + vbNewLine + Mid(Text2.Text, I + 2)
  'Text2.SelStart = I + Len(DAT)
  SetScriptSelection I + Len(DAT) - 10, 0
  
  Combo1.ListIndex = -1
  If Text2.Visible = True Then Text2.SetFocus
End Sub

Private Sub comList_Click()
  Text1.SetFocus
End Sub

Private Sub comList_DblClick()
  Dim TempHId As Boolean
  
  TempHId = UseHelpIndex
  ConfirmeListEnt
  If TempHId Then
    FrontCFrame.SendM Text1.Text
  End If
End Sub

Private Sub ConfirmeListEnt()
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  
  If comList.ListIndex < 0 Then Exit Sub
  If ComListMode = 0 Then
    Do
      I2 = I
      I = InStr(I + 1, Text1.Text, "/")
      If I = 0 Or I > Text1.SelStart Then Exit Do
    Loop
    If I2 = 0 Then comList.Visible = False: Exit Sub
    I3 = InStr(I2 + 1, Text1.Text, " ")
    If I3 = 0 Then I3 = Len(Text1.Text) + 1
    I = Text1.SelStart
    Text1.Text = Left(Text1.Text, I2 - 1) + comList.List(comList.ListIndex) + " " + Mid(Text1.Text, I3 + 1)
    Text1.SelStart = I2 + Len(comList.List(comList.ListIndex))
    comList.Visible = False
    ProofIPath
    Text1.SetFocus
  Else
    I = ComListMode - 1
    I2 = Text1.SelStart + 1
    If I2 = 0 Then I2 = Len(Text1.Text) + 1
    If I2 > I Then
      Text1.Text = Left(Text1.Text, I) + comList.List(comList.ListIndex) + Mid(Text1.Text, I2)
      Text1.SelStart = I + Len(comList.List(comList.ListIndex))
    End If
    comList.Visible = False
    ProofIPath
    Text1.SetFocus
  End If
End Sub

Private Sub DataPort_Change()
  Dim I As Integer
  Dim I2 As Integer
  Dim DAT As String
  
  If Len(DataPort.Text) Then
    DAT = DataPort.Text
    DataPort.Text = ""
    I = InStr(DAT, " ")
    If I = 0 Then Exit Sub
    I2 = InStr(I + 1, DAT, " ")
    If I2 = 0 Then
      I2 = Len(DAT) + 1
      PluginEvent LCase(Left(DAT, I - 1)), LCase(Mid(DAT, I + 1, I2 - I - 1)), ""
    Else
      PluginEvent LCase(Left(DAT, I - 1)), LCase(Mid(DAT, I + 1, I2 - I - 1)), Mid(DAT, I2 + 1)
    End If
  End If
End Sub

Public Sub AddTrayIcon()
  T.cbSize = Len(T)
  T.hIcon = TrayIcon(1)
  T.hWnd = Me.hWnd
  T.uID = 1&
  T.szTip = "Nettalk" + Chr$(0)
  T.uFlags = NIF_MESSAGE Or NIF_ICON Or NIF_TIP
  T.uCallbackMessage = MYWM_CALLBACK
  Shell_NotifyIcon NIM_ADD, T
End Sub

Public Sub ChangeFrontFrame(Shift As Integer)
  Dim SortedC() As Integer
  Dim I As Integer
  
  GiveSortedC SortedC
  For I = 1 To UBound(SortedC)
    If ChatFrames(SortedC(I)) Is FrontCFrame Then Exit For
  Next
  If Shift = 2 Then
    If I > UBound(SortedC) - 1 Then I = 0
    SetFrontFrame ChatFrames(SortedC(I + 1))
  Else
    If I < 2 Then I = UBound(SortedC) + 1
    SetFrontFrame ChatFrames(SortedC(I - 1))
  End If
End Sub

Public Sub ChangeFrontFrameUsed(Shift As Integer)
  Dim SortedC() As Integer
  Dim CFrame As ChatFrame
  Dim I As Integer
  Dim NextIcf As Integer
  Dim PFlag As Boolean
  
  GiveSortedC SortedC
  If Shift = 2 Then
    NextIcf = SortedC(UBound(SortedC))
  Else
    NextIcf = SortedC(1)
  End If
  For I = 1 To UBound(SortedC)
    Set CFrame = ChatFrames(SortedC(I))
    If CFrame Is FrontCFrame Then
      PFlag = True
    ElseIf CFrame.Changed > 1 Then
      If PFlag Then
        If Shift = 2 Then NextIcf = SortedC(I)
        Exit For
      Else
        If Shift = 0 Then NextIcf = SortedC(I)
      End If
    End If
  Next
  SetFrontFrame ChatFrames(NextIcf)
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  Dim SortedC() As Integer
  Dim CFrame As ChatFrame
  Dim TmpStr As String
      
  If Me.Visible = False Or Me.WindowState = 1 Then Exit Sub
  
  If Shift > 1 Or (KeyCode > 111 And KeyCode < 124) Then ' F1-F12
    If ntScript_Call("KeyShortcut", CStr(KeyCode), CStr(Shift)) = False Then
      KeyCode = 0
      Shift = 0
      Exit Sub
    End If
  End If
  
  KeyOffFlag = True
  If KeyCode = 9 And (Shift = 2 Or Shift = 3) Then
    ChangeFrontFrame Shift
  ElseIf (KeyCode = vbKeyF4 Or KeyCode = vbKeyW) And Shift = 2 Then
    FrontCFrame.CloseFrame
  ElseIf KeyCode = vbKeyT And Shift = 2 Then
    CopyTitel
  ElseIf KeyCode = vbKeyY And Shift = 2 Then
    PastCaption
  ElseIf KeyCode = vbKeyE And Shift = 2 Then
    menH2past_Click
  ElseIf KeyCode = vbKeyH And Shift = 2 Then
    Me.Visible = False
  ElseIf KeyCode = vbKeyB And Shift = 2 Then
    CorrectWord
  ElseIf KeyCode = vbKeyN And Shift = 2 Then
    CancelWord TOutSt
  ElseIf (KeyCode = vbKeyG And Shift = 2) Or (KeyCode = vbKeyF3 And Shift = 0) Then
    If Not FrontCFrame.TextF Is Nothing Then FrontCFrame.TextF.Search ""
  ElseIf (KeyCode = vbKeyG And Shift = 3) Or (KeyCode = vbKeyF3 And Shift = 1) Then
    If Not FrontCFrame.TextF Is Nothing Then FrontCFrame.TextF.Search "", True
  ElseIf KeyCode = vbKeyS And Shift = 2 Then
    If FrontCFrame.FrameType = cfRoom Then
      If FrontCFrame.UListF.ListCount > 0 Then
        TmpStr = WisperTo
        If Len(FrontCFrame.LastWisperer) > 0 Then
          WisperTo = FrontCFrame.LastWisperer
        End If
        If Len(WisperTo) = 0 Then
          WisperTo = FrontCFrame.IrcConn.ChatNoSign(FrontCFrame.UListF.List(0))
        End If
        If Left(WisperTo, 4) = "MSG:" Then WisperTo = Mid(WisperTo, 5)
        Check1.Visible = True
        If TmpStr = WisperTo And Check1.Value = 1 Then
          Check1.Value = 0
        Else
          Check1.Value = 1
        End If
        TfRedraw
      End If
    End If
  ElseIf KeyCode = vbKeyF1 Then
    If Not TextFeld Is Nothing Then
      If TextFeld.SelLength > 0 Then
        HTMLHelp_ShowTopic Trim(Mid(TextFeld.Text, TextFeld.SelStart + 1, TextFeld.SelLength))
      Else
        HTMLHelp_ShowTopic
      End If
    Else
      HTMLHelp_ShowTopic
    End If
  ElseIf KeyCode = vbKeyF5 Then
    RefreshBGP
    Form1.GlobalColorRefresh
  ElseIf (KeyCode = vbKeyF4 And Shift = 0) Or (KeyCode = vbKeyTab And Shift = 0) Then
    FillComList "$textnick", True
  ElseIf KeyCode = 27 Then
    comList.Visible = False
    txtList.Visible = False
  ElseIf KeyCode = vbKeyR And Shift = 2 Then
    PopUpTablist = True
  ElseIf KeyCode = vbKeyTab And Shift = 1 Then
    If Not TextFeld Is Nothing Then TextFeld.SelText = vbTab
  ElseIf KeyCode = vbKeyA And Shift = 2 Then
    SelectAllText
  Else
    KeyOffFlag = False
  End If
  
  '--  Men�k�rzel  --
  If mDatei.Visible = False And KeyOffFlag = False Then
    If KeyCode = vbKeyQ And Shift = 2 Then
      menQuit_Click
      KeyOffFlag = True
    ElseIf KeyCode = vbKeyC And Shift = 2 Then
      If Not TextFeld Is Nothing Then menBcopy_Click
    ElseIf KeyCode = 46 Then 'L�schen
      menBdel_Click
    ElseIf KeyCode = vbKeyF And Shift = 2 Then
      menBsearch_Click
      KeyOffFlag = True
    End If
  End If
  
  '-- mit Alt+S Senden --
  If KeyCode = vbKeyS And Shift = 4 Then Text1_KeyDown 13, 0: KeyOffFlag = True
  If KeyOffFlag = True Then KeyCode = 0: Shift = 0
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
  If txtList.Visible = True Then
    If KeyAscii = 27 Then
      comList.Visible = False
      Text1.SetFocus
    ElseIf KeyAscii = 32 Then
      Text1.SelStart = Len(Text1.Text)
      comList.Visible = False
      Text1.SetFocus
    End If
  End If
  If KeyOffFlag Then KeyAscii = 0
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
  If PopUpTablist = True Then PDColumList True: PopUpTablist = False
End Sub

Private Sub Form_Load()
  Dim DAT As String
  Dim ChFrame As New ChatFrame
  Dim I As Integer
  Dim I2 As Integer
  
  ReDim TabList(0)
  ReDim PluginList(0)
        
  If LCase(Command) <> "debugmode" Then
    StartListening Me.hWnd
    Setup_MsWheel
  End If
    
  MYWM_TASKBARCREATED = RegisterWindowMessage("TaskbarCreated")
  Randomize Timer
  IntBasicCodepage
  InitFormFont Me
  
  comList.Sort = True
    
  IconStaw.Picture = LoadResPicture(2, vbResBitmap)
  For I = 0 To 9
    TrayIcon.Add LoadImage(App.hInstance, I + 3, IMAGE_ICON, 16&, 16&, LR_COPYFROMRESOURCE)
  Next
  pTexthg.MouseIcon = LoadResPicture(11, vbResCursor)
  
  SetFormIcon hWnd, 14
    
  '######### Variabeln sezen ###########
'  TeVal.ConCancel = "Abgebrochen"
'  TeVal.ConClose = "~ Verbindung getrennt"
'  TeVal.ConErr = "Fehler"
'  TeVal.ConOK = "OK"
'  TeVal.ConStart = "~ Verbinden..."
'  TeVal.Join = "joined"
'  TeVal.Leave = "left"
'  TeVal.Nick = "hat sich umbenannt zu"
'  TeVal.Quit = "quited"
'  TeVal.Connected = "Verbunden"
'  TeVal.Closed = "Getrennt"
'  TeVal.Connecting = "Verbinden..."
'  TeVal.UserRank = "setzt den Mode:"
'  TeVal.DCCquestion = "bittet um eine DCC verbindung"
'  TeVal.RoomEnter = "Sie sind nun in"
'  TeVal.Kick = "Sie wurden heraus geworfen von"
'  TeVal.Kick2 = "heraus geworfen von"
'  TeVal.DCCok = "[Verbindung annehmen]"
'  TeVal.WisperTo = "Fl�stern an:"
'  TeVal.Wispers = "fl�stert"
'  TeVal.msgDel = "Sind Sie sicher das Sie diese Verbindung l�schen m�chten?"
'  TeVal.msgNoDel = "Die Verbindung muss erst geschlossen werden bevor sie gel�scht werden kann"
'  TeVal.dccFileNotFound = "Datei konnte nicht gefunden werden"
'  TeVal.FileExist = "Eine Datei mit diesem Namen existiert bereits soll sie �berschrieben werden?"
'  TeVal.AutoUpdate1 = "Ein neues Update ist verf�gbar"
'  TeVal.AutoUpdate2 = "Update Starten"
'  TeVal.ExtWorning = "Dieser Dateityp ist potentiell gef�hrlich!"
'  TeVal.DccCaption = "Dateitransfer"
'  TeVal.DccConnCancel = "DCC-Verbindung wurde getrennt"
'  TeVal.DccFinished = "DCC-Datei�bertragung ist abgeschlossen"
'  TeVal.Caption1 = "Neue Verbindung"
'  TeVal.Caption2 = "Verbindungseinstellungen �ndern"
'  TeVal.NoLogs = "Es wurden keine Eintr�ge gefunden"
'  TeVal.lsYes = "Ja"
'  TeVal.lsNo = "Nein"
'  TeVal.lsUnknowen = "Unbekannt"
'  TeVal.butOK = "OK"
'  TeVal.butCancel = "Abbrechen"
'  TeVal.butApply = "�bernehmen"
'  TeVal.menNew = "Neu"
'  TeVal.menDel = "L�schen"
'  TeVal.menValues = "Eigenschaften"
'  TeVal.menCut = "Ausschneiden"
'  TeVal.menCopy = "Kopieren"
'  TeVal.menPast = "Einf�gen"
'  TeVal.menConn = "Verbinden"
'  TeVal.menDis = "Trennen"
'  TeVal.menRooms = "R�ume"
'  TeVal.menSend = "Datei �ber DCC versenden"
'  TeVal.menColors = "Farben"
'  TeVal.menOpt = "Einstellungen"
'  TeVal.menFrinds = "Freunde"
'  TeVal.menHelp = "Hilfe"
'  TeVal.menSymbol = "Symbol"
'  TeVal.menMenue = "Men�"
'  TeVal.menScriptStart = "Script neustarten"
'  TeVal.menScriptStop = "Script anhalten"
'  TeVal.ls1Caption = "Bezeichnung"
'  TeVal.ls1Server = "Server"
'  TeVal.ls1Typ = "Typ"
'  TeVal.ls1State = "Status"
'  TeVal.ls1Date = "Erstellt"
'  TeVal.ls1Auto = "Automatisch"
'  TeVal.lsServers = "Server"
'  TeVal.lsRooms = "R�ume"
'  TeVal.lsFrinds = "Freunde"
'  TeVal.lsScript = "Script"
'  TeVal.ls2Name = "Name"
'  TeVal.ls2User = "User"
'  TeVal.ls2Topic = "Topic"
'  TeVal.ls3FileName = "Dateiname"
'  TeVal.ls3Progres = "Fortschritt"
'  TeVal.ls3Speed = "KB/s"
'  TeVal.ls3Time = "Zeit"
'  TeVal.ls3Size = "Gr��e"
'  TeVal.ls3User = "User"
'  TeVal.ls3Path = "Pfad"
'  TeVal.ls4Nick = "Nick"
'  TeVal.ls4State = "Status"
'  TeVal.ls4Server = "Server"
'  TeVal.ls4Online = "Online"
'  TeVal.ls5Maske = "Maske"
'  TeVal.ls5Von = "Von"
'  TeVal.ls5Vor = "Vor"
'  TeVal.cClientMsg = "Clientnachricht"
'  TeVal.cErrMsg = "Fehlernachricht"
'  TeVal.cNickW = "Nick (gefl�stert)"
'  TeVal.cLink = "Link"
'  TeVal.cText = "Standerttext"
'  TeVal.cNickN = "Nick (normal)"
'  TeVal.cNotice = "Notice"
'  TeVal.cMSG = "Servernachricht"
'  TeVal.chMSG = "Hinterlassende Nachricht"
'  TeVal.ccHG = "Konsolenhintergrund"
'  TeVal.ctUserL = "Text der Userliste"
'  TeVal.cULHG = "Hintergrund der Userliste"
'  TeVal.cTextF = "Textfeld-Text"
'  TeVal.cTextFHG = "Textfeld-Hintergrund"
'  TeVal.oGeneral = "Allgemein"
'  TeVal.oMsgs = "Nachrichten"
'  TeVal.oConnections = "Verbindungen"
'  TeVal.oShortcuts = "Shortcuts"
'  TeVal.oApp = "Aussehen"
'  TeVal.oPlugins = "Plugins"
'  TeVal.opPos1 = "Oben rechts"
'  TeVal.opPos2 = "Oben links"
'  TeVal.NewCTitel = "Verbindung1"
'  TeVal.fFunction = "Funktion"
'  TeVal.fOn = "aktiviert"
'  TeVal.fOff = "deaktiviert"
'  TeVal.FrindOnline = "ist Online"
'  TeVal.scrTimeout = "Script antwortet nicht. Soll es beendet werden?"
'  TeVal.SearchResults = "Suchergebnisse"
'  TeVal.TopicSet = "hat das Topic gesetzt"
'  TeVal.TimeOut = "Server-Timeout"
'  TeVal.cTimeStamp = "Timestamps"
'  TeVal.Ignore = "wird nun ignoriert"
'  TeVal.UnIgnore = "wird nun nicht mehr ignoriert"
'  TeVal.scReload = "Es ist eine neue ShortCut.txt verf�gbar, soll die Vorherige f�r diesen User mit der Neuen ersetzt werden?"
'  TeVal.ConRemove = "Soll diese Verbindung in der Serverliste gespeichert bleiben?"
'  TeVal.Away = "ist momentan nicht erreichbar"
  
  CoVal.ClientMSG = RGB(128, 32, 32)
  CoVal.ErrorMSG = RGB(255, 0, 0)
  CoVal.FlusterVon = RGB(0, 128, 0)
  CoVal.Link = RGB(0, 0, 255)
  CoVal.NormalText = 0
  CoVal.NormalVon = RGB(0, 0, 192)
  CoVal.Notice = RGB(162, 64, 0)
  CoVal.ServerMSG = RGB(162, 64, 0)
  CoVal.UserMSG = RGB(128, 0, 32)
  CoVal.bColumDisc = RGB(100, 100, 100)
  CoVal.bColumBusy = RGB(150, 0, 0)
  CoVal.TimeStamp = RGB(100, 100, 100)
  CoVal.OwnNick = RGB(20, 20, 200)
  CoVal.ButtonShadow = &H80000010
  CoVal.Light3D = &H80000016
  CoVal.BackColor = &H8000000F
  CoVal.MenuText = &H80000007
  CoVal.GrayText = &H80000011
  CoVal.Shadow3D = &H80000015
  CoVal.Highlight3D = &H80000014
  CoVal.ButtonText = &H80000012

  
'  CoVal.ButtonShadow = RGB(&H3B, &H4C, &HB1)
'  CoVal.Light3D = RGB(&H9F, &HAA, &HEB)
'  CoVal.BackColor = RGB(&H94, &H9A, &HBC)
'  CoVal.MenuText = vbWhite
'  CoVal.GrayText = RGB(&H4D, &H4D, &H4D)
'  CoVal.Shadow3D = RGB(&H29, &H35, &H7B)
'  CoVal.Highlight3D = &H80000014
  
  ListAllLangws
  LoadFormStrs Me, True
  '#####################################
           
  Set MenuBar = New plusMenu
  MenuBar.Inst pMenu
  MenuBar.Add TeVal.menNew, , True, , 18
  MenuBar.Add TeVal.menDel, , True, , 19, 33
  MenuBar.Add TeVal.menValues, , True, , 20, 32
  MenuBar.Add TeVal.menCut, True, True, True, 24
  MenuBar.Add TeVal.menCopy, True, True, , 22
  MenuBar.Add TeVal.menPast, True, True, False, 23
  MenuBar.Add TeVal.menConn, , True, True, 21, 31
  MenuBar.Add TeVal.menDis, , True, , 29, 30
  MenuBar.Add TeVal.menRooms, , , True, 5, 34
  MenuBar.Add TeVal.menColors, , , False, 28, , True
  MenuBar.Add TeVal.menOpt, , , True, 55
  MenuBar.Add TeVal.menScript, , True, False, 59
  MenuBar.Add TeVal.menFrinds, , True, , 61
  MenuBar.Add TeVal.menSend, , True, False, 35, 36
  MenuBar.Add TeVal.menHelp, , True, False, 25
  MenuBar.Add TeVal.menSymbol, , True, True, 38
  MenuBar.Add TeVal.menMenue, , True, , 26
  MenuBar.Add TeVal.menScriptStart, True, , True, 39, 40
  MenuBar.Add TeVal.menScriptStop, True, , , 41, 42
  
  ChFrame.Inst cfServerList, -1
  ChFrame.Caption = TeVal.lsServers
  ChFrame.UListF.SetColum 0, TeVal.ls1Caption, 160
  ChFrame.UListF.AddColum TeVal.ls1Server, 150
  ChFrame.UListF.AddColum TeVal.ls1State, 80
  ChFrame.UListF.AddColum TeVal.ls1Used, 80
  ChFrame.UListF.AddColum TeVal.ls1Date, 80
  ChFrame.UListF.AddColum TeVal.ls1Auto, 80
  ChFrame.UListF.CSortIndex = 4
  ChatFrames.Add ChFrame
  SetFrontFrame ChFrame
    
  If Len(Dir(AppPath + "Words.dat")) > 0 Then CCheckActiv = True
  
  '<Default>
  ReDim ComTranzList(0 To 3, 0 To 16) As String
'  ComTranzList(0, 0) = "j"
'  ComTranzList(1, 0) = "join $1"
'  ComTranzList(0, 1) = "k"
'  ComTranzList(1, 1) = "kick $(roomname) $1"
'  ComTranzList(2, 1) = "User kicken"
'  ComTranzList(0, 2) = "q"
'  ComTranzList(1, 2) = "query $1"
'  ComTranzList(0, 3) = "op"
'  ComTranzList(1, 3) = "mode $(roomname) +o $1"
'  ComTranzList(2, 3) = "User oppen"
  
  RemaindRooms = True
  RetryTime = 20
  AutoReJoin = True
  HideFormWithMin = True
  NoQuitWithX = True
  menH4oncreeen.Checked = False
  menH4ibalken.Checked = False
  menH4stumm.Checked = False
  IRCTimeOut = 600
  RListW = 150
  PrivLogMaxSize = 5000
  PublLogMaxSize = 5000
  SockProxyServerGlob = "192.168.0.1"
  SockProxyPortGlob = -80
  StartVisible = True
  BeepArr = "800/50 900/50 700/50 800/50"
  '</Default>
  
  SpCount = 1
  LoadScriptFile
  LoadMainValues
  RefreshBGP
  DnsResInitialize
  CheckDCCServ
  ListenDDE
  
  pText.BackColor = Text2.BackColor
  pDiff.BackColor = CoVal.BackColor
  pDiff2.BackColor = CoVal.BackColor
  Form1.BackColor = CoVal.BackColor
  pMain.BackColor = CoVal.BackColor
  
  If GetColorLightnes(Form1.pUList.ForeColor) < 128 Then InitSelectionDraw Me.hWnd
  
  pDiff.Width = 2
  pDiff2.Height = 2
  
  On Error Resume Next
  If FontTypeName = "" Then FontTypeName = "Courier New"
  If FontTypeSize = 0 Then FontTypeSize = 9
  Form1.pTBox.FontItalic = False
  Form1.pTBox.FontName = FontTypeName
  Form1.pTBox.FontSize = FontTypeSize
  Form1.pTBox.FontBold = FontTypeBold
  Form1.pTexthg.FontItalic = False
  Form1.pTexthg.FontName = FontTypeName
  Form1.pTexthg.FontBold = FontTypeBold
  Form1.pTexthg.FontSize = FontTypeSize
  Form1.Text1.FontItalic = False
  Form1.Text1.FontName = FontTypeName
  Form1.Text1.FontBold = FontTypeBold
  Form1.Text1.FontSize = FontTypeSize
  On Error GoTo 0
    
  LoadDCCValues
  LoadFriendsValues
  LoadValues
  AddTrayIcon
  PhraseCLine
  
  If NoRegEntry = False Then CheckReg
  menH4ibalken_Click
  menH4oncreeen_Click
  menH4stumm_Click
  ntScript_Call "AppLoad"
        
  DAT = Dir(AppPath + "update.exe.update")
  If Len(DAT) > 0 Then
    On Error Resume Next
    Kill AppPath + CutDirFromPath(DAT, ".")
    Name AppPath + DAT As AppPath + CutDirFromPath(DAT, ".")
    If Err = 0 Then Kill AppPath + DAT
    On Error GoTo 0
  End If
End Sub

Private Sub LoadValues(Optional Path As String)
  Dim OFi As Integer
  Dim InpData As String
  Dim I As Integer
  Dim I2 As Integer
  Dim StFrame As ChatFrame
  Dim OpendFlag As Boolean
  
  OFi = FreeFile
  If Path = "" Then
    ConnListCount = 0
    ReDim ConnList(0)
    Path = UserPath + "Servers.srv"
  End If
  
  On Error Resume Next
  If Dir(Path) <> "" Then
    Open Path For Input As #OFi
      Do Until EOF(OFi) = True
        Line Input #OFi, InpData
        If Left(InpData, 1) = ">" Then
          ReDim Preserve ConnList(ConnListCount)
          ConnList(ConnListCount).Caption = CheckConns(DecodeUTF8(Right(InpData, Len(InpData) - 1)))
          ConnListCount = ConnListCount + 1
        End If
        I = InStr(InpData, "=")
        If I > 0 Then
          Select Case UCase(Left(InpData, I - 1))
            Case "SERVER"
              ConnList(ConnListCount - 1).Server = Right(InpData, Len(InpData) - I)
            Case "PORT"
              ConnList(ConnListCount - 1).Port = Val(Right(InpData, Len(InpData) - I))
            Case "LASTUSED"
              ConnList(ConnListCount - 1).LastUsed = CDate(Right(InpData, Len(InpData) - I))
            Case "NICK"
              ConnList(ConnListCount - 1).Nick = DecodeUTF8(Right(InpData, Len(InpData) - I))
            Case "NAME"
              ConnList(ConnListCount - 1).Name = DecodeUTF8(Right(InpData, Len(InpData) - I))
            Case "PASSWORD"
              ConnList(ConnListCount - 1).Pass = DecodeUTF8(DeKeyPassw(Right(InpData, Len(InpData) - I)))
            Case "STATE"
              ConnList(ConnListCount - 1).State = Val(Right(InpData, Len(InpData) - I))
            Case "SERVERMODE"
              ConnList(ConnListCount - 1).ServerMode = Val(Right(InpData, Len(InpData) - I))
            Case "SERVERPASS"
              ConnList(ConnListCount - 1).ServerPass = DecodeUTF8(DeKeyPassw(Right(InpData, Len(InpData) - I)))
            Case "USERID"
              ConnList(ConnListCount - 1).UserID = DecodeUTF8(Right(InpData, Len(InpData) - I))
            Case "CREATEDATE"
              ConnList(ConnListCount - 1).CreateDate = CDate(Right(InpData, Len(InpData) - I))
            Case "CREATETYPE"
              ConnList(ConnListCount - 1).CreateType = Val(Right(InpData, Len(InpData) - I))
            Case "CTCKEY"
              ConnList(ConnListCount - 1).CTCKey = DecodeUTF8(DeKeyPassw(Right(InpData, Len(InpData) - I)))
            Case "CTCON"
              ConnList(ConnListCount - 1).CTCOn = Val(Right(InpData, Len(InpData) - I))
            Case "IGNORELIST"
              ConnList(ConnListCount - 1).IgnoreList = DecodeUTF8(SortIgnoreList(Right(InpData, Len(InpData) - I)))
            Case "SILENT"
              ConnList(ConnListCount - 1).Silent = Val(Right(InpData, Len(InpData) - I))
            Case "TEMP"
              ConnList(ConnListCount - 1).Temp = Val(Right(InpData, Len(InpData) - I))
            Case "CONNECTEDINDEX"
              ConnList(ConnListCount - 1).ConnectedIndex = Val(Right(InpData, Len(InpData) - I))
            Case "UTF8"
              ConnList(ConnListCount - 1).UTF8 = Val(Right(InpData, Len(InpData) - I))
            Case "UTF8DYN"
              ConnList(ConnListCount - 1).UTF8dyn = Val(Right(InpData, Len(InpData) - I))
            Case "CODEPAGE"
              ConnList(ConnListCount - 1).Codepage = Val(Right(InpData, Len(InpData) - I))
            Case "ADDCOMMAND"
              ConnList(ConnListCount - 1).Commands = ConnList(ConnListCount - 1).Commands + DecodeUTF8(Right(InpData, Len(InpData) - I)) + vbNewLine
            Case "USESSL"
              ConnList(ConnListCount - 1).UseSSL = Val(Right(InpData, Len(InpData) - I))
          End Select
        End If
      Loop
    Close #OFi
  End If
  If Err <> 0 Then MsgBox Err.Description + " (connection values)", vbCritical
  On Error GoTo 0
  RefreshList
  Me.Visible = StartVisible
  Me.Refresh
  For I = 1 To ChatFrames.Count
    Set StFrame = ChatFrames(I)
    If StFrame.ConnTyp = -1 Then Exit For
  Next
  If I > ChatFrames.Count Then Exit Sub
  For I2 = 1 To ConnListCount
    OpendFlag = False
    For I = 0 To ConnListCount - 1
      If ConnList(I).ConnectedIndex = I2 Then
        If (ConnList(I).State = 2 Or ConnList(I).State = 3) And RemaindConns = True And OpendFlag = False Then
          StFrame.UListF.ListIndex = ConnRevID(I)
          StartConnection
          OpendFlag = True
        Else
          ConnList(I).State = 0
        End If
      ElseIf ConnList(I).ConnectedIndex > ConnListCount Or ConnList(I).ConnectedIndex < 1 Then
        ConnList(I).State = 0
      End If
    Next
  Next
  RefreshList
End Sub

Private Sub SaveValues()
  Dim OFi As Integer
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim DAT As String
  Dim StFrame As ChatFrame
    
  On Error Resume Next
  OFi = FreeFile
  Open UserPath + "Servers.srv" For Output As #OFi
    For I = 0 To ConnListCount - 1
      Print #OFi, ">" + EncodeUTF8(ConnList(I).Caption)
      Print #OFi, "State=" + CStr(ConnList(I).State)
      Print #OFi, "Server=" + ConnList(I).Server
      Print #OFi, "Port=" + CStr(ConnList(I).Port)
      Print #OFi, "LastUsed=" + CStr(ConnList(I).LastUsed)
      Print #OFi, "Nick=" + EncodeUTF8(ConnList(I).Nick)
      Print #OFi, "Name=" + EncodeUTF8(ConnList(I).Name)
      Print #OFi, "Password=" + KeyPassw(EncodeUTF8(ConnList(I).Pass))
      Print #OFi, "ServerMode=" + CStr(ConnList(I).ServerMode)
      Print #OFi, "UserID=" + EncodeUTF8(ConnList(I).UserID)
      Print #OFi, "ServerPass=" + KeyPassw(EncodeUTF8(ConnList(I).ServerPass))
      Print #OFi, "CreateDate=" + CStr(ConnList(I).CreateDate)
      Print #OFi, "CreateType=" + CStr(ConnList(I).CreateType)
      Print #OFi, "CTCKey=" + KeyPassw(EncodeUTF8(ConnList(I).CTCKey))
      Print #OFi, "CTCOn=" + CStr(ConnList(I).CTCOn)
      Print #OFi, "IgnoreList=" + EncodeUTF8(ConnList(I).IgnoreList)
      Print #OFi, "Silent=" + CStr(ConnList(I).Silent)
      Print #OFi, "Temp=" + CStr(ConnList(I).Temp)
      If ConnList(I).State = 2 Or ConnList(I).State = 3 Then
        Print #OFi, "ConnectedIndex=" + CStr(ConnList(I).ConnectedIndex)
      End If
      Print #OFi, "UTF8=" + CStr(ConnList(I).UTF8)
      Print #OFi, "UTF8dyn=" + CStr(ConnList(I).UTF8dyn)
      Print #OFi, "Codepage=" + CStr(ConnList(I).Codepage)
      Print #OFi, "UseSSL=" + CStr(ConnList(I).UseSSL)
      I2 = -1
      Do
        I3 = I2
        I2 = InStr(I2 + 2, ConnList(I).Commands, vbNewLine)
        If I2 = 0 Then I2 = Len(ConnList(I).Commands) + 1
        If I2 - I3 > 2 Then Print #OFi, "AddCommand=" + EncodeUTF8(Mid(ConnList(I).Commands, I3 + 2, I2 - I3 - 2))
      Loop Until I2 = Len(ConnList(I).Commands) + 1
      Print #OFi, ""
    Next
  Close #OFi
  If Err Then MsgBox Err.Description + " (connection values)", vbCritical
  On Error GoTo 0
End Sub

Private Sub Form_Resize()
  On Error Resume Next
  If Me.WindowState = 1 Then
    If HideFormWithMin = True Then Me.Visible = False
    Exit Sub
  ElseIf Me.WindowState = 2 Then
    LastWState = 1
  Else
    If Me.Visible Then LastWState = 0
  End If
  If TrayIconState > 0 Then
    T.hIcon = TrayIcon(1)
    Shell_NotifyIcon NIM_MODIFY, T
    TrayIconState = 0
  End If
  If Me.Height < 250 * Screen.TwipsPerPixelY Then Me.Height = 250 * Screen.TwipsPerPixelY
  pDiff2.Top = Me.ScaleHeight - pTexthg.Height - pState.Height - 2
  pDiff2.Left = 0
  pDiff2.Width = Me.ScaleWidth - pDiff2.Left
  pMenu.Width = Me.ScaleWidth - pMenu.Left
  pMain.Width = Me.ScaleWidth - pMenu.Left
  If pDiff2.Top < pMain.Top + 30 Then pDiff2.Top = pMain.Top + 30
  pMain.Height = pDiff2.Top - pMain.Top
  pState.Top = Me.ScaleHeight - pState.Height
  pState.Width = Me.ScaleWidth - pState.Left
  pTexthg.Height = Me.ScaleHeight - pDiff2.Top - pState.Height - 2
  pTexthg.Top = pDiff2.Top + pDiff2.Height
  pTexthg.Width = Me.ScaleWidth - pTexthg.Left
  Text1.Left = 1
  Text1.Top = pTexthg.TextHeight("H") + 8
  Text1.Width = pTexthg.ScaleWidth - 2
  If pTexthg.ScaleHeight - Text1.Top < 16 Then
    Text1.Height = 17
  Else
    Text1.Height = pTexthg.ScaleHeight - 1 - Text1.Top
  End If
  If txtList.Visible = True Then
    txtList.Visible = False
    Text1.SetFocus
  End If
  TfRedraw
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
  If AppUnloading = False Then
    If UnloadMode = 0 And NoQuitWithX = True Then
      Me.Visible = False
      Cancel = 1
    ElseIf UnloadMode = 3 And Me.Visible = False Then
      Cancel = 1
    Else
      If ntScript_Call("AppUnload", CStr(UnloadMode)) = False Then Cancel = 1
    End If
  End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
  CleanUpApp
End Sub

Private Sub CleanUpApp()
  Dim I As Integer
  
  AppUnloading = True
  KillAllTimers
    
  For I = Forms.Count - 1 To 0 Step -1
    If Not Forms(I) Is Me Then Unload Forms(I)
  Next
  SaveMainValues
  
  CheckDCCServ True
  CloseAllConnections
  SaveDCCValues
  SaveFriendsValues
  DnsResTerminate
  SaveValues
  RemAllConnections
  ntScript_Unload
  
  Me.Visible = False
  UModeOSD = False
  UModeBalken = False
  
  For I = ChatFrames.Count To 1 Step -1
    If ChatFrames(I).FrameType = cfScript Then ntScript = Form1.Text2.Text
    ChatFrames.Remove I
  Next
  
  SaveScriptFile
  UnloadDDE
  UnloadSelectionDraw
  Shell_NotifyIcon NIM_DELETE, T
  CloseLogFiles
  StopListening
End Sub

Private Sub CloseAllConnections()
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  
  For I = IRCconns.Count To 1 Step -1
    I3 = 0
    For I2 = 1 To ChatFrames.Count
      If ChatFrames(I2).FrameType = cfStatus Then
        I3 = I3 + 1
        If ChatFrames(I2).IrcConn Is IRCconns(I) Then Exit For
      End If
    Next
    ConnList(IRCconns(I).ListID).ConnectedIndex = I3
    IRCconns(I).CloseConnection
  Next
  For I = DCCconns.Count To 1 Step -1
    DCCconns(I).CloseConnection
  Next
End Sub

Private Sub RemAllConnections()
  Dim I As Integer
  
  For I = IRCconns.Count To 1 Step -1
    IRCconns.Remove I
  Next
  For I = DCCconns.Count To 1 Step -1
    DCCconns.Remove I
  Next
  For I = DCCFileConns.Count To 1 Step -1
    DCCFileConns.Remove I
  Next
End Sub

Private Sub menBcopy_Click()
  Dim DAT As String
  
  If Not TextFeld Is Nothing Then
    If TextFeld.SelLength > 0 Then DAT = Mid(TextFeld.Text, TextFeld.SelStart + 1, TextFeld.SelLength)
  End If
  If Not FrontCFrame.TextF Is Nothing Then
    If Len(FrontCFrame.TextF.SelledText) > 0 Then DAT = FrontCFrame.TextF.SelledText
    FrontCFrame.TextF.Scroll
  End If
  If Len(DAT) Then SetClipboardText DAT, Me.hWnd
End Sub

Private Sub menBcut_Click()
  If Not TextFeld Is Nothing Then
    If TextFeld.SelLength > 0 Then
      SetClipboardText TextFeld.SelText, Me.hWnd
      menBdel_Click
    End If
  End If
End Sub

Private Sub SelectAllText()
  If Not TextFeld Is Nothing Then
    If TextFeld Is Text2 Then
        SetScriptSelection 0, Len(Text2.Text)
    Else
        TextFeld.SelStart = 0
        TextFeld.SelLength = Len(TextFeld.Text)
    End If
  End If
End Sub

Private Sub menBdel_Click()
  Dim I As Integer
  
  If Not TextFeld Is Nothing Then
    If Form1.mBearbeiten.Visible Then
      SendMessageA TextFeld.hWnd, WM_KEYDOWN, ByVal 46, 0
      SendMessageA TextFeld.hWnd, WM_KEYUP, ByVal 46, 0
    End If
  ElseIf FrontCFrame.FrameType = cfDCC Then
    For I = 1 To DCCFileConns.Count
      If DCCFileFrame.UListF.List(FrontCFrame.UListF.ListIndex, 0) = DCCFileConns(I).FileName Then
        SelledFC = I
        menH6del_Click
        Exit For
      End If
    Next
  ElseIf FrontCFrame.FrameType = cfServerList Then
    menHdel_Click
  ElseIf FrontCFrame.FrameType = cfFriends Then
    menH7del_Click
  End If
End Sub

Private Sub menBpast_Click()
  If TextFeld Is Nothing Then Exit Sub
  'TextFeld.SelText = Clipboard.GetText(vbCFText)
  SendMessageA TextFeld.hWnd, WM_PASTE, 0&, 0&
  If TextFeld.Visible = True Then TextFeld.SetFocus
End Sub

Private Sub menBsearch_Click()
  If Text1.Text <> "" And txtList.List(txtList.ListCount - 1) <> Text1.Text Then
    If Right(Text1.Text, 2) = vbNewLine Then
      txtList.Additem Left(Text1.Text, Len(Text1.Text) - 2)
    Else
      txtList.Additem Text1.Text
    End If
    If txtList.ListCount > 64 Then txtList.RemoveItem 0
  End If
  Text1.Text = "/search "
  Text1.SelStart = Len(Text1.Text)
  If Me.Visible = True And Text1.Visible = True Then Text1.SetFocus
End Sub

Private Sub menExConns_Click()
  Dim DAT As String
  Dim I As Integer
  Do
    ShowSave DAT, "srv-files" + Chr(0) + "*.srv", LastSPath, "Servers.srv", I, Me.hWnd
    If DAT = "" Then Exit Sub
    If Mid(DAT, Len(DAT) - 3, 1) <> "." And Mid(DAT, Len(DAT) - 4, 1) <> "." Then
      DAT = DAT + ".srv"
    End If
    On Error Resume Next
    If Dir(DAT) <> "" Then
      On Error GoTo 0
      If MsgBox(TeVal.FileExist, vbQuestion + vbYesNo) = vbYes Then Exit Do
    Else
      On Error GoTo 0
      Exit Do
    End If
  Loop
  SaveValues
  On Error Resume Next
  FileCopy UserPath + "Servers.srv", DAT
  If Err <> 0 Then MsgBox Err.Description, vbCritical
  On Error GoTo 0
  LastSPath = CutDirFromPath(DAT)
End Sub

Private Sub menH2copy_Click()
  If FrontCFrame.TextF Is Nothing Then Exit Sub
  SetClipboardText FrontCFrame.TextF.SelledText, Me.hWnd
  FrontCFrame.TextF.Scroll
End Sub

Private Sub menH2copytime_Click()
  If FrontCFrame.TextF Is Nothing Then Exit Sub
  SetClipboardText FrontCFrame.TextF.SelledTextEx, Me.hWnd
  FrontCFrame.TextF.Scroll
End Sub

Private Sub menH2edit_Click()
  If FrontCFrame.TextF Is Nothing Then Exit Sub
  FrontCFrame.TextF.ShowAsTXT
End Sub

Private Sub menH2past_Click()
  Dim I As Integer
  If FrontCFrame.TextF Is Nothing Then Exit Sub
  I = Text1.SelStart
  If Len(FrontCFrame.TextF.SelledText) Then
    Text1.Text = Left(Text1.Text, Text1.SelStart) + FrontCFrame.TextF.SelledText + Mid(Text1.Text, Text1.SelStart + Text1.SelLength + 1)
    Text1.SelStart = I + Len(FrontCFrame.TextF.SelledText)
  Else
    Text1.Text = Left(Text1.Text, Text1.SelStart) + WisperTo + Mid(Text1.Text, Text1.SelStart + Text1.SelLength + 1)
    Text1.SelStart = I + Len(WisperTo)
  End If
  FrontCFrame.TextF.Scroll
  TextChanged
  If Text1.Visible = True Then Text1.SetFocus
End Sub

Private Sub menH2prop_Click()
  Form8.Show 1, Me
End Sub

Private Sub menH2scroll_Click()
  If FrontCFrame.TextF Is Nothing Then Exit Sub
  FrontCFrame.TextF.Scroll True
End Sub

Private Sub menH2show_Click()
  If FrontCFrame.TextF Is Nothing Then Exit Sub
  FrontCFrame.TextF.ShowAsHTML
End Sub

Public Sub RefScMenue()
  Dim I As Integer
  Dim L1 As Integer
  Dim L2 As Integer
  Dim J1 As Integer
  Dim J2 As Integer
  
  For I = 0 To UBound(ComTranzList, 2)
    If L1 >= menH3shortcut.Count Then Load menH3shortcut(L1)
    If L2 >= menH3Subsc.Count Then Load menH3Subsc(L2)
    If J1 >= menH2shortcut.Count Then Load menH2shortcut(J1)
    If J2 >= menH2Subsc.Count Then Load menH2Subsc(J2)
    
    If Len(ComTranzList(2, I)) > 0 Then
      If Val(ComTranzList(3, I)) < 11 Then
        If Left(ComTranzList(2, I), 1) <> ">" Then
          menH3shortcut(L1).Visible = True
          menH3shortcut(L1).Caption = ComTranzList(2, I)
          menH3shortcut(L1).Tag = I
          L1 = L1 + 1
        Else
          menH3Subsc(L2).Visible = True
          menH3Subsc(L2).Caption = Mid(ComTranzList(2, I), 2)
          menH3Subsc(L2).Tag = I
          L2 = L2 + 1
        End If
      End If
      If (Val(ComTranzList(3, I)) > 10 And FrontCFrame.FrameType = cfRoom) Or Val(ComTranzList(3, I)) = 12 Then
        If Left(ComTranzList(2, I), 1) <> ">" Then
          menH2shortcut(J1).Visible = True
          menH2shortcut(J1).Caption = ComTranzList(2, I)
          menH2shortcut(J1).Tag = I
          J1 = J1 + 1
        Else
          menH2Subsc(J2).Visible = True
          menH2Subsc(J2).Caption = Mid(ComTranzList(2, I), 2)
          menH2Subsc(J2).Tag = I
          J2 = J2 + 1
        End If
      End If
    End If
  Next
  
  menH3strich1.Visible = L1 > 0 Or L2 > 0
  menH3Sub.Visible = L2 > 0
  menH2strich3.Visible = J1 > 0 Or J2 > 0
  menH2Sub.Visible = J2 > 0
  
  For I = L1 To menH3shortcut.Count - 1
    If I > 0 Then Unload menH3shortcut(I) Else menH3shortcut(I).Visible = False
  Next
  For I = L2 To menH3Subsc.Count - 1
    If I > 0 Then Unload menH3Subsc(I)
  Next
  For I = J1 To menH2shortcut.Count - 1
    If I > 0 Then Unload menH2shortcut(I) Else menH2shortcut(I).Visible = False
  Next
  For I = J2 To menH2Subsc.Count - 1
    If I > 0 Then Unload menH2Subsc(I)
  Next
End Sub

Private Sub menH3copyname_Click()
  SetClipboardText LastTrigger, Me.hWnd
End Sub

Private Sub menH3dccc_Click()
  If FrontCFrame.ConnTyp = 0 Then
    OpenDCC FrontCFrame.IrcConn.Nick, LastTrigger, "chat", "CHAT", FrontCFrame.IrcConn, 0
  End If
End Sub

Private Sub menH3file_Click()
  If FrontCFrame.ConnTyp = 0 Then
    CSendFile FrontCFrame.IrcConn.Nick, LastTrigger, FrontCFrame.IrcConn, 0
  End If
End Sub

Private Sub menH3filepast_Click()
  Dim DAT As String
  If Clipboard.GetFormat(vbCFBitmap) = False Or FrontCFrame.UListF Is Nothing Then Exit Sub
  DAT = UserPath + "ClipboadPicture" + CStr(Int(Rnd * 1000)) + ".bmp"
  On Error Resume Next
  SavePicture Clipboard.GetData(vbCFBitmap), DAT
  On Error GoTo 0
  If FrontCFrame.ConnTyp = 0 Then
    OpenDCC FrontCFrame.IrcConn.Nick, LastTrigger, DAT, "SEND", FrontCFrame.IrcConn, 0, FrontCFrame, True
  End If
End Sub

Private Sub menH3private_Click()
  FrontCFrame.PrivateFrame LastTrigger
End Sub

Private Sub menH3shortcut_Click(Index As Integer)
  If FrontCFrame.ConnTyp = 0 Then
    FrontCFrame.IrcConn.SendComm "/// " + LastTrigger + " " + WisperTo, FrontCFrame, menH3shortcut(Index).Tag
    If FrontCFrame.TCHange And Not FrontCFrame.TextF Is Nothing Then FrontCFrame.TextF.Refresh: FrontCFrame.TCHange = False
  End If
End Sub

Private Sub menH3Subsc_Click(Index As Integer)
  If FrontCFrame.ConnTyp = 0 Then
    FrontCFrame.IrcConn.SendComm "/// " + LastTrigger + " " + WisperTo, FrontCFrame, menH3Subsc(Index).Tag
    If FrontCFrame.TCHange And Not FrontCFrame.TextF Is Nothing Then FrontCFrame.TextF.Refresh: FrontCFrame.TCHange = False
  End If
End Sub

Private Sub menH2shortcut_Click(Index As Integer)
  If FrontCFrame.FrameType = cfRoom Or FrontCFrame.FrameType = cfStatus Or FrontCFrame.FrameType = cfPrivate Then
    FrontCFrame.IrcConn.SendComm "/// " + FrontCFrame.Caption + " " + WisperTo, FrontCFrame, menH2shortcut(Index).Tag
    If FrontCFrame.TCHange Then FrontCFrame.TextF.Refresh: FrontCFrame.TCHange = False
  End If
End Sub

Private Sub menH2Subsc_Click(Index As Integer)
  If FrontCFrame.FrameType = cfRoom Or FrontCFrame.FrameType = cfStatus Or FrontCFrame.FrameType = cfPrivate Then
    FrontCFrame.IrcConn.SendComm "/// " + FrontCFrame.Caption + " " + WisperTo, FrontCFrame, menH2Subsc(Index).Tag
    If FrontCFrame.TCHange Then FrontCFrame.TextF.Refresh: FrontCFrame.TCHange = False
  End If
End Sub

Private Sub menH3wisper_Click()
  If FrontCFrame.FrameType <> cfRoom Then Exit Sub
  FrontCFrame.UListF.GotoItem LastTrigger
  FrontCFrame.UListF.ScrollToIndex
  FrontCFrame.UListF.Refresh
  If FrontCFrame.UListF.ListIndex > -1 Then
    WisperTo = FrontCFrame.IrcConn.ChatNoSign(FrontCFrame.UListF.List(FrontCFrame.UListF.ListIndex))
    Form1.Check1.Visible = True
    Check1.Value = 1
    Form1.TfRedraw
  End If
End Sub

Private Sub menH4ibalken_Click()
  UModeBalken = UModeBalken = False
  menH4ibalken.Checked = UModeBalken
End Sub

Private Sub menH4oncreeen_Click()
  OsdModeOnOff
End Sub

Public Sub OsdModeOnOff()
  UModeOSD = UModeOSD = False
  menH4oncreeen.Checked = UModeOSD
  If UModeOSD = True Then
    Set OnScr = New OSDisplay
  Else
    If Not OnScr Is Nothing Then
      OnScr.StopOSD
      Set OnScr = Nothing
    End If
  End If
End Sub

Private Sub menH4open_Click()
  ShowMainWindow
End Sub

Public Sub ShowMainWindow()
  Dim I As Integer
  
  Unload Form4
  If OpFormIsLoaded = True Then
    For I = 0 To Forms.Count - 1
      If Forms(I).Name <> "Form1" And Forms(I).Name <> "Form5" Then Exit For
    Next
    If I > Forms.Count Then Form5.SetFocus
  Else
    If Me.Visible = False Then
      If LastWState = 1 Then Me.WindowState = 2 Else Me.WindowState = 0
    Else
      If Me.WindowState = 1 Then Me.WindowState = 0
    End If
    Me.Visible = True
    SetForegroundWindow Me.hWnd
    IsNowActiv
  End If
End Sub

Public Sub IsNowActiv()
  If TrayIconState > 0 Then
    T.hIcon = TrayIcon(1)
    Shell_NotifyIcon NIM_MODIFY, T
    TrayIconState = 0
  End If
End Sub

Private Sub menH4options_Click()
  If Form1.Visible = True Then
    Form5.Show 1, Form1
  Else
    Form5.Show 0
  End If
End Sub

Public Sub GlobalColorRefresh()
  UnloadSelectionDraw
  If GetColorLightnes(Form1.pUList.ForeColor) < 128 Then InitSelectionDraw Me.hWnd

  pDiff.BackColor = CoVal.BackColor
  pDiff2.BackColor = CoVal.BackColor
  Form1.BackColor = CoVal.BackColor
  pMain.BackColor = CoVal.BackColor
  Form_Resize
  pMain_Resize
  pUList_Resize
  pTBox_Resize
  pState_Resize
  MenuBar.Refresh
End Sub

Private Sub menH4quit_Click()
  Unload Me
End Sub

Public Sub menH4script_Click()
  ShowScript
End Sub

Private Sub menH4stumm_Click()
  SoundStumm = SoundStumm = False
  menH4stumm.Checked = SoundStumm
End Sub

Private Sub menH5list_Click(Index As Integer)
  Dim SortedC() As Integer
  GiveSortedC SortedC
  If Index + 1 <= UBound(SortedC) Then
    If SortedC(Index + 1) <= ChatFrames.Count Then
      SetFrontFrame ChatFrames(SortedC(Index + 1))
    End If
  End If
End Sub

Private Sub menH6alldel_Click()
  Dim I As Integer
  
  For I = DCCFileConns.Count To 1 Step -1
    If DCCFileConns(I).State = 1 Then
      If DCCFileConns(I).ByteCount < DCCFileConns(I).FileSize _
      And DCCFileConns(I).SendGetMode > 0 Then
        On Error Resume Next
        Kill DCCFileConns(I).DestPath + ".dcc"
        On Error GoTo 0
      End If
      DCCFileConns.Remove I
      RefrDCCFileList Nothing, True
    End If
  Next
  If DCCFileConns.Count = 0 And Not DCCFileFrame Is Nothing Then
    DCCFileFrame.QuitFrame
    Set DCCFileFrame = Nothing
  End If
End Sub

Private Sub menH6brake_Click()
  If DCCFileConns.Count >= SelledFC Then
    If DCCFileConns(SelledFC).State = 4 Then
      menH6brake.Checked = menH6brake.Checked = False
      DCCFileConns(SelledFC).SlowMode = menH6brake.Checked
    End If
  End If
End Sub

Private Sub menH6del_Click()
  If DCCFileConns.Count >= SelledFC Then
    If DCCFileConns(SelledFC).State = 1 Then
      If DCCFileConns(SelledFC).ByteCount < DCCFileConns(SelledFC).FileSize _
      And DCCFileConns(SelledFC).SendGetMode > 0 Then
        On Error Resume Next
        Kill DCCFileConns(SelledFC).DestPath + ".dcc"
        On Error GoTo 0
      End If
      DCCFileConns.Remove SelledFC
      RefrDCCFileList Nothing, True
      If DCCFileConns.Count = 0 And Not DCCFileFrame Is Nothing Then
        DCCFileFrame.QuitFrame
        Set DCCFileFrame = Nothing
      End If
    End If
  End If
End Sub

Private Sub menH6disconn_Click()
  If DCCFileConns.Count >= SelledFC Then
    If DCCFileConns(SelledFC).State > 1 Then
      DCCFileConns(SelledFC).CloseConnection
    End If
  End If
End Sub

Private Sub menH6open_Click()
  If DCCFileConns.Count >= SelledFC Then
    If DCCFileConns(SelledFC).State = 1 And DCCFileConns(SelledFC).FileSize = DCCFileConns(SelledFC).ByteCount Then
      CallWebSite DCCFileConns(SelledFC).DestPath
    End If
  End If
End Sub

Private Sub menH6opendir_Click()
  If DCCFileConns.Count >= SelledFC Then
    CallWebSite CutDirFromPath(DCCFileConns(SelledFC).DestPath)
  End If
End Sub

Private Sub menH6retry_Click()
  If DCCFileConns.Count >= SelledFC Then
    If DCCFileConns(SelledFC).SendGetMode = 0 Then
      DCCFileConns(SelledFC).RequestFile
    Else
      LastCommandLine = DCCFileConns(SelledFC).NoteCType + DCCFileConns(SelledFC).NoteServer + _
      ":" + CStr(DCCFileConns(SelledFC).NotePort) + "/" + DCCFileConns(SelledFC).NoteRoom
      PhraseCLine DCCFileConns(SelledFC).RequCommand
    End If
  End If
End Sub

Private Sub menH6values_Click()
  Form7.Show 1
End Sub

Private Sub menH7del_Click()
  Dim I As Integer
  
  If FrontCFrame Is Nothing Then Exit Sub
  I = FrontCFrame.UListF.ListIndex
  If I > -1 Then
    RemWatchedUser FrontCFrame.UListF.GetListText(I, 0), FrontCFrame.UListF.GetListText(I, 2)
  End If
End Sub

Private Sub menH7private_Click()
  Dim I As Integer
  
  If FrontCFrame Is Nothing Then Exit Sub
  I = FrontCFrame.UListF.ListIndex
  If I > -1 Then
    LastCommandLine = "irc://" + FrontCFrame.UListF.GetListText(I, 2) _
    + "//" + FrontCFrame.UListF.GetListText(I, 0)
    PhraseCLine
  End If
End Sub

Private Sub menH7refresh_Click()
  Dim I As Integer
  
  If FrontCFrame Is Nothing Then Exit Sub
  I = FrontCFrame.UListF.ListIndex
  If I > -1 Then
    If RequestWUState(FrontCFrame.UListF.GetListText(I, 2)) = False Then
      LastCommandLine = "irc://" + FrontCFrame.UListF.GetListText(I, 2)
      PhraseCLine
    End If
  End If
End Sub

Private Sub menHclose_Click()
  If Not SellCFrame Is Nothing Then SellCFrame.CloseFrame
End Sub

Private Sub menHcopy_Click()
  Dim I As Integer
  Dim StFrame As ChatFrame
  For I = 1 To ChatFrames.Count
    Set StFrame = ChatFrames(I)
    If StFrame.FrameType = cfServerList Then Exit For
  Next
  If StFrame.UListF.ListIndex > -1 And StFrame.UListF.ListIndex < StFrame.UListF.ListCount Then
    I = ConnID(StFrame.UListF.ListIndex)
    Load Form2
    Form2.Check1.Visible = True
    Form2.Text3 = CheckConns(ConnList(I).Caption)
    Form2.Text1 = ConnList(I).Server
    Form2.Text2 = ConnList(I).Port
    Form2.Text6 = ConnList(I).Nick
    Form2.Text4 = ConnList(I).Pass
    Form2.Text10 = ConnList(I).Commands
    Form2.Check2.Value = ConnList(I).ServerMode
    Form2.Check3.Value = Abs(ConnList(I).UTF8dyn)
    Form2.Check4.Value = ConnList(I).UseSSL
    Form2.Check5.Value = Abs(ConnList(I).UTF8)
    Form2.Caption = TeVal.Caption1
    Form2.Text5 = ConnList(I).Name
    Form2.Text7 = ConnList(I).UserID
    Form2.Text12 = ConnList(I).ServerPass
    If "Server:" + Form2.Text1 = Left(Form2.Text3, Len(Form2.Text1.Text) + 7) Then Form2.Option1 = True
    Form2.LoadList
    Form2.Show 1
    If Len(Form2.Text3.Text) > 0 Then
      ReDim Preserve ConnList(ConnListCount)
      I = ConnListCount
      ConnListCount = ConnListCount + 1
      If Form2.Option1.Value Then
        ConnList(I).Caption = CheckConns("Server:" + Form2.Text1.Text)
      Else
        ConnList(I).Caption = CheckConns(Form2.Text3)
      End If
      ConnList(I).Name = Form2.Text5
      ConnList(I).ServerPass = Form2.Text12
      ConnList(I).UserID = Form2.Text7
      ConnList(I).ServerMode = Form2.Check2.Value
      ConnList(I).UTF8 = Abs(Form2.Check5.Value)
      ConnList(I).UTF8dyn = Abs(Form2.Check3.Value)
      ConnList(I).UseSSL = Form2.Check4.Value
      ConnList(I).Server = Form2.Text1
      ConnList(I).Port = Form2.Text2
      ConnList(I).Nick = Form2.Text6
      ConnList(I).Pass = Form2.Text4
      ConnList(I).CreateDate = Now
      ConnList(I).Commands = Form2.Text10
      UTF8on = CBool(ConnList(I).UTF8dyn)
      If Len(ConnList(I).Commands) > 1 Then
        Do While Right(ConnList(I).Commands, 2) = vbNewLine
          ConnList(I).Commands = Left(ConnList(I).Commands, Len(ConnList(I).Commands) - 2)
        Loop
      End If
      RefreshList
      If Form2.Check1 = 1 Then StartConnection I
    End If
    Unload Form2
  End If
End Sub

Private Sub menHdel_Click()
  Dim I As Integer
  For I = 1 To ChatFrames.Count
    If ChatFrames(I).FrameType = cfServerList Then
      If ChatFrames(I).UListF.ListIndex > -1 Then
        RemoveServer ConnID(ChatFrames(I).UListF.ListIndex), True
        RefreshList
        Exit For
      End If
    End If
  Next
End Sub

Public Sub RemoveServer(ListIndex As Integer, Optional AskBeforeDel As Boolean)
  Dim I As Integer
  Dim I2 As Integer
  
  If ListIndex > -1 And ListIndex < ConnListCount Then
    If ConnList(ListIndex).State <> 0 Then MsgBox TeVal.msgNoDel, vbInformation: Exit Sub
    If AskBeforeDel Then If MsgBox(TeVal.msgDel, vbQuestion + vbYesNo) = vbNo Then Exit Sub
    For I = ListIndex To ConnListCount - 2
      For I2 = 1 To IRCconns.Count
        If IRCconns(I2).ListID = I + 1 Then IRCconns(I2).ListID = I: Exit For
      Next
      ConnList(I) = ConnList(I + 1)
    Next
    ConnListCount = ConnListCount - 1
    If ConnListCount > 0 Then ReDim Preserve ConnList(ConnListCount - 1)
  End If
End Sub

Private Sub menHedit_Click()
  Dim I As Integer
  Dim I2 As Integer
  Dim ConnI As Integer
  Dim StFrame As ChatFrame
  
  For I = 1 To ChatFrames.Count
    Set StFrame = ChatFrames(I)
    If StFrame.FrameType = cfServerList Then Exit For
  Next
  
  If SellCFrame Is Nothing Then
    If StFrame.UListF.ListIndex < 0 Or StFrame.UListF.ListIndex >= StFrame.UListF.ListCount Then Exit Sub
    I = ConnID(StFrame.UListF.ListIndex)
  Else
    I = 0
    If SellCFrame.ConnTyp = 0 Then ' #IRC
      For I = 1 To IRCconns.Count
        If IRCconns(I).ListID = SellCFrame.IrcConn.ListID Then Exit For
      Next
      If I > IRCconns.Count Then Exit Sub
      I = IRCconns(I).ListID
    Else
      Exit Sub
    End If
  End If
  
  For ConnI = 1 To IRCconns.Count
    If IRCconns(ConnI).ListID = I Then Exit For
  Next
  If ConnI <= IRCconns.Count Then
    ConnList(I).Nick = IRCconns(ConnI).Nick
    ConnList(I).Pass = IRCconns(ConnI).NickPass
    ConnList(I).UserID = IRCconns(ConnI).UserID
    ConnList(I).Name = IRCconns(ConnI).Realname
    ConnList(I).ServerMode = IRCconns(ConnI).JoinMode
    ConnList(I).UTF8 = CInt(IRCconns(ConnI).UTF8)
    ConnList(I).UTF8dyn = CInt(IRCconns(ConnI).UTF8dyn)
  End If
  Load Form2
  Form2.Text3 = ConnList(I).Caption
  Form2.Check1.Visible = False
  Form2.Text1 = ConnList(I).Server
  Form2.Text2 = ConnList(I).Port
  Form2.Text6 = ConnList(I).Nick
  Form2.Text4 = ConnList(I).Pass
  Form2.Text10 = ConnList(I).Commands
  Form2.Check2.Value = ConnList(I).ServerMode
  Form2.Check3.Value = Abs(ConnList(I).UTF8dyn)
  Form2.Check4.Value = ConnList(I).UseSSL
  Form2.Check5.Value = Abs(ConnList(I).UTF8)
  Form2.Caption = TeVal.Caption2
  Form2.Text5 = ConnList(I).Name
  Form2.Text7 = ConnList(I).UserID
  Form2.Text12 = ConnList(I).ServerPass
  Form2.Check2.Enabled = True
  
  If Form2.Check5.Value = 1 And Form2.Check3.Value = 1 Then
    Form2.Check3.Enabled = False
  Else
    Form2.Check3.Enabled = True
  End If
  
  If "Server:" + Form2.Text1 = Left(Form2.Text3, Len(Form2.Text1.Text) + 7) Then Form2.Option1 = True
  Form2.LoadList
  Form2.Show 1
  
  If Len(Form2.Text3.Text) > 0 Then
    If UCase(ConnList(I).Caption) <> UCase(Form2.Text3) Then ConnList(I).Caption = CheckConns(Form2.Text3)
    If Form2.Option1.Value Then
      If UCase(ConnList(I).Caption) <> UCase("Server:" + Form2.Text1.Text) Then ConnList(I).Caption = CheckConns("Server:" + Form2.Text1.Text)
    Else
      If UCase(ConnList(I).Caption) <> UCase(Form2.Text3) Then ConnList(I).Caption = CheckConns(Form2.Text3)
    End If
    ConnList(I).Name = Form2.Text5
    ConnList(I).ServerPass = Form2.Text12
    ConnList(I).UserID = Form2.Text7
    ConnList(I).ServerMode = Form2.Check2.Value
    ConnList(I).UTF8dyn = Abs(Form2.Check3.Value)
    ConnList(I).UTF8 = Abs(Form2.Check5.Value)
    ConnList(I).UseSSL = Form2.Check4.Value
    ConnList(I).Server = Form2.Text1
    ConnList(I).Port = Form2.Text2
    If Not ConnList(I).Nick Like Form2.Text6 Then ConnList(I).Nick = Form2.Text6
    ConnList(I).Pass = Form2.Text4
    ConnList(I).Commands = Form2.Text10
    UTF8on = CBool(ConnList(I).UTF8dyn)
    If Len(ConnList(I).Commands) > 1 Then
      Do While Right(ConnList(I).Commands, 2) = vbNewLine
        ConnList(I).Commands = Left(ConnList(I).Commands, Len(ConnList(I).Commands) - 2)
      Loop
    End If

    For ConnI = 1 To IRCconns.Count
      If IRCconns(ConnI).ListID = I Then Exit For
    Next
    If ConnI <= IRCconns.Count Then
      If IRCconns(ConnI).Nick <> ConnList(I).Nick And IRCconns(ConnI).State = 4 Then
        IRCconns(ConnI).SendLine "nick " + ConnList(I).Nick
      Else
        IRCconns(ConnI).Nick = ConnList(I).Nick
      End If
      
      IRCconns(ConnI).Caption = ConnList(I).Caption
      IRCconns(ConnI).NickPass = ConnList(I).Pass
      IRCconns(ConnI).UserID = ConnList(I).UserID
      IRCconns(ConnI).ServerPass = ConnList(I).ServerPass
      IRCconns(ConnI).Realname = ConnList(I).Name
      IRCconns(ConnI).StartCommands = ConnList(I).Commands
      IRCconns(ConnI).JoinMode = ConnList(I).ServerMode
      IRCconns(ConnI).UTF8dyn = ConnList(I).UTF8dyn
      
      If IRCconns(ConnI).Server <> ConnList(I).Server Or _
        IRCconns(ConnI).Port <> ConnList(I).Port Or _
        Abs(CInt(IRCconns(ConnI).UTF8)) <> ConnList(I).UTF8 Then
        If IRCconns(ConnI).State > 1 Then
          IRCconns(ConnI).CloseConnection
          IRCconns(ConnI).Server = ConnList(I).Server
          IRCconns(ConnI).Port = ConnList(I).Port
          If Not IRCconns(ConnI).UTF8 = CBool(ConnList(I).UTF8) Then ConnList(I).UTF8dyn = Abs(CBool(ConnList(I).UTF8))
          IRCconns(ConnI).UTF8 = CBool(ConnList(I).UTF8)
          IRCconns(ConnI).Connect
        Else
          IRCconns(ConnI).Server = ConnList(I).Server
          IRCconns(ConnI).Port = ConnList(I).Port
          If Not IRCconns(ConnI).UTF8 = CBool(ConnList(I).UTF8) Then ConnList(I).UTF8dyn = Abs(CBool(ConnList(I).UTF8))
          IRCconns(ConnI).UTF8 = CBool(ConnList(I).UTF8)
        End If
      End If
      For I = 1 To ChatFrames.Count
        If ChatFrames(I).IrcConn Is IRCconns(ConnI) And ChatFrames(I).FrameType = cfStatus Then
          ChatFrames(I).Caption = IRCconns(ConnI).Caption
          Exit For
        End If
      Next
    End If
    RefreshList
    ColumRedraw False
  End If
  Unload Form2
End Sub

Private Sub menHilfeHilfe_Click()
  HTMLHelp_ShowTopic
End Sub

Private Sub menHilfeInfo_Click()
  Form6.Show 1
End Sub

Private Sub menHilfeUpdate_Click()
  ShowUpdater
End Sub

Private Sub menHlog_Click()
  If Not SellCFrame Is Nothing Then
    LoadFromLog SellCFrame.Caption, SellCFrame.FrameType
  End If
End Sub

Private Sub menHneu_Click()
  mVnewIRC_Click
End Sub

Private Sub menHprop_Click()
  Form8.Show 1, Me
End Sub

Private Sub menHpublicnotice_Click()
  If Not SellCFrame Is Nothing Then
    menHpublicnotice.Checked = AddRemOsdRL(SellCFrame.Caption)
  End If
End Sub

Private Sub menHsilent_Click()
  If Not SellCFrame Is Nothing Then
    menHsilent.Checked = menHsilent.Checked = False
    If SellCFrame.ConnTyp = 0 Then ' #IRC
      SellCFrame.IrcConn.Silent = menHsilent.Checked
    End If
  End If
End Sub

Private Sub menHtrennen_Click()
  If Not SellCFrame Is Nothing Then
    If SellCFrame.ConnTyp = 0 Then ' #IRC
      SellCFrame.IrcConn.CloseConnection
    End If
  End If
End Sub

Private Sub menHverbinden_Click()
  StartConnection
End Sub

Private Sub menImConns_Click()
  Dim File As String
  On Error Resume Next
  Do
    File = ShowOpen("srv-files" + Chr(0) + "*.srv", Me.hWnd, LastLPath)
    If Len(File) = 0 Then Exit Sub
  Loop While Dir(File) = ""
  If Err <> 0 Then MsgBox Err.Description, vbCritical
  On Error GoTo 0
  LoadValues File
  SaveValues
  LastLPath = CutDirFromPath(File)
End Sub

Private Sub menLListe_Click(Index As Integer)
  Dim I As Integer
  If Len(Dir(AppPath + menLListe(Index).Caption + ".ulng")) > 0 Then
    For I = 0 To menLListe.Count - 1
      menLListe(I).Checked = I = Index
    Next
    SaveNextLangw menLListe(Index).Caption + ".ulng"
  End If
End Sub

Private Sub menOEdit_Click()
  If Form5.Visible = True Then
    SetForegroundWindow Form5.hWnd
  Else
    Form5.Show 1
  End If
End Sub

Private Sub menQuit_Click()
  Unload Me
End Sub

Private Sub menSaveText_Click()
  Dim DAT As String
  Dim ExIndex As Integer
  If FrontCFrame.TextF Is Nothing Then Exit Sub
  ExIndex = 1
  Do
    ShowSave DAT, "HTML" + Chr(0) + "*.htm" + Chr(0) + "Text" + Chr(0) + "*.txt" + Chr(0) + "*.*" + Chr(0) + "*.*", LastSPath, "Chat", ExIndex, Me.hWnd
    If DAT = "" Then Exit Sub
    If Mid(DAT, Len(DAT) - 3, 1) <> "." And Mid(DAT, Len(DAT) - 4, 1) <> "." Then
      If ExIndex = 1 Then
        DAT = DAT + ".htm"
      ElseIf ExIndex = 2 Then
        DAT = DAT + ".txt"
      End If
    End If
    On Error Resume Next
    If Dir(DAT) <> "" Then
      On Error GoTo 0
      If MsgBox(TeVal.FileExist, vbQuestion + vbYesNo) = vbYes Then Exit Do
    Else
      On Error GoTo 0
      Exit Do
    End If
  Loop
  LastSPath = CutDirFromPath(DAT)
  If LCase(Right(DAT, 4)) = ".htm" Or LCase(Right(DAT, 5)) = ".html" Or ExIndex = 1 Then
    FrontCFrame.TextF.ShowAsHTML DAT
  Else
    FrontCFrame.TextF.ShowAsTXT DAT
  End If
End Sub

Private Sub menSEdit_Click()
  ShowScript
End Sub

Public Sub ShowScript()
  Dim CFrame As New ChatFrame
  Dim I As Integer
  If Me.Visible = False Then ShowMainWindow
  I = 0
  For Each CFrame In ChatFrames
    If CFrame.FrameType = cfScript Then
      SetFrontFrame CFrame
      I = 1
      Exit For
    End If
  Next
  If I = 0 Then
    CFrame.Caption = "Script"
    CFrame.Inst cfScript, -1
    ChatFrames.Add CFrame
    SetScriptText ntScript
    SetFrontFrame CFrame
  End If
End Sub

Private Sub menSExport_Click()
  Dim DAT As String
  Dim I As Integer
  Do
    ShowSave DAT, "Script" + Chr(0) + "*.ntscript" + Chr(0) + "Text" + Chr(0) + "*.txt", LastSPath, "Nettalk-Script.ntscript", I, Me.hWnd
    If DAT = "" Then Exit Sub
    If Mid(DAT, Len(DAT) - 3, 1) <> "." And Mid(DAT, Len(DAT) - 4, 1) <> "." Then
        DAT = DAT + ".txt"
    End If
    On Error Resume Next
    If Dir(DAT) <> "" Then
      On Error GoTo 0
      If MsgBox(TeVal.FileExist, vbQuestion + vbYesNo) = vbYes Then Exit Do
    Else
      On Error GoTo 0
      Exit Do
    End If
  Loop
  On Error Resume Next
  FileCopy UserPath + "script.txt", DAT
  If Err <> 0 Then MsgBox Err.Description, vbCritical
  On Error GoTo 0
  LastSPath = CutDirFromPath(DAT)
End Sub

Private Sub menSImport_Click()
  Dim File As String
  On Error Resume Next
  Do
    File = ShowOpen("Script" + Chr(0) + "*.ntscript" + Chr(0) + "Text" + Chr(0) + "*.txt" + Chr(0) + "*.*" + Chr(0) + "*.*", Me.hWnd, LastLPath)
    If File = "" Then Exit Sub
  Loop While Dir(File) = ""
  LoadScriptFile File
  If Err <> 0 Then MsgBox Err.Description, vbCritical
  On Error GoTo 0
  LastLPath = CutDirFromPath(File)
  reStartScript
End Sub

Private Sub menSStart_Click()
  reStartScript
End Sub

Public Sub reStartScript()
  Dim I As Integer
  For I = 1 To ChatFrames.Count
    If ChatFrames(I).FrameType = cfScript Then Exit For
  Next
  ntScript_Unload
  If Not I > ChatFrames.Count Then ntScript = Form1.Text2.Text
  SaveScriptFile
  ntScript_Reset
End Sub

Private Sub menSStop_Click()
  ntScript_Unload
End Sub

Public Sub ShowCValues()
  Dim StFrame As ChatFrame
  Dim I As Integer
  If FrontCFrame.FrameType <> cfServerList Then
    For I = 1 To ChatFrames.Count
      Set StFrame = ChatFrames(I)
      If StFrame.FrameType = cfServerList Then Exit For
    Next
    For I = 0 To StFrame.UListF.ListCount - 1
      If FrontCFrame.ConnTyp = 0 Then
        If ConnID(I) = FrontCFrame.IrcConn.ListID Then Exit For
      End If
    Next
    If I < StFrame.UListF.ListCount Then StFrame.UListF.ListIndex = I: menHedit_Click
  Else
    menHedit_Click
  End If
End Sub

Private Sub MenuBar_Click(Index As Integer, Button As Integer)
  Select Case Index
    Case 1
      mVnewIRC_Click
    Case 2
      menHdel_Click
    Case 3
      ShowCValues
    Case 4
      menBcut_Click
    Case 5
      menBcopy_Click
    Case 6
      menBpast_Click
    Case 7
      If FrontCFrame.ConnTyp = 0 Then
        FrontCFrame.IrcConn.Connect
      ElseIf FrontCFrame.ConnTyp = 2 Then
        FrontCFrame.DCCConn.Connect
      End If
    Case 8
      If FrontCFrame.ConnTyp = 0 Then
        FrontCFrame.IrcConn.CloseConnection
      ElseIf FrontCFrame.ConnTyp = 2 Then
        FrontCFrame.DCCConn.CloseConnection
      End If
    Case 9
      If FrontCFrame.ConnTyp = 0 Then
        'If FrontCFrame.IrcConn.ChannelCount > 10000 Then
        '  FrontCFrame.SendM "/list >9"
        'ElseIf FrontCFrame.IrcConn.ChannelCount > 500 Then
        '  FrontCFrame.SendM "/list >1"
        'Else
          FrontCFrame.SendM "/list"
        'End If
      End If
    Case 11
      menOEdit_Click
    Case 12
      menSEdit_Click
    Case 13
      ShowFriedsList
    Case 14
      If FrontCFrame.ConnTyp = 0 Then
        CSendFile FrontCFrame.IrcConn.Nick, "", FrontCFrame.IrcConn, 0
      End If
    Case 15
      menHilfeHilfe_Click
    Case 16
      Me.Visible = False
    Case 17
      HideShowMenue Form1.mDatei.Visible = False
    Case 18
      reStartScript
    Case 19
      menSStop_Click
  End Select
End Sub

Private Sub HideShowMenue(menVisible As Boolean)
  Form1.mBearbeiten.Visible = menVisible
  Form1.mDatei.Visible = menVisible
  Form1.mVerbindungen.Visible = menVisible
  Form1.menHilfe.Visible = menVisible
  Form1.menScript.Visible = menVisible
  Form1.menLangw.Visible = menVisible
  MenuBar.SetIcon 17, 27 + CInt(Form1.mDatei.Visible)
  MenuBar.Refresh
  MenuBar.ClearIndex
End Sub



Private Sub mVnewIRC_Click()
  Dim I As Integer
  Dim StFrame As ChatFrame
  Load Form2
  Form2.Text3 = CheckConns(TeVal.NewCTitel)
  Form2.Check1.Visible = True
  Form2.Text7 = LastUsedUserID
  Form2.Text1 = ""
  Form2.Text6 = LastUsedNick
  Form2.Text4 = ""
  Form2.Text5 = LastUsedRealName
  Form2.Text12 = ""
  Form2.Caption = TeVal.Caption1
  Form2.Option1.Value = True
  Form2.Check3.Value = Abs(CInt(UTF8on))
  Form2.Check5.Value = 0
  Form2.Check4.Value = 0
  Form2.LoadList
  Form2.Show 1
  If Form2.Text3.Text <> "" Then
    ReDim Preserve ConnList(ConnListCount)
    I = ConnListCount
    ConnListCount = ConnListCount + 1
    If Form2.Option1.Value Then
      ConnList(I).Caption = CheckConns("Server:" + Form2.Text1.Text)
    Else
      ConnList(I).Caption = CheckConns(Form2.Text3)
    End If
    ConnList(I).Name = Form2.Text5
    ConnList(I).ServerPass = Form2.Text12
    ConnList(I).UserID = Form2.Text7
    ConnList(I).ServerMode = Form2.Check2.Value
    ConnList(I).UTF8 = Abs(Form2.Check5.Value)
    ConnList(I).UTF8dyn = Abs(Form2.Check3.Value)
    ConnList(I).Server = Form2.Text1
    ConnList(I).Port = Form2.Text2
    ConnList(I).Nick = Form2.Text6
    ConnList(I).Pass = Form2.Text4
    ConnList(I).CreateDate = Now
    ConnList(I).Commands = Form2.Text10
    ConnList(I).Codepage = BasicCodepage
    ConnList(I).UseSSL = Form2.Check4.Value
    UTF8on = CBool(ConnList(I).UTF8dyn)
    If Len(ConnList(I).Commands) > 1 Then
      Do While Right(ConnList(I).Commands, 2) = vbNewLine
        ConnList(I).Commands = Left(ConnList(I).Commands, Len(ConnList(I).Commands) - 2)
      Loop
    End If
    If Form2.Check1.Value = 1 Then StartConnection I
    RefreshList
  End If
  Unload Form2
  SaveValues
End Sub

Private Sub mVnewtpc_Click()
  Load Form3
  Form3.Picture1.Visible = True
  Form3.Picture2.Visible = False
  Form3.Text1.TabIndex = 0
  Form3.Text2.TabIndex = 1
  Form3.Show 1
  If Len(Form3.Text1.Text) Then
    StartDCCConnection "DCC:CHAT Server " + Form3.Text1.Text + " " + CStr(Val(Form3.Text2.Text)) + " -1 chat Client", , 1
  End If
  Unload Form3
End Sub

Private Sub pDiff_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 1 Then pDiff.BackColor = &H8000000C
End Sub

Private Sub pDiff_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 1 And pDiff.BackColor = &H8000000C Then pDiff.Left = pDiff.Left + X - pDiff.Width / 2
End Sub

Private Sub pDiff_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If pDiff.BackColor <> &H8000000C Then Exit Sub
  If pMain.ScaleWidth < pDiff.Left + pDiff.Width + 20 Then pDiff.Left = pMain.ScaleWidth - pDiff.Width - 20
  pUList.Width = pMain.ScaleWidth - pDiff.Left - pDiff.Width - 1
  pMain_Resize
  pDiff.BackColor = CoVal.BackColor
  If Me.Visible = True Then Text1.SetFocus
End Sub

Private Sub pDiff2_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 1 Then pDiff2.BackColor = &H8000000C
End Sub

Private Sub pDiff2_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 1 And pDiff2.BackColor = &H8000000C Then pDiff2.Top = pDiff2.Top + Y - pDiff2.Height / 2
End Sub

Private Sub pDiff2_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If pDiff2.BackColor <> &H8000000C Then Exit Sub
  If Me.ScaleHeight - pDiff2.Top - pState.Height - 2 > 16 Then
    pTexthg.Height = Me.ScaleHeight - pDiff2.Top - pState.Height - 2
  Else
    pTexthg.Height = 16
  End If
  Form_Resize
  pDiff2.BackColor = CoVal.BackColor
  If Me.Visible = True Then Text1.SetFocus
End Sub

Private Function PosToIndex(X As Single) As Integer
  Dim Rind() As Integer
  Dim I As Integer
  For I = 0 To UBound(TabList)
    If TabList(I) > X Then Exit For
  Next
  GiveSortedC Rind
  If I < ChatFrames.Count Then
    If Int(X / pTapStrip.ScaleWidth) = Int(TabList(I) / pTapStrip.ScaleWidth) Then
      PosToIndex = Rind(I + 1)
    End If
  End If
End Function

Private Sub pMain_Paint()
  pMain.Line (1, pTapStrip.Height)-(pTapStrip.ScaleWidth - 1, pTapStrip.Height + 4), CoVal.BackColor, BF
  pMain.Line (0, 0)-(0, pMain.ScaleHeight - 1), CoVal.ButtonShadow
  pMain.Line (pMain.ScaleWidth - 1, 0)-(pMain.ScaleWidth - 1, pMain.ScaleHeight - 1), CoVal.Highlight3D
  pMain.Line (0, pMain.ScaleHeight - 1)-(pMain.ScaleWidth, pMain.ScaleHeight - 1), CoVal.Highlight3D
End Sub

Private Sub pTapStrip_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Dim I As Long
  Dim TrIndex As Integer
  If Button = 1 Then
    If X > pTapStrip.ScaleWidth - 18 And X < pTapStrip.ScaleWidth - 5 And Y < 25 And ColClose.Disable = False Then
      ColClose.Down = True
      I = 1
    End If
    If I = 0 And Y < pTBox.Top Then
      TrIndex = PosToIndex(X + Int(Y / 23) * pTapStrip.ScaleWidth)
      If TrIndex > 0 And TrIndex <= ChatFrames.Count Then SetFrontFrame ChatFrames(TrIndex)
    Else
      ColumRedraw True
    End If
  End If
  If UserInfoPic.Visible = True And Y > 22 Or Y < 0 And UInfoMode = 1 Then UserInfoPic.Visible = False
End Sub

Private Sub pTapStrip_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Dim I As Integer
  Dim I2 As Integer
  Dim XPos As Long
  Dim nPoint As POINTAPI
  Dim nHWnd As Long
  Dim SortedC() As Integer
  
  GetCursorPos nPoint
  nHWnd = WindowFromPoint(nPoint.X, nPoint.Y)
  If nHWnd = pTapStrip.hWnd Then
    If GetCapture() <> nHWnd Then
      SetCapture nHWnd
    End If
    If Y < pTBox.Top Then I = PosToIndex(X + Int(Y / 23) * pTapStrip.ScaleWidth)
  Else
    ReleaseCapture
  End If
  If I > 0 Then
    If UInfoMode <> 1 Then UserInfoPic.Visible = False
    UserInfoPic.Left = 0
    UserInfoPic.FontBold = True
    UserInfoPic.Top = pTBox.Top + pMain.Top
    UserInfoPic.Height = 17
    If FrontCFrame.TextF Is Nothing Then
      UserInfoPic.Width = Me.ScaleWidth
    Else
      UserInfoPic.Width = pTBox.ScaleWidth - VScroll1.Width
    End If
    GiveSortedC SortedC
    If I > 1 Then
      For I2 = 0 To UBound(SortedC)
        If SortedC(I2) = I Then
          XPos = TabList(I2 - 2) + 6 + 16 - Int(Y / 23) * pTapStrip.ScaleWidth
          Exit For
        End If
      Next
    Else
      XPos = 8 + 16
    End If
    If XPos + TextWW(ChatFrames(I).Caption, UserInfoPic.hDC) + 3 > UserInfoPic.ScaleWidth Then XPos = UserInfoPic.ScaleWidth - TextWW(ChatFrames(I).Caption, UserInfoPic.hDC) - 3
    If XPos < 22 Then XPos = 22
    UInfoMode = 1
    UserInfoPic.Visible = True
    UserInfoPic.ForeColor = CoVal.NormalText
    UserInfoPic.Cls
    UserInfoPic.Line (0, 0)-(UserInfoPic.ScaleWidth, UserInfoPic.ScaleHeight - 1), pTBox.BackColor, BF
    'UserInfoPic.CurrentX = xPos
    'UserInfoPic.CurrentY = 2
    'UserInfoPic.Print ChatFrames(I).Caption
    TextOutW UserInfoPic.hDC, XPos, 2, StrPtr(ChatFrames(I).Caption), Len(ChatFrames(I).Caption)
    PrintIcon UserInfoPic, XPos - 18, 2, 50
    UserInfoPic.Line (0, UserInfoPic.ScaleHeight - 1)-(UserInfoPic.ScaleWidth, UserInfoPic.ScaleHeight - 1), &H80000003
    If Button = 1 Then
      If (Abs(RealDrag - X) > 10 And RealDrag > 0) Or RealDrag = -1 Then
        If StartDrag = 0 Then StartDrag = I
        If DragPos <> I Then
          DragPos = I
          ColumRedraw True
        End If
        RealDrag = -1
      End If
      If RealDrag = 0 Then RealDrag = X
    End If
  Else
    If UserInfoPic.Visible = True And UInfoMode = 1 Then UserInfoPic.Visible = False
  End If
End Sub

Private Sub pTapStrip_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Dim I As Integer
  Dim I2 As Integer
  Dim Trigg As Long
  Dim TrIndex As Integer
  Dim TempCFrame1 As ChatFrame
  Dim TempCFrame2 As ChatFrame
  
  RealDrag = 0
  Trigg = pTapStrip.ScaleWidth
  If Button = 1 Then
    If X > pTapStrip.ScaleWidth - 18 And X < pTapStrip.ScaleWidth - 5 And Y < 25 And ColClose.Disable = False Then
      FrontCFrame.CloseFrame
    End If
    ColClose.Down = False
    If StartDrag > 0 And DragPos > 0 Then
      TrIndex = PosToIndex(X + Int(Y / 23) * pTapStrip.ScaleWidth)
      
      If TrIndex > 0 And StartDrag > 1 And StartDrag <> TrIndex Then
        If ChatFrames(StartDrag).FrameType = cfStatus And ChatFrames(TrIndex).FrameType = cfRoom Then
          For I = 1 To ChatFrames.Count
            If ChatFrames(I).FrameType = cfStatus And ChatFrames(I).IrcConn Is ChatFrames(TrIndex).IrcConn Then
              TrIndex = I
              Exit For
            End If
          Next
        ElseIf ChatFrames(StartDrag).FrameType = cfStatus And ChatFrames(TrIndex).ConnTyp = -1 Then
          TrIndex = 1
        End If
        
        Set TempCFrame1 = ChatFrames(StartDrag)
        ChatFrames.Remove StartDrag
        If StartDrag > TrIndex Then
          ChatFrames.Add TempCFrame1, , , TrIndex
        Else
          ChatFrames.Add TempCFrame1, , , TrIndex - 1
        End If
      End If
      
    End If
    StartDrag = 0
    DragPos = 0
    ColumRedraw True
  ElseIf Button = 2 Then
    TrIndex = PosToIndex(X + Int(Y / 23) * pTapStrip.ScaleWidth)
    UserInfoPic.Visible = False
    If TrIndex > 1 Then
      Set SellCFrame = ChatFrames(TrIndex)
      Me.menHneu.Visible = False
      Me.menHcopy.Visible = False
      Me.menHedit.Visible = True
      Me.menHdel.Visible = False
      Me.menHstrich1.Visible = False
      Me.menHStrich2.Visible = True
      Me.menHclose.Visible = True
      Me.menHlog.Visible = False
      Me.menHpublicnotice.Visible = False
      Me.menHsilent.Visible = False
      Me.menHStrich3.Visible = False
      Me.menHtrennen.Visible = True
      Me.menHverbinden.Visible = True
      Me.menHprop.Visible = False
      Me.menHStrich4.Visible = False
      If SellCFrame.FrameType <> cfStatus Or SellCFrame.ConnTyp > 1 Then
        Me.menHedit.Visible = False
        Me.menHStrich2.Visible = False
        Me.menHtrennen.Visible = False
        Me.menHverbinden.Visible = False
      End If
      If SellCFrame.ConnTyp = SellCFrame.FrameType Or SellCFrame.FrameType = cfPrivate Then
        If SellCFrame.ConnTyp < 2 Then
          Me.menHlog.Visible = True
          Me.menHStrich3.Visible = True
        End If
      End If
      If SellCFrame.ConnTyp = 0 Then ' #IRC
        If ConnList(SellCFrame.IrcConn.ListID).State = 0 Or ConnList(SellCFrame.IrcConn.ListID).State = 3 Then
          Form1.menHverbinden.Enabled = True
          Form1.menHtrennen.Enabled = False
        Else
          Form1.menHverbinden.Enabled = False
          Form1.menHtrennen.Enabled = True
        End If
        If SellCFrame.FrameType = cfRoom Then
          menHpublicnotice.Checked = AddRemOsdRL(SellCFrame.Caption, True)
          Me.menHpublicnotice.Visible = True
          Me.menHprop.Visible = True
          Me.menHStrich4.Visible = True
        ElseIf SellCFrame.FrameType = cfStatus Then
          menHsilent.Checked = SellCFrame.IrcConn.Silent
          menHsilent.Visible = True
          Me.menHStrich3.Visible = True
        End If
      End If
      If SellCFrame.ConnTyp = 2 Then ' #DCC
        Form1.menHverbinden.Visible = False
        Form1.menHtrennen.Visible = False
        Me.menHStrich2.Visible = False
      End If
      If SellCFrame.FrameType = cfScript Then ' #Script
          Form1.menHverbinden.Visible = False
          Form1.menHtrennen.Visible = False
          Me.menHStrich2.Visible = False
      End If
      If SellCFrame.FrameType <> cfServerList And SellCFrame.FrameType <> cfDCC Then Me.PopupMenuEx Me.menH
      'Set SellCFrame = Nothing '<-
    End If
  End If
  Form1.Text1.SetFocus
  pTapStrip.MousePointer = 0
End Sub

Private Sub PDColumList(Optional KeyCall As Boolean)
  Dim I As Integer
  Dim I2 As Integer
  Dim SortedC() As Integer
  GiveSortedC SortedC
  For I = 1 To ChatFrames.Count
    If I2 > 0 Then Load menH5list(I2)
    Select Case ChatFrames(SortedC(I)).Changed
      Case 1, 2
        menH5list(I2).Caption = "~    " + ChatFrames(SortedC(I)).Caption
      Case 3
        menH5list(I2).Caption = ">    " + ChatFrames(SortedC(I)).Caption
      Case 4
        menH5list(I2).Caption = ">> " + ChatFrames(SortedC(I)).Caption
      Case Else
        menH5list(I2).Caption = "      " + ChatFrames(SortedC(I)).Caption
    End Select
    I2 = I2 + 1
  Next
  If KeyCall = True Then
    Form1.PopupMenuEx menH5, 8, Me.ScaleWidth - 4, 45
  Else
    Form1.PopupMenuEx menH5, 8
  End If
  For I = 1 To menH5list.Count - 1
    Unload menH5list(I)
  Next
End Sub

Public Sub PopupMenuEx(Menu As Object, Optional FLAGS, Optional X, Optional Y, Optional DefaultMenu)
  Dim Xex As Single
  Dim Yex As Single
  Dim nPoint As POINTAPI
  Dim hMenu As Long
  Dim nCnt As Long
  Dim I As Long
  Dim MII As MENUITEMINFO
  Dim strBuffer As String * 32
  Dim FlgV As Long
    
  If Form1.mDatei.Visible Then
    hMenu = GetMenu(Me.hWnd)
    Menu.Visible = True
    nCnt = GetMenuItemCount(hMenu)
    'Debug.Print nCnt

    For I = 0 To nCnt - 1
      MII.dwTypeData = strBuffer
      MII.ccH = Len(strBuffer)
      MII.fMask = &H10
      MII.cbSize = Len(MII)
      GetMenuItemInfo hMenu, I, True, MII
      If Len(MII.dwTypeData) = 0 Then Exit For
    Next
    
    If I < nCnt Then
      GetCursorPos nPoint
  
      If IsMissing(X) = False And IsMissing(Y) = False Then
        nPoint.X = X
        nPoint.Y = Y
        ClientToScreen Me.hWnd, nPoint
      End If
      
      If IsMissing(FLAGS) = False Then FlgV = CLng(FLAGS)
      TrackPopupMenu GetSubMenu(hMenu, I), FlgV, nPoint.X, nPoint.Y, 0, Me.hWnd, ByVal 0&
    End If
    
    Menu.Visible = False
  Else
    Me.PopupMenu Menu, FLAGS, X, Y, DefaultMenu
  End If
End Sub

Public Sub ColumRedraw(Optional NoMenRef As Boolean)
  Dim IntCaption As String
  Dim Zhl As Integer
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Boolean
  Dim IconInd As Integer
  Dim LenTab As Integer
  Dim DAT As String
  Dim TCFrame As ChatFrame
  Dim SortedC() As Integer
  Dim ColumW As Integer
  Dim TopPos As Integer
  Dim OldSPCount As Integer
  Dim MixedEColor As Long
  
  LastVisIndex = 0
  FirstVisIndex = 0
  If Not MenuBar Is Nothing And NoMenRef = False Then MoMenue
  MixedEColor = GetMixedColor(CoVal.BackColor, CoVal.Highlight3D, 80)
  
  pTapStrip.Line (0, 0)-(pTapStrip.ScaleWidth, pTapStrip.ScaleHeight), CoVal.BackColor, BF
  DrawFlow pTapStrip, 0, 0, pTapStrip.ScaleWidth, TsrHeight + 1, CoVal.BackColor, CoVal.Highlight3D
  pTapStrip.Line (1, TsrHeight)-(pTapStrip.ScaleWidth - 1, TsrHeight), CoVal.ButtonShadow
  pTapStrip.Line (1, TsrHeight + 1)-(pTapStrip.ScaleWidth - 1, TsrHeight + 1), CoVal.Highlight3D
  
  
  ReDim TabList(ChatFrames.Count)
  I2 = 3
  GiveSortedC SortedC
  
  ColumW = (pTapStrip.ScaleWidth - ChatFrames.Count * 27 - 80) / ChatFrames.Count
  If ColumW < 50 Then ColumW = 50
  If ColumW > 128 Then ColumW = 128
  
  I = ChatFrames.Count Mod (pTapStrip.ScaleWidth / (ColumW + 27) - 0.1)
  If I > 0 Then ColumW = (pTapStrip.ScaleWidth - ChatFrames.Count * 27 - 80) / I Else ColumW = 128
  If ColumW < 50 Then ColumW = 50
  If ColumW > 128 Then ColumW = 128
  
  OldSPCount = SpCount
  I = 0
  SpCount = 1
  For Zhl = 1 To ChatFrames.Count
    Set TCFrame = ChatFrames(SortedC(Zhl))
    If TCFrame Is FrontCFrame Then pTapStrip.Font.Bold = True Else pTapStrip.Font.Bold = False
    If TextWW(TCFrame.Caption, pTapStrip.hDC) > ColumW Then
      IntCaption = TextChrs(TCFrame.Caption, ColumW - pTapStrip.TextWidth("...."), pTapStrip)
      IntCaption = IntCaption + "..."
      LenTab = ColumW + 27
    Else
      IntCaption = TCFrame.Caption
      pTapStrip.Font.Bold = True
      If TextWW(TCFrame.Caption, pTapStrip.hDC) > ColumW Then
        LenTab = ColumW + 27
      Else
        LenTab = TextWW(IntCaption, pTapStrip.hDC) + 27
      End If
    End If
    If I2 + LenTab + 32 > pTapStrip.ScaleWidth Then
      TopPos = TopPos + TsrHeight
      I2 = 3
      SpCount = SpCount + 1
      DrawFlow pTapStrip, 0, TopPos + 2, pTapStrip.ScaleWidth, TsrHeight - 3, CoVal.BackColor, CoVal.Highlight3D
      pTapStrip.Line (1, TopPos + TsrHeight)-(pTapStrip.ScaleWidth - 1, TopPos + TsrHeight), CoVal.ButtonShadow
      pTapStrip.Line (1, TopPos + TsrHeight + 1)-(pTapStrip.ScaleWidth - 1, TopPos + TsrHeight + 1), CoVal.Highlight3D
    End If
    pTapStrip.FillStyle = 0
    If TCFrame Is FrontCFrame Then
      pTapStrip.FillColor = pTBox.BackColor
      pTapStrip.Line (I2 + 1, TopPos + TsrHeight + 1)-(I2 + LenTab + 2, TopPos + TsrHeight + 1), pTBox.BackColor
      pTapStrip.Line (I2 + 1, TopPos + TsrHeight)-(I2 + LenTab, TopPos + TsrHeight), pTBox.BackColor
      pTapStrip.Circle (I2 + LenTab - 10, TopPos + 13), 9, pTBox.BackColor  'CoVal.BackColor
      pTapStrip.Line (I2 + 1, TopPos + 4)-(I2 + LenTab - 8, TopPos + TsrHeight - 1), pTapStrip.FillColor, BF 'REM
    Else
      pTapStrip.FillColor = Me.BackColor
      pTapStrip.Circle (I2 + LenTab - 10, TopPos + 13), 9, Me.BackColor 'CoVal.BackColor
      DrawFlow pTapStrip, I2 + 1, TopPos + 4, LenTab - 9, TsrHeight - 4, CoVal.Highlight3D, CoVal.BackColor, True  'REM
    End If
        
    
    pTapStrip.Line (I2 + LenTab - 1, TopPos + 10)-(I2 + LenTab - 7, TopPos + TsrHeight - 1), pTapStrip.FillColor, BF
    
    pTapStrip.Line (I2 + 1, TopPos + 4)-(I2 + LenTab - 6, TopPos + 4), CoVal.Highlight3D
    pTapStrip.Line (I2 + 1, TopPos + 5)-(I2 + LenTab - 4, TopPos + 5), CoVal.Highlight3D
    pTapStrip.Line (I2 + 1, TopPos + 4)-(I2 + 1, TopPos + TsrHeight), CoVal.Highlight3D
    pTapStrip.Line (I2 + 2, TopPos + 4)-(I2 + 2, TopPos + TsrHeight), CoVal.Highlight3D
    
    If TCFrame Is FrontCFrame Then
      pTapStrip.PSet (I2 + 1, TopPos + 23), CoVal.Highlight3D
      pTapStrip.PSet (I2, TopPos + 23), CoVal.Highlight3D
      PixelDraw.DrawEdge pTapStrip, I2 + LenTab - 6, TopPos + 4, CoVal.Highlight3D, pTBox.BackColor, CoVal.ButtonShadow, MixedEColor
      pTapStrip.Font.Bold = True
    Else
      PixelDraw.DrawEdge pTapStrip, I2 + LenTab - 6, TopPos + 4, CoVal.Highlight3D, Me.BackColor, CoVal.ButtonShadow, MixedEColor
      pTapStrip.Font.Bold = False
    End If
    pTapStrip.Line (I2 + LenTab - 1, TopPos + 10)-(I2 + LenTab - 1, TopPos + TsrHeight), CoVal.ButtonShadow
    pTapStrip.Line (I2 + LenTab, TopPos + 12)-(I2 + LenTab, TopPos + TsrHeight), CoVal.ButtonShadow
    
    If DragPos = SortedC(Zhl) Then
      pTapStrip.Line (I2 + LenTab - 1, TopPos + 3)-(I2 + LenTab, TopPos + TsrHeight - 1), CoVal.ButtonText, BF
    End If
    
    Select Case TCFrame.FrameType
      Case cfRoom
        If TCFrame.Changed = 0 Then IconInd = 3
        If TCFrame.Changed = 1 Then IconInd = 70
        If TCFrame.Changed = 2 Then IconInd = 6
        If TCFrame.Changed = 3 Then IconInd = 54 - (TCFrame.ntBlink Mod 2) * 51
        If TCFrame.Changed = 4 Then IconInd = 4 - (TCFrame.ntBlink Mod 2)
      Case cfPrivate
        If TCFrame.Changed = 0 Then IconInd = 7
        If TCFrame.Changed = 1 Then IconInd = 72
        If TCFrame.Changed > 1 Then IconInd = 4 + (TCFrame.ntBlink Mod 2) * 3
      Case cfStatus
        pTapStrip.Line (I2 - 1, TopPos + 1)-(I2 - 1, TopPos + TsrHeight), CoVal.ButtonShadow
        pTapStrip.Line (I2 - 2, TopPos + 1)-(I2 - 2, TopPos + TsrHeight), CoVal.Highlight3D
        Select Case TCFrame.ConnTyp
          Case 0
            If TCFrame.Changed = 0 Then IconInd = 0
            If TCFrame.Changed = 1 Then IconInd = 17
            If TCFrame.Changed > 1 Then IconInd = 8 - (TCFrame.ntBlink Mod 2) * 8
          Case 1
            If TCFrame.Changed = 0 Then IconInd = 1
            If TCFrame.Changed = 1 Then IconInd = 71
            If TCFrame.Changed = 2 Then IconInd = 6
            If TCFrame.Changed = 3 Then IconInd = 54 - (TCFrame.ntBlink Mod 2) * 53
            If TCFrame.Changed = 4 Then IconInd = 4 - (TCFrame.ntBlink Mod 2) * 3
          Case 2
            If TCFrame.Changed = 0 Then IconInd = 15
            If TCFrame.Changed > 0 Then IconInd = 16 - (TCFrame.ntBlink Mod 2)
        End Select
      Case cfRoomList
        IconInd = 5
      Case cfServerList
        IconInd = 2
      Case cfScript
        IconInd = 59
      Case cfDCC
        IconInd = 37
      Case cfLog
        IconInd = 60
      Case cfFriends
        IconInd = 61
      Case cfDebug
        IconInd = 60
      Case cfWindow
        IconInd = 78
    End Select
  
    PrintIcon pTapStrip, I2 + 4, TopPos + 6, IconInd
    pTapStrip.CurrentX = I2 + 22
    pTapStrip.CurrentY = TopPos + 7
    I2 = I2 + LenTab + 3
    pTapStrip.ForeColor = CoVal.ButtonText
    Select Case TCFrame.ConnTyp
      Case 0
        If TCFrame.IrcConn.State < 3 Then
          pTapStrip.ForeColor = CoVal.bColumDisc
        ElseIf TCFrame.IrcConn.State = 3 Or TCFrame.IrcConn.LogedIn = False Then
          pTapStrip.ForeColor = CoVal.bColumBusy
        End If
      Case 2
        If TCFrame.DCCConn.State = 3 Then
          pTapStrip.ForeColor = CoVal.bColumBusy
        ElseIf TCFrame.DCCConn.State < 3 Then
          pTapStrip.ForeColor = CoVal.bColumDisc
        End If
    End Select
    If TCFrame Is FrontCFrame Then pTapStrip.ForeColor = CoVal.NormalText
    
    'pTapStrip.Print IntCaption
    TextOutW pTapStrip.hDC, pTapStrip.CurrentX, pTapStrip.CurrentY, StrPtr(IntCaption), Len(IntCaption)
    
    TabList(I) = I2 + pTapStrip.ScaleWidth * (SpCount - 1)
    I = I + 1
    If FirstVisIndex = 0 And I2 > 64 Then FirstVisIndex = Zhl
    If I2 > pTapStrip.ScaleWidth - 16 Then LastVisIndex = Zhl: Exit For
  Next
    
  pTapStrip.Line (pTapStrip.ScaleWidth - 18, 5)-(pTapStrip.ScaleWidth - 5, 18), CoVal.BackColor, BF
  If FrontCFrame.FrameType <> cfServerList And FrontCFrame.FrameType <> cfDCC Then
    PrintIcon pTapStrip, pTapStrip.ScaleWidth - 19, 5, 9
  End If
  pTapStrip.Line (pTapStrip.ScaleWidth - 18, 5)-(pTapStrip.ScaleWidth - 5, 5), CoVal.ButtonShadow
  pTapStrip.Line (pTapStrip.ScaleWidth - 5, 18)-(pTapStrip.ScaleWidth - 5, 4), CoVal.ButtonShadow
  pTapStrip.Line (pTapStrip.ScaleWidth - 18, 5)-(pTapStrip.ScaleWidth - 18, 18), CoVal.ButtonShadow
  pTapStrip.Line (pTapStrip.ScaleWidth - 5, 18)-(pTapStrip.ScaleWidth - 19, 18), CoVal.ButtonShadow
  
  If ColClose.Down = True Then
    pTapStrip.DrawMode = 6
    pTapStrip.Line (pTapStrip.ScaleWidth - 18, 5)-(pTapStrip.ScaleWidth - 5, 18), vbBlack, BF
    pTapStrip.DrawMode = 13
  End If
        
  pTapStrip.Line (0, 0)-(0, pTapStrip.ScaleHeight), CoVal.ButtonShadow
  pTapStrip.Line (0, 0)-(pTapStrip.ScaleWidth, 0), CoVal.ButtonShadow
  pTapStrip.Line (pTapStrip.ScaleWidth - 1, 0)-(pTapStrip.ScaleWidth - 1, pTapStrip.ScaleHeight), CoVal.Highlight3D
  If OldSPCount <> SpCount Then pMain_Resize
End Sub


Private Sub pMenu_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  MenuBar.MouseDown Button, Shift, X, Y
End Sub

Private Sub pMenu_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  MenuBar.MouseMove Button, Shift, X, Y
End Sub

Private Sub pMenu_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  MenuBar.MouseUp Button, Shift, X, Y
End Sub

Private Sub pState_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 1 And X > pState.ScaleWidth - 16 Then
    ReleaseCapture
    SendMessageA Me.hWnd, &HA1, 17, ByVal 0&
    'FrontCFrame.UListF.Refresh
  End If
End Sub

Private Sub pState_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If X > pState.ScaleWidth - 16 Then
    pState.MousePointer = 8
  Else
    pState.MousePointer = 0
  End If
End Sub

Public Sub pStateRefresh()
  If StateRef = False Then
    StateRef = True
    StartTimer Me, 500, 3
  End If
End Sub

Public Sub pStateIntRefresh()
  Dim tRect As RECT
  Dim DataOut As String
  pState.Line (0, 0)-(pState.ScaleWidth, pState.ScaleHeight), CoVal.BackColor, BF
  DrawFlow pState, 0, 0, pState.ScaleWidth, 4, CoVal.ButtonShadow, CoVal.BackColor
  DrawFlow pState, 0, pState.ScaleHeight - 8, pState.ScaleWidth, 15, CoVal.BackColor, CoVal.ButtonShadow
  
  pState.Line (pState.ScaleWidth - 3, pState.ScaleHeight - 3)- _
  (pState.ScaleWidth - 2, pState.ScaleHeight - 2), CoVal.Highlight3D, BF
  pState.Line (pState.ScaleWidth - 3, pState.ScaleHeight - 7)- _
  (pState.ScaleWidth - 2, pState.ScaleHeight - 6), CoVal.Highlight3D, BF
  pState.Line (pState.ScaleWidth - 3, pState.ScaleHeight - 11)- _
  (pState.ScaleWidth - 2, pState.ScaleHeight - 10), CoVal.Highlight3D, BF
  pState.Line (pState.ScaleWidth - 7, pState.ScaleHeight - 7)- _
  (pState.ScaleWidth - 6, pState.ScaleHeight - 6), CoVal.Highlight3D, BF
  pState.Line (pState.ScaleWidth - 7, pState.ScaleHeight - 3)- _
  (pState.ScaleWidth - 6, pState.ScaleHeight - 2), CoVal.Highlight3D, BF
  pState.Line (pState.ScaleWidth - 11, pState.ScaleHeight - 3)- _
  (pState.ScaleWidth - 10, pState.ScaleHeight - 2), CoVal.Highlight3D, BF
  pState.Line (pState.ScaleWidth - 4, pState.ScaleHeight - 4)- _
  (pState.ScaleWidth - 3, pState.ScaleHeight - 3), CoVal.ButtonShadow, BF
  pState.Line (pState.ScaleWidth - 4, pState.ScaleHeight - 8)- _
  (pState.ScaleWidth - 3, pState.ScaleHeight - 7), CoVal.ButtonShadow, BF
  pState.Line (pState.ScaleWidth - 4, pState.ScaleHeight - 12)- _
  (pState.ScaleWidth - 3, pState.ScaleHeight - 11), CoVal.ButtonShadow, BF
  pState.Line (pState.ScaleWidth - 8, pState.ScaleHeight - 8)- _
  (pState.ScaleWidth - 7, pState.ScaleHeight - 7), CoVal.ButtonShadow, BF
  pState.Line (pState.ScaleWidth - 8, pState.ScaleHeight - 4)- _
  (pState.ScaleWidth - 7, pState.ScaleHeight - 3), CoVal.ButtonShadow, BF
  pState.Line (pState.ScaleWidth - 12, pState.ScaleHeight - 4)- _
  (pState.ScaleWidth - 11, pState.ScaleHeight - 3), CoVal.ButtonShadow, BF
  
'  pState.Line (pState.ScaleWidth - 7, pState.ScaleHeight - 1)- _
'  (pState.ScaleWidth - 1, pState.ScaleHeight - 7), CoVal.ButtonShadow
'  pState.Line (pState.ScaleWidth - 11, pState.ScaleHeight - 1)- _
'  (pState.ScaleWidth - 1, pState.ScaleHeight - 11), CoVal.ButtonShadow
'  pState.Line (pState.ScaleWidth - 15, pState.ScaleHeight - 1)- _
'  (pState.ScaleWidth - 1, pState.ScaleHeight - 15), CoVal.ButtonShadow
'  pState.Line (pState.ScaleWidth - 7, pState.ScaleHeight - 3)- _
'  (pState.ScaleWidth - 2, pState.ScaleHeight - 8), CoVal.Highlight3D
'  pState.Line (pState.ScaleWidth - 11, pState.ScaleHeight - 3)- _
'  (pState.ScaleWidth - 2, pState.ScaleHeight - 12), CoVal.Highlight3D
'  pState.Line (pState.ScaleWidth - 15, pState.ScaleHeight - 3)- _
'  (pState.ScaleWidth - 2, pState.ScaleHeight - 16), CoVal.Highlight3D
  
  tRect.Left = 235
  tRect.Top = 4
  tRect.Right = pState.ScaleWidth - 90
  tRect.Bottom = pState.ScaleHeight
  If LiteUpLM Then
    pState.ForeColor = CoVal.ButtonText
  Else
    pState.ForeColor = CoVal.Shadow3D
  End If
  DrawTextW pState.hDC, StrPtr(LastWispMsg), Len(LastWispMsg), tRect, 0
  
  If Not FrontCFrame Is Nothing Then
    If FrontCFrame.ConnTyp = 0 Then ' #IRC
      If Len(FrontCFrame.IrcConn.RealServer) = 0 Then
        DataOut = FrontCFrame.IrcConn.Nick _
        + "   [" + FrontCFrame.IrcConn.Server _
        + ":" + CStr(FrontCFrame.IrcConn.Port) + "]"
      Else
        DataOut = FrontCFrame.IrcConn.Nick _
        + "   [" + FrontCFrame.IrcConn.RealServer _
        + ":" + CStr(FrontCFrame.IrcConn.Port) + "]"
      End If
      PrintIcon pState, 1, 2, 0
      If FrontCFrame.IrcConn.CTCAktiv Then PrintIcon pState, pState.ScaleWidth - 55, 2, 66
      If FrontCFrame.IrcConn.State = 4 Then
        PrintIcon pState, pState.ScaleWidth - 80, 2, 67
      Else
        PrintIcon pState, pState.ScaleWidth - 80, 2, 68
      End If
    ElseIf FrontCFrame.ConnTyp = 2 Then '#DCC
      If FrontCFrame.DCCConn.GetSeocket Is Nothing Then
        DataOut = FrontCFrame.Caption
      Else
        DataOut = FrontCFrame.DCCConn.Nick _
        + "   [" + FrontCFrame.DCCConn.Server _
        + ":" + CStr(FrontCFrame.DCCConn.Port) + "]"
      End If
      PrintIcon pState, 1, 2, 12
      If FrontCFrame.DCCConn.State = 4 Then
        PrintIcon pState, pState.ScaleWidth - 80, 2, 67
      Else
        PrintIcon pState, pState.ScaleWidth - 80, 2, 68
      End If
    Else
      DataOut = "In:  " + FormatBytes(RecevedBytes) + "        Out:  " + FormatBytes(SendedBytes)
      PrintIcon pState, 1, 2, 39
    End If
  End If
  
  tRect.Left = 20
  tRect.Top = 4
  tRect.Right = 230
  If tRect.Right > pState.ScaleWidth - 90 Then tRect.Right = pState.ScaleWidth - 90
  tRect.Bottom = pState.ScaleHeight
  pState.ForeColor = CoVal.ButtonText
  DrawText pState.hDC, DataOut, Len(DataOut), tRect, 0
End Sub

Public Sub pMain_Resize()
  On Error Resume Next
  If pMain.ScaleHeight - pTBox.Top - 1 < 1 Then Exit Sub
  If pMain.ScaleHeight - pUList.Top - 20 < 0 Then ColumRedraw True: Exit Sub
  pDiff.Left = pMain.ScaleWidth - RListW - pDiff.Width - 1
  pDiff.Top = SpCount * (TsrHeight - 1) + 8
  If pMain.ScaleHeight - pDiff.Top > 0 Then
    pDiff.Height = pMain.ScaleHeight - pDiff.Top
  Else
    pDiff.Height = 1
  End If
  If pDiff.Left < 70 Then pDiff.Left = 70
  If pDiff.Left > pMain.ScaleWidth - 10 Then pDiff.Left = pMain.ScaleWidth - 10
  pTBox.Left = 1
  pTBox.Top = SpCount * TsrHeight + 2
  If pMain.ScaleHeight - pTBox.Top - 1 > 0 Then
    pTBox.Height = pMain.ScaleHeight - pTBox.Top - 1
  Else
    pTBox.Height = 1
  End If
  pUList.Top = SpCount * TsrHeight + 2
  
  pTapStrip.Top = 0
  pTapStrip.Left = 0
  pTapStrip.Height = SpCount * TsrHeight + 2
  pTapStrip.Width = pMain.ScaleWidth
  
  Select Case FrontCFrame.FrameType
    Case cfScript
      On Error Resume Next
      pText.Top = SpCount * TsrHeight + 2
      pText.Left = 1
      If pMain.ScaleHeight - pText.Top - 1 > 0 Then pText.Height = pMain.ScaleHeight - pText.Top - 1
      If pMain.ScaleWidth - 3 > 0 Then pText.Width = pMain.ScaleWidth - 3
      Text2.Left = 7
      Text2.Top = 40
      If pText.ScaleHeight - Text2.Top > 0 Then Text2.Height = pText.ScaleHeight - Text2.Top
      If pText.ScaleWidth - Text2.Left > 0 Then Text2.Width = pText.ScaleWidth - Text2.Left
      On Error GoTo 0
    Case cfRoomList, cfServerList, cfDCC, cfFriends
      pUList.Left = 1
      If pMain.ScaleWidth - pUList.Left - 1 > 0 Then pUList.Width = pMain.ScaleWidth - pUList.Left - 1
      If pMain.ScaleHeight - pUList.Top - 1 > 0 Then pUList.Height = pMain.ScaleHeight - pUList.Top - 1
    Case Else
      If FrontCFrame.FrameType = cfRoom Then
        pUList.Left = pDiff.Left + pDiff.Width
        If pMain.ScaleWidth - pUList.Left - 1 > 0 Then pUList.Width = pMain.ScaleWidth - pUList.Left - 1
        If pMain.ScaleHeight - pUList.Top - 1 > 0 Then pUList.Height = pMain.ScaleHeight - pUList.Top - 1
        If pDiff.Left - 2 > 0 Then pTBox.Width = pDiff.Left - 1
      Else
        If pMain.ScaleWidth - pTBox.Left - 1 > 0 Then pTBox.Width = pMain.ScaleWidth - pTBox.Left - 1
      End If
  End Select
  ColumRedraw True
End Sub

Private Sub pState_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Me.Visible = True Then Text1.SetFocus
End Sub

Private Sub pState_Paint()
  pStateIntRefresh
End Sub

Private Sub pState_Resize()
  pStateIntRefresh
End Sub

Private Sub pMenu_Resize()
  If Not MenuBar Is Nothing Then MenuBar.Refresh
End Sub

Private Sub pTBox_DblClick()
  If Not FrontCFrame.TextF Is Nothing Then FrontCFrame.TextF.DblClick
End Sub

Private Sub pTBox_KeyDown(KeyCode As Integer, Shift As Integer)
  If Not FrontCFrame.TextF Is Nothing Then FrontCFrame.TextF.KeyDown KeyCode, Shift
End Sub

Private Sub pTBox_KeyPress(KeyAscii As Integer)
  If KeyAscii > 31 Then
    If Text1.SelStart > 0 Or KeyAscii = 47 Then ' 47 -> "/"
      ProofIndex
    End If
    Text1.SelText = Chr(KeyAscii)
  End If
  Text1.SetFocus
End Sub

Private Sub pTBox_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  MGMouseDown X, Y
  If Not FrontCFrame.TextF Is Nothing Then FrontCFrame.TextF.MouseDown Button, Shift, X, Y
End Sub

Private Sub pTBox_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 Then MGTrackMousePos X, Y
  If Not FrontCFrame.TextF Is Nothing Then FrontCFrame.TextF.MouseMove Button, Shift, X, Y
  ScrollBarIndex = 1
End Sub

Private Sub pTBox_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If MGMouseUp = False Then
    If Not FrontCFrame.TextF Is Nothing Then FrontCFrame.TextF.MouseUp Button, Shift, X, Y
  End If
End Sub

Private Sub pTBox_Paint()
  If RefrMini = True Then RefrMini = False: SetFrontFrame FrontCFrame
  If Not FrontCFrame.TextF Is Nothing Then FrontCFrame.TextF.Paint
End Sub

Private Sub pTBox_Resize()
  If Not FrontCFrame.TextF Is Nothing Then
    FrontCFrame.TextF.Resize
    If Me.Visible = True And Me.WindowState <> 1 Then FrontCFrame.TextF.Refresh
  End If
  pScriptNotify.Top = 1
  pScriptNotify.Left = pTBox.ScaleWidth - VScroll1.Width - pScriptNotify.Width
End Sub

Public Sub ShowScriptNotify(Text As String, ForeColor As Long, BackColor As Long, FrameColor As Long, FontSize As Integer)
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim Lines(8) As String
  Dim TextH As Long
  Dim TextW As Long
  Dim PicStart As Long
  Dim PicWidth As Long
  
  If Len(Text) Then
    If (BackColor = 0 And ForeColor = 0) Or BackColor < 0 Or ForeColor < 0 Then
      pScriptNotify.BackColor = pTBox.BackColor
      pScriptNotify.ForeColor = CoVal.NormalText
    Else
      pScriptNotify.BackColor = BackColor
      pScriptNotify.ForeColor = ForeColor
    End If
    If FontSize < 1 Or FontSize > 32 Then
      pScriptNotify.FontSize = 10
    Else
      pScriptNotify.FontSize = FontSize
    End If
        
    I = -1
    For I3 = 0 To UBound(Lines)
      I2 = I
      I = InStr(I + 2, Text, vbNewLine)
      If I = 0 Then I = Len(Text) + 1
      Lines(I3) = Mid(Text, I2 + 2, I - I2 - 2)
      If TextWW(Lines(I3), Form1.pScriptNotify.hDC) > TextW Then
        TextW = TextWW(Lines(I3), Form1.pScriptNotify.hDC)
      End If
      If I = Len(Text) + 1 Then Exit For
    Next
    
    TextH = pScriptNotify.TextHeight(Lines(0))
    pScriptNotify.Height = TextH * (I3 + 1) + 4
    pScriptNotify.Width = TextW + 10
        
    If BackColor = 0 And HGDrawMode = 1 Then
      PicWidth = BgPicture.ScaleWidth - pScriptNotify.Width
      PicStart = pScriptNotify.Width - BgPicture.ScaleWidth
      If PicWidth < 0 Then PicWidth = 0
      If PicStart < 0 Then PicStart = 0
      pScriptNotify.FillStyle = 1
      pScriptNotify.Line (0, 0)-(PicStart, pScriptNotify.ScaleHeight - 1), pTBox.BackColor, BF
      BitBlt pScriptNotify.hDC, PicStart, 0, pScriptNotify.Width, pScriptNotify.Height, BgPicture.hDC, PicWidth, 1, vbSrcCopy
    Else
      pScriptNotify.FillStyle = 0
    End If
    If pScriptNotify.BackColor <> FrameColor Then
      pScriptNotify.FillColor = pScriptNotify.BackColor
      pScriptNotify.Line (0, 0)-(pScriptNotify.ScaleWidth - 1, pScriptNotify.ScaleHeight - 1), FrameColor, B
    End If
        
    For I = 0 To I3
      TextOutW Form1.pScriptNotify.hDC, 5, 2 + I * TextH, StrPtr(Lines(I)), Len(Lines(I))
    Next
    
    pScriptNotify.Top = 1
    pScriptNotify.Left = pTBox.ScaleWidth - VScroll1.Width - pScriptNotify.Width
    
    Form1.pScriptNotify.Visible = True
  Else
    Form1.pScriptNotify.Visible = False
  End If
  
End Sub


Public Sub TfRedraw()
  Dim XPos As Long
  Dim DAT As String
  
  pTexthg.Line (0, 0)-(0, pTexthg.ScaleHeight - 1), CoVal.ButtonShadow
  pTexthg.Line (0, 0)-(pTexthg.ScaleWidth - 1, 0), CoVal.ButtonShadow
  pTexthg.Line (pTexthg.ScaleWidth - 1, 0)-(pTexthg.ScaleWidth - 1, pTexthg.ScaleHeight - 1), CoVal.Highlight3D
  pTexthg.Line (0, pTexthg.ScaleHeight - 1)-(pTexthg.ScaleWidth, pTexthg.ScaleHeight - 1), CoVal.Highlight3D
  DrawFlow pTexthg, 1, 1, pTexthg.ScaleWidth - 2, 4, CoVal.ButtonShadow, Text1.BackColor
  pTexthg.Line (1, 5)-(pTexthg.ScaleWidth - 2, Text1.Top), Text1.BackColor, BF
  pTexthg.Line (1, Text1.Top - 1)-(pTexthg.ScaleWidth - 2, Text1.Top - 1), &H80000003
  pTexthg.Font.Bold = False
   
  If Check1.Visible = True Then
    If Len(WisperTo) = 0 Then
      Check1.Visible = False
    Else
      Check1.Top = 12 - Check1.Height / 2
      Check1.Left = pTexthg.ScaleWidth - 20 - TextWW(TeVal.WisperTo + " " + WisperTo, pTexthg.hDC)
      'pTexthg.CurrentX = Check1.Left + 16
      'pTexthg.CurrentY = 5
      pTexthg.ForeColor = CoVal.NormalText
      'pTexthg.Print TeVal.WisperTo + " " + WisperTo
      DAT = TeVal.WisperTo + " " + WisperTo
      TextOutW pTexthg.hDC, Check1.Left + 16, 5, StrPtr(DAT), Len(DAT)
    End If
  End If
    
  'pTexthg.CurrentX = 5
  'pTexthg.CurrentY = 5
  pTexthg.ForeColor = CoVal.NormalVon
  'pTexthg.Print TOutSt(4)
  TextOutW pTexthg.hDC, 5, 5, StrPtr(TOutSt(4)), Len(TOutSt(4))
  XPos = TextWW(TOutSt(4) + " ", pTexthg.hDC)
  'pTexthg.CurrentX = xPos
  'pTexthg.CurrentY = 5
  pTexthg.ForeColor = CoVal.NormalText
  'pTexthg.Print TOutSt(0)
  TextOutW pTexthg.hDC, XPos, 5, StrPtr(TOutSt(0)), Len(TOutSt(0))
  XPos = XPos + TextWW(TOutSt(0), pTexthg.hDC)
  'pTexthg.CurrentX = xPos
  'pTexthg.CurrentY = 5
  pTexthg.Font.Bold = True
  pTexthg.ForeColor = CoVal.NormalText
  'pTexthg.Print TOutSt(1)
  TextOutW pTexthg.hDC, XPos, 5, StrPtr(TOutSt(1)), Len(TOutSt(1))
  XPos = XPos + TextWW(TOutSt(1), pTexthg.hDC)
  pTexthg.Font.Bold = False
  'pTexthg.CurrentX = xPos
  'pTexthg.CurrentY = 5
  pTexthg.ForeColor = CoVal.NormalText
  'pTexthg.Print TOutSt(2)
  TextOutW pTexthg.hDC, XPos, 5, StrPtr(TOutSt(2)), Len(TOutSt(2))
  XPos = XPos + TextWW(TOutSt(2), pTexthg.hDC) + 20
  'pTexthg.CurrentX = xPos
  'pTexthg.CurrentY = 5
  pTexthg.ForeColor = CoVal.bColumDisc
  If TextWW(TOutSt(3), pTexthg.hDC) + 20 + XPos > Check1.Left And Check1.Visible = True Then
    'pTexthg.Print TextChrs(TOutSt(3), Check1.Left - xPos - 10 - pTexthg.TextWidth("..."), pTexthg) + "..."
    DAT = TextChrs(TOutSt(3), Check1.Left - XPos - 10 - pTexthg.TextWidth("..."), pTexthg) + "..."
    TextOutW pTexthg.hDC, XPos, 5, StrPtr(DAT), Len(DAT)
  Else
    'pTexthg.Print TOutSt(3)
    TextOutW pTexthg.hDC, XPos, 5, StrPtr(TOutSt(3)), Len(TOutSt(3))
    If DrawIconAt = 0 Then
      XPos = XPos + TextWW(TOutSt(3), pTexthg.hDC)
    Else
      XPos = DrawIconAt + 25 + TextWW(TOutSt(5), pTexthg.hDC)
      If (XPos > Check1.Left And Check1.Visible = True) Or XPos > pTexthg.ScaleWidth Then
        If DrawIconAt + 16 > Check1.Left And Check1.Visible = True Then
          PrintIcon pTexthg, DrawIconAt, Text1.Top - 16, 58
          XPos = Check1.Left - TextWW(TOutSt(5), pTexthg.hDC) - 16
        Else
          PrintIcon pTexthg, DrawIconAt, Text1.Top - 16, 57
          XPos = DrawIconAt - TextWW(TOutSt(5), pTexthg.hDC)
        End If
      Else
        PrintIcon pTexthg, DrawIconAt, Text1.Top - 16, 56
        XPos = DrawIconAt + 18
      End If
    End If
    'pTexthg.CurrentX = xPos
    'pTexthg.CurrentY = 5
    pTexthg.ForeColor = vbRed
    'pTexthg.Print TOutSt(5)
    TextOutW pTexthg.hDC, XPos, 5, StrPtr(TOutSt(5)), Len(TOutSt(5))
  End If
End Sub

Private Sub pText_GotFocus()
  If Text2.Visible = True Then Text2.SetFocus
End Sub

Private Sub pText_Paint()
  pText.Line (0, 32)-(pText.ScaleWidth, 32), &H80000003
End Sub

Private Sub pTexthg_Click()
  If pTexthg.MousePointer = 99 Then CorrectWord
End Sub

Private Sub pTexthg_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If DrawIconAt > 0 Then
    If Y < Text1.Top And X > DrawIconAt - 8 And X < DrawIconAt + 24 Then
      If pTexthg.MousePointer = 0 Then pTexthg.MousePointer = 99
    Else
      If pTexthg.MousePointer > 0 Then pTexthg.MousePointer = 0
    End If
  Else
    If pTexthg.MousePointer > 0 Then pTexthg.MousePointer = 0
  End If
End Sub

Private Sub pTexthg_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Y > 4 And Y < Text1.Top - 3 And X > Check1.Left And Check1.Visible = True Then
    Check1.Value = ((Check1.Value - 1) * -1)
  End If
  If Me.Visible = True Then Text1.SetFocus
End Sub

Private Sub pTexthg_Paint()
  TfRedraw
End Sub

Private Sub pUList_Click()
  If Not FrontCFrame.UListF Is Nothing Then FrontCFrame.UListF.Click
End Sub

Private Sub pUList_DblClick()
  If Not FrontCFrame.UListF Is Nothing Then FrontCFrame.UListF.DblClick
End Sub

Private Sub pUList_GotFocus()
  Set TextFeld = Nothing
End Sub

Private Sub pUList_KeyDown(KeyCode As Integer, Shift As Integer)
  If Not FrontCFrame.UListF Is Nothing Then FrontCFrame.UListF.KeyDown KeyCode, Shift
End Sub

Private Sub pUList_KeyPress(KeyAscii As Integer)
  If Not FrontCFrame.UListF Is Nothing Then FrontCFrame.UListF.KeyPress KeyAscii
End Sub

Private Sub pUList_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  MGMouseDown X, Y
  If Not FrontCFrame.UListF Is Nothing Then FrontCFrame.UListF.MouseDown Button, Shift, X, Y
End Sub

Private Sub pUList_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Button = 2 Then MGTrackMousePos X, Y
  If Not FrontCFrame.UListF Is Nothing Then
    FrontCFrame.UListF.MouseMove Button, Shift, X, Y
    ScrollBarIndex = 0
  End If
End Sub

Private Sub pUList_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If MGMouseUp = False Then
    If Not FrontCFrame.UListF Is Nothing Then FrontCFrame.UListF.MouseUp Button, Shift, X, Y
  End If
End Sub

Private Sub pUList_OLEDragDrop(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single)
  Dim I As Integer
  Dim TempCFrame As ChatFrame
  
  If Not FrontCFrame.UListF Is Nothing Then
    If Data.GetFormat(vbCFFiles) = False Or FrontCFrame.UListF.ListCount = 0 Then Exit Sub
    Set TempCFrame = FrontCFrame
    For I = 1 To Data.Files.Count
      If Len(Dir(Data.Files(I))) > 0 Then
        If I > 16 Then Exit For
        If TempCFrame.ConnTyp = 0 Then
          OpenDCC TempCFrame.IrcConn.Nick, _
          TempCFrame.IrcConn.ChatNoSign(TempCFrame.UListF.List(TempCFrame.UListF.ListIndex)), _
          TempCFrame.IrcConn.ChatNoSign(Data.Files(I)), "SEND", TempCFrame.IrcConn, 0, FrontCFrame
        End If
      End If
    Next
  End If
End Sub

Private Sub pUList_OLEDragOver(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, X As Single, Y As Single, State As Integer)
  If Not FrontCFrame.UListF Is Nothing Then
    FrontCFrame.UListF.MouseMove Button, Shift, X, Y
    ScrollBarIndex = 0
  End If
End Sub

Private Sub pUList_Paint()
  If Not FrontCFrame.UListF Is Nothing Then FrontCFrame.UListF.Refresh -1, -1, 0, False, 1
End Sub

Private Sub pUList_Resize()
  If Not FrontCFrame.UListF Is Nothing Then
    FrontCFrame.UListF.Resize
    If pUList.Visible = True And pTBox.Visible = True Then
      RListW = pUList.Width
    End If
  End If
End Sub

Private Sub SearchInIndex()
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim FindEntr As Boolean
  
  If comList.Visible = True And Len(Text1.Text) <= 1024 Then
    If ComListMode > 0 Then
      FillComList ListedType, False, True
      Exit Sub
    End If
    Do
      I2 = I
      I = InStr(I + 1, Text1.Text, "/")
      If I = 0 Or I > Text1.SelStart Then Exit Do
    Loop
    If I2 = 0 Then comList.Visible = False: Exit Sub
    I3 = InStr(I2 + 1, Text1.Text, " ")
    If I3 = 0 Then I3 = Len(Text1.Text) + 1
    I = comList.ListIndex
'    If I > -1 Then
'      If Left(comList.List(I), I3 - I2) = LCase(Mid(Text1.Text, I2, I3 - I2)) Then
'        FindEntr = True
'      Else
'        I = -1
'      End If
'    End If
'    If I < 0 Then

'    End If
    
    If Right(Text1.Text, 2) = "//" Then
      comList.Visible = False
    ElseIf Mid(Text1.Text, Text1.SelStart, 1) = " " Then
      If comList.ListIndex > -1 Then
        Text1.Text = Left(Text1.Text, I2 - 1) + comList.List(comList.ListIndex) + Mid(Text1.Text, I3)
        Text1.SelStart = I2 + Len(comList.List(comList.ListIndex))
      End If
      comList.Visible = False
      ProofIPath
    Else
      For I = 0 To comList.ListCount - 1
        If Left(comList.List(I), I3 - I2) = LCase(Mid(Text1.Text, I2, I3 - I2)) Then
          comList.ListIndex = I
          FindEntr = True
          Exit For
        End If
      Next
      If FindEntr = False Then comList.ListIndex = -1
    End If
  End If
End Sub

Private Sub ProofIndex()
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  
  comList.ListIndex = -1
  If Len(Text1.Text) > 1024 Then Exit Sub
  If Form1.Text1.SelStart = 0 Then
    I = 1
  Else
    If Mid(Form1.Text1.Text, Form1.Text1.SelStart, 1) = Chr(10) Then I = 1
  End If
  If I = 1 Then
    comList.Clear
    For I = 0 To UBound(ComTranzList, 2)
      If Len(ComTranzList(0, I)) Then
        If FrontCFrame.FrameType = cfRoom Or Val(ComTranzList(3, I)) = 12 Or Val(ComTranzList(3, I)) < 11 Then
          comList.Additem "/" + ComTranzList(0, I)
        End If
      End If
    Next
    For I = 0 To UBound(ComIndex)
      If CStr(Val(Left(ComIndex(I), 1))) = Left(ComIndex(I), 1) And Len(ComIndex(I)) > 2 Then
        I2 = Val(Left(ComIndex(I), 1))
        If FrontCFrame.ConnTyp = -1 And I2 = 3 Then I2 = -1
        If FrontCFrame.ConnTyp = 0 And (I2 = 0 Or I2 = 2 Or I2 = 3) Then I2 = -1
        If I2 = -1 Then
          I2 = InStr(ComIndex(I), " ")
          If I2 = 0 Then I2 = Len(ComIndex(I)) + 1
          comList.Additem Mid(ComIndex(I), 2, I2 - 2)
        End If
      End If
    Next
    If comList.ListCount = 0 Then Exit Sub
    I = 0
    I2 = 0
    Do
      I3 = I
      I = InStr(I + 1, Text1.Text, Chr(13) + Chr(10))
      If I = 0 Or I > Text1.SelStart Then Exit Do
      I2 = I2 + 1
    Loop
    
    UseHelpIndex = False
    comList.Width = 130
    comList.Height = 58
    If pTBox.TextHeight("H") * (I2 + 1) < Text1.Height Then
      If comList.Height + (pTBox.TextHeight("H") * (I2 + 1) + Text1.Top + pTexthg.Top) >= Me.ScaleHeight Then
        comList.Top = pTBox.TextHeight("H") * I2 + Text1.Top + pTexthg.Top - comList.Height
      Else
        comList.Top = pTBox.TextHeight("H") * (I2 + 1) + Text1.Top + pTexthg.Top
      End If
    Else
      comList.Top = Text1.Top + pTexthg.Top - comList.Height
    End If
    comList.Left = TextWW(Mid(Text1.Text, I3 + 1, Text1.SelStart - I3), pTBox.hDC)
    ComListMode = 0
    'comList.ReSortList
    'comList.ListIndex = 0
    comList.Visible = True
  End If
End Sub

Private Sub FillComList(Types As String, Optional KeyShell As Boolean, Optional Scroll As Boolean)
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim I4 As Integer
  Dim DAT As String
  
  If comList.Visible = True Then
    If Scroll Then
      'Zu Eintrag scrollen
      I = ComListMode - 1
      If I > Text1.SelStart Then comList.Visible = False: Exit Sub
      If InStr("$expression", Types) = 0 Then I2 = InStr(I + 1, Text1.Text, " ")
      If I2 = 0 And InStr("$textnick", Types) Then I2 = InStr(I + 1, Text1.Text, ":")
      If I2 = 0 And InStr("$textnick $servers", Types) Then I2 = InStr(I + 1, Text1.Text, "?")
      If I2 = 0 And InStr("$nick $textnick $servers $nick+room $lastrooms", Types) Then I2 = InStr(I + 1, Text1.Text, ",")
      If I2 = 0 And InStr("$textnick $servers", Types) Then I2 = InStr(I + 1, Text1.Text, "!")
      If I2 = 0 And InStr("$textnick", Types) Then I2 = InStr(I + 1, Text1.Text, ".")
      
      If I2 > 0 And comList.ListIndex > -1 Then
        If InStr(1, comList.List(comList.ListIndex), Mid(Text1.Text, I2, 1)) And Mid(Text1.Text, I2, 1) <> " " Then I2 = 0
      End If

      If I2 = 0 Then I2 = Len(Text1.Text) + 1
      If Not I2 > I Then Exit Sub
'      If I2 = Len(Text1.Text) + 1 Or Len(Text1.Text) > LastTextLen + 1 Then
      If Text1.SelStart < I2 Then
        I2 = Text1.SelStart + 1
        
        DAT = LCase(Mid(Text1.Text, I + 1, I2 - I - 1))
        
        If UseHelpIndex Then
          comList.Visible = False
          comList.Clear
          For I = 0 To UBound(ComIndex)
            If CStr(Val(Left(ComIndex(I), 1))) = Left(ComIndex(I), 1) And Len(ComIndex(I)) > 2 Then
              I2 = Val(Left(ComIndex(I), 1))
              If FrontCFrame.ConnTyp = -1 And I2 = 3 Then I2 = -1
              If FrontCFrame.ConnTyp = 0 And (I2 = 0 Or I2 = 2 Or I2 = 3) Then I2 = -1
              
              If I2 = -1 Then
                If AreStringsIncl(DAT, LCase(ComIndex(I))) Then
                  I2 = InStr(ComIndex(I), " '")
                  If I2 > 0 And I2 < Len(ComIndex(I)) + 2 Then
                    comList.Additem Mid(ComIndex(I), I2 + 2)
                  End If
                End If
              End If
            End If
          Next
          comList.ListIndex = 0
          comList.Visible = True
        Else
          For I3 = 0 To comList.ListCount - 1
            I4 = InStr(1, comList.List(I3), DAT, vbTextCompare)
            If I4 > 1 Then
              If InStr(1, " !#%&'()*+,-./:;<=>?@[\]^_`{|}~������", Mid(comList.List(I3), I4 - 1, 1)) = 0 Then I4 = 0
            End If
            If (LCase(Left(comList.List(I3), I2 - I - 1)) = DAT Or I4 > 0) And Len(DAT) > 0 Then
              comList.ListIndex = I3
              Exit For
            End If
          Next
          If I3 = comList.ListCount And Len(DAT) > 0 Then comList.ListIndex = -1
        End If
      ElseIf Text1.SelStart >= I2 Then
        If comList.ListIndex > -1 And Text1.SelStart = I2 Then
          Text1.Text = Left(Text1.Text, I) + comList.List(comList.ListIndex) + Mid(Text1.Text, I2)
          Text1.SelStart = I + Len(comList.List(comList.ListIndex)) + 1
        End If
        comList.Visible = False
      End If
      LastTextLen = Len(Text1.Text)
    ElseIf KeyShell Then
      If comList.ListIndex + 1 < comList.ListCount Then
        comList.ListIndex = comList.ListIndex + 1
      Else
        comList.ListIndex = 0
      End If
    End If
  Else
    'List anzeigen
    comList.Width = 130
    comList.Height = 58
    UseHelpIndex = False
    If KeyShell = False And Text1.SelStart > 0 Then
      If Mid(Text1.Text, Text1.SelStart, 1) <> " " Then Exit Sub
    End If
    comList.Clear
    If LCase(Types) = "$nick" Or LCase(Types) = "$textnick" Or LCase(Types) = "$nick+room" Then
      If FrontCFrame.FrameType = cfRoom Then
        For I = 0 To FrontCFrame.UListF.ListCount - 1
          comList.Additem FrontCFrame.IrcConn.ChatNoSign(FrontCFrame.UListF.GetListText(I, 0))
        Next
      ElseIf FrontCFrame.FrameType = cfPrivate Then
        If FrontCFrame.ConnTyp = 0 Then
          comList.Additem FrontCFrame.IrcConn.Nick
          comList.Additem FrontCFrame.Caption
        Else
          Exit Sub
        End If
      End If
    End If
    If LCase(Types) = "$room" Or LCase(Types) = "$nick+room" Or LCase(Types) = "$allrooms" Then
      For I = 1 To ChatFrames.Count
        If ChatFrames(I).FrameType = cfRoom And ChatFrames(I).ConnTyp = 0 Then
          If ChatFrames(I).IrcConn Is FrontCFrame.IrcConn Then
            comList.Additem ChatFrames(I).Caption
          End If
        End If
      Next
      For I = 0 To comList.ListCount - 1
        If comList.List(I) = FrontCFrame.Caption Then
          comList.ListIndex = I
          Exit For
        End If
      Next
    End If
    If LCase(Types) = "$banned" And Len(FrontCFrame.BannList) > 0 Then
      I = -1
      Do
        I2 = InStr(I + 2, FrontCFrame.BannList, " ")
        If I2 = 0 Then Exit Do
        comList.Additem Mid(FrontCFrame.BannList, I + 4, I2 - I - 4)
        I = InStr(I2, FrontCFrame.BannList, vbNewLine)
      Loop Until I = 0
      comList.Width = 350
    End If
    If LCase(Types) = "$topic" And Len(FrontCFrame.ChanTopic) > 0 Then
      comList.Additem FrontCFrame.ChanTopic
      comList.Width = 350
    End If
    If LCase(Types) = "$quitmsg" And Len(QuitMsg) > 0 Then comList.Additem QuitMsg: comList.Width = 350
    If LCase(Types) = "$finger" And Len(FingerData) > 0 Then comList.Additem FingerData: comList.Width = 350
    If LCase(Types) = "$servers" Then
      For I = 0 To ConnListCount - 1
        comList.Additem ConnList(I).Caption
      Next
      comList.Additem "irc://"
      comList.Additem "telnet://"
    End If
    If LCase(Types) = "$lastrooms" Or LCase(Types) = "$allrooms" Then
      I = 0
      Do
        I2 = I
        I = InStr(I + 1, LastUsedChans, "|")
        If I = 0 Then Exit Do
        If I2 > 0 Then
          comList.Additem Mid(LastUsedChans, I2 + 1, I - I2 - 1)
        End If
      Loop
    End If
    If LCase(Types) = "$expression" Then 'Command search function
      comList.Width = 480
      comList.Height = 226
      UseHelpIndex = True
    End If
    
    If Left(Types, 1) <> "$" Then
      I = 0
      Do
        I2 = I
        I = InStr(I + 1, Types, ";")
        If I = 0 Then I = Len(Types) + 1
        comList.Additem Mid(Types, I2 + 1, I - I2 - 1)
      Loop Until I = Len(Types) + 1
    End If
    'If comList.ListCount = 0 Then Exit Sub
    If comList.ListIndex = -1 Then comList.ListIndex = 0
    I = -1
    I2 = 0
    Do
      I3 = I + 1
      I = InStr(I + 2, Text1.Text, Chr(13) + Chr(10))
      If I = 0 Or I > Text1.SelStart Then Exit Do
      I2 = I2 + 1
    Loop
    If pTBox.TextHeight("H") * (I2 + 1) < Text1.Height Then
      If comList.Height + (pTBox.TextHeight("H") * (I2 + 1) + Text1.Top + pTexthg.Top) >= Me.ScaleHeight Then
        comList.Top = pTBox.TextHeight("H") * I2 + Text1.Top + pTexthg.Top - comList.Height
      Else
        comList.Top = pTBox.TextHeight("H") * (I2 + 1) + Text1.Top + pTexthg.Top
      End If
    Else
      comList.Top = Text1.Top + pTexthg.Top - comList.Height
    End If
    For I = Text1.SelStart To I3 + 1 Step -1
      If Mid(Text1.Text, I, 1) = " " Then Exit For
    Next
    comList.Left = TextWW(Mid(Text1.Text, I3 + 1, I - I3), pTBox.hDC)
    ComListMode = I + 1
    'comList.ReSortList
    'comList.ListIndex = 0
    comList.Visible = True
    FillComList Types, False, True
    ListedType = Types
  End If
End Sub

Private Sub ProofIPath()
  Dim I As Integer
  Dim I2 As Integer
  Dim I3 As Integer
  Dim I4 As Integer
  Dim I5 As Integer
  Dim I6 As Integer
  Dim LCount As Integer
  Dim DAT As String
  
  If Len(Text1.Text) > 1024 Then Exit Sub
  If Len(TOutSt(4)) > 0 Or Len(Text1.Text) = 0 Then
    TOutSt(0) = ""
    TOutSt(1) = ""
    TOutSt(2) = ""
    TOutSt(3) = ""
    TOutSt(4) = ""
    TOutSt(5) = ""
    DrawIconAt = 0
  End If
  Do
    I2 = I
    Do
      I = InStr(I + 1, Text1.Text, "/")
      If I < 3 Then Exit Do
    Loop Until Mid(Text1.Text, I - 2, 2) = vbNewLine
    If I = 0 Or I > Text1.SelStart Then Exit Do
  Loop
  If I2 > 0 Then I3 = InStr(I2, Text1.Text, Chr(13))
  If I2 = 0 Or (I3 < Text1.SelStart And I3 > 0) Then TfRedraw: Exit Sub
  LCount = -1
  Do
    LCount = LCount + 1
    I3 = I2
    I2 = InStr(I2 + 1, Text1.Text, " ")
    If I2 = 0 Then I2 = Len(Text1.Text) + 1
    If Len(DAT) = 0 Then DAT = LCase(Mid(Text1.Text, I3, I2 - I3))
  Loop Until I2 > Text1.SelStart
  For I = 0 To UBound(ComIndex)
    If CStr(Val(Left(ComIndex(I), 1))) = Left(ComIndex(I), 1) And Len(ComIndex(I)) > 2 Then
      I2 = Val(Left(ComIndex(I), 1))
      'If FrontCFrame.ConnTyp = -1 And I2 = 3 Then I2 = 3
      If FrontCFrame.ConnTyp = 0 And (I2 = 0 Or I2 = 2) Then I2 = 3
      If I2 = 3 Then
        I2 = InStr(ComIndex(I), " ")
        If I2 = 0 Then I2 = Len(ComIndex(I)) + 1
        If LCase(DAT) = Mid(ComIndex(I), 2, I2 - 2) Then Exit For
      End If
    End If
  Next
  If I < UBound(ComIndex) + 1 Then
    I4 = I
    I = Len(DAT) + 1
    I3 = -1
    TOutSt(4) = DAT
    Do
      I3 = I3 + 1
      I2 = I
      I5 = InStr(I + 1, ComIndex(I4), "{")
      I = InStr(I + 1, ComIndex(I4), " ")
      If I = 0 Then I = Len(ComIndex(I4)) + 1
      If I5 = 0 Or I5 > I + 1 Then I5 = I
      If Mid(ComIndex(I4), I2 + 1, 1) = "'" Or LCount = -1 Then
        LCount = -1
        TOutSt(3) = "(" + Mid(ComIndex(I4), I2 + 2) + ")"
        Exit Do
      ElseIf I3 < LCount And I - I2 > 2 Then
        TOutSt(0) = TOutSt(0) + " " + Mid(ComIndex(I4), I2 + 1, I5 - I2 - 1)
      ElseIf I3 > LCount And I - I2 > 2 Then
        TOutSt(2) = TOutSt(2) + " " + Mid(ComIndex(I4), I2 + 1, I5 - I2 - 1)
      ElseIf I - I2 > 2 Then
        TOutSt(1) = TOutSt(1) + " " + Mid(ComIndex(I4), I2 + 1, I5 - I2 - 1)
        I6 = InStr(I5, ComIndex(I4), "}")
        If Mid(ComIndex(I4), I5, 1) = "{" And I6 > 0 And Text1.SelStart = Len(Text1.Text) Then
          FillComList Mid(ComIndex(I4), I5 + 1, I6 - I5 - 1)
        End If
      End If
    Loop Until I = Len(ComIndex(I4)) + 1
  End If
  TfRedraw
End Sub

Private Sub Text1_Change()
  If FrontCFrame.FrameType = cfStatus And FrontCFrame.ConnTyp = 0 Then
    If Left(Text1.Text, 1) <> "/" And Left(Text1.Text, 1) <> "?" And Len(Text1.Text) > 0 Then
      Text1.Text = "/" + Text1.Text
      Text1.SelStart = 0
      ProofIndex
      Text1.SelStart = Len(Text1.Text)
    End If
  End If
  If Len(Text1.Text) < 1024 Then ProofIPath: CorrectText Text1, TOutSt
End Sub

Private Sub Text1_GotFocus()
  txtList.Visible = False
  Set TextFeld = Text1
End Sub

Private Sub Text1_KeyDown(KeyCode As Integer, Shift As Integer)
  Dim I As Integer
    
  If KeyCode = vbKeyTab And (Shift = 2 Or Shift = 3) Then
    ChangeFrontFrame Shift
    KeyCode = 0
  ElseIf comList.Visible = True Then
    If KeyCode = vbKeyUp And comList.ListIndex > 0 Then comList.ListIndex = comList.ListIndex - 1
    If KeyCode = vbKeyDown And comList.ListIndex + 1 < comList.ListCount Then comList.ListIndex = comList.ListIndex + 1
  Else
    If KeyCode = vbKeyUp Then
      I = InStr(1, Text1.Text, Chr(13))
      If Text1.SelStart < I Or I = 0 Then
        If Len(Text1.Text) > 0 And txtList.List(txtList.ListCount - 1) <> Text1.Text And Len(Text1.Text) < 2048 Then
          If Right(Text1.Text, 2) = vbNewLine Then
            txtList.Additem Left(Text1.Text, Len(Text1.Text) - 2)
          Else
            txtList.Additem Text1.Text
          End If
          If txtList.ListCount > 64 Then txtList.RemoveItem 0
        End If
        txtList.ListIndex = txtList.ListCount - 1
        txtList.Top = Text1.Top + pTexthg.Top - 1 - txtList.Height
        txtList.Left = Text1.Left
        txtList.Width = Text1.Width
        txtList.Visible = True
        If txtList.Visible = True And Me.Visible = True Then txtList.SetFocus
        txtList_Click
        KeyCode = 0
      End If
    End If
  End If
  If KeyCode = 13 And Shift < 2 Then
    If comList.Visible = True Then ConfirmeListEnt
    If FrontCFrame.SendM(Text1.Text) = True And Len(Text1.Text) > 0 Then
      If txtList.List(txtList.ListCount - 1) <> Text1.Text Then
        If Right(Text1.Text, 2) = vbNewLine Then
          txtList.Additem Left(Text1.Text, Len(Text1.Text) - 2)
        Else
          txtList.Additem Text1.Text
        End If
        If txtList.ListCount > 128 Then txtList.RemoveItem 0
      End If
      Text1.Text = ""
      ProofIPath
    End If
    comList.Visible = False
    KeyCode = 0
  ElseIf KeyCode = 33 Or KeyCode = 34 Then
    pTBox_KeyDown KeyCode, Shift
  End If
End Sub

Private Sub Text1_KeyPress(KeyAscii As Integer)
  InputCodepage = GetInputCodepage
  'Debug.Print InputCodepage
  If KeyAscii = 10 Then
    comList.Visible = False
  ElseIf KeyAscii = 47 Then ' 47 -> "/"
    ProofIndex
  End If
  If KeyAscii = 13 Or KeyAscii = 9 Then KeyAscii = 0
  If Len(Text1.Text) < 2 And KeyAscii = 8 Then comList.Visible = False
End Sub

Private Sub Text1_KeyPressed(KeyAscii As Integer)
  TextChanged
End Sub

Public Sub TextChanged()
    SearchInIndex
    ProofIPath
End Sub

'Private Sub Text1_KeyUp(KeyCode As Integer, Shift As Integer)
'  If KeyCode <> 38 And KeyCode <> 40 Then SearchInIndex
'  If KeyCode <> 27 Then ProofIPath
'End Sub

Private Sub Text1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  ProofIPath
  comList.Visible = False
End Sub

Private Sub Text1_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
  ScrollBarIndex = 2
  If pTexthg.MousePointer = 99 Then pTexthg.MousePointer = 0
End Sub

Private Sub Text2_GotFocus()
  Set TextFeld = Text2
End Sub

Private Sub Text2_KeyDown(KeyCode As Integer, Shift As Integer)
  ScriptIn = 0
End Sub

Private Sub Text2_KeyPress(KeyAscii As Integer)
  Dim I As Long
  Dim I2 As Long
  Dim I3 As Long
  
  If KeyAscii = 13 Then
    I3 = GetScriptSelStart
    I = I3
    I2 = I3
    Do
      I = I - 1
      If I < 1 Then Exit Do
      If Mid(Text2.Text, I, 1) = Chr(10) Then Exit Do
      If Mid(Text2.Text, I, 1) <> " " Then I2 = I - 1
    Loop
    If LCase(Mid(Text2.Text, I2 + 1, 2)) = "do" Then
      I2 = I2 + 2
    ElseIf LCase(Mid(Text2.Text, I2 + 1, 4)) = "sub " Then
      I2 = I2 + 2
    ElseIf LCase(Right(Trim(Mid(Text2.Text, I2 + 1, I3 - I2)), 5)) = " then" Then
      I2 = I2 + 2
    End If
    If I2 > 0 Then
      ScriptIn = I2 - I
      If Not TextFeld Is Nothing Then TextFeld.SelText = Space(ScriptIn)
      SetScriptSelection I3, 0
    End If
  End If
End Sub

Private Sub Text2_KeyUp(KeyCode As Integer, Shift As Integer)
  If ScriptIn > 0 Then SetScriptSelection GetScriptSelStart + ScriptIn, 0
End Sub

Private Sub txtList_Click()
  Text1.Text = txtList.List(txtList.ListIndex)
  Text1.SelStart = 0
  Text1.SelLength = Len(Text1.Text)
  'ProofHelpIndex
End Sub

Private Sub txtList_DblClick()
  Text1.SetFocus
End Sub

Private Sub txtList_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = 40 And txtList.ListIndex = txtList.ListCount - 1 Then 'Pfeil runter
    txtList.Visible = False
    'Text1.SetFocus
  End If
End Sub

Private Sub txtList_KeyPress(KeyAscii As Integer)
  If KeyAscii = 13 Then
    KeyAscii = 0
    Text1.SetFocus
    txtList.Visible = False
    Text1_KeyDown 13, 0
  ElseIf KeyAscii = 32 Then
    KeyAscii = 0
  End If
End Sub

Private Sub txtList_LostFocus()
  txtList.Visible = False
End Sub

Private Sub UlistScr_Change()
  FrontCFrame.UListF.Change
End Sub

Private Sub UlistScr_Scroll()
  FrontCFrame.UListF.Scroll
End Sub

Private Sub UserInfoPic_DblClick()
  If UInfoMode = 5 Then UserInfoPic.Visible = False
End Sub

Private Sub UserInfoPic_KeyDown(KeyCode As Integer, Shift As Integer)
  If UInfoMode = 4 Or UInfoMode = 5 Then
    UserInfoPic.Visible = False
    If KeyCode = 27 Or KeyCode = 13 Then KeyCode = 0
  End If
End Sub

Private Sub UserInfoPic_LostFocus()
  Dim I As Integer
  If Text1.Visible = True And Me.Visible = True Then Text1.SetFocus
  If UInfoMode = 4 Then
    UserInfoPic.Visible = False
  End If
  If UInfoMode = 5 Then
    UserInfoPic.Visible = False
    Text1.SelStart = LastColorTextPos
    Text1.SelText = ">"
  End If
End Sub

Private Sub UserInfoPic_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  Dim DAT As String
  If UInfoMode = 4 Then
    DAT = "<$" + Format(Int(Y / 17), "00")
    If Int(Y / 17) > 15 Then DAT = "<$15"
    Text1.SelText = DAT
    UInfoMode = 5
  ElseIf UInfoMode = 5 Then
    DAT = "," + Format(Int(Y / 17), "00")
    If Int(Y / 17) > 15 Then DAT = ",15"
    Text1.SelText = DAT
    UserInfoPic.Visible = False
  End If
  LastColorTextPos = Text1.SelStart
End Sub

Private Sub VScroll1_Change()
  If Not FrontCFrame.TextF Is Nothing Then FrontCFrame.TextF.Scroll
End Sub

Private Sub VScroll1_Scroll()
  If Not FrontCFrame.TextF Is Nothing Then FrontCFrame.TextF.Scroll
End Sub



