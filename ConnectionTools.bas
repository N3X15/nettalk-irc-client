Attribute VB_Name = "ConnectionTools"
Option Explicit
Private DCCServer As DccServ
Private DCCScriptServers As New Collection

Public Sub CheckDCCServ(Optional Unload As Boolean)
  If DCCServerPort > 0 And Unload = False Then
    If DCCServer Is Nothing Then Set DCCServer = New DccServ
    If DCCServer.Port <> DCCServerPort Then DCCServer.Listen Val(DCCServerPort)
  Else
    If Not DCCServer Is Nothing Then Set DCCServer = Nothing
  End If
End Sub

Public Function AddDCCScriptServer(Port As Long, cMode As Integer, NoWindow As Integer) As Integer
  Dim I As Integer
  Dim NewDCCServer As DccServ
   
  If Port < 1 Then Exit Function
  For I = 1 To DCCScriptServers.Count
    If DCCScriptServers(I).Port = Port Then Exit For
  Next
  If I > DCCScriptServers.Count Then
    Set NewDCCServer = New DccServ
    NewDCCServer.GetMode = Abs(Sgn(cMode)) + Abs(Sgn(NoWindow)) * 2
    If NewDCCServer.Listen(Port) Then
      DCCScriptServers.Add NewDCCServer
      AddDCCScriptServer = 1
    Else
      Set NewDCCServer = Nothing
    End If
  End If
End Function

Public Sub RemDCCScriptServer(Port As Long)
  Dim I As Integer
  
  For I = DCCScriptServers.Count To 1 Step -1
    If DCCScriptServers(I).Port = Port Or Port = 0 Then
      DCCScriptServers.Remove I
    End If
  Next
End Sub

