VERSION 5.00
Begin VB.Form Form3 
   BorderStyle     =   3  'Fester Dialog
   Caption         =   "Verbinden"
   ClientHeight    =   1800
   ClientLeft      =   45
   ClientTop       =   540
   ClientWidth     =   4680
   BeginProperty Font 
      Name            =   "MS Shell Dlg"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Form3.frx":0000
   LinkTopic       =   "Form3"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   120
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   312
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'Fenstermitte
   Begin VB.PictureBox Picture2 
      BackColor       =   &H80000005&
      BorderStyle     =   0  'Kein
      Height          =   1215
      Left            =   0
      ScaleHeight     =   1215
      ScaleWidth      =   4695
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   0
      Width           =   4695
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   420
         Sorted          =   -1  'True
         TabIndex        =   10
         Top             =   480
         Width           =   3495
      End
      Begin VB.TextBox Text3 
         Height          =   285
         Left            =   420
         MaxLength       =   1024
         TabIndex        =   6
         Top             =   495
         Width           =   3765
      End
      Begin VB.Label Label3 
         BackColor       =   &H80000005&
         Caption         =   "User:"
         Height          =   255
         Left            =   420
         TabIndex        =   2
         Top             =   240
         Width           =   3945
      End
   End
   Begin VB.PictureBox Picture1 
      BackColor       =   &H80000005&
      BorderStyle     =   0  'Kein
      Height          =   1215
      Left            =   0
      ScaleHeight     =   1215
      ScaleWidth      =   4695
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   1995
      Width           =   4695
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   1140
         MaxLength       =   256
         TabIndex        =   0
         Top             =   240
         Width           =   2985
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   1155
         MaxLength       =   8
         TabIndex        =   5
         Text            =   "6667"
         Top             =   735
         Width           =   855
      End
      Begin VB.Label Label1 
         BackColor       =   &H80000005&
         Caption         =   "Host:"
         Height          =   255
         Left            =   450
         TabIndex        =   8
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label2 
         BackColor       =   &H80000005&
         Caption         =   "Port:"
         Height          =   255
         Left            =   450
         TabIndex        =   1
         Top             =   720
         Width           =   1215
      End
   End
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Caption         =   "Abbrechen"
      Height          =   330
      Left            =   2400
      TabIndex        =   4
      Top             =   1380
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   330
      Left            =   1080
      TabIndex        =   3
      Top             =   1380
      Width           =   1215
   End
   Begin VB.Line Line2 
      BorderColor     =   &H80000014&
      X1              =   0
      X2              =   312
      Y1              =   83
      Y2              =   83
   End
   Begin VB.Line Line1 
      BorderColor     =   &H80000010&
      X1              =   0
      X2              =   312
      Y1              =   82
      Y2              =   82
   End
End
Attribute VB_Name = "Form3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Combo1_Change()
  If Len(Combo1.Text) > 0 Then Command1.Enabled = True Else Command1.Enabled = False
End Sub

Private Sub Combo1_Click()
  Combo1_Change
End Sub

Private Sub Command1_Click()
  Me.Visible = False
End Sub

Private Sub Command2_Click()
  Me.Visible = False
  Text1.Text = ""
  Text2.Text = ""
  Text3.Text = ""
  Combo1.Text = ""
End Sub

Private Sub Form_Load()
'  Me.Height = Me.Height - (Me.ScaleHeight - 120) * Screen.TwipsPerPixelY
'  Me.Width = Me.Width - (Me.ScaleWidth - 312) * Screen.TwipsPerPixelX
'  Debug.Print Me.Height ' -> 2280
'  Debug.Print Me.Width  ' -> 4770
  
  InitFormFont Me
  
  Me.Caption = App.Title
  Picture1.Left = 0
  Picture1.Top = 0
  Picture2.Left = 0
  Picture2.Top = 0
  Combo1.Visible = False
  Command1.Caption = TeVal.butOK
  Command2.Caption = TeVal.butCancel
  Command1.Enabled = False
End Sub

Private Sub Text1_Change()
  If Len(Text1.Text) > 0 Then Command1.Enabled = True Else Command1.Enabled = False
End Sub

Private Sub Text3_Change()
  If Len(Text3.Text) > 0 Then Command1.Enabled = True Else Command1.Enabled = False
End Sub
