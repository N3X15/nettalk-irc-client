Attribute VB_Name = "SkinPaket"
Option Explicit

Public Sub CreateSkinPaket(DestPath As String)
  Dim DAT() As Byte
  Dim OFi As Integer
  Dim BildPath As String
  BildPath = HGBildPath
  On Error Resume Next
  If InStr(BildPath, "\") = 0 Then
    BildPath = AppPath + Dir(AppPath + BildPath)
  End If
  OFi = FreeFile
  Open BildPath For Binary As #OFi
    If LOF(OFi) = 0 Then
      BildPath = ""
    Else
      ReDim DAT(LOF(OFi) - 1)
      Get OFi, 1, DAT
    End If
  Close #OFi
  Open DestPath For Output As #OFi
    Print #OFi, "'Info:"
    Print #OFi, "FileType = Nettalkskin"
    Print #OFi, "Version = " + App.Title + " " & App.Major & "." & App.Minor & "." & App.Revision
    Print #OFi, "Autor = " + LastUsedRealName + " (" + LastUsedNick + ")"
    Print #OFi, ""
    
    If HGDrawMode > 0 And Len(BildPath) > 0 Then
      Print #OFi, "'Files:"
      Print #OFi, "File = BgSkinPicture.dat"
      Print #OFi, "FileLen = " + CStr(FileLen(BildPath))
      Print #OFi, ""
    End If
    
    Print #OFi, "'Colors:"
    Print #OFi, "ClientMSG = " + CStr(CoVal.ClientMSG)
    Print #OFi, "ErrorMSG = " + CStr(CoVal.ErrorMSG)
    Print #OFi, "FlusterVon = " + CStr(CoVal.FlusterVon)
    Print #OFi, "Link = " + CStr(CoVal.Link)
    Print #OFi, "NormalText = " + CStr(CoVal.NormalText)
    Print #OFi, "NormalVon = " + CStr(CoVal.NormalVon)
    Print #OFi, "Notice = " + CStr(CoVal.Notice)
    Print #OFi, "ServerMSG = " + CStr(CoVal.ServerMSG)
    Print #OFi, "UserMSG = " + CStr(CoVal.UserMSG)
    Print #OFi, "bColumDisc = " + CStr(CoVal.bColumDisc)
    Print #OFi, "bColumBusy = " + CStr(CoVal.bColumBusy)
    Print #OFi, "KonBackColor = " + CStr(Form1.pTBox.BackColor)
    Print #OFi, "UListForeColor = " + CStr(Form1.pUList.ForeColor)
    Print #OFi, "UListBackColor = " + CStr(Form1.pUList.BackColor)
    Print #OFi, "TextForeColor = " + CStr(Form1.Text1.ForeColor)
    Print #OFi, "TextBackColor = " + CStr(Form1.Text1.BackColor)
    Print #OFi, "TimeStamp = " + CStr(CoVal.TimeStamp)
    Print #OFi, "OwnNick = " + CStr(CoVal.OwnNick)
    Print #OFi, "ButtonShadow = " + CStr(CoVal.ButtonShadow)
    Print #OFi, "Light3D = " + CStr(CoVal.Light3D)
    Print #OFi, "BackColor = " + CStr(CoVal.BackColor)
    Print #OFi, "MenuText = " + CStr(CoVal.MenuText)
    Print #OFi, "GrayText = " + CStr(CoVal.GrayText)
    Print #OFi, "Shadow3D = " + CStr(CoVal.Shadow3D)
    Print #OFi, "Highlight3D = " + CStr(CoVal.Highlight3D)
    Print #OFi, "ButtonText = " + CStr(CoVal.ButtonText)
    Print #OFi, "PicturePath = " + "BgSkinPicture.dat" 'HGBildPath
    Print #OFi, "PictureMode = " + CStr(HGDrawMode)
    Print #OFi, "FontName = " + FontTypeName
    Print #OFi, "FontSize = " + CStr(FontTypeSize)
    Print #OFi, "FontBold = " + CStr(CInt(FontTypeBold))
    Print #OFi, ""
    Print #OFi, "'Binarys:"
    Print #OFi, "-"
  Close #OFi
  Open DestPath For Binary As #OFi
    If Len(BildPath) > 0 Then Put OFi, LOF(OFi) + 1, DAT
  Close #OFi
  If Err <> 0 Then MsgBox Err.Description, vbCritical
  On Error GoTo 0
End Sub

Public Sub ReadSkinPaket(srcPath As String)
  Dim OFi As Integer
  Dim OFi2 As Integer
  Dim DAT() As Byte
  Dim InpData As String
  Dim I As Integer
  Dim I2 As Integer
  Dim BinFiles(1 To 16) As String
  Dim BinFileLen(1 To 16) As Long
  Dim PosInFile As Long
  
  CoVal.OwnNick = -1
  On Error Resume Next
  OFi = FreeFile
  Open srcPath For Input As #OFi
    Do Until EOF(OFi) = True
      If Err <> 0 Then Exit Do
      Line Input #OFi, InpData
      PosInFile = PosInFile + Len(InpData) + 2
      If InpData = "-" Then Exit Do
      I = InStr(InpData, "=")
      If I > 0 Then
        Select Case Trim(LCase(Left(InpData, I - 1)))
          Case "clientmsg"
            CoVal.ClientMSG = Val(Right(InpData, Len(InpData) - I))
          Case "errormsg"
            CoVal.ErrorMSG = Val(Right(InpData, Len(InpData) - I))
          Case "flustervon"
            CoVal.FlusterVon = Val(Right(InpData, Len(InpData) - I))
          Case "link"
            CoVal.Link = Val(Right(InpData, Len(InpData) - I))
          Case "normaltext"
            CoVal.NormalText = Val(Right(InpData, Len(InpData) - I))
          Case "normalvon"
            CoVal.NormalVon = Val(Right(InpData, Len(InpData) - I))
          Case "notice"
            CoVal.Notice = Val(Right(InpData, Len(InpData) - I))
          Case "servermsg"
            CoVal.ServerMSG = Val(Right(InpData, Len(InpData) - I))
          Case "usermsg"
            CoVal.UserMSG = Val(Right(InpData, Len(InpData) - I))
          Case "bcolumdisc"
            CoVal.bColumDisc = Val(Right(InpData, Len(InpData) - I))
          Case "bcolumbusy"
            CoVal.bColumBusy = Val(Right(InpData, Len(InpData) - I))
          Case "konbackcolor"
            Form1.pTBox.BackColor = Val(Right(InpData, Len(InpData) - I))
          Case "ulistforecolor"
            Form1.pUList.ForeColor = Val(Right(InpData, Len(InpData) - I))
          Case "ulistbackcolor"
            Form1.pUList.BackColor = Val(Right(InpData, Len(InpData) - I))
          Case "textforecolor"
            Form1.Text1.ForeColor = Val(Right(InpData, Len(InpData) - I))
          Case "textbackcolor"
            Form1.Text1.BackColor = Val(Right(InpData, Len(InpData) - I))
          Case "timestamp"
            CoVal.TimeStamp = Val(Right(InpData, Len(InpData) - I))
          Case "ownnick"
            CoVal.OwnNick = Val(Right(InpData, Len(InpData) - I))
          Case "buttonshadow"
            CoVal.ButtonShadow = Val(Right(InpData, Len(InpData) - I))
          Case "light3d"
            CoVal.Light3D = Val(Right(InpData, Len(InpData) - I))
          Case "backcolor"
            CoVal.BackColor = Val(Right(InpData, Len(InpData) - I))
          Case "menutext"
            CoVal.MenuText = Val(Right(InpData, Len(InpData) - I))
          Case "graytext"
            CoVal.GrayText = Val(Right(InpData, Len(InpData) - I))
          Case "shadow3d"
            CoVal.Shadow3D = Val(Right(InpData, Len(InpData) - I))
          Case "highlight3d"
            CoVal.Highlight3D = Val(Right(InpData, Len(InpData) - I))
          Case "buttontext"
            CoVal.ButtonText = Val(Right(InpData, Len(InpData) - I))
          Case "picturepath"
            HGBildPath = UserPath + Trim(Right(InpData, Len(InpData) - I)) 'Modified
          Case "picturemode"
            HGDrawMode = Val(Right(InpData, Len(InpData) - I))
          Case "fontname"
            FontTypeName = Trim(Right(InpData, Len(InpData) - I))
          Case "fontsize"
            FontTypeSize = Val(Right(InpData, Len(InpData) - I))
          Case "fontbold"
            FontTypeBold = CBool(Val(Right(InpData, Len(InpData) - I)))
            
          Case "file"
            I2 = I2 + 1
            BinFiles(I2) = Trim(Right(InpData, Len(InpData) - I))
          Case "filelen"
            BinFileLen(I2) = Val(Right(InpData, Len(InpData) - I))
        End Select
      End If
    Loop
  Close #OFi
  If CoVal.OwnNick = -1 Then CoVal.OwnNick = CoVal.NormalVon
  
  OFi = FreeFile
  Open srcPath For Binary As #OFi
    For I = 1 To I2
      If BinFileLen(I) > 0 Then
        ReDim DAT(1 To BinFileLen(I))
        Get #OFi, PosInFile + 1, DAT
        OFi2 = FreeFile
        If Len(Dir(UserPath + BinFiles(I))) > 0 Then Kill UserPath + BinFiles(I)
        Open UserPath + BinFiles(I) For Binary As #OFi2
          Put #OFi2, 1, DAT
        Close #OFi2
        PosInFile = PosInFile + BinFileLen(I)
      End If
    Next
  Close #OFi
  
  If Err <> 0 Then MsgBox Err.Description + " (Skinload)", vbCritical
  On Error GoTo 0
End Sub

