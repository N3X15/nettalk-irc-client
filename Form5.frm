VERSION 5.00
Begin VB.Form Form5 
   BorderStyle     =   3  'Fester Dialog
   Caption         =   "Einstellungen"
   ClientHeight    =   8700
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   12405
   Icon            =   "Form5.frx":0000
   BeginProperty Font 
      Name            =   "MS Shell Dlg"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form5"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   580
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   827
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'Fenstermitte
   Begin VB.PictureBox picOpt 
      BackColor       =   &H00808080&
      BorderStyle     =   0  'Kein
      Height          =   5775
      Index           =   3
      Left            =   2160
      ScaleHeight     =   5775
      ScaleWidth      =   4815
      TabIndex        =   25
      TabStop         =   0   'False
      Tag             =   "Script/Plugins"
      Top             =   600
      Visible         =   0   'False
      Width           =   4815
      Begin VB.Label Label8 
         Caption         =   "In Arbeit."
         Height          =   255
         Left            =   1560
         TabIndex        =   33
         Top             =   2760
         Width           =   1935
      End
      Begin VB.Line Line17 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   5760
         Y2              =   5760
      End
      Begin VB.Label Label7 
         Caption         =   "Script/Plugins"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   28
         Top             =   120
         Width           =   4335
      End
      Begin VB.Line Line14 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   490
         Y2              =   490
      End
   End
   Begin VB.PictureBox picOpt 
      BackColor       =   &H00808080&
      BorderStyle     =   0  'Kein
      Height          =   5775
      Index           =   2
      Left            =   7080
      ScaleHeight     =   5775
      ScaleWidth      =   4815
      TabIndex        =   34
      TabStop         =   0   'False
      Tag             =   "Shortcuts"
      Top             =   480
      Visible         =   0   'False
      Width           =   4815
      Begin VB.PictureBox Picture1 
         BackColor       =   &H80000010&
         BorderStyle     =   0  'Kein
         Height          =   3975
         Left            =   120
         ScaleHeight     =   265
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   305
         TabIndex        =   37
         TabStop         =   0   'False
         Top             =   960
         Width           =   4575
         Begin VB.TextBox liCommand 
            BorderStyle     =   0  'Kein
            Height          =   240
            Index           =   0
            Left            =   2385
            MaxLength       =   512
            TabIndex        =   42
            Top             =   15
            Width           =   2175
         End
         Begin VB.TextBox liCut 
            BorderStyle     =   0  'Kein
            Height          =   240
            Index           =   0
            Left            =   1200
            MaxLength       =   64
            TabIndex        =   41
            Top             =   15
            Width           =   1170
         End
         Begin VB.TextBox liTitel 
            BorderStyle     =   0  'Kein
            Height          =   240
            Index           =   0
            Left            =   15
            MaxLength       =   64
            TabIndex        =   40
            Top             =   15
            Width           =   1170
         End
      End
      Begin VB.Label Label12 
         Caption         =   "Shortcut"
         Height          =   255
         Left            =   1320
         TabIndex        =   39
         Top             =   720
         Width           =   855
      End
      Begin VB.Label Label11 
         Caption         =   "Titel"
         Height          =   255
         Left            =   120
         TabIndex        =   38
         Top             =   720
         Width           =   615
      End
      Begin VB.Line Line6 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   490
         Y2              =   490
      End
      Begin VB.Label Label10 
         Caption         =   "Shortcuts"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   36
         Top             =   120
         Width           =   4335
      End
      Begin VB.Line Line5 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   5760
         Y2              =   5760
      End
      Begin VB.Label Label9 
         Caption         =   "Befehl"
         Height          =   255
         Left            =   2520
         TabIndex        =   35
         Top             =   720
         Width           =   735
      End
   End
   Begin VB.PictureBox picOpt 
      BackColor       =   &H00808080&
      BorderStyle     =   0  'Kein
      Height          =   5775
      Index           =   1
      Left            =   1920
      ScaleHeight     =   5775
      ScaleWidth      =   4815
      TabIndex        =   12
      TabStop         =   0   'False
      Tag             =   "Chatten"
      Top             =   360
      Visible         =   0   'False
      Width           =   4815
      Begin VB.CommandButton Command4 
         Caption         =   "..."
         Height          =   255
         Left            =   3720
         TabIndex        =   32
         Top             =   1440
         Width           =   375
      End
      Begin VB.CheckBox Check12 
         Caption         =   "Automatisches wiederverbinden"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   4800
         Width           =   4575
      End
      Begin VB.TextBox Text1 
         Height          =   285
         Left            =   840
         TabIndex        =   21
         Text            =   "60"
         Top             =   5160
         Width           =   615
      End
      Begin VB.CheckBox Check11 
         Caption         =   "Automatisches Rejoin"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   4080
         Width           =   4575
      End
      Begin VB.CheckBox Check16 
         Caption         =   "Nachricht einblenden wenn aktiviert"
         Height          =   255
         Left            =   600
         TabIndex        =   19
         Top             =   3360
         Width           =   4095
      End
      Begin VB.TextBox Text2 
         Height          =   285
         Left            =   600
         MaxLength       =   1024
         TabIndex        =   18
         Top             =   2925
         Width           =   3735
      End
      Begin VB.CheckBox Check13 
         Caption         =   "Signalton �ber PC-Speaker ausgeben"
         Height          =   255
         Left            =   600
         TabIndex        =   15
         Top             =   1080
         Width           =   4095
      End
      Begin VB.CheckBox Check14 
         Caption         =   "Wave-Datei wiedergeben"
         Height          =   255
         Left            =   600
         TabIndex        =   14
         Top             =   1440
         Width           =   3015
      End
      Begin VB.CheckBox Check15 
         Caption         =   "Nachricht einblenden wenn aktiviert"
         Height          =   255
         Left            =   600
         TabIndex        =   13
         Top             =   1800
         Width           =   4095
      End
      Begin VB.Line Line12 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   490
         Y2              =   490
      End
      Begin VB.Label Label5 
         Caption         =   "Chatten"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   26
         Top             =   120
         Width           =   4335
      End
      Begin VB.Line Line4 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   5760
         Y2              =   5760
      End
      Begin VB.Line Line3 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   4560
         Y2              =   4560
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   3840
         Y2              =   3840
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   2280
         Y2              =   2280
      End
      Begin VB.Label Label1 
         Caption         =   "Alle"
         Height          =   255
         Left            =   360
         TabIndex        =   24
         Top             =   5160
         Width           =   495
      End
      Begin VB.Label Label2 
         Caption         =   "sec erneut versuchen"
         Height          =   255
         Left            =   1680
         TabIndex        =   23
         Top             =   5160
         Width           =   3015
      End
      Begin VB.Label Label4 
         Caption         =   "Bei eingang �ffentlicher Nachrichten in folgenden R�umen:"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   2520
         Width           =   4575
      End
      Begin VB.Label Label3 
         Caption         =   "Bei eingang privater Nachrichten:"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   720
         Width           =   4575
      End
   End
   Begin VB.PictureBox picOpt 
      BackColor       =   &H00808080&
      BorderStyle     =   0  'Kein
      Height          =   5775
      Index           =   0
      Left            =   1560
      ScaleHeight     =   5775
      ScaleWidth      =   4815
      TabIndex        =   4
      TabStop         =   0   'False
      Tag             =   "Allgemein"
      Top             =   120
      Visible         =   0   'False
      Width           =   4815
      Begin VB.CheckBox Check6 
         Caption         =   "Raumliste beim Start wieder automatisch aufrufen"
         Height          =   255
         Left            =   120
         TabIndex        =   31
         Top             =   5040
         Width           =   4575
      End
      Begin VB.CheckBox Check5 
         Caption         =   "R�ume beim Start wieder automatisch betreten"
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   4680
         Width           =   4575
      End
      Begin VB.CheckBox Check4 
         Caption         =   "Verbindungen beim Start wieder automatisch aufbauen"
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   4320
         Width           =   4575
      End
      Begin VB.CheckBox Check10 
         Caption         =   "Fenster wenn minimirt ganz ausblenden"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   3600
         Width           =   4575
      End
      Begin VB.CheckBox Check9 
         Caption         =   "Nettalk mit X Beenden"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   3240
         Width           =   4575
      End
      Begin VB.CheckBox Check8 
         Caption         =   "Timestamp f�r Ereignisse anzeigen"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   2520
         Width           =   4575
      End
      Begin VB.CheckBox Check7 
         Caption         =   "Timestamp f�r Textnachrichten anzeigen"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   2160
         Width           =   4575
      End
      Begin VB.CheckBox Check3 
         Caption         =   "Server-Status loggen"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1440
         Width           =   4575
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Raum-Text loggen"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   1080
         Width           =   4575
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Privat-Text loggen"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   720
         Width           =   4575
      End
      Begin VB.Label Label6 
         Caption         =   "Allgemein"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   27
         Top             =   120
         Width           =   4335
      End
      Begin VB.Line Line13 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   490
         Y2              =   490
      End
      Begin VB.Line Line11 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   5760
         Y2              =   5760
      End
      Begin VB.Line Line10 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   4080
         Y2              =   4080
      End
      Begin VB.Line Line9 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   3000
         Y2              =   3000
      End
      Begin VB.Line Line8 
         BorderColor     =   &H80000010&
         X1              =   120
         X2              =   4800
         Y1              =   1920
         Y2              =   1920
      End
   End
   Begin VB.PictureBox OList 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00808080&
      BorderStyle     =   0  'Kein
      Height          =   5775
      Left            =   120
      ScaleHeight     =   385
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   90
      TabIndex        =   3
      Top             =   120
      Width           =   1350
   End
   Begin VB.CommandButton Command3 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   340
      Left            =   2520
      TabIndex        =   2
      Top             =   6000
      Width           =   1215
   End
   Begin VB.CommandButton Command2 
      Cancel          =   -1  'True
      Caption         =   "Abbrechen"
      Height          =   340
      Left            =   3840
      TabIndex        =   1
      Top             =   6000
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "�bernehmen"
      Height          =   340
      Left            =   5160
      TabIndex        =   0
      Top             =   6000
      Width           =   1215
   End
End
Attribute VB_Name = "Form5"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim LastFocus As Integer


Private Sub OListRedraw()
  Dim menText As RECT
  Dim I As Integer
  OList.Line (0, 0)-(OList.ScaleWidth, OList.ScaleHeight), &H80000016, BF
  OList.Line (0, 0)-(0, OList.ScaleHeight - 1), &H80000010
  OList.Line (0, 0)-(OList.ScaleWidth - 1, 0), &H80000010
  OList.Line (OList.ScaleWidth - 1, 0)-(OList.ScaleWidth - 1, OList.ScaleHeight - 1), &H80000014
  OList.Line (0, OList.ScaleHeight - 1)-(OList.ScaleWidth - 1, OList.ScaleHeight - 1), &H80000014
  
  For I = 0 To picOpt.Count - 1
    OList.ForeColor = &H80000007
    
    menText.Left = 5
    menText.Top = I * 64 + 32
    menText.Right = OList.ScaleWidth - 5
    menText.Bottom = menText.Top + 20
    
    If I = OpFrameIndex Then
      DrawFlow OList, 5, menText.Top - 24, OList.ScaleWidth - 5, 45, &H80000016, Me.BackColor, True
      DrawFlow OList, 5, menText.Top - 25, OList.ScaleWidth - 5, 1, &H80000016, &H80000014, True
      DrawFlow OList, 5, menText.Top + 21, OList.ScaleWidth - 5, 1, &H80000016, &H80000010, True
      OList.FontBold = True
      If picOpt(I).Visible = False Then picOpt(I).Visible = True
    Else
      OList.FontBold = False
      If picOpt(I).Visible = True Then picOpt(I).Visible = False
    End If
    PrintIcon OList, (OList.ScaleWidth - 16) / 2, menText.Top - 18, I + 20
    DrawText OList.hdc, picOpt(I).Tag, Len(picOpt(I).Tag), menText, 1
  Next
End Sub

Private Sub Check12_Click()
  CheckEnable
End Sub

Private Sub Check14_Click()
  CheckEnable
End Sub

Private Sub Command1_Click()
  Dim I As Integer
  Dim I2 As Integer
  For I = 0 To liTitel.Count - 1
    If liTitel(I).Text + liCut(I).Text + liCommand(I).Text <> "" Then I2 = I
  Next
  ReDim ComTranzList(0 To 3, 0 To I2)
  For I = 0 To I2
    If Left(liCommand(I).Text, 1) = "/" Then liCommand(I).Text = Mid(liCommand(I).Text, 2)
    If Left(liCut(I).Text, 1) = "/" Then liCut(I).Text = Mid(liCut(I).Text, 2)
    ComTranzList(2, I) = liTitel(I).Text
    ComTranzList(0, I) = liCut(I).Text
    ComTranzList(1, I) = liCommand(I).Text
  Next
  
  'Page 1
  LogmodePrivate = CBool(Check1.Value)
  LogmodeRoom = CBool(Check2.Value)
  LogmodeState = CBool(Check3.Value)
  ShowTimeStampText = CBool(Check7.Value)
  ShowTimeStampComm = CBool(Check8.Value)
  NoQuitWithX = CBool(Check9.Value) = False
  HideFormWithMin = CBool(Check10.Value)
  RemaindConns = CBool(Check4.Value)
  RemaindRooms = CBool(Check5.Value)
  RemaindList = CBool(Check6.Value)
  
  'Page2
  PlayBeep = CBool(Check13.Value)
  PlayWav = CBool(Check14.Value)
  EaOSDPriv = CBool(Check15.Value)
  EaOSDRoom = CBool(Check16.Value)
  AutoReJoin = CBool(Check11.Value)
  
  If Check12.Value = 1 Then
    RetryTime = CInt(Text1.Text)
  Else
    RetryTime = 0
  End If
  OsdRoomList = Text2.Text
End Sub

Private Sub Command2_Click()
  Unload Me
End Sub

Private Sub Command3_Click()
  Command1_Click
  Unload Me
End Sub

Private Sub Form_Load()
  Dim I As Integer
  Me.Height = 6900
  Me.Width = 6585
  
  For I = 0 To Int(Picture1.ScaleHeight / liTitel(0).Height)
    If I > 0 Then Load liTitel(I): Load liCut(I): Load liCommand(I)
    liTitel(I).Top = I * (liTitel(0).Height + 1) + 1
    liCut(I).Top = I * (liTitel(0).Height + 1) + 1
    liCommand(I).Top = I * (liTitel(0).Height + 1) + 1
    If I <= UBound(ComTranzList, 2) Then
      liTitel(I).Text = ComTranzList(2, I)
      liCut(I).Text = ComTranzList(0, I)
      liCommand(I).Text = ComTranzList(1, I)
    Else
      liTitel(I).Text = ""
      liCut(I).Text = ""
      liCommand(I).Text = ""
    End If
    liTitel(I).Visible = True
    liCut(I).Visible = True
    liCommand(I).Visible = True
  Next
  Picture1.Height = (liTitel(0).Height * (I + 1) + 2) * Screen.TwipsPerPixelY
  
  For I = 0 To picOpt.Count - 1
   picOpt(I).Left = 104
   picOpt(I).Top = 8
   picOpt(I).BackColor = Me.BackColor
  Next
  
  '------------------------------
  'Page 1
  Check1.Value = CInt(LogmodePrivate) * -1
  Check2.Value = CInt(LogmodeRoom) * -1
  Check3.Value = CInt(LogmodeState) * -1
  Check7.Value = CInt(ShowTimeStampText) * -1
  Check8.Value = CInt(ShowTimeStampComm) * -1
  Check9.Value = CInt(NoQuitWithX = False) * -1
  Check10.Value = CInt(HideFormWithMin) * -1
  Check4.Value = CInt(RemaindConns) * -1
  Check5.Value = CInt(RemaindRooms) * -1
  Check6.Value = CInt(RemaindList) * -1
  
  'Page2
  Check13.Value = CInt(PlayBeep) * -1
  Check14.Value = CInt(PlayWav) * -1
  Check15.Value = CInt(EaOSDPriv) * -1
  Check16.Value = CInt(EaOSDRoom) * -1
  Check11.Value = CInt(AutoReJoin) * -1
  If RetryTime = 0 Then
    Check12.Value = 0
    Text1.Text = 30
  Else
    Check12.Value = 1
    Text1.Text = RetryTime
  End If
  Text2.Text = OsdRoomList
  '------------------------------
  
  CheckEnable
  
  If OpFrameIndex > picOpt.Count - 1 Then OpFrameIndex = picOpt.Count - 1
  OListRedraw
End Sub

Private Sub CheckEnable()
  If Check12.Value = 0 Then
    Label1.Enabled = False: Label2.Enabled = False: Text1.Enabled = False
  Else
    Label1.Enabled = True: Label2.Enabled = True: Text1.Enabled = True
  End If
  If Check14.Value = 0 Then
    Command4.Enabled = False
  Else
    Command4.Enabled = True
  End If
End Sub

Private Sub liCommand_GotFocus(Index As Integer)
  liTitel_GotFocus Index
End Sub

Private Sub liCommand_LostFocus(Index As Integer)
  If Left(liCommand(Index).Text, 1) = "/" Then liCommand(Index).Text = Mid(liCommand(Index).Text, 2)
End Sub

Private Sub liCut_GotFocus(Index As Integer)
  liTitel_GotFocus Index
End Sub

Private Sub liCut_LostFocus(Index As Integer)
  If Left(liCut(Index).Text, 1) = "/" Then liCut(Index).Text = Mid(liCut(Index).Text, 2)
End Sub

Private Sub liTitel_GotFocus(Index As Integer)
  LastFocus = Index
  Picture1.Cls
  Picture1.Refresh
End Sub

Private Sub OList_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If Int(Y / 64) < picOpt.Count Then OpFrameIndex = Int(Y / 64)
  OListRedraw
End Sub

Private Sub Picture1_Paint()
  Picture1.Line (0, liTitel(LastFocus).Top - 1) _
  -(Picture1.ScaleWidth, liTitel(LastFocus).Top + liTitel(LastFocus).Height), &H8000000D, BF
End Sub
