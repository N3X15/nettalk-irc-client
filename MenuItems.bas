Attribute VB_Name = "MenuItems"
Option Explicit

Private Declare Function SetMenuItemBitmaps Lib "user32" _
        (ByVal hMenu As Long, ByVal nPosition As Long, _
        ByVal wFlags As Long, ByVal hBitmapUnchecked As _
        Long, ByVal hBitmapChecked As Long) As Long
        
Private Declare Function GetMenu Lib "user32" (ByVal _
        hWnd As Long) As Long
        
Private Declare Function GetSubMenu Lib "user32" (ByVal _
        hMenu As Long, ByVal nPos As Long) As Long

Const MF_BYPOSITION = &H400&

Public Sub SetMenuItems()
  Form1.MenuIcon.AutoRedraw = True
  PrintIcon Form1.MenuIcon, 0, 0, 54
  Form1.MenuIcon.Picture = Form1.MenuIcon.Image
End Sub

Public Sub DrawMenuItems()
  Dim h1 As Long
  Dim h2 As Long
  h1 = GetMenu(Form1.hWnd)
  h2 = GetSubMenu(h1, 0)
  SetMenuItemBitmaps h2, 0, MF_BYPOSITION, Form1.MenuIcon.Picture, Form1.MenuIcon.Picture
End Sub

Public Sub EndDrawItems()
  Form1.menh.Visible = False
End Sub

